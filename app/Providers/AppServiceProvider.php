<?php

namespace App\Providers;

use App\Services\Menu\MenuServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Session;


class AppServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot(Request $request) {
		Schema::defaultStringLength(191);
		/*if(env('APP_ENV')=="production"){
			\URL::forceScheme('https');
			$this->app['request']->server->set('HTTPS', true);
		}*/

        $menuService = new MenuServices();
        $url = $request->getPathInfo();

        $city_session=Session::get('cityvalue');

       

        View::share('menuService', $menuService);
        View::share('url', $url);
        View::share('city_session', $city_session);

        Blade::if('can', function ($user_id, $permiso) {
            $user = Auth::user();
            if ($user_id == $user->id || $user->can($permiso)) {
                return true;
            } else {
                return false;
            }

        });
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		//
		
		/*$this->app->bind(
			\Auth0\Login\Contract\Auth0UserRepository::class,
			\App\Repository\UserRepository::class
		);*/
	}
}
