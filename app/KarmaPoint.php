<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KarmaPoint extends Model
{
    protected $table = 'karma_points';
    protected $fillable = [
    	'commentator_id',
    	'receiver_id',
    	'product_id',
    	'score',
    	'comment',
        'borrower_id'
    ];



        public function receiver()
    {
    	return  $this->hasOne('App\User','id','receiver_id');
    }

         public function commentator()
    {
    	return  $this->hasOne('App\User','id','commentator_id');
    }

       public function product()
    {
    	return  $this->hasOne('App\Product','id','product_id');
    }
}
