<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowerMessage extends Model
{
    

    protected $table="borrower_messages";

    protected $fillable=["borrow_id","user_id","message"];
    protected $with=["borrow"];
           public function user()
    {
    	return  $this->belongsTo(User::class,'user_id','id');
    }

  public function borrow()
    {
    	return  $this->belongsTo(Borrower::class,'borrow_id','id');
    }

}
