<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Notifications\ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable,EntrustUserTrait;
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'auth0id',
        'first_name',
        'last_name',
        'email',
        'password',
        'facebook_id',
        'profile_img',
        'country_id',
        'state_id',
        'city_id',
        'expire_date',
        'phone',
        'zip_code',
        'direction'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       // 'password', 'remember_token',
    ];

    protected $appends = ["nombre_completo", "rol","url_foto_perfil","foto_perfil"];

    public function addNew($input)
    {
        $check = static::where('email',$input['email'])->first();

        if( is_null($check) )
        {
            return static::create($input);
        }

        return $check;
    }

    public function user_plan()
    {
    return $this->hasMany(UserPlan::class, 'user_id', 'id');
    
    }

    public function sendPasswordResetNotification($token)
{
    $this->notify(new ResetPasswordNotification($token));
}

    // get Full Name
    public function getFullName()
    {
        return ucwords("$this->first_name $this->last_name");
    }

  public function roles()
    {
        return $this->belongsToMany(Role::class);
    }


    public function getNombreCompletoAttribute()
    {
        return ucwords("$this->first_name $this->last_name");
    }

    public function getRol()
    {
        $rol = $this->roles()->first();
        return $rol;
    }

    public function getRolId()
    {
        $rol = $this->roles()->first();
        if($rol){
            return $rol->id;
        }
        return null;
    }

    public function getRolAttribute()
    {
        $rol_name = '';
        $rol = $this->roles()->first();
        if ($rol) {
            $rol_name = $rol->display_name;
        }
        return ucwords($rol_name);
    }

        public function getPhotoAttribute()
    {
       if ($this->profile_img == null || $this->profile_img == "") {
            return url("/img/user_default.jpg");
        }elseif($this->facebook_id!=null)
{
return $this->profile_img;
}
else{
            return url("/img/profile/".$this->profile_img);
 
        }

            return url("/img/profile/".$this->profile_img);
    }

    public function getUrlFotoPerfilAttribute()
    {
       if ($this->profile_img == null || $this->profile_img == "") {
            return url("/img/user_default.jpg");
        }

        return url($this->profile_img);
    }
    
        public function getFotoPerfilAttribute($imagen)
    {
       if ($imagen == null || $imagen == "") {
            return url("/img/user_default.jpg");
        }

        return $imagen;
    }

    public function permiso($user_id, $permiso)
    {
        $user = $this;
        if ($user_id == $user->id || $user->can($permiso)) {
            return true;
        } else {
            return false;
        }
    }


    public function isAdmin()
    {
        foreach ($this->roles as $role)
        {
            if ($role->name == 'admin')
            {
                return true;
            }
        }
        return false;
    }

    // Pais
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    // Estado
    public function state()
    {
        return $this->belongsTo(State::class, 'state_id', 'id');
    }

    // Ciudad
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    // Comunidades
      public function communities()
    {
        return $this->belongsToMany('App\Community','community_user','user_id','community_id')->withPivot('community_id');
    }

    
 
    // Productos
    public function products()
    {
        return $this->hasMany(Product::class, 'user_id', 'id');
    }

      public function borrows()
    {
        return $this->hasMany(Borrower::class, 'user_id', 'id');
    }
}
