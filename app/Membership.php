<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
	protected $table = 'membership_fees';
	
    protected $fillable = [
        'fees', 
        'created_by',
        'fees_plan',
        'type'
    ];
}
