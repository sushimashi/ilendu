<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPlan extends Model
{
    protected $fillable = ['id','user_id','plan_id'];

    protected $table = 'user_plans';

    	public function plan()
	{
		return $this->belongsTo(Membership::class,'plan_id','id');
	}

	  	public function user()
	{
		return $this->belongsTo(User::class,'user_id','id');
	}
}
