<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_Community extends Model
{
   protected $table = "product_communities";
   public $timestamps =false;

   protected $fillable= ['product_id','community_id'];
}
