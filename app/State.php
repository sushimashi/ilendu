<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
     public $table = 'states';

     public $timestamps = false;

     protected $fillable = [
     	'name',
     	'country_id'
     ];
     protected $rules = [
     	'name' => 'required',
     	'country_id' => 'required'
     ];

     // Pais
     public function country()
     {
     	return $this->belongsTo(Country::class, 'country_id', 'id');
     }

      // cities
    public function cities()
    {
        return $this->hasMany('App\City','state_id');
    }


     // Usuarios
     public function users()
     {
          return $this->hasMany(User::class, 'state_id', 'id');
     }
}
