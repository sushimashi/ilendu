<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Community extends Model
{
    protected $table = 'communities';
    protected $fillable = [
    	'name',
    	'description',
    	'com_image',
    	'status',
    	'created_by',
        'public',
        'city_id'
    ];

    // usuario (creador)
    public function user()
    {
    	return $this->hasOne('App\User', 'id', 'created_by');
    }

    // usuarios (integrantes de comunidad)
    public function communityUsers()
    {
        return $this->belongsToMany(User::class, 'community_user')->withPivot('is_approve'); 
    }

       // usuarios (integrantes de comunidad)
    public function communityProducts()
    {
        return $this->belongsToMany(Product::class, 'product_communities'); 
    }

     public function products()
    {
        return $this->belongsToMany('App\Product','product_communities','community_id','product_id');
    }

       public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }
}
