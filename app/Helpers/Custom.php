<?php
// use Illuminate\Support\Facades\DB;
use \Statickidz\GoogleTranslate;

if (!function_exists('translate')) {
	function translate($text)
	{
		$source = 'es';
		$target = 'en';

		$trans = new GoogleTranslate();
		$result = $trans->translate($source, $target, $text);

		return $result;
	}
}

if (!function_exists('changeLang')) {
	function changeLang($source, $target, $text)
	{

		$trans = new GoogleTranslate();
		return $trans->translate($source, $target, $text);

	}
}

/**Catgory LIst***/
if (!function_exists('get_category_list')) {
	function get_category_list(){
	    $category = \App\Category::where('is_active',1)->select('name','id','name_spanish')->get()->toArray();
		return $category;
	}
}
/***Category List Ends***/

/***Compresss Image**/
if (!function_exists('compress_img')) {
	function compress_img($source_url,$destination_url,$percent = 0.2){
        $info = getimagesize($source_url);
        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source_url);
        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source_url);
        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source_url);
        // header('Content-Type: image/png');
		list($width, $height) = getimagesize($source_url);
		$new_width = $width * $percent;
		$new_height = $height * $percent;
		$image_p = ImageCreateTrueColor($new_width, $new_height);
		ImageCopyResampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
		ImagePNG($image_p,$destination_url);
	}
}
/***Compresss Image Ends**/

/***Return User Roles***/
if(!function_exists('get_role')){
	function get_role($search = ""){
		$roles = Auth::user()->roles;
		$return_roles = [];
		if($roles){
			foreach($roles as $role){
				$return_roles[] = $role->name;
			}
			// $search_result = in_array(ucfirst($search), $return_roles);
			// if($search_result == 1)
			// 	$return_result[] = strtolower($search);
			// else
			// 	$return_result = implode(",",$return_roles);
		}
		return $return_roles;
	}
}
/***Return User Roles Ends***/

function get_countries(){
	    $countries = \App\Country::all();
        return $countries;
	}


/**Get Cities List***/
if(!function_exists('get_cities')){

	function get_cities(){

	    $cities = \App\City::where([
	    	'country_id' => 44,
	    	'status' => 1
	    ])->get();

        return $cities;
	}
	
}

function strictEmpty($var) {

    // Delete this line if you want space(s) to count as not empty
    $var = trim($var);
    
    if(isset($var) === true && $var === '') {
    
        return true;
    
    }
    else {
    
        return false;
    
    }
}
/**Get Cities List Ends***/