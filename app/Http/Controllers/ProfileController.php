<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KarmaPoint;
use App\User;
use Response;
use View;
use App\Product;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "score"=>"required",
            "comment"=>"required"
        ]);

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        $user=User::find($id);
        $comments=KarmaPoint::where('receiver_id',$id)->paginate(5);

        $products=Product::where('user_id',$id)->where('is_active',1)->paginate(8);


        if (count($comments)) 
        {
        $rating=round(KarmaPoint::where('receiver_id',$id)->get()->sum('score')/5);
        }else{
            $rating=0;
        }
      
             if (request()->ajax()) 
        { 

            if ($request->pagination=="products-tab")
             {
            return Response::json(['view'=>View::make('site.profile.pagination.products', array('user'=>$user,'products'=>$products))->render(),'pagination'=>$request->pagination]);

            }else{

                               return Response::json(['view'=>View::make('site.profile.pagination.index', array('comments'=>$comments,'user'=>$user,'products'=>$products))->render(),'pagination'=>$request->pagination]);

            }

         
        }
        return view('site.profile.index',compact('comments','user','rating','products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
