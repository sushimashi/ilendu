<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Community;
use App\User;
use App\Product;
use App\Product_Community;
use App\Community_user;
use App\Country;
use App\State;
use App\City;
use Auth;
use DB,Mail;
use App\Category;
use Session,File;
use Response;
use View;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;


class CommunityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request) {
        $isLoggedIn = Auth::check();
        $user = Auth::user();
        $_global_city=Session::get('cityvalue');
        
        
        $_paginate=8;
       if (!empty($_global_city)) 
        {
        $communities=Community::select("communities.*")
	      ->where('communities.city_id',$_global_city)
	      ->where("communities.status",1)
	      ->paginate($_paginate);
        }else
        {
        $communities=Community::select("communities.*")
	      ->where('communities.city_id',$user->city_id)
	      ->where("communities.status",1)
	      ->paginate($_paginate);
        } 



        $cities = [];
        $total=count($communities);

         if (request()->ajax()) 
        { 
               return Response::json(View::make('site.pagination.index', array('communities'=>$communities,'total'=>$total))->render());

         
        }


        return view('site.joincommunity',compact('communities','user','cities','categories','states'))
            ->with('isLoggedIn', $isLoggedIn);

    }

    public function filter_communities_by_name(Request $request)
    {
    	  
    	  $city_id=Session::get('cityvalue');
          $country_id=Session::get('countryvalue');
    	  $name=$request->name;
    	  $_paginate=8;


          #return response()->json(compact('country_id',#),404);

    	  $data=Community::select("communities.*")
	      ->where('communities.name','like','%'.$name.'%')
	      ->where('communities.city_id',$city_id)
	      ->where("communities.status",1)
          ->orWhereHas('city.state',function($q) use($country_id)
          {
            $q->where('country_id',$country_id);
          })
          ->where('communities.name','like','%'.$name.'%')
          ->where("communities.status",1)
	      ->paginate($_paginate);

	       $total=count($data);

      return Response::json(View::make('site.pagination.index', array('communities'=>$data,'total'=>$total))->render());
    }

    public function filter_communities(Request $request)
    {
    	$category_id=$request->category_id;
    	$country_id=$request->country_id;
    	$city_id=$request->city_id;
    	$state_id=$request->state_id;
        $_paginate=8;


	     $data=Community::select("communities.*")
	    ->where("communities.city_id",$city_id)
	    ->where("communities.status",1)
	    ->paginate($_paginate);



	    $total=count($data);

      return Response::json(View::make('site.pagination.index', array('communities'=>$data,'total'=>$total))->render());

   	

    }

    /**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$isLoggedIn = Auth::check();
		$user = Auth::user();
		return view('site.membership')->with('isLoggedIn', $isLoggedIn)->with('user', $user);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	
	 * @return \Illuminate\Http\Response * @param  \Illuminate\Http\Request  $request
	 */
	public function store(Request $request) 
	{
		$this->validate($request, [
			'name' => 'required | unique:communities',
			'description' => 'required',
            'status'=>'required',
            'com_image'=>'required'
		]);

		

		$com_image = $request->com_image;
        $name = 'kyc'.rand(5,123).'.'.$com_image->getClientOriginalExtension();
        $com_image->move(public_path().'/community_image/', $name);
		$community = new Community;
		$community->name = $request->name;
		$community->description = $request->description;
		$community->status = 1;
		$community->public = $request->status;
        $community->com_image = $name;
		$community->created_by = Auth::id();
		$community->city_id = auth()->user()->city->id;
        $community->save();
        auth()->user()->communities()->attach($community->id,['is_approve' =>1]);

            return response()->json(["message"=>"Community created successfully","success"=>true],200);

	}
	 public function saveJqueryImageUpload(Request $request,$id,$old)
    {//print_r($old);die;

       $community = Community::find(47992);
        $status = "";

        if ($request->hasFile('profile_picture')) {
            $image = $request->file('profile_picture');
            $img = Image::make($image);


           $img->fit(2000);
            $name = 'kyc'.rand(5,123).'.jpg';

             $img->save(public_path().'/community_image/'.$name,100);

            // Rename image
            //echo "<pre>";print_r($name);die;
           // $image->move(public_path().'/community_image/', $name);
            $community->com_image = $name;
            $community->update();
            $status = "uploaded";            
        }
        
        return response($status,200);
    }

	/**
	 * Display the specified resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request,$id) {

		$isLoggedIn = Auth::check();
		$user = Auth::user();
		$countries = Country::get();
        $states = State::where('country_id',$user->country_id)->get();
        $allcities = City::where('state_id',$user->state_id)->get();
        $cities = City::join('products','products.city_id','cities.id')->select('cities.id','cities.name')
                        ->groupBy('products.city_id')
                        ->select('cities.id','cities.name')
                        ->havingRaw('COUNT(products.id) >= 5')
                        ->get();
		$community = Community::where('id', $id)->get();
		$image = Community::select('com_image')->where('id', $id)->get();
		$paginate=6;
		$product_communities = 
			DB::table('product_communities')
            ->join('products', 'product_communities.product_id', '=', 'products.id')
            ->join('users','users.id','=','products.user_id')
            ->select(
            	'products.product_name',
            	'products.id',
            	'products.user_id',
            	'products.product_img',
            	'users.first_name',
            	'users.last_name',
            	'users.profile_img',
            	'users.facebook_id',
                'products.is_premium',
                'products.give_away',
                'products.is_active'
            )
            ->where('product_communities.community_id',$id)
            ->where('products.is_active',1)
            ->paginate($paginate);
  
        $community_user = 
        	DB::table('community_user')
        	->where('community_id',$id)
        	->where(['user_id' => $user->id,'is_approve' => 1])
        	->get();

        	 $product_categories=DB::table('product_communities')
            ->join('products', 'product_communities.product_id', '=', 'products.id')
            ->join('users','users.id','=','products.user_id')
            ->select(
            	'products.product_category'
            )
            ->where('product_communities.community_id',$id)->get()->pluck('product_category')->unique();


        $members=DB::table('community_user')
            ->select('users.id','users.first_name','users.last_name','users.profile_img')
            ->where('community_id',$id)
            ->join('users','users.id','=','community_user.user_id')
            ->get();

            $categories=Category::whereIn('id',$product_categories)->get();
            $items="";

            switch (count($members)) {
                case 1:

                    $items="1,1,1,1";

                    break;

                case 2:
                    $items="1,2,2,2";

                break;
                
                default:
            $items="1,3,3,3";

                    break;
            }
            if (count($members)>3) 
            {

            }
  if (request()->ajax()) 
        { 
               return Response::json(View::make('site.community.pagination.index', array('product_communities'=>$product_communities,'community_user'=>$community_user,'categories'=>$categories,'members'=>$members,'items'=>$items))->render());

         
        }
        $pending=0;
        if ($isLoggedIn) 
        {

               $is_pending=auth()->user()->communities()->wherePivot('community_id',$id)->get();



               if (count($is_pending)>0) 
               {
               	$pending=1;
               }

        }




        if(count($community_user)>0)
        {
        	$is_join = 1;
        } else
        {
        	$is_join = 0;
        }


		return view('site.community.community_detail', compact('community','user','image','countries','states','allcities','cities','product_communities','items','is_join','pending','categories','members'))->with('isLoggedIn', $isLoggedIn);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Community $community) 
    {
		
     return view('site.community.edit',compact('community'));

	}

	public function community_products(Request $request)
	{

		$_busqueda=$request->busqueda;
		$id=$request->id;
	    $user = Auth::user();
		$paginate=6;



		$product_communities = 
			DB::table('product_communities')
            ->join('products', 'product_communities.product_id', '=', 'products.id')
            ->join('users','users.id','=','products.user_id')
            ->select(
            	'products.product_name',
            	'products.id',
            	'products.user_id',
            	'products.product_img',
            	'users.first_name',
            	'users.last_name',
            	'users.profile_img',
            	'users.facebook_id',
                'products.is_active',
                'products.is_premium',
                'products.give_away'
            )
            ->where('products.product_name','like','%'.$_busqueda.'%')
            ->where('product_communities.community_id',$id)
            ->paginate($paginate);
  
        $community_user = 
        	DB::table('community_user')
        	->where('community_id',$id)
        	->where(['user_id' => $user->id,'is_approve' => 1])
        	->get();

  

      return Response::json(View::make('site.community.pagination.index', array('product_communities'=>$product_communities,'community_user'=>$community_user))->render());


	}

	public function community_categories(Request $request)
	{

		$_category_id=$request->category_id;
		$id=$request->id;
	    $user = Auth::user();
		$paginate=6;


         if (isset($request->filter_radio)) {
            

            switch ($request->filter_radio) {
            case 'premium':
                        $premium=1;
        
                $product_communities = 
            DB::table('product_communities')
            ->join('products', 'product_communities.product_id', '=', 'products.id')
            ->join('users','users.id','=','products.user_id')
            ->select(
                'products.product_name',
                'products.id',
                'products.user_id',
                'products.product_img',
                'users.first_name',
                'users.last_name',
                'users.profile_img',
                'users.facebook_id',
                'products.is_active',
                'products.is_premium',
                'products.give_away'
            )
            ->where('products.is_active',1)
            ->where("products.is_premium",$premium)
            ->where("products.give_away",0)
            ->where('product_communities.community_id',$id)
            ->paginate($paginate);
  

                break;
            case 'gift':
                        $gift=1;

              
        

  $product_communities = 
            DB::table('product_communities')
            ->join('products', 'product_communities.product_id', '=', 'products.id')
            ->join('users','users.id','=','products.user_id')
            ->select(
                'products.product_name',
                'products.id',
                'products.user_id',
                'products.product_img',
                'users.first_name',
                'users.last_name',
                'users.profile_img',
                'users.facebook_id',
                'products.is_active',
                'products.is_premium',
                'products.give_away'
            )
            ->where('products.is_active',1)
            ->where("products.give_away",$gift)
            ->where('product_communities.community_id',$id)
            ->paginate($paginate);

                break;

                case 'all':
               Session::forget('category');



  $product_communities = 
            DB::table('product_communities')
            ->join('products', 'product_communities.product_id', '=', 'products.id')
            ->join('users','users.id','=','products.user_id')
            ->select(
                'products.product_name',
                'products.id',
                'products.user_id',
                'products.product_img',
                'users.first_name',
                'users.last_name',
                'users.profile_img',
                'users.facebook_id',
                'products.is_active',
                'products.is_premium',
                'products.give_away'
            )
            ->where('products.is_active',1)
            ->where('product_communities.community_id',$id)
            ->paginate($paginate);
    
            break;

        
        }
        }else{


     $product_communities = 
            DB::table('product_communities')
            ->join('products', 'product_communities.product_id', '=', 'products.id')
            ->join('users','users.id','=','products.user_id')
            ->select(
                'products.product_name',
                'products.id',
                'products.user_id',
                'products.product_img',
                'users.first_name',
                'users.last_name',
                'users.profile_img',
                'users.facebook_id',
                'product_category',
                'products.is_premium',
                'products.give_away',
                'products.is_active'
            )
            ->where('products.product_category','=',$_category_id)
            ->where('product_communities.community_id',$id)
            ->where('products.is_active',1)
            ->paginate($paginate);
  

        }


	
  
        $community_user = 
        	DB::table('community_user')
        	->where('community_id',$id)
        	->where(['user_id' => $user->id,'is_approve' => 1])
        	->get();

  

      return Response::json(View::make('site.community.pagination.index', array('product_communities'=>$product_communities,'community_user'=>$community_user))->render());


	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request) {
		//


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'description' => 'required'
        ]);


        if ($validator->fails()) {
            return back();
        }

    
        $community = Community::find($request->id);
        $community->name = $request->name;
        $community->description = $request->description;
        $community->status = 1;
        $community->public = $request->status;


        if ($request->hasFile('com_image')) {

        $com_image = $request->com_image;
        $name = 'kyc'.rand(5,123).'.'.$com_image->getClientOriginalExtension();
        $com_image->move(public_path().'/community_image/', $name);

        $community->com_image = $name;
        }
        $community->created_by = Auth::id();
        $community->city_id = auth()->user()->city->id;
        $update=$community->save();


        if ($update) 
        {
          return response()->json(["success"=>true,"message"=>"community edited successfully"]);

        }else{
                    return response()->json(["success"=>false,"message"=>"Error"]);

        }


	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Community $community) {

        $com = Community::find($community->id);
        $com->delete();
          return response()->json(["success"=>true,"message"=>"community deleted successfully"]);
	}

	public function joincommunity($id){

		$add=Community_user::firstOrCreate(['user_id' => Auth::user()->id,'community_id' => $id],[
				'is_approve' => 1]);
		

		return redirect('communities');
	}

	public function unjoincommunity($id){

		$community=Community::find($id);


		//we get the products

		$products=Auth::user()->products()->get();

		foreach ($products as $key => $p) 
		{

			//delete the product
			$p->communities()->detach();
		}

		$unjoin=Community_user::where(['user_id' => Auth::user()->id,'community_id' => $id])->delete();
		

		return redirect('communities');
	}
	

	public function categorysearch($id,$search=null)
	{
		session(['categorysearch'=> $search]);

		$categoryName;
		Session::get('applocale') === 'en'?
		$categoryName = 'categories.name':
		$categoryName = 'categories.name_spanish';

		if($search == ""){

			Session::forget('categorysearch');

			$product_communities = 
				DB::table('product_communities')
	            ->join('products', 'product_communities.product_id', '=', 'products.id')
	            ->join('users','users.id','=','products.user_id')
	            ->join('categories','categories.id','=','products.product_category')
	            ->select(
	            	'products.product_name',
	            	'products.id',
	            	'products.user_id',
	            	'products.product_img',
	            	'users.first_name',
	            	'users.last_name',
	            	'users.profile_img',
	            	'users.facebook_id'
	            )
	            ->where('product_communities.community_id',$id)
	            ->paginate(6);
		}
		
        $product_communities = 
        	DB::table('product_communities')
            ->join('products', 'product_communities.product_id', '=', 'products.id')
            ->join('users','users.id','=','products.user_id')
            ->join('categories','categories.id','=','products.product_category')
            ->select(
            	'products.product_name',
            	'products.id',
            	'products.user_id',
            	'products.product_img',
            	'users.first_name',
            	'users.last_name',
            	'users.profile_img',
            	'users.facebook_id'
            )
            ->where('product_communities.community_id',$id)
            ->where($categoryName,'like','%'.$search.'%')
            ->paginate(6);
           
    	$isLoggedIn = Auth::check();
		$user = Auth::user();

        $product_communities->setPath(url('/').'/categorysearch/'.$search.'/'.$id);
		
		return view('site.search', compact('product_communities'))->with('search',$search);
	}

	public function email($id,$uid) {
		$community = Community::find($id);
		$communityAdmin = User::find($community->created_by);
		$user = User::find($uid);

    	$community_user = DB::table('community_user')->insert([
				'user_id' => Auth::user()->id,
				'community_id' => $id
				]);
    	$userid =  DB::getPdo()->lastInsertId();

    	$data = array(
		    	'toEmail' => $communityAdmin->email, 
				'toName' => $communityAdmin->first_name .' '. $communityAdmin->last_name ,
				'community_name' => $community->name,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'email' => $user->email,
				'communityId' => $id,
				'userid' => $userid,
			);

	     Mail::send('emails.communityrequest', $data, function ($message) use ($data){
				$message->to($data['toEmail'], $data['toName']);
				$message->from('noreply@ilendu.com','Ilendu');
				$message->subject('Join Community Request');

			});

	     return 'success';
   }

   public function changeStatus($id,$userid,$status){
   //	print_r($userid);die;
		$communitystatus = DB::table('community_user')->where('id',$userid)->get();
		//print_r($communitystatus);die;
		if($communitystatus[0]->is_approve == 1){
			return redirect('communities/'.$id)->with('message', 'Already Accepted');	
		}elseif($communitystatus[0]->is_approve == 2){
			return redirect('communities/'.$id)->with('message', 'Already Rejected');
		}else{
			DB::table('community_user')->where('id',$userid)->update([
				'is_approve' => $status
				]);

			$requesteruser = User::find($communitystatus[0]->user_id);
			$community = Community::find($communitystatus[0]->community_id);

			$data = array('toEmail' => $requesteruser->email,
				'toName' => $requesteruser->name,
				'communityname' => $community->name
				);

			if($status == 2){
				$_id=$communitystatus[0]->user_id;
				$reject=Community_user::where(['user_id' => $_id,'community_id' => $id])->delete();

				Mail::send('emails.rejection_community', $data, function ($message) use ($data) {
					$message->to($data['toEmail'], $data['toName']);
					$message->from('noreply@ilendu.com','Ilendu');
					$message->subject('Reject Your Joining Request');
				});
				return redirect('communities/'.$id)->with('message', 'Request Rejected');
			}else{
				Mail::send('emails.accept_community', $data, function ($message) use ($data) {
					$message->to($data['toEmail'], $data['toName']);
					$message->from('noreply@ilendu.com','Ilendu');
					$message->subject('Accept Your Joining Request');
				});
				return redirect('communities/'.$id)->with('message', 'Request Accepted');
			}
			return redirect('communities/'.$id)->with('message', 'Request Accepted');	
			
		}


	}

}
