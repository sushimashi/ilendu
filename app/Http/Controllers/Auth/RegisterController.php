<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Carbon;
use App\Country;
use Illuminate\Http\Request;
use App\Community;
use App\City;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
   // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'countryOfNewUser' => 'required',
            'stateOfNewUser' => 'required',
            'phone'=>'required',
            'direction'=>'required',
            'cityOfNewUser' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

       protected function registered(Request $request, $user)
    {

          //User register now here you can run any code you want 
          if (\Request::ajax()){

       
            return response()->json(['success'=>true,'message'=>'User authenticated successfully']);

            exit();
        }
        return redirect($this->redirectPath());
    }

    protected function create(array $data)
    {   

        $_expire_date=Carbon\Carbon::now()->addMonths(1);
        #$country=Country::find($data['countryOfNewUser']);
        #$code=str_replace('+','',$country->phonecode);
        $phone=$data['phone'];

        session(['cityvalue'=> $data['cityOfNewUser']]);
        session(['cheked' => '1']);
        $user= User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'zip_code'=>$data['zip_code'],
            'email' => $data['email'],
            'phone'=>$phone,
            'direction'=>$data['direction'],
            'country_id' => $data['countryOfNewUser'],
            'state_id' => $data['stateOfNewUser'],
            'city_id' => $data['cityOfNewUser'],
            'password' => bcrypt($data['password']),
            'expire_date'=>$_expire_date
        ]);




        $community=Community::where('city_id',$data['cityOfNewUser'])->first();

        if (empty($community)) 
        {
             $city=City::where("id",$data['cityOfNewUser'])->first();
        	$community=Community::create(['name'=>$city->name,'public'=>1,'created_by'=>1,'city_id'=>$city->id,'description'=>$city->name.' community','status'=>1]);
        }

        $community->communityUsers()->attach($user, ['is_approve' => 1]);


        return $user;

   // return response()->json(['success'=>true,'message'=>'User authenticated successfully']);

    }
}
