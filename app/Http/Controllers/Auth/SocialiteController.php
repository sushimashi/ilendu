<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Socialite;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Country;
use App\State;
use App\City;
use App\Community;
use App\Community_user;
use Intervention\Image\ImageManagerStatic as Image;



class SocialiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleFacebookCallback()
    {
        
            
            $user = Socialite::driver('facebook')
                            ->fields([
                                'first_name', 
                                'last_name', 
                                'email', 
                                'gender', 
                                'verified',
                                'location'
                            ])->user();


            $create['auth0id'] = "facebook|".$user->getId();
            $create['first_name'] = $user->user['first_name'];
            $create['last_name'] = $user->user['last_name'];
            $create['email'] = $user->getEmail();
            $create['facebook_id'] = $user->getId();
            $name = 'profile_'.rand(5,123).'.png';

Image::make($user->getAvatar())->save(public_path('img/profile/' . $name));


            $create['profile_img'] = $name;

            $userModel = new User;
            $createdUser = $userModel->addNew($create);
           // Auth::loginUsingId($createdUser->id);
            $user = User::find($createdUser->id);
            $user->facebook_id=$create['facebook_id'];
            $user->save();

            if ($user->country_id==null) 
            {
            $ip=request()->server('HTTP_X_FORWARDED_FOR');

            $location =  \Location::get($ip);
            $country=Country::firstOrCreate(['name'=>$location->countryName]);

            if ($location->regionName=="Santiago Metropolitan") 
            {
                $location->regionName="Metropolitana";
            }

            $state=State::where('name','like','%'.$location->regionName.'%')->where('country_id',$country->id)->first();

            if (!empty($state)) 
            {
              $user->state_id=$state->id;

               $city=City::where('name','like','%'.$location->cityName.'%')->where('state_id',$state->id)->first();

             if (!empty($city)) 
            {
              $user->city_id=$city->id;

            }else{
             $user->city_id=107958;   
            }

            }else{
             $user->state_id=3930;   
            }

           

            $user->country_id=$country->id;
            $user->save();

            $community=Community::firstorCreate([
                "name"=>$city->name,
                "city_id"=>$city->id
               ],[
                "description"=>$city->name,
                "com_image"=>"city_icon.png",
                "status"=>1,
                "created_by"=>1,
                "public"=>1
            ]);

            Community_user::firstorCreate(["user_id"=>$user->id,"community_id"=>$community->id,"is_approve"=>1]);
            }
            $credentials=array('facebook_id'=>$user->facebook_id,'password'=>$password);
             session(['cityvalue'=> $user->city_id]);
             session(['countryvalue'=> $user->country_id]);
        
            #Auth::attempt(['email' =>$user->email, 'password' =>$user->password]);
            Auth::login($user, true);


            return redirect('/home');

    
    }
}
