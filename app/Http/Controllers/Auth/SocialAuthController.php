<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Socialite;
use Exception;
use Auth,this;   

class SocialAuthController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Get the user info from provider and check if user exist for specific provider
     * then log them in otherwise
     * create a new user then log them in 
     * Once user is logged in then redirect to authenticated home page
     *
     * @return Response
     */
    public function callback($provider)
    {

        try {
            $user = Socialite::driver($provider)->user();
            $uname = @$user->getname() ? explode(" ",$user->getname()) : "";
            $data['auth0id'] = $provider."|".$user->getid();
            $data['name'] = $user->getname();
            $data['email'] = $user->getemail();
            $data['password'] = $user->getemail();
            $data['first_name'] = @$uname ? $uname[0] : "";
            $data['last_name'] = @$uname ? $uname[1] : "";

            $authUser = $this->findOrCreate($data);
            Auth::loginUsingId($authUser->id);
            return redirect()->route('home');

        } catch (Exception $e) {
            $user = Socialite::driver($provider)->stateless()->user();
            return redirect()->route('home');
        }
    }
    public function findOrCreate($data){

    	$checkIfExist = User::where('email',$data['email'])->first();
        if($checkIfExist){
            return $checkIfExist;
        }
        return User::create($data);
	}
}