<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Session;
use Redirect;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function login(Request $request)
    {
     $credentials = [
        'email' => $request['email'],
        'password' => $request['password'],
    ];

    // Dump data
    if (Auth::attempt($credentials)) {
        

      session(['cityvalue'=> auth()->user()->city_id]);
      session(['countryvalue'=> auth()->user()->country_id]);     
    return Redirect::to('/home');
    }else
    {
return back()->withErrors(['error' => ['the email or password is incorrect']]);

    }

}
    

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function logout(){
    Auth::logout();
    Session::flush();
    Session::forget('cityvalue');
    Session::forget('countryvalue');
    return Redirect::to('/');
}


}
