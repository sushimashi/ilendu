<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth,Carbon;
use App\Transaction;
use App\User;
use App\UserPlan;


use GuzzleHttp\Client; // Instalado con composer, ver composer.json

class ChargeController extends Controller
{
  

  private const QVO_API_URL = 'https://playground.qvo.cl'; //Change it to https://api.qvo.cl on production
  
  private const QVO_API_TOKEN ='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21tZXJjZV9pZCI6ImNvbV9ZMjQ0TFVZcU1abjBkV0Q4VVlEYTdRIiwiYXBpX3Rva2VuIjp0cnVlfQ.c5UQr5LVYVQAxEIylUmjJNVDpXt5ahw4tl-u1JJBres'; // Reemplazar por el token de producción cuando quieras pasar a producción

  public function charge()
  {
    return view('charge', ['product' => self::PRODUCT]);
  }

  public function pay(Request $request)
  {
    $amount = $request['amount'];
    $plan_id=$request['plan_id'];
    $url=$request->input('url');

    $initTransactionResponse = $this->initTransaction($amount,$plan_id,$url);

    return redirect($initTransactionResponse->redirect_url);
  }

  private function initTransaction($amount,$plan_id,$url)
  {

    $guzzleClient = new Client();
    $chargeURL = self::QVO_API_URL."/webpay_plus/charge";

    $body = $guzzleClient->request('POST',
      $chargeURL, [
        'json' => [
          'amount' => $amount,
          'return_url' => url('/charge/return_after_form?plan_id='.$plan_id)
        ],
        'headers' => [
          'Authorization' => 'Bearer '.self::QVO_API_TOKEN
        ],
        'http_errors' => false
      ]
    )->getBody();

    return json_decode($body);
  }

  public function returnAfterForm(Request $request)
  {


    $transactionID = (string)$request['transaction_id'];
    $getTransactionResponse = $this->getTransaction($transactionID);
    if ($getTransactionResponse->status == 'successful') {
      
      $_expire_date=Carbon\Carbon::now()->addMonths(1);

      $user=User::find(auth()->user()->id);
      $user->expire_date=$_expire_date;
      $user->save();

      $user_plan=UserPlan::create([
        "user_id"=>auth()->user()->id,
        "plan_id"=>$request->plan_id
      ]);

      $transaction = new Transaction;
      $transaction->transaction_id = $getTransactionResponse->id;
      $transaction->user_id = Auth::user()->id;
      $transaction->price = $getTransactionResponse->amount;
      $transaction->raw_data = json_encode($getTransactionResponse);
      $transaction->plan_id=$request->plan_id;
      $transaction->status = 1;
      $transaction->save();

      return view('site.product.charge_success', ['response' => $getTransactionResponse]);
    }
    else {

         return back();

    }


  }

  private function getTransaction($transactionID)
  {
    $guzzleClient = new Client();
    $getTransactionURL = self::QVO_API_URL."/transactions/".$transactionID;

    $body = $guzzleClient->request('GET',
      $getTransactionURL, [
        'headers' => [
          'Authorization' => 'Bearer '.self::QVO_API_TOKEN
        ],
        'http_errors' => false
      ]
    )->getBody();

    return json_decode($body);
  }
}