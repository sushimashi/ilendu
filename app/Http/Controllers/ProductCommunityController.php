<?php

namespace App\Http\Controllers;

use App\Product_Community;
use Illuminate\Http\Request;

class ProductCommunityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product_Community  $product_Community
     * @return \Illuminate\Http\Response
     */
    public function show(Product_Community $product_Community)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product_Community  $product_Community
     * @return \Illuminate\Http\Response
     */
    public function edit(Product_Community $product_Community)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product_Community  $product_Community
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product_Community $product_Community)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product_Community  $product_Community
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product_Community $product_Community)
    {
        //
    }
}
