<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Payment;
use App\User;
use App\Membership;
use App\Product;
use App\Transaction;
use App\Borrower;
use session;
use Mail;
use GuzzleHttp\Client;

class CheckoutController extends Controller
{
    /**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	public function store(Request $request) {
		$msg = "";
		$hashSecretWord = 'Yjk4NzQ1ZDctZmUwMS00N2NlLThlM2MtYWJmNDdjNjk0NDdi'; //2Checkout Secret Word
		$hashSid = 901382184; //2Checkout account number
		$hashTotal = $_REQUEST['total']; //Sale total to validate against
		$hashOrder = $_REQUEST['order_number']; //2Checkout Order Number
		$StringToHash = strtoupper(md5($hashSecretWord . $hashSid . $hashOrder . $hashTotal));
		
		if ($StringToHash != $_REQUEST['key']) 
		{
		  	$msg = 'Fail Transaction';
		} else 
		{
		  	$msg = 'Thanks For Borrow Product.Successfully Transaction';
			$payment_history = new Payment;
			$payment_history->invoice_id = $request->invoice_id;
			$payment_history->product_id = $request->li_0_product_id;
			$payment_history->price = $request->total;
			$payment_history->quantity = $request->li_0_quantity;
			$payment_history->card_holder_name = $request->card_holder_name;
			$payment_history->address = $request->street_address;
			/*$payment_history->address2 = $request->street_address2;*/
			$payment_history->city = $request->city;
			$payment_history->country =$request->ip_country;
			$payment_history->zip = $request->zip;
			$payment_history->phone = $request->phone;
			$payment_history->credit_card_processed = $request->credit_card_processed;
			$payment_history->raw_data = json_encode($request->all());
			$payment_history->save();

			$borrower = new Borrower;
			$borrower->user_id= $request->user_id;
			$borrower->product_id = $request->li_0_product_id;
			$borrower->save();

			$expire_date = date('Y-m-d', strtotime(date('Y-m-d'). '+ 30 days'));
			$user = User::find($request->user_id);
			$user->expire_date = $expire_date;
			$user->save();

			$product = Product::join('users', 'user_id', '=', 'users.id')->select('users.email', 'users.name', 'products.*')->where('products.id', $request->li_0_product_id)->get();

			$data = array('toEmail' => $product[0]->email,
			'toName' => $product[0]->name,
			'borrower' => $borrower->id,
			'title' => "Your Product's Borrower Details",
			'product_name' => $product[0]->product_name,
			'product_img' => $product[0]->product_img,
			'first_name' => $user->first_name,
			'last_name' => $user->last_name,
			'email' => $user->email);

			Mail::send('emails.borrow_email', $data, function ($message) use ($data) {
				$message->to($data['toEmail'], $data['toName']);
				$message->subject('Product Borrower');
			});


		}


		return redirect()->route('productdetail',$request->li_0_product_id)->with('message', $msg);
	

	}


	/***QVO Payment Gateway***/
	
	public function QvoPayment($product_id = 0)
	{
	

		$headers = [
		    'Content-Type' => 'application/json',
		    'Authorization' => 'Bearer '.env('Payment_token'),
		];
		
		$user = User::find(Auth::user()->id);
		$client = new Client([
		    'headers' => $headers
		]);
		if($user->customerId == null){

			$r = $client->request('POST', env('Payment_url').'customers', [
				'json' => [
				    'email' => Auth::user()->email,
				    'name' => Auth::user()->name
				  ]
			]);

			$response = $r->getBody()->getContents();
			$response = json_decode($response);
			$customerId = $response->id;
			User::where('id',Auth::user()->id)->update([
				'customerId' =>$customerId
			]);
		}else{
			$customerId = $user->customerId;
		}

		$c = $client->request('POST', env('Payment_url').'customers/'.$customerId.'/cards/inscriptions', [
			'json' => [
			    'return_url' => env('Payment_redirect_url')."/".$product_id,
			  ]
		]);
		$cardresponse = $c->getBody()->getContents();
		$cardresponse = json_decode($cardresponse);
		
		return \Redirect::to($cardresponse->redirect_url);
	}

	/***Callback for Qvo Payment Gateway***/
	public function callbackQvo($product_id = 0,Request $request){
		$membership_fees = Membership::orderBy('id', 'desc')->first();
		$fees = 0;
		if(isset($membership_fees)){
			$fees = $membership_fees->fees;
		}

		$headers = [
		    'Content-Type' => 'application/json',
		    'Authorization' => 'Bearer '.env('Payment_token'),
		];
		$user = User::find(Auth::user()->id);
		$customerId = $user->customerId;
		$client = new Client([
		    'headers' => $headers
		]);

		$card = $client->request('GET', env('Payment_url').'customers/'.$customerId.'/cards/inscriptions/'.$request->uid
		);

		$response = $card->getBody()->getContents();
		$response = json_decode($response);
		if($response->status == 'succeeded'){
			$charge = $client->request('POST', env('Payment_url').'customers/'.$customerId.'/cards/'.$response->card->id.'/charge', [
				'json' => [
				    "amount" =>(int)$fees,
			    	"description"=> "Membership Payment"
				  ]
			]);

			$chargeresponse = $charge->getBody()->getContents();
			$chargeresponse = json_decode($chargeresponse);
			$transaction = new Transaction;
			$transaction->transaction_id = $chargeresponse->id;
			$transaction->user_id = @Auth::user()->id;
			$transaction->product_id = $product_id;
			$transaction->price = $chargeresponse->amount;
			$transaction->raw_data = json_encode($chargeresponse);
			if ($chargeresponse->status == 'successful') {
				$transaction->status = 1;
			}else{
				$transaction->status = 2;
			}
			$transaction->save();

			if ($chargeresponse->status == 'successful') {
				$borrower = new Borrower;
				$borrower->user_id= Auth::user()->id;
				$borrower->product_id = $transaction->product_id;
				$borrower->save();

				$expire_date = date('Y-m-d', strtotime(date('Y-m-d'). '+ 30 days'));
				$user = User::find(Auth::user()->id);
				$user->expire_date = $expire_date;
				$user->save();

				$product = Product::join('users', 'user_id', '=', 'users.id')->select('users.email', 'users.name', 'products.*')->where('products.id', $product_id)->get();

				$data = array('toEmail' => $product[0]->email,
				'toName' => $product[0]->name,
				'borrower' => $borrower->id,
				'title' => "Your Product's Borrower Details",
				'product_name' => $product[0]->product_name,
				'product_img' => $product[0]->product_img,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'email' => $user->email);

				Mail::send('emails.borrow_email', $data, function ($message) use ($data) {
					$message->to($data['toEmail'], $data['toName']);
					$message->subject('Product Borrower');
				});


				$isLoggedIn = Auth::check();
				$user = Auth::user();
				return view('site.successfulpayment')->with('isLoggedIn', $isLoggedIn)->with('user', $user);

				/*return redirect()->route('productdetail',$transaction->product_id)->with('message', 'Thanks For Borrow Product.Successfully Transaction');*/
			}
		}else{
			return redirect('/')->with('message', 'Transaction failed!');
		}
	}

}
