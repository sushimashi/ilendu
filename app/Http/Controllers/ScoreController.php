<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KarmaPoint;


class ScoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            "comment"=>"required",
            "score"=>"required"
        ]);

        KarmaPoint::whereNull('receiver_id')->orWhereNull('commentator_id')->delete();
        
        $data=KarmaPoint::create([
                    "product_id"=>$request->product_id,
                    "receiver_id"=>$request->receiver_id,
                    "commentator_id"=>$request->commentator_id,
                    "borrower_id"=>$request->borrower_id,
                    "score"=>$request->score,
                    "comment"=>$request->comment
                ]);

     


        if ($data) 
        {
        return response()->json(["success"=>true,"message"=>"comment posted successfully"]);
        }else
        {
        return response()->json(["success"=>false,"message"=>"An error has occurred"]);

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
