<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Session,URL,Response,View;

class CategoryController extends Controller {

	public function __construct() {
	}


	  public function getProductsByName(Request $request)
    {
    	$name= $request->name;
    	$category_id= $request->category_id;
    	$_paginate=8;
    	$city_id = Session::get('cityvalue');
    	$premium=0;
    	$gift=0;
    	#$city_id = 5011;

        $user_country=Session::get('countryvalue');

		$united_states=231;


    	switch ($request->filter_radio) {
    		case 'premium':
    		    		$premium=1;
    		     $data= 
	    	Product::select("products.*","users.first_name","users.last_name","users.email")
				    ->join("users","users.id","products.user_id")
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where("products.is_premium",$premium)
				    ->where("products.give_away",0)
				    ->where('products.city_id',$city_id)
				    ->orWhere(['products.share_all'=>1])
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where("products.is_premium",$premium)
				    ->where("products.give_away",0)
                    ->where('products.country_id',$user_country);

                      if (count($data->get())==0) 
                    {
                    $data=$data->orWhere('products.country_id',$united_states)
                    ->where(['products.share_all'=>1])
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where("products.is_premium",1)
				    ->where("products.give_away",0);
                    }

				     $data=$data->orderBy('created_at','DESC')->paginate($_paginate);			
    			break;
    		case 'gift':
    		    		$gift=1;
    		   $data= 
	    	Product::select("products.*","users.first_name","users.last_name","users.email")
				    ->join("users","users.id","products.user_id")
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where("products.give_away",$gift)
				    ->where('products.city_id',$city_id)
				    ->orWhere(['products.share_all'=>1])
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where("products.give_away",1)
                    ->where('products.country_id',$user_country);

                        if (count($data->get())==0) 
                    {

                    $data=$data->orWhere('products.country_id',$united_states)
                    ->where(['products.share_all'=>1])
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where("products.give_away",1);

                    }

                   
				     $data=$data->orderBy('created_at','DESC')->paginate($_paginate);	
    			break;

    			case 'all':


    		     $data= 
	    	Product::select("products.*","users.first_name","users.last_name","users.email")
				    ->join("users","users.id","products.user_id")
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where('products.city_id',$city_id)
				    ->orWhere(['products.share_all'=>1])
				    ->where( function ( $q ) use ( $name, $category_id) {
			          $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where("products.is_premium",$premium)
				    ->where("products.give_away",0)
                    ->where('products.country_id',$user_country);


                        if (count($data->get())==0) 
                    {
                    	
                    $data=$data->orWhere('products.country_id',$united_states)
                    ->where(['products.share_all'=>1])
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1);

                    }


                 
				    $data=$data->orderBy('created_at','DESC')->paginate($_paginate);
    			break;
    		
    	
    	}
    	
    	

	   


	    $total = count($data);
	    
	    if (request()->ajax()) 
        { 

            return Response::json(View::make('category.pagination.index', array('products'=>$data,'total'=>$total))->render());         
        }


      	return Response::json(View::make('category.pagination.index', array(
      		'products' => $data,
      		'total' => $total
      	))->render());

    }	


    public function categoryByFilter(Request $request)
	{


	
		$premium=0;
    	$gift=0;
    	$category_id= $request->category_id;
    	$_paginate=8;
    	$city_id = Session::get('cityvalue');
    	
    	#$city_id = 5011;

        $user_country=Session::get('countryvalue');

		$united_states=231;


    		switch ($request->filter) {
    		case 'premium':
    		    		$premium=1;


    		     $data= 
	    	Product::select("products.*","users.first_name","users.last_name","users.email")
				    ->join("users","users.id","products.user_id")
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where("products.is_premium",$premium)
				    ->where("products.give_away",0)
				    ->where('products.city_id',$city_id)
				    ->where('products.country_id',$user_country)
				    ->orWhere(['products.share_all'=>1])
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where("products.is_premium",$premium)
				    ->where("products.give_away",0)
                    ->where('products.country_id',$user_country);

                    if (count($data->get())==0) 
                    {
                    $data=$data->orWhere('products.country_id',$united_states)
                    ->where(['products.share_all'=>1])
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where("products.is_premium",1)
				    ->where("products.give_away",0);
                    }

                
				    $data=$data->orderBy('created_at','DESC')->paginate($_paginate);			
    			break;
    		case 'gift':
    		    		$gift=1;

    		   $data= 
	    	Product::select("products.*","users.first_name","users.last_name","users.email")
				    ->join("users","users.id","products.user_id")
				    ->where( function ( $q ) use ( $category_id) {
			            $q->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where("products.give_away",$gift)
				    ->where('products.city_id',$city_id)
				    ->where('products.country_id',$user_country)
				    ->orWhere(['products.share_all'=>1])
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where("products.give_away",1)
                    ->where('products.country_id',$user_country);
                       if (count($data->get())==0) 
                    {

                    $data=$data->orWhere('products.country_id',$united_states)
                    ->where(['products.share_all'=>1])
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where("products.give_away",1);

                    }


				    $data=$data->orderBy('created_at','DESC')->paginate($_paginate);	
    			break;

    			case 'all':


    		     $data= 
	    	Product::select("products.*","users.first_name","users.last_name","users.email")
				    ->join("users","users.id","products.user_id")
				    ->where( function ( $q ) use ( $category_id) {
			            $q->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
				    ->where('products.city_id',$city_id)
				    ->where('products.country_id',$user_country)
				    ->orWhere(['products.share_all'=>1])
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1)
                    ->where('products.country_id',$user_country);

                        if (count($data->get())==0) 
                    {

                    $data=$data->orWhere('products.country_id',$united_states)
                    ->where(['products.share_all'=>1])
				    ->where( function ( $q ) use ( $name, $category_id) {
			            $q->where('product_name','like','%'.$name.'%')
			            ->where('product_category','=',$category_id);	            
			        })
				    ->where("products.is_active",1);

                    }

                  
				    $data=$data->orderBy('created_at','DESC')->paginate($_paginate);

    			break;
    		
    	
    	}



    	$total = count($data);
	    
	    if (request()->ajax()) 
        { 

            return Response::json(View::make('category.pagination.index', array('products'=>$data,'total'=>$total))->render());         
        }
	}

	/**List Proudcts based on category***/
	public function getProductsByCategory($id = 0){



		$city_id = Session::get('cityvalue');
		#$city_id=5011;
		$user_country=Session::get('countryvalue');

	    $united_states=231;




		$category = Category::find($id);
    	$_paginate = 8;
    	$users = [];

		$p = Product::where('product_category',$id)
		->where('city_id',$city_id)
		->where('is_active',1)
		->orWhere(['share_all'=>1])
		->where('country_id',$user_country)
		->where('product_category',$id)
		->where('is_active',1);

		if (count($p->get())==0) 
		{
		$product=$p->orWhere('country_id',$united_states)
		->where('is_active',1)
		->where('share_all',1)
		->where('product_category',$id);
		}
		

		$product = $p->get();
        $products = $p->orderBy('created_at','DESC')->paginate($_paginate);

	    $total = count($product);

		foreach($product as $a)
		{
			$users[$a->id] = User::where('id', $a->user_id)->get();
		}


		   if (request()->ajax()) 
        { 
            return Response::json(View::make('category.pagination.index', array('products'=>$products,'total'=>$total,'users'=>$users,'category'=>$category))->render());         
        }
		return view('category.products', compact('products','users','category'));
	}

}