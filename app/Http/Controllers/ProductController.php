<?php
namespace App\Http\Controllers;
use App\Borrower;
use App\Product;
use App\User;
use App\Membership;
use App\Category;
use App\Community;
use App\Product_Community;
use App\Country;
use App\State;
use App\City;
use App\KarmaPoint;
use Auth;
use File;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Mail,DB,Input,Carbon;
use Session,URL,View;
use App\ProductImage;
use Storage;
use App\BorrowerMessage;
use Response;
use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;
use App\Transaction;
use Crypt;

class ProductController extends Controller 
{
	

	public function __construct() 
	{
		$this->middleware('auth', ['except' => ['index','is_notified','search','cityproduct','categoryproduct','changeStatus','show']]);
	}
	/**
	* Display a listing of the resource.
	*
	* @return \Illuminate\Http\Response
	*/



	public function index( Request $request ) {
		$isLoggedIn = Auth::check();
		$user = Auth::user();
		$products = Product::with('user')->where('is_active',1)->paginate(8);
//$total = $products->lastpage();
		$countries = Country::get();
		$cities = '';
		$cities = City::join('products','products.city_id','cities.id')->select('cities.id','cities.name')
		->groupBy('products.city_id')
		->select('cities.id','cities.name')
		->havingRaw('COUNT(products.id) >= 5')
		->get();
		if($cities){
			foreach ($cities as $key => $value) {
				if($value->id == Session::get('cityvalue')){
					$products = Product::with('category','user')->where('is_active',1)->where('in_front',1)->where('city_id',$value->id)->paginate(8);
				}
			}
		}
		if($user){
			$ipcity = City::join('products','products.city_id','cities.id')->where('cities.name',Session::get('ipcity'))->select('products.id','cities.id as city_id')->get();
			if(Session::get('ipcity') == "" || (count($ipcity) < 5) ){
				$message1 = "Sorry,Product was not more than 100 For Your City.";
//return view('site.nocatlog')->with('isLoggedIn', $isLoggedIn)->with('user', $user)->with('cities', $cities);
			}
			/*if($ipcity){
				$products = Product::with('user')->where('is_active',1)->where('city_id',$ipcity[0]->city_id)->paginate(8);
			}*/


		}
	/*if ($request->ajax()) {
		$view = view('site.product.product_data',compact('products','message1','countries'))->render();
return response()->json(['html'=>$view,'total'=>$total]);
}*/
return view('site.product.products', compact('products','message1','countries'))->with('isLoggedIn', $isLoggedIn)->with('user', $user)->with('cities', $cities);
}
	/**
	* Show the form for creating a new resource.
	*
	* @return \Illuminate\Http\Response
	*/


	

	public function product_communities($id)
	{
		$product=Product::find($id)->communities()->get();

		return response()->json(compact('product'));
	}
	

	public function deleteOrder(Request $request)
	{
		$_borrower=Borrower::find($request->id);

		if ($_borrower->user_id==auth()->user()->id) 
		{

			$_product=Product::find($_borrower->product_id);
			$_product->is_active=1;
			$_product->in_front=1;
			$_product->borrowed=0;
			$_product->save();

			$_borrower->status="Canceled";
			$_borrower->save();
				//$order=Borrower::destroy($request->id);

			if ($_borrower) 
			{
				return response()->json(['success'=>true]);
			}else
			{
				return response()->json(['success'=>false]);

			}
		}else
		{
			return response()->json(['success'=>false]);

		}

		

	}


	public function borrow_detail($id)
	{

		$borrow=Borrower::find($id);
		$product=Product::find($borrow->product->id);
		$products=ProductImage::where('product_id',$product->id)->get();
	//check if user has commented

		$check=KarmaPoint::where(['product_id'=>$product->id,'commentator_id'=>auth()->user()->id,'borrower_id'=>$borrow->id])->count();

		$userimg = User::find($product->user_id);


		if ($check) 
		{
			$has_commented=true;
		}else
		{
			$has_commented=false;
		}



		return view('site.product.borrow_detail',compact('has_commented','product','products','userimg','borrow'));

	}

	public function user_communities()
	{
		
		$user_id = Auth::user()->id;

		$joined_community =DB::table('communities')
		->leftJoin('community_user', 'communities.id', '=', 'community_user.community_id')
		->where('communities.created_by',$user_id)
		->orWhere('community_user.user_id',$user_id)
		->select('communities.id as id','communities.name as text')
		->get();

		$list = ['results'=>collect($joined_community)];
		return response()->json($list);		
	}


	public function getCommunities()
	{
		$user = Auth::user();

		$joined_community =DB::table('communities')
		->join('users', 'communities.created_by', '=', 'users.id')
		->where('users.country_id',$user->country_id)
		->select('communities.id as id','communities.name as text')
		->get();
		$list = ['results'=>collect($joined_community)];
		return response()->json($list);		
	}

	public function create() {
		$isLoggedIn = Auth::check();
		$user = Auth::user();
		$user_id = Auth::user()->id;
		$category = Category::where('is_active',1)->get();
		//$joined_community = Community::where('created_by', $user_id)->get();

		$communities=auth()->user()->communities()->get();


		$countries = Country::get();
		//$allcity = City::get();
		$cities = City::join('products','products.city_id','cities.id')->select('cities.id','cities.name')
		->groupBy('products.city_id')
		->select('cities.id','cities.name')
		->havingRaw('COUNT(products.id) >= 5')
		->get();
		/*return view('site.product.add_product', compact('category','cities','countries') )->with('isLoggedIn', $isLoggedIn)->with('user', $user);*/
		return view('site.product.add_product', compact('category','cities','countries','communities') )->with('isLoggedIn', $isLoggedIn)->with('user', $user);
	}
	/**
	* Store a newly created resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
	public function store(Request $request) {



		if ($request->only_img==1)
		{
			$product = new Product;

			if ($request->file('file')) 
			{
				$image = $request->file('file');
				$name = $image->getClientOriginalName();


				$filename = rand() . '.' . $image->getClientOriginalExtension();
				Image::make($image)->orientate()->save( public_path('/img/thumbnail/' . $filename ) );

				Image::make($image)->orientate()->save( public_path('/img/' . $filename ) );

				$product_image=new ProductImage;
				$product->user_id=auth()->user()->id;
				$product->save();

				$product_image->photo=$filename;
				$product_image->product_id=$product->id;


				$product_image->save();
				$product->product_img= $product_image->photo;

				$product->save();
				return response()->json(['success'=>true,'message'=>'Product added successfully'],200);

			}
		}
		
		$user_id = Auth::user()->id;
		$data = $request->all();
		$product = new Product;
		/*$validator = Validator::make($request->all(), [
			'product_name' => 'required',
			'product_img' => 'required',
			'product_category' => 'required',
			'product_img' => 'max:10240',
			'is_premium' => 'required_if:give_away,1',
			'return_term'=>'required'
		]);*/


		//return $request->all();

		$this->validate($request,[
			'product_name' => 'required',
			'product_img' => 'required',
			'product_category' => 'required',
			'product_img' => 'max:10240',
			'return_term'=>'required'
		]);

		if ($request->hasFile('product_img')) {
			$image = $request->file('product_img');
		}else{
			flash('Debe agregar una imagen del producto.')->error()/*->important()*/;
			return redirect('/products/create');
		}
		/*
		if ($validator->fails()) {
			return redirect('/products/create')
				->withInput()
				->withErrors($validator);
			}*/
			if($request->is_premium == 'on'){
				$is_premium = 1;
			}else{
				$is_premium = 0;
			}



			$is_active = 1;

			$ipcity = City::where('name',Session::get('ipcity'))->select('cities.id','cities.name')->get();
			$product->user_id = $user_id;
			$product->product_name = $request->product_name;
			$product->product_category = $request->product_category;
			$product->product_desc = $request->product_desc;
			$product->is_premium = $is_premium;
			$product->is_active = 1;
			$product->in_front = 1;
			$product->return_term=$request->return_term;
			$product->country_id = auth()->user()->country->id;

			if (auth()->user()->isAdmin()) 
			{

				$product->share_all = 1;

			}

     #   $product->return_term=Carbon\Carbon::now()->addWeeks($request->return_term);
			$product->city_id = Auth::user()->city_id ? Auth::user()->city_id : @$ipcity[0]->id ;
			if (isset($request->is_premium)) 
			{
				$product->is_premium = $request->is_premium;

			}

			if (isset($request->give_away)) 
			{
				$product->give_away = $request->give_away;

			}
			$product->save();

			/* Store thumbnail images */
		// $image_resize = Image::make($image->getRealPath());
		// $image_resize->resize(182, 182);
		// $image_resize->save(public_path('/img/thumbnail/' . $image->getClientOriginalName()));
			foreach ($image as $key => $value)
			{



				$name = $value->getClientOriginalName();


				$filename = rand() . '.' . $value->getClientOriginalExtension();
				Image::make($value)->orientate()->save( public_path('/img/thumbnail/' . $filename ) );

				Image::make($value)->orientate()->save( public_path('/img/' . $filename ) );


				$product_image=new ProductImage;

				$product_image->photo=$filename;
				$product_image->product_id=$product->id;
				$product_image->save();
       // $destinationThumbPath = public_path('/img/thumbnail');
       // compress_img($value->getPathname(),$destinationThumbPath.'/'.$name,0.3);
		//$destinationPath = public_path('/img');
		//$value->move($destinationPath, $name);

			}


			$product_image=ProductImage::where('product_id',$product->id)->first();
			$product->product_img= $product_image->photo;
			$product->save();


			if (count($request->coms)>0) 
			{
				foreach ($request->coms as $key => $value) 
				{
					Product_Community::create([
						'community_id' => $value,
						'product_id' =>$product->id
					]);
				}
			}


			$request->session()->put('key','1');
			return response()->json(['success'=>true,'message'=>'Product added successfully'],200);
		}
	/**
	* Display the specified resource.
	*
	* @param  \App\Product  $product
	* @return \Illuminate\Http\Response
	*/
	public function show(Product $product) 
	{
		
		$communities=[];
		$is_expire=false;
		$user_product_status=null;
		if (Auth::check()) 
		{
			$user_id = Auth::user()->id;
			$user = User::find($user_id);


        //check user status

			$user_product_status=$user->borrows()->where('product_id',$product->id)->latest()->first();

		#return compact('user_product_status','user');

        //if user doesn't have a plan it becomes a guest
			if ($user->user_plan()->count()>0) 
			{

				$user_plan=$user->user_plan()->latest()->first()->plan()->first()->type;

			}else
			{
				$user_plan="guest";
			}

			$current_date=Carbon\Carbon::now();
			$expire = Carbon\Carbon::parse($user->expire_date);
			$_check=$current_date->greaterThanOrEqualTo($expire);

			if ($_check)
			{
				$is_expire=true;
			}


		}else{
			$user=null;
			
		}




		


		
		$products=ProductImage::where('product_id',$product->id)->get();
		
		$isLoggedIn = \Auth::check();
		
		$userimg = User::find($product->user_id);
		$countries = Country::get();
		$cities = City::join('products','products.city_id','cities.id')->select('cities.id','cities.name')
		->groupBy('products.city_id')
		->select('cities.id','cities.name')
		->havingRaw('COUNT(products.id) >= 5')
		->get();
		$categories=\App\Category::get();
		$pending=false;

		$code='';
		$phone='';
		$first_name='';
		$last_name='';
		$email='';
		$borrow='';


		if ($product->borrower) 
		{
			//we find the borrower
			$borrow=$product->borrower;



			switch ($borrow->status) {
				case 'Pending':


				if ($product->share_all==0) 
				{
					if (empty($user)) 
					{
						return back();
					}elseif($user->id !=$borrow->user_id)
					{
						return back();
					}
				}


				$code=str_replace('+','',$borrow->product->user->country->phonecode);
				$phone='+'.$code.$borrow->product->user->phone;
				$first_name=$borrow->product->user->first_name;
				$last_name=$borrow->product->user->last_name;
				$email=$borrow->product->user->email;
				$pending=true;
				break;

				case 'Accept':

				$code=str_replace('+','',$borrow->product->user->country->phonecode);
				$phone='+'.$code.$borrow->product->user->phone;
				$first_name=$borrow->product->user->first_name;
				$last_name=$borrow->product->user->last_name;
				$email=$borrow->product->user->email;
				$pending=true;
				break;


			}


		}


		$f=Membership::where('fees_plan','monthly')->first();



		return view('site.product.product_detail', compact('product','user_product_status','first_name','last_name','email','code','products','phone','borrow', 'user', 'userimg','cities','countries','categories','is_expire','user_plan','f'))->with('isLoggedIn', $isLoggedIn);



		
	}


	/**
	* Show the form for editing the specified resource.
	*
	* @param  \App\Product  $product
	* @return \Illuminate\Http\Response
	*/

	public function edit(Request $request , $id)
	{




		$image = $request->file('product_img');



		$product = Product::find($request->product_id);

		$product->product_desc=$request->product_desc;
		$product->product_name=$request->product_name;
		$product->product_category=$request->category;
		$product->return_term=$request->return_term;
		$product->city_id = auth()->user()->city_id;

		$product->is_active = $request->has('is_active') ? 1 : 0;
		$product->in_front = $request->has('is_active') ? 1 : 0;
#$product->return_term=Carbon\Carbon::now()->addWeeks($request->return_term);

		if (isset($request->is_premium)) 
		{
			$product->is_premium = $request->is_premium;

		}else{
			$product->is_premium =0;

		}

		if (isset($request->give_away)) 
		{
			$product->give_away = $request->give_away;

		}else{
			$product->give_away =0;

		}

		$product_images=ProductImage::where('product_id',$request->product_id)->get();
		if( $request -> hasfile('product_img') )
		{
			if( $id > 0 )
			{
				$usersImage = public_path('/img/thumbnail/'.$product->product_img);
				if(@$product->product_img){
					if (File::exists($usersImage))
					{
						unlink($usersImage);
					}
				}
			}
			foreach ($product_images as $key => $value)
			{
				$img_routes = public_path('/img/'.$value->photo);
				unlink($img_routes);

//unlink(public_path('/img/thumbnail/'.$value->photo ));

				ProductImage::where('product_id',$value->product_id)->delete();

			}
			foreach ($image as $key => $value)
			{
				$name = $value->getClientOriginalName();

				$filename = time() . '.' . $value->getClientOriginalExtension();
				Image::make($value)->orientate()->save( public_path('/img/thumbnail/' . $filename ) );
				Image::make($value)->orientate()->save( public_path('/img/' . $filename ) );

				$product_image=new ProductImage;
				/* Store thumbnail images */

				$product_image->photo=$filename;
				$product_image->product_id=$product->id;
				$product_image->save();
//$destinationThumbPath = public_path('/img/thumbnail');
//compress_img($value->getPathname(),$destinationThumbPath.'/'.$name,0.3);
//$destinationPath = public_path('/img');
//$value->move($destinationPath, $name);
			}

			$_image=ProductImage::where('product_id',$product->id)->first();
			$product->product_img= $_image->photo;
		}

		$product->save();
		$product->communities()->sync($request->coms);

		if (count($request->coms)>0) 
		{
			foreach ($request->coms as $key => $value) 
			{
				Product_Community::firstOrCreate([
					'community_id' => $value],
					[
						'product_id' =>$product->id
					]);
			}
		}

		return redirect('/products/'.$product->id);


	}
	/**
	* Update the specified resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @param  \App\Product  $product
	* @return \Illuminate\Http\Response
	*/
	public function update(Request $request, $id)
	{
		
	}
	/**
	* Remove the specified resource from storage.
	*
	* @param  \App\Product  $product
	* @return \Illuminate\Http\Response
	*/
	

	public function destroy(Product $product) {
		$product = Product::find($product->id);
		$product->delete();
		return response()->json(["success"=>true,"message"=>"Product deleted successfully"]);
	}


	public function returnedOrder($id)
	{
		$product=Product::find($id);
		$product->borrowed=0;
		$product->is_active=1;
		$product->in_front=1;

		$product->save();

		if ($product->share_all) 
		{
			$borrow = Borrower::where(["product_id"=>$id,"user_id"=>auth()->user()->id])->latest()->first();

		}else{
			$borrow = Borrower::where(["product_id"=>$id])->latest()->first();

		}
		$borrow->status='Returned';
		$borrow->save();

	//we generate the first evaluation
		$karma=new KarmaPoint();
		$karma->product_id=$product->id;
		$karma->score=0;
		$karma->save();


		//send the mail
		$data = array('toEmail' => $borrow->user->email,
			'toName' => $borrow->user->first_name,
			'borrow_id'=>$borrow->id,
			'product_name' => $product->product_name,
			'product_img' => $product->product_img,
			'first_name' => $borrow->user->first_name,
			'last_name' => $borrow->user->last_name,
			'product_id'=>$product->id,
			'email' => $product->user->email);

		Mail::send('emails.returned_email', $data, function ($message) use ($data) {
			$message->to($data['toEmail'], $data['toName']);
			$message->from('noreply@ilendu.com','Ilendu');
			$message->subject('Returned Product');
		});

		Mail::send('emails.returned_email', $data, function ($message) use ($data) {
			$message->to('admin@ilendu.com', 'Ilendu');
			$message->from('noreply@ilendu.com','Ilendu');
			$message->subject('Returned Product');
		});




		return response()->json(["success"=>true,"borrow_id"=>$borrow->id]);

	}

	public function receivedOrder($id)
	{
		
		$borrow = Borrower::where(['product_id'=>$id,'user_id'=>auth()->user()->id])->latest()->first();

		$borrow->status="Received";

		$borrow->save();

                //send the mail
		$data = array('toEmail' => $borrow->user->email,
			'toName' => $borrow->user->first_name,
			'product_name' => $borrow->product->product_name,
			'product_img' => $borrow->product->product_img,
			'first_name' => $borrow->user->first_name,
			'last_name' => $borrow->user->last_name,
			'product_id'=>$borrow->product->id,
			'email' => $borrow->user->email);



		Mail::send('emails.received_email', $data, function ($message) use ($data) {
			$message->to($data['email'], $data['toName']);
			$message->from('noreply@ilendu.com','Ilendu');
			$message->subject('Received Product');
		});    

                //admin
		$data = array('toEmail' => $borrow->product->user->email,
			'toName' => $borrow->user->first_name,
			'product_name' => $borrow->product->product_name,
			'product_img' => $borrow->product->product_img,
			'first_name' => $borrow->product->user->first_name,
			'last_name' => $borrow->product->user->last_name,
			'product_id'=>$borrow->product->id,
			'email' => $borrow->product->user->email);

		Mail::send('emails.received_email_admin', $data, function ($message) use ($data) {
			$message->to($data['email'], $data['toName']);
			$message->from('noreply@ilendu.com','Ilendu');
			$message->subject('Received Product');
		});




		return response()->json(["success"=>true,"borrow_id"=>$borrow->id]);

	}


	public function productBorrowAll(Request $request,$id)
	{

		$this->validate($request,[
			"direction"=>"required",
			"postal_code"=>"required"
		]);


		$category=Category::find($request->category_id);

        $stripe = Stripe::make('sk_test_hMHQb50MvSTceiF00kY1GeKQ');

		$charge = $stripe->charges()->create([
			'customer' => auth()->user()->customerId,
			'currency' => 'USD',
			'amount' =>floatval($category->price),
			'description' => 'Add in wallet',
		]);

		if($charge['status'] == 'succeeded') {

			$transaction = new Transaction;
			$transaction->transaction_id = $charge["id"];
			$transaction->user_id = Auth::user()->id;
			$transaction->price = $charge["amount"]/100;
			$transaction->raw_data = json_encode($charge);
			$transaction->plan_id=$request->plan_id;
			$transaction->status = 1;
			$transaction->save();
		

		$product=Product::find($id);

		$user_id = Auth::user()->id;
		$borrower_id=Product::find($id)->user->id;
		$borrower=User::find($borrower_id);
		$borrow = new Borrower;
		$borrow->user_id = $user_id;
		$borrow->product_id = $id;
		$borrow->message=($request->message!="")?$request->message:"Hi i'm interested in your product";
		$borrow->status='Sent';
		$borrow->direction=$request->direction;
		$borrow->postal_code=$request->postal_code;
		$borrow->state_id=$request->state_id;
		$borrow->country_id=$request->country_id;
		$borrow->save();

		//stripe payment goes here


		$user = User::find($user_id);



		$borrow_message=BorrowerMessage::create([
			"borrow_id"=>$borrow->id,
			"user_id"=>auth()->user()->id,
			"message"=>($request->message!="")?$request->message:"Hi i'm interested in your product"
		]);

		$data = array('toEmail' => $borrow->user->email,
			'toName' => $borrow->user->first_name,
			'borrow_id'=>$borrow->id,
			'product_name' => $borrow->product->product_name,
			'product_img' => $borrow->product->product_img,
			'first_name' => $borrow->user->first_name,
			'last_name' => $borrow->user->last_name,
			'product_id'=>$borrow->product->id,
			'email' => $borrow->product->user->email);


		Mail::send('emails.sent_email', $data, function ($message) use ($data) {
			$message->to($data['toEmail'], $data['toName']);
			$message->from('noreply@ilendu.com','Ilendu');
			$message->subject('Sent Product');
		});


                //ilendu mail

		Mail::send('emails.sent_admin_email', $data, function ($message) use ($data) {
			$message->to('admin@ilendu.com','Ilendu');
			$message->from('noreply@ilendu.com','Ilendu');
			$message->subject('Sent Product');
		});

return response()->json(['success'=>true,'body' => View::make('site.product.responses.request_pending_share')->render(),'message'=>'sended']);

		}else{
		return response()->json(['success'=>false,'message'=>'error']);	
		}

		
		
	}



	/* Borrow product */
	public function productBorrow(Request $request,$id)
	{	


		$user_id = Auth::user()->id;
		$borrower_id=Product::find($id)->user->id;
		$borrower=User::find($borrower_id);
		$borrow = new Borrower;
		$borrow->user_id = $user_id;
		$borrow->product_id = $id;
		$borrow->message=$request->message;
		$borrow->status='Pending';

		//get product


		$days=3;
		$borrow->save();
		$user = User::find($user_id);
		$code=str_replace('+','',$user->country->phonecode);
		$product = Product::join('users', 'user_id', '=', 'users.id')->select('users.email', 'users.first_name', 'products.*')->where('products.id', $id)->get();
		$borrow->expire_date=Carbon\Carbon::now()->addWeeks($product[0]->return_term);
		$borrow->expiration_notify=Carbon\Carbon::now()->addDays($days);

		$borrow->save();

		$product[0]->is_active=0;
		$product[0]->in_front=0;
		$product[0]->save();


		$borrow_message=BorrowerMessage::create([
			"borrow_id"=>$borrow->id,
			"user_id"=>auth()->user()->id,
			"message"=>($request->message!="")?$request->message:"Hi i'm interested in your product"
		]);

		$data = array('toEmail' => $product[0]->email,
			'toName' => $product[0]->name,
			'borrower' => $borrow->id,
			'phone'=>'+'.$code.$user->phone,
			'title' => "You have received a borrow request!",
			'product_name' => $product[0]->product_name,
			'message_box'=>$borrow->message,
			'product_img' => $product[0]->product_img,
			'first_name' => $user->first_name,
			'last_name' => $user->last_name,
			'email' => $user->email);
		Mail::send('emails.borrow_email', $data, function ($message) use ($data) {
			$message->to($data['toEmail'], $data['toName']);
			$message->from('noreply@ilendu.com','Ilendu');
			$message->subject('Product Borrower');
		});
		return response()->json(['body' => View::make('site.product.responses.request_status',["phone"=>'+'.$code.$borrower->phone,"first_name"=>$borrower->first_name,"last_name"=>$borrower->last_name,"email"=>$borrower->email])->render(),'message'=>'sended']);
	}


	/* Add CreaditCard Detail */
	public function creditcardDetail(Request $request) {
		$user = User::find($request->userid);
		$user->credit_card = $request->credit_card;
		$user->expireMonth = $request->expireMonth;
		$user->expireYear = $request->expireYear;
		$user->CVC = $request->CVC;
		$user->save();
		return "Sended";
	}

	public function changeStatus(Request $request,$id,$status)
	{

		$borrower = Borrower::find($id);
		if($borrower->status == 'Accept'){
			if ($request->ajax())
			{
				return response()->json(["message"=>"Already Accepted","status"=>200]);
			}else
			{
				return redirect('/products/'.$borrower->product_id)->with('message', 'Already Accepted');

			}


		}elseif($borrower->status == 'Reject'){
			if ($request->ajax())
			{
				return response()->json(["message"=>"Already Rejected","status"=>200]);
			}else{
				return redirect('/products/'.$borrower->product_id)->with('message', 'Already Rejected');

			}

		}else{
			$borrower->status = $status;
			$borrower->save();
			$user = User::find($borrower->user_id);
			$product = Product::join('users', 'user_id', '=', 'users.id')->select('users.email', 'users.first_name', 'products.*')->where('products.id', $borrower->product_id)->get();


			$data = array('toEmail' => $product[0]->email,
				'toName' => $product[0]->name,
				'product_name' => $product[0]->product_name,
				'product_img' => $product[0]->product_img,
				'first_name' => $user->first_name,
				'last_name' => $user->last_name,
				'product_id'=>$product[0]->id,
				'email' => $user->email);
			
			if($status == 'Reject'){
				Mail::send('emails.rejection_email', $data, function ($message) use ($data) {
					$message->to($data['email'], $data['toName']);
					$message->from('noreply@ilendu.com','Ilendu');
					$message->subject('Reject Your Request');
				});
			}
			else
			{
				$active=Product::find($product[0]->id);
				$active->borrowed=1;

				if ($active->give_away==1) 
				{
					$active->user_id=$user->id;
					$active->is_active=1;
					$active->in_front=1;
					$active->is_premium=1;
					$active->borrowed=0;

					//delete borrower
					$borrower->delete();
				}
				$active->save();




				Mail::send('emails.accept_email', $data, function ($message) use ($data) {
					$message->to($data['email'], $data['toName']);
					$message->from('noreply@ilendu.com','Ilendu');
					$message->subject('Accept Your Request');
				});
			}

			if($status == 'Reject'){

				if ($request->ajax())
				{
					return response()->json(["message"=>"Request Rejected","status"=>200]);
				}else{
					return redirect('/products/'.$borrower->product_id)->with('message', 'Request Rejected');

				}


			}else{

				if ($request->ajax())
				{
					return response()->json(["message"=>"Request Accepted","status"=>200]);
				}else{
					return redirect('/products/'.$borrower->product_id)->with('message', 'Request Accepted');

				}


			}

			
		}
	}





	public function is_notified()
	{


		$borrowers=Borrower::all();
		$now = Carbon\Carbon::now();


		foreach ($borrowers as $key => $value) {
			$date = Carbon\Carbon::parse($value->expire_date);
			$expired_notify= Carbon\Carbon::parse($value->expiration_notify);

			if ($expired_notify->isSameDay($now) && $value->status=="Pending") 
			{
				$value->delete();
			}


			if ($date->isSameDay($now)){

				if ($value->notified==0 && $value->status="Accept") 
				{

  	   //mail data

					$data = array('toEmail' => $value->user->email,
						'toName' => $value->user->first_name,
						'first_name' => $value->user->first_name,
						'last_name' => $value->user->last_name,
						'email' => $value->email,
						'requested_user'=>$value->product->user->first_name.' '.$value->product->user->last_name,
						'product_img'=>$value->product->product_img,
						'subject'=> 'Product devolution'
					);


					Mail::send('emails.more_time_product', $data, function ($message) use ($data) {
						$message->to($data['toEmail'], $data['toName']);
						$message->from('noreply@ilendu.com','Ilendu');
						$message->subject($data['subject']);
					});

					$value->notified=1;
					$value->save();
				}else
				{
					// no more chances
					Mail::send('emails.return_product', $data, function ($message) use ($data) {
						$message->to($data['toEmail'], $data['toName']);
						$message->from('noreply@ilendu.com','Ilendu');
						$message->subject($data['subject']);
					});

				}


			}

		}

	}

	public function more_time($id)
	{

		$borrow_id=Crypt::decrypt($id);  
		return view('site.more_time',compact('borrow_id'));
	}


		public function ver()
	{

		$value=Borrower::find(71);

        $data = array('toEmail' => $value->user->email,
						'toName' => $value->user->first_name,
						'first_name' => $value->user->first_name,
						'borrow_id'=>$value->id,
						'last_name' => $value->user->last_name,
						'email' => $value->email,
						'requested_user'=>$value->product->user->first_name.' '.$value->product->user->last_name,
						'product_img'=>$value->product->product_img,
						'subject'=> 'Product devolution'
					);


					Mail::send('emails.more_time_product', $data, function ($message) use ($data) {
						$message->to($data['toEmail'], $data['toName']);
						$message->from('noreply@ilendu.com','Ilendu');
						$message->subject($data['subject']);
					});
		
	}



	public function add_more_time(Request $request)
	{



		$borrow=Borrower::find($request->borrow_id);
		$date = Carbon\Carbon::parse($borrow->expire_date);

		$borrow->expire_date=$date->addDays($request->days);

		$borrow->save();


		// send message to user

		$data = array('toEmail' => $borrow->product->user->email,
						'toName' => $borrow->product->user->first_name,
						'user_borrower'=>$borrow->user->first_name.' '.$borrow->user->last_name,
						'expire_date'=> Carbon\Carbon::parse($borrow->expire_date)->format('d/m/Y'),
						'first_name' => $borrow->product->user->first_name,
						'last_name' => $borrow->product->user->last_name,
						'product_img'=>$borrow->product->product_img,
						'comment'=>$request->message,
						'subject'=> 'Product Return'
					);


		Mail::send('emails.more_time_response', $data, function ($message) use ($data) {
						$message->to($data['toEmail'], $data['toName']);
						$message->from('noreply@ilendu.com','Ilendu');
						$message->subject($data['subject']);
					});


		return redirect('home');
	}


	public function search(Request $request)
	{
		$search = $request->search;
		session(['searchvalue'=> $search]);
		if($search == ""){
			Session::forget('searchvalue');
		}
		$products = Product::where('product_name','like','%'.$search.'%')
		->orderBy('id')
		->where('is_active',1)
		->where('city_id',Session::get('cityvalue'))
		->paginate(8);
		$isLoggedIn = Auth::check();
		$user = Auth::user();
		$countries = Country::get();
		$cities = City::join('products','products.city_id','cities.id')->select('cities.id','cities.name')
		->groupBy('products.city_id')
		->select('cities.id','cities.name')
		->havingRaw('COUNT(products.id) >= 5')
		->get();
		$products->setPath(url('/').'/search/?search='.$search);
		return view('site.product.search_product', compact('products','countries'))->with('isLoggedIn', $isLoggedIn)->with('user', $user)->with('search',$search)->with('cities',$cities);
	}
	public function productList(){
		$isLoggedIn = Auth::check();
		$user = Auth::user();
		$countries = Country::get();
		$length = $_REQUEST['length'];
		$states = State::where('country_id',$user->country_id)->get();
		$allcities = City::where('state_id',$user->state_id)->get();
		$cities = City::join('products','products.city_id','cities.id')->select('cities.id','cities.name')
		->groupBy('products.city_id')
		->select('cities.id','cities.name')
		->havingRaw('COUNT(products.id) >= 5')
		->get();
		$products = Product::with('category')->orderBy('id','desc')->limit($length)->get();
		$categories = Category::select('id','name')->where('is_active',1)->get();
		return view('site.productlist', compact('products','countries','categories'))->with('isLoggedIn', $isLoggedIn)->with('user', $user)->with('cities', $cities);
	}
	public function productlistdata()
	{
		$columns = array(
			0 =>'id',
			1 =>'product_name',
			2=> 'product_img',
			3=> 'product_category',
			4=> 'is_active',
			5=> 'in_front',
			6=> 'id',
			7=> 'category_id',
		);
		$totalData = Product::count();
		$totalFiltered = $totalData;
		$length = $_REQUEST['length'];
		$start = $_REQUEST['start'];
		$search = $_REQUEST['search']['value'];
		if(empty($search))
		{
			$products = Product::with('category')
			->offset($start)
			->limit($length)
			->get();
		}
		else {
			$products =  Product::with('category')
			->orWhere('product_name', 'LIKE',"%{$search}%")
			->orWhereHas("category",function($query) use($search){
				$query->where('name','LIKE',"%{$search}%");
			})
			->offset($start)
			->limit($length)
			->get();
			$totalFiltered = Product::with('category')
			->orWhere('product_name', 'LIKE',"%{$search}%")
			->orWhereHas("category",function($query) use($search){
				$query->where('name','LIKE',"%{$search}%");
			})
			->count();
		}
		$data = array();
		if(!empty($products))
		{
			foreach ($products as $product)
			{
				$nestedData['id'] = $product->id;
				$nestedData['product_name'] = $product->product_name;
				if(File::exists(public_path().'/img/'.$product->product_img))
					{$nestedData['product_img'] = URL::to('/img/').'/' .$product->product_img;
			}else{
				$nestedData['product_img'] = URL::to('/img/img_bg.jpg');
			}
			$nestedData['product_category'] = $product['category']->name;
			$nestedData['is_active'] =$product->is_active;
			$nestedData['in_front'] = $product->in_front;
			$nestedData['category_id'] = $product->product_category;
			$data[] = $nestedData;
		}
	}
	$json_data = array(
		"draw"            => intval($_REQUEST['draw']),
		"recordsTotal"    => intval($totalData),
		"recordsFiltered" => intval($totalFiltered),
		"data"            => $data
	);
	return json_encode($json_data);
}
public function productListedit(Request $request, $id)
{
	$product = Product::find($request->product_id);
	$name = $request->old_file;
	if( $request -> hasfile('product_img') )
	{
		if( $id > 0 )
		{
			$usersImage = public_path('/img/'.$product->product_img);
			if(@$product->product_img){
				if (File::exists($usersImage))
				{
					unlink($usersImage);
				}
			}
		}
		$product_img = $request->file('product_img');
		$name = 'kyc'.rand(5,123).'.'.$product_img->getClientOriginalExtension();
		$product_img->move(public_path().'/img/', $name );
	}
	$product->product_name = $product->product_name;
	$product->product_category =$request->get('category');
	$product->product_img = $name;
	$product->save();
	return redirect('/productlist');
}
public function productDetail($id) {

	$product = Product::find($id);
	$user_id = Auth::user()->id;
	$user = User::find($user_id);
	if((strtotime($user->expire_date)) < strtotime(date('Y-m-d'))){
		$user->expireDays = 0;
	}elseif($user->expire_date != null){
		$date1=date_create(date('Y-m-d'));
		$date2=date_create(date('Y-m-d',strtotime($user->expire_date)));
		$diff=date_diff($date1,$date2);
		$user->expireDays = $diff->days;
	}else{
		$user->expireDays = 0;
	}
	$membership_fees = Membership::orderBy('id', 'desc')->first();
	if(isset($membership_fees)){
		$user->membershipFees = $membership_fees->fees;
	}
	$where = array('user_id' => $user_id, 'product_id' => $product->id);
	$isLoggedIn = \Auth::check();
	$borrower = Borrower::where($where)->orderBy('id','desc')->first();

	$products = Product::where('user_id', $user_id)->get();
	$userimg = User::find($product->user_id);
	$countries = Country::get();
	$cities = City::join('products','products.city_id','cities.id')->select('cities.id','cities.name')
	->groupBy('products.city_id')
	->select('cities.id','cities.name')
	->havingRaw('COUNT(products.id) >= 5')
	->get();
	return view('site.product_detail_admin', compact('product', 'products', 'borrower', 'user', 'userimg','cities','countries'))->with('isLoggedIn', $isLoggedIn);
}
public function productComment(Request $request){
	if($request->is_active == 'on'){
		$status = 1;
	}else{
		$status = 0;
	}
	if($request->is_premium == 'on'){
		$is_premium = 1;
	}else{
		$is_premium = 0;
	}

	$product = Product::find($request->id);
	$product->is_active = $status;
	$product->comment = $request->comment;
	$product->is_premium = $is_premium;
	$product->save();
	$product = Product::join('users', 'user_id', '=', 'users.id')->select('users.email', 'users.name', 'products.*')->where('products.id', $request->id)->get();
	$user = User::find(Auth::user()->id);
	$data = array('toEmail' => $product[0]->email,
		'toName' => $product[0]->name,
		'product_name' => $product[0]->product_name,
		'product_img' => $product[0]->product_img,
		'first_name' => $user->first_name,
		'last_name' => $user->last_name,
		'email' => $user->email,
		'status' =>	$product[0]->is_active,
		'comment' => $request->comment);
	if($product[0]->is_active == 1){
		$data['subject'] = 'Product Approval';
	}else{
		$data['subject'] = 'Product Disapprove';
	}
	Mail::send('emails.approve_email', $data, function ($message) use ($data) {
		$message->to($data['toEmail'], $data['toName']);
		$message->from('noreply@ilendu.com','Ilendu');
		$message->subject($data['subject']);
	});
	return redirect('productlist')->with('message', 'Product Updated!');
}
public function productStatusChange($id,$status){
	$product = Product::find($id);
	$product->is_active = $status;
	$product->save();
	return response()->json(['status'=>$status]);
	return $status;
}
public function productFrontPage($id,$status){
	$product = Product::find($id);
	$product->in_front = $status;
	$product->save();
	return $status;
}
public function cityproduct($id)
{
	$paginate=9;
	$isLoggedIn = Auth::check();
	$user = Auth::user();
	$cities = City::join('products','products.city_id','cities.id')
	->select('cities.id','cities.name')
	->groupBy('products.city_id')
	->select('cities.id','cities.name')
	->get();
	
	$user_country=Session::get('countryvalue');
	$united_states=231;


	$products = Product::where('city_id',$id)
	->where('is_active',1)
	->where('in_front',1)
	->orWhere('country_id',$user_country)
	->where('is_active',1)
	->where('in_front',1)
	->where('share_all',1);

	if (count($products->get())==0) 
	{
		$products=$products->orWhere('country_id',$united_states)
		->where('is_active',1)
		->where('in_front',1)
		->where('share_all',1);
	}
	$products=$products->orderBy('created_at','DESC')->paginate($paginate);

	session(['cityvalue'=> $id]);


	return view('site.cityproduct', compact('products'))->with('isLoggedIn', $isLoggedIn)->with('user', $user)->with('cities',$cities);
}
public function categoryproduct(Request $request,$id)
{


	$isLoggedIn = Auth::check();
	$user = Auth::user();
	$cities = City::join('products','products.city_id','cities.id')->select('cities.id','cities.name')
	->groupBy('products.city_id')
	->select('cities.id','cities.name')
	->havingRaw('COUNT(products.id) >= 1')
	->get();
	$paginate=9;
	$products=collect([]);


	$user_country=Session::get('countryvalue');
#$cid=5167;
	$cid=Session::get('cityvalue');
	session(['category'=> $id]);

	$united_states=231;


	if ($id!="undefined") 
	{


		switch ($request->filter_radio) {
			case 'premium':
			$premium=1;

			$products = Product::with('category','user')
			->where('product_category',$id)
			->where('is_active',1)
			->where("products.is_premium",$premium)
			->where("products.give_away",0)
			->where('in_front',1);
			if ($request->search_type=="cities") 
			{
				$products=$products->where('city_id', $cid)
				->where('country_id',$user_country);

			}elseif ($request->search_type=="country") 
			{

				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="united") 
			{

				$products=$products->where('country_id',$united_states);

			}

			$products=$products->orWhere(['share_all'=>1]);
			if ($request->search_type=="cities") 
			{
				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="country") 
			{
				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="united") 
			{
				$products=$products->where('country_id',$united_states);

			}
			$products=$products->where('product_category',$id)
			->where('is_active',1)
			->where("products.is_premium",$premium)
			->where("products.give_away",0)
			->where('in_front',1)
			->orderBy('created_at','DESC')
			->paginate($paginate)
			->appends('filter_radio','premium')
			->appends('search_type',$request->search_type);

		// if there is not products
			if (count($products)==0) 
			{
				$products = Product::with('category','user')
				->where('product_category',$id)
				->where('is_active',1)
				->where("products.is_premium",$premium)
				->where("products.give_away",0)
				->where('in_front',1);
				$products=$products->where('country_id',$united_states);
				$products=$products->orWhere(['share_all'=>1]);
				$products=$products->where('country_id',$united_states);
				$products=$products->where('product_category',$id)
				->where('is_active',1)
				->where("products.is_premium",$premium)
				->where("products.give_away",0)
				->where('in_front',1)
				->orderBy('created_at','DESC')
				->paginate($paginate)
				->appends('filter_radio','premium')
				->appends('search_type',$request->search_type);

			}




			break;
			case 'gift':
			$gift=1;
			$products = Product::with('category','user')
			->where('product_category',$id)
			->where('is_active',1)
			->where("products.give_away",$gift)
			->where('in_front',1);
			if ($request->search_type=="cities") 
			{
				$products=$products->where('city_id', $cid)
				->where('country_id',$user_country);
			}elseif ($request->search_type=="country") 
			{

				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="united") 
			{
				$products=$products->where('country_id',$united_states);

			}

			$products=$products->orWhere(['share_all'=>1]);
			if ($request->search_type=="cities") 
			{
				$products=$products->where('country_id',$user_country);

			}elseif ($request->search_type=="country") 
			{
				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="united") 
			{
				$products=$products->where('country_id',$united_states);

			}
			$products=$products->where('product_category',$id)
			->where('is_active',1)
			->where("products.give_away",$gift)
			->where('in_front',1)
			->orderBy('created_at','DESC')
			->paginate($paginate)
			->appends('filter_radio','gift')
			->appends('search_type',$request->search_type);

			// if there is not products
			if (count($products)==0) 
			{
				$products = Product::with('category','user')
				->where('product_category',$id)
				->where('is_active',1)
				->where("products.give_away",$gift)
				->where('in_front',1);
				$products=$products->where('country_id',$united_states);
				$products=$products->orWhere(['share_all'=>1]);
				$products=$products->where('country_id',$united_states);
				$products=$products->where('product_category',$id)
				->where('is_active',1)
				->where("products.give_away",$gift)
				->where('in_front',1)
				->orderBy('created_at','DESC')
				->paginate($paginate)
				->appends('filter_radio','gift')
				->appends('search_type',$request->search_type);
			}


			break;

			case 'all':

			Session::forget('category');

			$products = Product::with('category','user')
			->where('product_category',$id)
			->where('is_active',1)
			->where('in_front',1);

			if ($request->search_type=="cities") 
			{

				$products=$products->where('city_id', $cid)
				->where('country_id',$user_country);
			}elseif ($request->search_type=="country") 
			{

				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="united") 
			{

				$products=$products->where('country_id',$united_states);

			}
			$products=$products->orWhere(['share_all'=>1])
			->where('product_category',$id)
			->where('is_active',1)
			->where('in_front',1);
			if ($request->search_type=="cities") 
			{
				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="country") 
			{
				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="united") 
			{
				$products=$products->where('country_id',$united_states);

			}
			$products=$products->orderBy('created_at','DESC')
			->paginate($paginate)
			->appends('filter_radio','all')
			->appends('search_type',$request->search_type);


			// if there is not products
			if (count($products)==0) 
			{
				$products = Product::with('category','user')
				->where('product_category',$id)
				->where('is_active',1)
				->where('in_front',1);
				$products=$products->where('country_id',$united_states);
				$products=$products->orWhere(['share_all'=>1])
				->where('product_category',$id)
				->where('is_active',1)
				->where('in_front',1);
				$products=$products->where('country_id',$united_states);
				$products=$products->orderBy('created_at','DESC')
				->paginate($paginate)
				->appends('filter_radio','all')
				->appends('search_type',$request->search_type);

			}

			break;


		}

	}else{

		switch ($request->filter_radio) {
			case 'premium':
			$premium=1;
			$products = Product::with('category','user')
			->where('is_active',1)
			->where("products.is_premium",$premium)
			->where("products.give_away",0)
			->where('in_front',1);
			if ($request->search_type=="cities") 
			{
				$products=$products->where('city_id', $cid)
				->where('country_id',$user_country);
			}elseif ($request->search_type=="country") 
			{
				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="united") 
			{
				$products=$products->where('country_id',$united_states);

			}
			$products=$products->orWhere(['share_all'=>1]);
			if ($request->search_type=="cities") 
			{
				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="country") 
			{
				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="united") 
			{
				$products=$products->where('country_id',$united_states);

			}
			$products=$products->where('is_active',1)
			->where("products.is_premium",$premium)
			->where("products.give_away",0)
			->where('in_front',1)
			->orderBy('created_at','DESC')
			->paginate($paginate)
			->appends('filter_radio','premium')
			->appends('search_type',$request->search_type);

		// if there is not products

			if (count($products)==0) 
			{
				$products = Product::with('category','user')
				->where('is_active',1)
				->where("products.is_premium",$premium)
				->where("products.give_away",0)
				->where('in_front',1);
				$products=$products->where('country_id',$united_states);
				$products=$products->orWhere(['share_all'=>1]);
				$products=$products->where('country_id',$united_states);
				$products=$products->where('is_active',1)
				->where("products.is_premium",$premium)
				->where("products.give_away",0)
				->where('in_front',1)
				->orderBy('created_at','DESC')
				->paginate($paginate)
				->appends('filter_radio','premium')
				->appends('search_type',$request->search_type);
			}


			break;
			case 'gift':
			$products = Product::with('category','user')
			->where('is_active',1)
			->where("give_away",1)
			->where('in_front',1);
			if ($request->search_type=="cities") 
			{
				$products=$products->where('city_id', $cid)
				->where('country_id',$user_country);
			}elseif ($request->search_type=="country") 
			{
				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="united") 
			{
				$products=$products->where('country_id',$united_states);

			}
			$products=$products->orWhere(['share_all'=>1]);
			if ($request->search_type=="cities") 
			{
				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="country") 
			{
				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="united") 
			{
				$products=$products->where('country_id',$united_states);

			}
			$products=$products->where('is_active',1)
			->where("give_away",$gift)
			->where('in_front',1)
			->orderBy('created_at','DESC')
			->paginate($paginate)
			->appends('filter_radio','gift')
			->appends('search_type',$request->search_type);
		// if there is not products

			if (count($products)==0) 
			{

				$products = Product::with('category','user')
				->where('is_active',1)
				->where("give_away",1)
				->where('in_front',1);
				$products=$products->where('country_id',$united_states);
				$products=$products->orWhere(['share_all'=>1]);
				$products=$products->where('country_id',$united_states);
				$products=$products->where('is_active',1)
				->where("give_away",$gift)
				->where('in_front',1)
				->orderBy('created_at','DESC')
				->paginate($paginate)
				->appends('filter_radio','gift')
				->appends('search_type',$request->search_type);
			}


			break;

			case 'all':
			Session::forget('category');
			$products = Product::with('category','user')
			->where('is_active',1)
			->where('in_front',1);
			if ($request->search_type=="cities") 
			{
				$products=$products->where('city_id',$cid)->where('country_id',$user_country);
			}elseif ($request->search_type=="country") 
			{
				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="united") 
			{
				$products=$products->where('country_id',$united_states);

			}
			$products=$products->orWhere(['share_all'=>1]);
			if ($request->search_type=="cities") 
			{
				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="country") 
			{
				$products=$products->where('country_id',$user_country);
			}elseif ($request->search_type=="united") 
			{
				$products=$products->where('country_id',$united_states);

			}
			$products=$products->where('is_active',1)
			->where('in_front',1)
			->orderBy('created_at','DESC')
			->paginate($paginate)
			->appends('filter_radio','all')
			->appends('search_type',$request->search_type);
		// if there is not products


			if (count($products)==0) 
			{
				
				$products = Product::with('category','user')
				->where('is_active',1)
				->where('in_front',1);
				$products=$products->where('country_id',$united_states);
				$products=$products->orWhere(['share_all'=>1]);		
				$products=$products->where('country_id',$united_states);
				$products=$products->where('is_active',1)
				->where('in_front',1)
				->orderBy('created_at','DESC')
				->paginate($paginate)
				->appends('filter_radio','all')
				->appends('search_type',$request->search_type);

			}

			break;


		}

	}




		//$products->setPath(url('/').'/categoryproduct/'.$id);
	return view('site.cityproduct', compact('products'))->with('isLoggedIn', $isLoggedIn)->with('user', $user)->with('cities',$cities);
}


public function getusersproducts(Request $request)
{
	$p = Product::where('user_id',$request->id);
	$product = $p->get();
	$products =$p->paginate(12);
	$user = User::find($request->id);
	return view('site.product.homeuser_product', compact('products','user'));
	
}
}