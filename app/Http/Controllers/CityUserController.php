<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Membership;
use App\Product;
use App\Borrower;
use App\Country;
use App\Cityuser;
use App\State;
use App\City;
use Auth;
use DB;

class CityUserController extends Controller
{
     public function index() {

		$isLoggedIn = Auth::check();
		$user = Auth::user();
		$countries = Country::get();
		$cities = City::join('products','products.city_id','cities.id')->select('cities.id','cities.name')
                        ->groupBy('products.city_id')
                        ->select('cities.id','cities.name')
                        ->havingRaw('COUNT(products.id) >= 5')
                        ->get();

        $usercity = DB::table('city_user')
        				->join('countries','countries.id','city_user.country_id')
        				->join('states','states.id','city_user.state_id')
        				->join('cities','cities.id','city_user.city_id')
        				->select('city_user.*','countries.name as countryname','states.name as statename','cities.name as cityname')
        				->get();
		return view('site.city_user', compact('usercity','cities','countries'))
			->with('isLoggedIn', $isLoggedIn)->with('user', $user);

	}


    public function country_ajax(Request $request)
    {


        if (isset($request->term)) 
        {
                   $listProducts= DB::table('countries')->select('id as id','name as text')->where('name','like','%'.$request->term.'%')->get();

        }else{
                    $listProducts= DB::table('countries')->select('id as id','name as text')->get();

        }

        $list = ['results'=>collect($listProducts)];
        return response()->json($list);
    }

    public function cities(Request $request,$id)
    {

        if ($request->term!="") 
        {

     $cities= City::select('id as id','name as text')->whereHas('state',function($query) use($id){
        $query->where('country_id',$id);
     })->where('name','like','%'.$request->term.'%')->get();


        }else
        {
        $cities= City::select('id as id','name as text')->whereHas('state',function($query) use($id){
        $query->where('country_id',$id);
     })->get();
     
        }
        $none=["id"=>0,"text"=>"none"];
        $cities->push($none);


   

        return response()->json(["results"=>$cities]);
    }

     public function countries(Request $request)
    {

        if ($request->term!="") 
        {
        $countries= DB::table('countries')->select('id as id','name as text')->where('name','like','%'.$request->term.'%')->get();

        }else
        {
        $countries= DB::table('countries')->select('id as id','name as text')->get();
     
        }

        return response()->json(["results"=>$countries]);

    }

    public function userlistdata()
    {
         $columns = array( 
                            0 =>'id', 
                            1 =>'email',
                            2=> 'countryname',
                            3=> 'statename',
                            4=> 'cityname',
                        );
        $totalData = Cityuser::count();    
        $totalFiltered = $totalData; 
        $length = $_REQUEST['length'];
        $start = $_REQUEST['start'];
        $search = $_REQUEST['search']['value'];
        if(empty($search))
        {            
         $usercity = Cityuser::with('country','state','city')
                     ->offset($start)
                     ->limit($length)
                     ->get();
        }
        else { 
           $usercity = Cityuser::with('country','state','city')
                             ->orWhere('email', 'LIKE',"%{$search}%")
                             ->orWhereHas("country",function($query) use($search){
                              $query->where('name','LIKE',"%{$search}%");
                                 })
                              ->orWhereHas("state",function($query) use($search){
                              $query->where('name','LIKE',"%{$search}%");
                                 })
                               ->orWhereHas("city",function($query) use($search){
                              $query->where('name','LIKE',"%{$search}%");
                                 })
                            ->offset($start)
                            ->limit($length)
                            ->get();

            $totalFiltered = Cityuser::with('country','state','city')
                           ->orWhere('email', 'LIKE',"%{$search}%")
                             ->orWhereHas("country",function($query) use($search){
                              $query->where('name','LIKE',"%{$search}%");
                                 })
                              ->orWhereHas("state",function($query) use($search){
                              $query->where('name','LIKE',"%{$search}%");
                                 })
                               ->orWhereHas("city",function($query) use($search){
                              $query->where('name','LIKE',"%{$search}%");
                                 })
                           ->count();
         }
         $data = array();  
        if(!empty($usercity))
        {
            foreach ($usercity as $user_city)
            {
                $nestedData['id'] = $user_city->id;
                $nestedData['email'] = $user_city->email;
                $nestedData['countryname'] = $user_city->country->name;
                $nestedData['statename'] = $user_city->state->name;
                $nestedData['cityname'] =$user_city->city->name;
                $data[] = $nestedData;

            }
        }
        $json_data = array(
                    "draw"            => intval($_REQUEST['draw']),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        return json_encode($json_data); 
        }

}
