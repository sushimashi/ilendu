<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client; // Instalado con composer, ver composer.json
use App\User;
use Redirect;
use App\Transaction;

class SubscriptionController extends Controller
{
    private const QVO_API_URL = 'https://playground.qvo.cl'; // Reemplazar por https://api.qvo.cl en producción
    private const QVO_API_TOKEN ='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21tZXJjZV9pZCI6ImNvbV9ZMjQ0TFVZcU1abjBkV0Q4VVlEYTdRIiwiYXBpX3Rva2VuIjp0cnVlfQ.c5UQr5LVYVQAxEIylUmjJNVDpXt5ahw4tl-u1JJBres'; // Reemplazar por el token de producción cuando quieras pasar a producción

   public function pay(Request $request)
   {

   	//first we check if the user has not created a suscription yet

   	if (empty(auth()->user()->customerId)) 
   	{
   	$qvoCreateCustomerResponse = $this->createQVOCustomer($request->input('name'), $request->input('email'), $request->input('phone'));


    if(isset($qvoCreateCustomerResponse->error)) {
      $errorMessage = $qvoCreateCustomerResponse->error->message;
      return $errorMessage;
    }
   	$user=User::find(auth()->user()->id);
   	$user->customerId=$qvoCreateCustomerResponse->id;
   	$user->save();


   	//we suscribe the card

   	$qvoInitCardInscriptionResponse = $this->initCardInscription($qvoCreateCustomerResponse->id);
   
   	
    $cardInscriptionURL = $qvoInitCardInscriptionResponse->redirect_url;
   

   	}else
   	{
	$qvoInitCardInscriptionResponse = $this->initCardInscription(auth()->user()->customerId);
     	
     $cardInscriptionURL = $qvoInitCardInscriptionResponse->redirect_url;
   	}
   	return Redirect::away($cardInscriptionURL);

   

   }


   public function create()
   {

   	return view('site.product.add_payment');
   }

    public function initCardInscription($qvoCustomerID)
  {
    $guzzleClient = new Client();
    $initCardInscriptionURL = self::QVO_API_URL.'/customers/'.$qvoCustomerID.'/cards/inscriptions';
    $returnURL = url("/subscription/card_inscription_return?qvo_customer_id=".$qvoCustomerID);

    $body = $guzzleClient->request('POST', $initCardInscriptionURL,
      [
      'json' => [
        'return_url' => $returnURL,
      ],
      'headers' => [
        'Authorization' => 'Bearer '.self::QVO_API_TOKEN
      ]
    ])->getBody();

    return json_decode($body);
  }


  function charge($qvoCustomerID,$cardID)
  {
  	$client = new GuzzleHttp\Client();
  	$initCardInscriptionURL = self::QVO_API_URL.'/customers/'.$qvoCustomerID.'/cards/'.$cardID.'/charge';
$body = $client->request('POST',$initCardInscriptionURL, [
  'json' => [
    'amount' => 3000,
    'description' => 'For the watch'
  ],
  'headers' => [
        'Authorization' => 'Bearer '.self::QVO_API_TOKEN

  ]
])->getBody();

return json_decode($body);
  }

   function checkCardInscription($qvoCustomerID, $uid)
  {
    $guzzleClient = new Client();
    $checkCardInscriptionURL = self::QVO_API_URL."/customers/".$qvoCustomerID."/cards/inscriptions/".$uid;

    $body = $guzzleClient->request('GET', $checkCardInscriptionURL, [
      'headers' => [
        'Authorization' => 'Bearer '.self::QVO_API_TOKEN
      ]
    ])->getBody();

    return json_decode($body);
  }

  public function cardInscriptionReturn(Request $request)
  {
    $uid = $request['uid'];
    $qvoCustomerID = $request['qvo_customer_id'];



    $cardInscriptionResponse = $this->checkCardInscription($qvoCustomerID, $uid);
    if($cardInscriptionResponse->status == 'succeeded')
    {


      $success=$this->charge($qvoCustomerID,$cardInscriptionResponse->id);
       if ($success->status == 'successful') 
       {

        	$transaction = new Transaction;
			$transaction->transaction_id = $success->id;
			$transaction->user_id = Auth::user()->id;
			$transaction->price = $chargeresponse->amount;
			$transaction->raw_data = json_encode($chargeresponse);
			$transaction->status = 1;
			$transaction->save();

		}
      
    }
    else {
      $errorMessage = $cardInscriptionResponse->error;
      return $errorMessage;
    }
  }

   public function createQVOCustomer($name, $email, $phone)
  {
    $guzzleClient = new Client();
    $createCustomerURL = self::QVO_API_URL.'/customers';

    $body = $guzzleClient->request('POST',
      $createCustomerURL, [
        'json' => [
          'name' => $name,
          'email' => $email,
          'phone' => $phone
        ],
        'headers' => [
          'Authorization' => 'Bearer '.self::QVO_API_TOKEN
        ],
        'http_errors' => false
      ]
    )->getBody();

    return json_decode($body);
  }


}
