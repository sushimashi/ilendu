<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Membership;
use App\Product;
use App\Borrower;
use App\Country;
use App\State;
use App\City;
use Auth;

class DashboardController extends Controller
{
    public function index() {

		$isLoggedIn = Auth::check();
		$user = Auth::user();
		$users = User::paginate(10);
		$countries = Country::get();
		foreach ($users as $key => $value) {
			if((strtotime($value->expire_date)) < strtotime(date('Y-m-d'))){
				$value->expireDays = 0;
			}elseif($value->expire_date != null){
				$date1=date_create(date('Y-m-d'));
				$date2=date_create(date('Y-m-d',strtotime($value->expire_date)));
				$diff=date_diff($date1,$date2);
				$value->expireDays = $diff->days;
			}else{
				$value->expireDays = 0;
			}
		}
		$membershipfees = Membership::orderBy('id','desc')->first();
		$cities = City::join('products','products.city_id','cities.id')->select('cities.id','cities.name')
                        ->groupBy('products.city_id')
                        ->select('cities.id','cities.name')
                        ->havingRaw('COUNT(products.id) >= 5')
                        ->get();
		return view('site.dashboard', compact('users','membershipfees','cities','countries'))
			->with('isLoggedIn', $isLoggedIn)->with('user', $user);

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$isLoggedIn = Auth::check();
		$user = Auth::user();
		return view('site.membership')->with('isLoggedIn', $isLoggedIn)->with('user', $user);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	
	 * @return \Illuminate\Http\Response * @param  \Illuminate\Http\Request  $request
	 */
	public function store(Request $request) {
		$membership = new Membership;
		$membership->fees = $request->fees;
		$membership->created_by = Auth::id();
        $membership->save();
		return redirect('dashboard');

	}

	/**
	 * Display the specified resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Request $request) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request) {

	}

	public function membership(Request $request){
		$expire_date = date('Y-m-d', strtotime(date('Y-m-d'). '+ '.$request->days.' days'));
		$user = User::find($request->userid);
		$user->expire_date = $expire_date;
		$user->save();
		return $this->index();
	}

	public function userProductDetail($id){
		$isLoggedIn = Auth::check();
		$user = Auth::user();
		$countries = Country::get();
		$cities = City::join('products','products.city_id','cities.id')->select('cities.id','cities.name')
                        ->groupBy('products.city_id')
                        ->select('cities.id','cities.name')
                        ->havingRaw('COUNT(products.id) >= 5')
                        ->get();
		$lendproducts = Product::where('user_id',$id)->get();
		$borrowproducts = Borrower::join('products as p', 'borrowers.product_id', '=', 'p.id')
							->join('users', 'p.user_id', '=', 'users.id')
							->where('borrowers.user_id',$id)
							->select('p.*','users.name as username','users.profile_img')
							->get();
		return view('site.product.user_product', compact('lendproducts','borrowproducts','countries'))->with('isLoggedIn', $isLoggedIn)->with('user', $user)->with('cities', $cities);
	}

	/*public Function updateCategory(){
		$products = Product::all();
		foreach ($products as $key => $value) {
			if($value->product_category == 'Electronics'){
				$status = 3;
			}elseif($value->product_category == 'Books'){
				$status = 1;
			}elseif($value->product_category == 'Clothes'){
				$status = 2;
			}elseif($value->product_category == 'Others'){
				$status = 7;
			}
			$product = Product::find($value->id);
			$product->product_category = $status;
			$product->save();


			$product = Product::find($value->id);
			$product->is_active = 1;
			$product->save();

		}

		echo "Updated Successfully";
	}*/
}
