<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Category;

class CategoryController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct() {
      
        /*
        $this->middleware("permission:garantias_ver");
        $this->middleware("permission:garantias_crear")->only("create", "store");
        $this->middleware("permission:garantias_editar")->only("edit", "update");
        $this->middleware("permission:garantias_eliminar")->only("destroy");
       */
        View::share('titulo', "Categorias");
    }

    public function index(Request $request)
    {
        if ($request->ajax()) 
        {
            $categories = Category::all();
            return response()->json($categories);
        }

        return view('admin.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
             'name_spanish' => 'required | unique:categories',
            'name' => 'required | unique:categories',
            'price'=>'required'
        ]);

        $save = Category::create([
            "name_spanish" => strtolower($request->name_spanish),
            "name" => strtolower($request->name),
            "price"=>$request->required 
        ]);

        if ($save == true){
            return response()->json(["success" => true]);
        } else{
            return response()->json(["success" => false]);
        }
    }

    //Habilitar / Deshabilitar Categoria
    public function enableCategory($id)
    {
        $category = Category::find($id);

        if($category->is_active === 1){
            $category->update([ 'is_active' => 0 ]);
            return response()->json([
                'message' => 'Categoria deshabilitada!'
            ]);
        }else{
            $category->update([ 'is_active' => 1 ]);
            return response()->json([
                'message' => 'Categoria habilitada!'
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('admin.categories.edit',compact('category'));
    }

    // actualizar categoria
    public function updateCategory(Request $request, $id)
    {
         $this->validate($request, [
            'name' => 'required',
            'name_spanish' => 'required',
            'price'=>'required'
        ]);

         $category = Category::find($id);
         $category->name = $request->name;
         $category->name_spanish = $request->name_spanish;
         $category->price=$request->price;
         $update = $category->save();

         if ($update) 
        {
            return response()->json(["success" => true]);
        }else{
            return response()->json(["success" => false]);

        }
    }    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroy($id);

       return response()->json([
            "success" => true,
            "message" => "Categoria Eliminada!"
        ]);

    }
}
