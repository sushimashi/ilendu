<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductScrap;
use App\Category;
use App\Community;
use App\Product_Community;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\Country;


class ProductScrapController extends Controller
{   

    public function __construct()
    {
        View::share('titulo', "Productos");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function borrowedProductsDataTable(Request $request)
    {
       $products = Product::whereHas('city',function($q){

          $q->where('country_id',44);
        })->where('borrowed',1)->get();
        #return response()->json($request->all(),404);

         if(isset($request->city_id) && $request->city_id!='')
        {


        $products=Product::where('city_id',$request->city_id)->where('borrowed',1)->get();

     

        return response()->json($products);

        }else{
         return response()->json($products);

        }
    }

    public function index()
    {   

        $countries=Country::all();
        $count_products = Product::whereHas('city',function($q){

          $q->where('country_id',44);
        })->count();

        $count_total_products=count(Product::all());


        return view('admin.products.index',compact('countries','count_products','count_total_products'));
    }

    // obtener categorias
    public function productCategories()
    {
        $categories = Category::all(['id', 'name_spanish']);
        return response()->json($categories);
    }

    // index datatable
    public function DataTable(Request $request)
    {

         $products = ProductScrap::all();
        #return response()->json($request->all(),404);

       
         return response()->json($products);

        
    }

    // tabla de productos premium
    public function premiumProductsDataTable(Request $request)
    {

          $products = Product::whereHas('city',function($q){

          $q->where('country_id',44);
        })->where('is_premium', 1)->get();
        #return response()->json($request->all(),404);

         if(isset($request->city_id) && $request->city_id!='')
        {


        $products=Product::where('city_id',$request->city_id)->where('is_premium', 1)->get();

     

        return response()->json($products);

        }else{
         return response()->json($products);

        }
    }

    // vista de productos premium
    public function premiumProducts()
    {
                $countries=Country::all();
        $count_products = Product::whereHas('city',function($q){

          $q->where('country_id',44);
        })->count();

        $count_total_products=count(Product::all());


        return view('admin.products.premiumProducts',compact('countries','count_products','count_total_products'));
    }

    // tabla de productos en exhibicion
    public function displayedProductsDataTable(Request $request)
    {

          $products = Product::whereHas('city',function($q){

          $q->where('country_id',44);
        })->where('in_front', 1)->get();
        #return response()->json($request->all(),404);

         if(isset($request->city_id) && $request->city_id!='')
        {


        $products=Product::where('city_id',$request->city_id)->where('in_front', 1)->get();

     

        return response()->json($products);

        }else{
         return response()->json($products);

        }
    }

    // vista de productos en exhibicion
    public function displayedProducts()
    {
        $countries=Country::all();
         $count_products = Product::whereHas('city',function($q){

          $q->where('country_id',44);
        })->count();

                $count_total_products=count(Product::all());

        return view('admin.products.displayedProducts',compact('countries','count_products','count_total_products'));
    }

    public function borrowedProducts()
    {

                $countries=Country::all();
 $count_products = Product::whereHas('city',function($q){

          $q->where('country_id',44);
        })->count();

                $count_total_products=count(Product::all());

              return view('admin.products.borrowedProducts',compact('countries','count_products','count_total_products'));
  
    }

    // tabla de productos sin exhibir
    public function undisplayedProductsDataTable(Request $request)
    {

         $products = Product::whereHas('city',function($q){

          $q->where('country_id',44);
        })->where('in_front', 0)->get();
        #return response()->json($request->all(),404);

         if(isset($request->city_id) && $request->city_id!='')
        {


        $products=Product::where('city_id',$request->city_id)->where('in_front', 0)->get();

     

        return response()->json($products);

        }else{
         return response()->json($products);

        }
    }

    // vista de productos sin exhibir
    public function undisplayedProducts()
    {

        $countries=Country::all();
 $count_products = Product::whereHas('city',function($q){

          $q->where('country_id',44);
        })->count();

                $count_total_products=count(Product::all());

        return view('admin.products.undisplayedProducts',compact('countries','count_products','count_total_products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    // exhibir producto
    public function displayProduct($product_id)
    {
        $product = Product::find($product_id);

        if ( $product->in_front === 0 ){
            
            $product->update([ 'in_front' => 1 ]);
            return response()->json([
                'message' => 'Producto en exhibicion!'
            ]);

        } else {

            $product->update([ 'in_front' => 0 ]);
           return response()->json([
               'message' => 'Producto removido de exhibicion!'
           ]);

        }  

    }

    // añadir/remover categoria "Premium
    public function premiumCategory($product_id)
    {
        $product = Product::find($product_id);

        $product->is_premium === 1 ?
        $product->update([ 'is_premium' => 0 ]):
        $product->update([ 'is_premium' => 1 ]);

        return response()->json([
            'message' => 'Categoria actualizada!'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "product_name" => 'required|unique:products',
            "product_description" => 'required',
            "product_category" => 'required',
            'is_premium' => 'required_if:give_away,1',
            'return_term'=>'required'
        ]);

        $urlImg;
        if ($request->hasFile('productImage')) {

            $image = $request->file('productImage');
            $name = $image->getClientOriginalName();
            $destinationThumbPath = public_path('/img/thumbnail');
            compress_img($image->getPathname(),$destinationThumbPath.'/'.$name,0.3);
            $destinationPath = public_path('/img');
            $image->move($destinationPath, $name);
            $urlImg = $name;

        }else{

            $createProductError =  json_encode([
                "errors" => [
                    "productImgIsRequired" => ['Debe agregar una imagen del producto.']
                ]
            ]);
            return response($createProductError, 422);
        }

        $category = Category::find($request->product_category);
        if(! isset($category) ){
            $category = Category::create([
                'name_spanish' => strtolower($request->product_category),
                'name' => translate(strtolower($request->product_category)),
                'is_active' => 1,
            ]);
        }

        $product = Product::create([
            "user_id" => $request->user()->id,
            "product_name" => $request->product_name,
            "product_desc" => $request->product_description,
            "product_img" => $urlImg,
            "product_category" => isset($category) ? $category->id : $request->product_category,
            "is_premium" => $request->has('is_premium') ? 1 : 0,
            "is_active" => 1,
            "in_front" => 0,
            'give_away'=>$request->has('give_away') ? 1 : 0,
            "return_term"=>$request->return_term,
            "city_id" =>  $request->user()->city_id
        ]);

        if (count($request->coms)>0) 
        {
         foreach ($request->coms as $key => $value) 
         {
            Product_Community::create([
                            'community_id' => $value,
                            'product_id' =>$product->id
                        ]);
         }
        }

        if ( $product ) {
            return response()->json([
                'state' => 'Exito!',
                'message' => 'Producto creado exitosamente!',
                'type' => 'success',
            ]);
        } else{
            return response()->json([
                'state' => 'Error',
                'message' => 'Ha ocurrido un error...',
                'type' => 'error',
            ]);            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        $communities=$product->communities()->get();

        return view('admin.products.edit', compact('product','communities'));
    }

    public function updateProduct(Request $request, $id)
    {

        $this->validate($request,[
            "product_name" => 'required',
            "product_description" => 'required',
            "product_category" => 'required',
            'is_premium' => 'required_if:give_away,1',
            'return_term'=>'required'
        ]);
         $product = Product::find($id);
         $urlImg;
         $productImg = $request->file('productImage');

         if ( isset($productImg) ){

            $name = $productImg->getClientOriginalName();
            $destinationThumbPath = public_path('/img/thumbnail');
            compress_img($productImg->getPathname(),$destinationThumbPath.'/'.$name,0.3);
            $destinationPath = public_path('/img');
            $productImg->move($destinationPath, $name);
            $urlImg = $name;

         }else{
            $urlImg = $product->product_img;
         }

         $product->update([
            "product_name" => $request->product_name,
            "product_desc" => $request->product_description,
            "product_img" => $urlImg,
            "product_category" => $request->product_category,
            "is_premium" => $request->has('is_premium') ? 1 : 0,
            "is_active" => 1,
            "in_front" => 1,
            "give_away"=>$request->has('give_away') ? 1 : 0,
            "return_term"=>$request->return_term
        ]);

             if (count($request->coms)>0) 
        {

          Product_Community::where([
                            'product_id' =>$product->id
                        ])->delete();

         foreach ($request->coms as $key => $value) 
         {
            

            Product_Community::create([
                            'community_id' => $value,
                            'product_id' =>$product->id
                        ]);
         }
        }

       if ( $product ) {
           return response()->json([
               'state' => 'Exito!',
               'message' => 'Producto actualizado exitosamente!',
               'type' => 'success',
           ]);
       } else{
           return response()->json([
               'state' => 'Error',
               'message' => 'Ha ocurrido un error...',
               'type' => 'error',
           ]);            
       }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();

        return response()->json([
           'message' => 'Producto eliminado!'
       ]);
    }

     public function delete_multi(Request $request)
    {   
        $p_ids = $request->input('chk');
        if ( empty($p_ids) == 1 || count($p_ids) <= 0 )
        {
            if($request->ajax())
            {
                $arr                        = array();
                $arr['status']          = 500;          
                $arr['message']     = 'No Checkbox selected!';
                
                echo json_encode($arr);
                return;
            }
            else
            {
                $request->session()->flash('success', 'No se han seleccionado Checkbox !');
                redirect($_SERVER["HTTP_REFERER"]);
            }   
        }       
        else
        {   
            foreach($p_ids as $id)
            {
                 $p = Product::find($id);
                 $p->delete();
            }
            
            if($request->ajax())
            {
                $arr                        = array();
                $arr['status']          = 200;          
                $arr['message']     = 'Productos Eliminados exitosamente!!';
                
                echo json_encode($arr);
                return;
            }
            else
            {
                $request->session()->flash('success', 'Productos Eliminados!');
                redirect($_SERVER["HTTP_REFERER"]);
            }   
        }
    }
}
