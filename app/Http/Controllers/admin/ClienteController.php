<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cliente;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;


class ClienteController extends Controller
{
    public function __construct()
    {
        // $this->middleware("permission:clientes_ver");
        // $this->middleware("permission:clientes_crear")->only("create", "store");
        // //$this->middleware("permission:clientes_editar")->only("edit", "update");
        // $this->middleware("permission:clientes_eliminar")->only("destroy");
        View::share('titulo', "Usuarios");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $clientes = Cliente::orderBy('id', 'DESC');
            $buscar = $request->buscar;
            $length = $request->length;
            if ($buscar != "") {

                $clientes = $clientes->where('nombre', 'like', '%' . $buscar . '%')
                    ->orwhere('email', 'like', '%' . $buscar . '%')
                    ->orwhere('telefono', 'like', '%' . $buscar . '%')
                    ->orwhere('celular', 'like', '%' . $buscar . '%');
            }

            $clientes = $clientes->paginate($length);
           return view('admin.clientes.tabla', compact('clientes'));
        } else {
            $clientes = Cliente::orderBy('id', 'DESC')->paginate(10);
            return view('admin.clientes.index', compact('clientes'));
        }
    }


    public function getDireccion(Request $request)
    {
        $cliente=Cliente::find($request->cliente_id);

        if ($cliente!=null) 
        {
         switch ($request->tipo) {
            case 'fiscal':
                $direccion=$cliente->direccion_fiscal;
                break;
            
            case 'local':
                $direccion=$cliente->direccion;
                break;

            default:
                       $direccion="";

        break;
        }
        return response()->json(["success"=>true,"direccion"=>$direccion]);

        }else
        {
            return response()->json(["success"=>false,'error'=>"seleccione un cliente primero"]);
        
        }
        



    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */



         
    public function store(Request $request)
    {
           $this->validate($request, [
            'nombre' => 'required',
            'cedula' => 'required',
            'email' => 'required|email',
            'telefono' => 'required',
        ]);


            $fecha_nacimiento =  $request->fecha;
   //guardamos el usuario
            $c=Cliente::create(['nombre'=>$request->nombre,'fecha'=>$fecha_nacimiento,'cedula'=>$request->cedula,'email'=>$request->email,'direccion'=>$request->direccion,'telefono'=>$request->telefono,'celular'=>$request->celular,'nombre_contacto_referente'=>$request->nombre_contacto_referente,'numero_contrato'=>$request->numero_contrato,'email_gerencia'=>$request->email_gerencia,'email_contabilidad'=>$request->email_contabilidad,'email_otro'=>$request->email_otro,'comentario'=>$request->comentario]);

            if ($c) 
            {
            flash("El cliente <b>$request->nombres</b> fue creado con éxito")->success();
            return redirect('admin/clientes');
            }else{
            flash("Ha ocurrido un error")->success();

            return redirect('admin/clientes');

            }
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = Cliente::findOrFail($id);
        return view("admin.clientes.show",compact("cliente"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::findOrFail($id);
        $user = Auth::user();
        if(!$user->permiso($cliente->user_id,"clientes_editar")){
            abort(403);
        }

        return view('admin.clientes.edit', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

          $this->validate($request, [
            'nombre' => 'required',
            'cedula' => 'required',
            'email' => 'required|email',
            'telefono' => 'required',
        ]);

        $cliente = Cliente::findOrFail($id);
        $user = Auth::user();
        if(!$user->permiso($cliente->user_id,"clientes_editar")){
            abort(403);
        }
 $this->validate($request, [
            'nombre' => 'required',
            'fecha' => 'required',
        ]);

            $fecha_nacimiento =$request->fecha;
   //guardamos el usuario
            $c=Cliente::where('id',$id)->update(['nombre'=>$request->nombre,'fecha'=>$fecha_nacimiento,'cedula'=>$request->cedula,'email'=>$request->email,'direccion'=>$request->direccion,'telefono'=>$request->telefono,'celular'=>$request->celular,'nombre_contacto_referente'=>$request->nombre_contacto_referente,'numero_contrato'=>$request->numero_contrato,'email_gerencia'=>$request->email_gerencia,'email_contabilidad'=>$request->email_contabilidad,'email_otro'=>$request->email_otro,'comentario'=>$request->comentario]);

            if ($c) 
            {
           
                flash("El cliente <b>$request->nombres</b> fue actualizado con éxito")->success();
                return redirect('admin/clientes');
            }else{
            flash("Ha ocurrido un error")->success();

            return redirect('admin/clientes');

            }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cliente = Cliente::findOrFail($id);
        try {
            $result = $cliente->delete();
            if ($result == 1) {
                return response()->json([
                    'success' => true,
                    'mensaje' => "Se ha eliminado correctamente",
                    'cerrar_sesion' => false
                ]);
            } else {
                return response()->json([
                    'error' => "Error al eliminar el cliente"
                ]);
            }
        } catch (\Exception $e) {
            $cliente->estado = 0;
            $cliente->save();
            return response()->json([
                'success' => true,
                'mensaje' => "Se ha deshabilitado el cliente correctamente",
                'cerrar_sesion' => false
            ]);
        }
    }
}
