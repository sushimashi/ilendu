<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Services\DataTable;
use Yajra\Datatables\Datatables;
use App\Borrower;
use Illuminate\Support\Facades\View;
use Mail;


class BorrowController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct() {
      
        /*
        $this->middleware("permission:garantias_ver");
        $this->middleware("permission:garantias_crear")->only("create", "store");
        $this->middleware("permission:garantias_editar")->only("edit", "update");
        $this->middleware("permission:garantias_eliminar")->only("destroy");
       */
        View::share('titulo', "Solicitudes");
    }


    public function search($status,Request $request)
    {
 if ($request->ajax()) 
        {

            //condicion en borrow is_admin

           return $borrow = Borrower::select('products.id as product_id','countries.name as pais','borrowers.id','products.product_name as producto','users.first_name as usuario','borrowers.message as mensaje','borrowers.status','borrowers.created_at as fecha')
           ->where('status',$status)
           ->join('products','products.id','borrowers.product_id')
           ->join('users','users.id','borrowers.user_id')
           ->join('countries','countries.id','users.country_id')
           ->get();

        
            

            return response()->json($borrow);
        }

    }


    public function searchCountry(Request $request)
    {
 if ($request->ajax()) 
        {


           return $borrow = Borrower::select('products.id as product_id','countries.name as pais','borrowers.id','products.product_name as producto','users.first_name as usuario','borrowers.message as mensaje','borrowers.status','borrowers.created_at as fecha')->whereHas('product',function($q){
                $q->where('user_id',1);
            })->where('status',$request->status)
           ->where('countries.id',$request->id)
           ->join('products','products.id','borrowers.product_id')
           ->join('users','users.id','borrowers.user_id')
           ->join('countries','countries.id','users.country_id')
           ->get();

        
            

            return response()->json($borrow);
        }

    }




    public function index(Request $request)
    {
        if ($request->ajax()) 
        {


           return $borrow = Borrower::select('products.id as product_id','borrowers.id','countries.name as pais','products.product_name as producto','users.first_name as usuario','borrowers.message as mensaje','borrowers.status','borrowers.created_at as fecha')->where('status','Sent')
           ->join('products','products.id','borrowers.product_id')
           ->join('users','users.id','borrowers.user_id')
           ->join('countries','countries.id','users.country_id')
           ->get();

        
            

            return response()->json($borrow);
        }

        return view('admin.borrows.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
    }

    //Enviar 
    public function status($id, Request $request)
    {
        $borrow = Borrower::find($id);


        switch ($request->case) {
            case 'Sent':
                 $borrow->update([ 'status'  => 'Sent' ]);

                //send the mail
                 $data = array('toEmail' => $borrow->user->email,
                'toName' => $borrow->user->first_name,
                'borrow_id'=>$borrow->id,
                'product_name' => $borrow->product->product_name,
                'product_img' => $borrow->product->product_img,
                'first_name' => $borrow->user->first_name,
                'last_name' => $borrow->user->last_name,
                'product_id'=>$borrow->product->id,
                'email' => $borrow->product->user->email);

                Mail::send('emails.sent_email', $data, function ($message) use ($data) {
                    $message->to($data['toEmail'], $data['toName']);
                    $message->from('noreply@ilendu.com','Ilendu');
                    $message->subject('Sent Product');
                });


                //ilendu mail

                 Mail::send('emails.sent_admin_email', $data, function ($message) use ($data) {
            $message->to('admin@ilendu.com','Ilendu');
            $message->from('noreply@ilendu.com','Ilendu');
            $message->subject('Sent Product');
        });


                  return response()->json([
                'success' => true,
                'message' => 'Product Sent!'
                 ]);
                break;


                case 'Accept':
                 $borrow->update(['status'=>'Accept']);

                //send the mail
                 $data = array('toEmail' => $borrow->user->email,
                'toName' => $borrow->user->name,
                'product_name' => $borrow->product->product_name,
                'product_img' => $borrow->product->product_img,
                'first_name' => $borrow->user->first_name,
                'last_name' => $borrow->user->last_name,
                'product_id'=>$borrow->product->id,
                'email' => $borrow->user->email);



                Mail::send('emails.accept_email', $data, function ($message) use ($data) {
                    $message->to($data['email'], $data['toName']);
                    $message->from('noreply@ilendu.com','Ilendu');
                    $message->subject('Accept Your Request');
                });



                  return response()->json([
                'success' => true,
                'message' => 'Producto Aceptado!'
                 ]);
                break;

                case 'Received':
                 $borrow->update(['status'=>'Received']);

                //send the mail
                 $data = array('toEmail' => $borrow->user->email,
                'toName' => $borrow->user->name,
                'product_name' => $borrow->product->product_name,
                'product_img' => $borrow->product->product_img,
                'first_name' => $borrow->user->first_name,
                'last_name' => $borrow->user->last_name,
                'product_id'=>$borrow->product->id,
                'email' => $borrow->user->email);



                Mail::send('emails.received_email', $data, function ($message) use ($data) {
                    $message->to($data['email'], $data['toName']);
                    $message->from('noreply@ilendu.com','Ilendu');
                    $message->subject('Received Product');
                });    

                //admin
                 $data = array('toEmail' => $borrow->product->user->email,
                'toName' => $borrow->product->user->name,
                'product_name' => $borrow->product->product_name,
                'product_img' => $borrow->product->product_img,
                'first_name' => $borrow->product->user->first_name,
                'last_name' => $borrow->product->user->last_name,
                'product_id'=>$borrow->product->id,
                'email' => $borrow->product->user->email);

                  Mail::send('emails.received_admin_email', $data, function ($message) use ($data) {
                    $message->to($data['email'], $data['toName']);
                    $message->from('noreply@ilendu.com','Ilendu');
                    $message->subject('Received Product');
                });



                  return response()->json([
                'success' => true,
                'message' => 'Product received!'
                 ]);
                break;


            
         
        }


       

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    // actualizar categoria
    public function updateCategory(Request $request, $id)
    {
       
    }    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $borrow = Borrower::find($id);
        $borrow->update([ 'status' => 'Reject' ]);

       return response()->json([
            "success" => true,
            "message" => "peticion rechazada!"
        ]);

    }
}
