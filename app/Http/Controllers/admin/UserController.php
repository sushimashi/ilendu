<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\User;
use App\Country;
use App\State;
use App\City;
use App\DataTables\UserDatatable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\Role;

class UserController extends Controller
{

    public function __construct()
    {
        View::share('titulo', "Usuarios");
    }

    public function addRole(Request $request,$user)
    {

        $user=User::find($user);
        $role=Role::find(1);
        if (isset($request->role)) 
        {
         $user->roles()->detach($role);
        return response()->json(["message"=>"Rol eliminado con exito","success"=>true]);

        }else{
            $user->roles()->attach($role);
            return response()->json(["message"=>"Rol agregado con exito","success"=>true]);

        }






    }

    public function Datatable(Request $request)
    {


           $users = User::select('*');

           if (isset($_GET['city_id']) && $_GET['city_id']!="") 
           {
           $users =$users->where('city_id',$_GET['city_id']);

           }
            if (isset($_GET['country_id']) && $_GET['country_id']!="") 
           {
           $users =$users->where('country_id',$_GET['country_id']);

           }
           $users =$users->with(['country','city'])->get();

       
      
        return response()->json($users);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $countries = Country::all(['id', 'name']);
        return view('admin.users.index', [
            'countries' => $countries,
            'titulo'=>'usuarios'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required | unique:users',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required'
        ]);

        // verificar si se añadio imagen de usuario
        $url = null;
        if($request->hasFile('userImage')){

            $image = $request->file('userImage');
            $destinationPath = public_path('/img/profile');
            $image->move($destinationPath, $image->getClientOriginalName());
            $url = $image->getClientOriginalName();

        }/*else{

            return response()->json([
                "errors" => [
                    "userImgIsRequired" => ["Debe agregar una imagen."]
                ]
            ], 422);
        }*/

        // crear ciudad si no existe
      /*  $city = null;
        if( 
            City::where('name', $request->city)
                ->where('state_id', $request->state)
                ->where('country_id', $request->country)
                ->first() == null  
        ){
            $city = City::create([
                'name' => strtolower($request->city),
                'state_id' => $request->state,
                'country_id'=> $request->country,
                'email' => $request->email,
                'status' => 0
            ]);
        }*/

        // crear usuario
        $user = User::create([
            'first_name' => $request->firstName,
            'last_name' => $request->lastName,
            'email' => $request->email,
            'password' => bcrypt('1234'),
            'profile_img' => $url,
            'country_id' => $request->country,
            'state_id' => $request->state,
            // 'city_id' => isset($city)? $city->id : $request->city
            'city_id' => $request->city
        ]);

        if ( $user ) {
            return response()->json([
                'state' => 'Exito!',
                'message' => 'Usuario creado exitosamente!',
                'type' => 'success',
            ]);
        } else{
            return response()->json([
                'state' => 'Error',
                'message' => 'Ha ocurrido un error...',
                'type' => 'error',
            ]);            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $country = Country::where('id', $user->country_id)->get(['name'])->first();
        $state = State::where('id', $user->state_id)->get(['name'])->first();
        $city = City::where('id', $user->city_id)->get(['name'])->first();
        return view('admin.users.show', compact('user', 'country', 'state', 'city'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $countries = Country::all(['id', 'name']);
        $states = State::where('country_id', $user->country_id)->get(['id', 'name']);
        $cities = City::where('state_id', $user->state_id)->get(['id', 'name']);
        return view('admin.users.edit', compact(
            'user',
            'countries',
            'cities',
            'states'
        ));
    }

    public function updateUser(Request $request, $id)
    {
        $request->validate([
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required | email',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required'
        ]);

         $user = User::find($id);
         $url;
         $userImg = $request->file('userImage');

         if ( isset($userImg) ){

            $destinationPath = public_path('/img/profile');
            $userImg->move($destinationPath, $userImg->getClientOriginalName());
            $url = $userImg->getClientOriginalName();

         }else{
            $url = $user->profile_img;
         }

         // crear ciudad si no existe
         // $city = null;
        // if( 
        //     City::where('name', $request->city)
        //         ->where('state_id', $request->state)
        //         ->where('country_id', $request->country)
        //         ->first() == null  
        // ){
        //      $city = City::create([
        //          'name' => $request->city,
        //          'state_id' => $request->state,
        //          'country_id'=> $request->country,
        //          'email' => $request->userEmail,
        //          'status' => 0
        //      ]);
        //  }

         // $city_id = isset($city)? $city->id : $request->city;
         
         if( $user->city_id !=  $city_id){
             foreach ($user->products as $product) {
                 $product->update(['city_id' => $city_id]);
             }
             session(['cityvalue'=> $request->cityUser]);
         }

         $user->update([
            'first_name' => $request->firstName,
            'last_name' => $request->lastName,
            'email' => $request->email,
            'profile_img' => $url,
            'country_id' => $request->country,
            'state_id' => $request->state,
            // 'city_id' => $city_id
            'city_id' => $request->city
        ]);

        if ( $user ) {
            return response()->json([
                'state' => 'Exito!',
                'message' => 'Usuario Actualizado!',
                'type' => 'success'
            ]);
        } else{
            return response()->json([
                'state' => 'Error',
                'message' => 'Ha ocurrido un error...',
                'type' => 'error'
            ]);            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         User::find($id)->delete();

         return response()->json([
            'message' => 'Usuario eliminado!'
        ]);
    }
}
