<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Membership;


class FeesController extends Controller
{

     public function __construct() {
      
        /*
        $this->middleware("permission:garantias_ver");
        $this->middleware("permission:garantias_crear")->only("create", "store");
        $this->middleware("permission:garantias_editar")->only("edit", "update");
        $this->middleware("permission:garantias_eliminar")->only("destroy");
       */
        View::share('titulo', "Membresias");
    }


   public function index(Request $request)
    {
        if ($request->ajax()) 
        {
            $memberships = Membership::all();
            return response()->json($memberships);
        }

        $plans = ["monthly","semiannual","annual"];
        return view('admin.fees.index', compact('plans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'fees' => 'required',
            'fees_plan'=>'required'
        ]);

        $save = Membership::create([
            "fees" => $request->fees,
            "fees_plan" => $request->fees_plan,
            "created_by" => auth()->user()->id
        ]);

        if ($save==true) 
        {
            return response()->json(["success" => true]);
        
        }else
        {
            return response()->json(["success" => false]);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $membership = Membership::find($id);
        $plans=["monthly","semiannual","annual"];
        return view('admin.fees.edit',compact('membership','plans'));
    }

    // actualizar plan de tarifa
    public function updateRates(Request $request, $id)
    {
         $this->validate($request, [
             'fees' => 'required',
             'fees_plan'=>'required'
        ]);

         $membership = Membership::find($id);
         $membership->fees = $request->fees;
         $membership->fees_plan = $request->fees_plan;
         $update = $membership->save();

         if ($update) 
        {
            return response()->json(["success" => true]);
        }else{
            return response()->json(["success" => false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Membership::destroy($id);

       return response()->json([
            "success" => true,
            'message' => 'Plan de Tarifa Eliminado!'
        ]);

    }

}
