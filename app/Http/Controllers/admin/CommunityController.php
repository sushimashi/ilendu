<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use App\Community;
use App\City;
use App\Country;

class CommunityController extends Controller
{

    public function __construct()
    {
        View::share('titulo', "Comunidades");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

      $countries=Country::all();


        return view('admin.communities.index',compact('countries','states','cities'));
    }

    // DataTable
    public function DataTable(Request $request)
    {
        $communities = Community::whereHas('city',function($q){

          $q->where('country_id',44);
        })->get();
        #return response()->json($request->all(),404);
foreach ($communities as $key => &$value) 
        {
          $value->cant_users=count($value->communityUsers);
          $value->cant_prods=count($value->products);

        }
         if(isset($request->city_id) && $request->city_id!='')
        {


        $communities=Community::where('city_id',$request->city_id)->get();

        foreach ($communities as $key => &$value) 
        {
          $value->cant_users=count($value->communityUsers);
          $value->cant_prods=count($value->products);

        }

        return response()->json($communities);

        }else{
                  return response()->json($communities);

        }
    }

    // Comunidades Inactivas
    public function inactiveCommunities()
    {
        return view('admin.communities.inactiveCommunities');
    }

    // DataTable comunidades inactivas
    public function inactiveCommunitiesDataTable()
    {
        $communities = Community::where('status', 0)->get();
        foreach ($communities as $key => &$value) 
        {
          $value->cant_users=count($value->communityUsers);
          $value->cant_prods=count($value->products);

        }
        return response()->json($communities);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    // habilitar / deshabilitar comunidad
    public function enableCommunity($community_id)
    {
        $community = Community::find($community_id);

        if ( $community->status === 0 ){
            
            $community->update([ 'status' => 1 ]);
            return response()->json([
                'message' => 'Comunidad Habilitada!'
            ]);

        } else {

            $community->update([ 'status' => 0 ]);
           return response()->json([
               'message' => 'Comunidad Deshabilitada'
           ]);

        }  
    }

    // Usuarios por Comunidad
    public function communityUsers($community_id)
    {
        $community = Community::find($community_id);
        return response()->json($community->communityUsers);
    }

        // Productos por Comunidad
    public function communityProducts(Request $request,$community_id)
    {
        $community = Community::find($community_id);


        if (isset($request->estado) && $request->estado!="") 
        {
        
          switch ($request->estado) {
            case 'all':
                    return response()->json($community->products);

              break;
          case 'prestados':
         $data=Community::whereHas('products',function($q){
          $q->where('borrowed',1);

        })->where('id',$community_id)->first()->products;

            return response()->json($data);

            break;

        case 'disponibles':
         $data=Community::whereHas('products',function($q){
          $q->where('borrowed',0)
          ->where('is_active',1);

        })->where('id',$community_id)->first()->products;

            return response()->json($data);

            break;

          default:
                    return response()->json($community->products);

         break;
            
          }

        }else{

            return response()->json($community->products);

        }

    }


    // Remover usuario de Comunidad
    public function removeUserOfCommunity( $community_id ,$user_id)
    {
        Community::find($community_id)->communityUsers()->detach($user_id);
        return response()->json([
            'message' => 'Usuario removido de comunidad!'
        ]);
    }

     // Remover usuario de Comunidad
    public function removeProductOfCommunity( $community_id ,$product_id)
    {
        Community::find($community_id)->products()->detach($product_id);
        return response()->json([
            'message' => 'Producto removido de comunidad!'
        ]);
    }

    // aprobar / desaprobar usuario en comunidad
    public function approveUser($community_id ,$user_id)
     {
        $sql = 'SELECT `is_approve` FROM community_user
                WHERE community_id =  :community_id AND user_id = :user_id';

        $is_approve = DB::select($sql, [
            'community_id' => $community_id,
            'user_id' => $user_id
        ]); 

         if ( $is_approve[0]->is_approve === 0 ){
            
            Community::find($community_id)->communityUsers()->updateExistingPivot($user_id, [
                'is_approve' => 1], false);

            return response()->json([
                'message' => 'Usuario aprobado!'
            ]);

        } else {

            Community::find($community_id)->communityUsers()->updateExistingPivot($user_id, [
                'is_approve' => 0], false);

            return response()->json([
                'message' => 'Usuario removido!'
            ]);

        }  
     } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:communities',
            'description' => 'required',
            'public'=>'required'
        ]);

        $urlImg;
        if ($request->hasFile('communityImage')) {

            $com_image = $request->file('communityImage');
            $name = 'kyc'.rand(5,123).'.'.$com_image->getClientOriginalExtension();
            $com_image->move(public_path().'/community_image/', $name);
            $urlImg = $name;

        }/*else{

            return response()->json([
                "errors" => [
                    "communityImgIsRequired" => ["Debe agregar una imagen."]
                ]
            ], 422);
        }*/

        $community = Community::create([
            "name" => $request->name,
            "description" => $request->description,
            "com_image" => $urlImg,
            "status" => $request->has('status') ? 1 : 0,
            "created_by" => $request->user()->id,
            "public"=>$request->public,
            "city_id"=>auth()->user()->city_id
        ]);

        if ( $community ) {
            return response()->json([
                'state' => 'Exito!',
                'message' => 'Comunidad creada exitosamente!',
                'type' => 'success',
            ]);
        } else{
            return response()->json([
                'state' => 'Error',
                'message' => 'Ha ocurrido un error...',
                'type' => 'error',
            ]);            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $community = Community::find($id);
        return view('admin.communities.edit', compact('community'));
    }

    // actualizar comunidad
    public function updateCommunity(Request $request, $community_id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'public'=>'required'
        ]);

        $community = Community::find($community_id);

        $urlImg;
        if ($request->hasFile('communityImage')) {

          $com_image = $request->file('communityImage');
          $name = 'kyc'.rand(5,123).'.'.$com_image->getClientOriginalExtension();
          $com_image->move(public_path().'/community_image/', $name);
          $urlImg = $name;

        }else{

            $urlImg = $community->com_image;       
        }

        $community->update([
            "name" => $request->name,
            "description" => $request->description,
            "com_image" => $urlImg,
            "status" => $request->has('status') ? 1 : 0,
            "public"=>$request->public,
        ]);

        return response()->json([
            'state' => 'Exito!',
            'message' => 'Comunidad actualizada exitosamente!',
            'type' => 'success',
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       Community::destroy($id);

       return response()->json([
          'message' => 'Comunidad eliminada!'
      ]);
    }
}
