<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use DB;
use App\User;

class EstadisticaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

       public function __construct() 
       {
      
        View::share('titulo', "Estadisticas");
       }

    public function date(Request $request)
    {

        $dates=explode('-', $request->date);
        $year=$dates[1];
        $month=$dates[0];


        $users=DB::table('users')->select(DB::raw('count(id) as `data`'), DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),  DB::raw('YEAR(created_at) year, MONTH(created_at) month'))->selectRaw("MONTHNAME(created_at) as mes")
->groupby('year','month')
->where('country_id',$request->country_id);
if ($request->city_id!=0) 
{
 $users=$users->where('city_id',$request->city_id);

}
 $users=$users->whereYear('created_at','=',$year)
 ->whereMonth('created_at', '=', $month)
->get();

$data=array();
$c_arr=[];
$arr=[];
$meses=$users->pluck('mes');
$usuarios=$users->pluck('data');




    for ($i=0; $i<count($meses);$i++) 
    {
            
        array_push($arr,$meses[$i]);
        array_push($arr,$usuarios[$i]);
        array_push($data,$arr);

        $arr=[];


        
    }


      //Type of users

 $all_users=DB::table('users')
 ->where('country_id',isset($request->country_id)?$request->country_id:44);
  if ($request->city_id!=0) 
{
 $all_users=$all_users->where('city_id',$request->city_id);

}
 $all_users=$all_users->whereYear('created_at', '=',$year)
->whereMonth('created_at','=', $month)
 ->get();

 $premiums=0;
 $guest=0;
 $type=array();

 foreach ($all_users as $key => $value) 
 {

        $_u=User::find($value->id);
    //if user doesn't have a plan it becomes a guest
        if ($_u->user_plan()->count()>0) 
        {

            $user_plan=$_u->user_plan()->latest()->first()->plan()->first()->type;

            if ($user_plan=="premium") 
            {
                $premiums++;
            }

        }else
        {
            $guest++;
        }
    # code...
 }


 array_push($type,array("name"=>"premiums","y"=>$premiums));
 array_push($type,array("name"=>"guest","y"=>$guest));

 $type_b=[];
 $arr_t=[];
 array_push($arr_t,"premiums");
 array_push($arr_t,$premiums);
array_push($type_b,$arr_t);
$arr_t=[];

 array_push($arr_t,"guest");
 array_push($arr_t,$guest);

 array_push($type_b,$arr_t);



    return response()->json(compact('meses','usuarios','data','type','type_b'));



    }


    public function index(Request $request)
    {


$users=DB::table('users')->select(DB::raw('count(id) as `data`'), DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"),  DB::raw('YEAR(created_at) year, MONTH(created_at) month'))->selectRaw("MONTHNAME(created_at) as mes")
->groupby('year','month')
->where('country_id',isset($request->country_id)?$request->country_id:43);

if ($request->city_id!=0) 
{
 $users=$users->where('city_id',$request->city_id);

}
 $users=$users->whereYear('created_at', '=',isset($request->date)?$request->date:date('Y'))
->get();



 $countries=DB::table('users')->select(DB::raw('count(users.id) as `data`'),'countries.name as pais')
->join('countries','countries.id','users.country_id');

if ($request->city_id!=0) 
{
 $countries=$countries->where('city_id',$request->city_id);

}
$countries=$countries->where('country_id',isset($request->country_id)?$request->country_id:43)
->groupby('users.country_id')
->get();


$users_by_country=DB::table('users')->where('country_id',isset($request->country_id)?$request->country_id:43)->count();

$all=DB::table('users')->count();

$all_com=DB::table('communities')->count();

$products_by_country=DB::table('products')->where('country_id',isset($request->country_id)?$request->country_id:43)->count();

$all_products=DB::table('products')->count();

  //Type of users

 $all_users=DB::table('users')
 ->where('country_id',isset($request->country_id)?$request->country_id:43);
 if ($request->city_id!=0) 
{
 $all_users=$all_users->where('city_id',$request->city_id);

}
  $all_users=$all_users->whereYear('created_at', '=',isset($request->date)?$request->date:date('Y'))
 ->get();
 
 $premiums=0;
 $guest=0;
 $type=array();

 foreach ($all_users as $key => $value) 
 {

        $_u=User::find($value->id);
    //if user doesn't have a plan it becomes a guest
        if ($_u->user_plan()->count()>0) 
        {

            $user_plan=$_u->user_plan()->latest()->first()->plan()->first()->type;

            if ($user_plan=="premium") 
            {
                $premiums++;
            }

        }else
        {
            $guest++;
        }
    # code...
 }


 array_push($type,array("name"=>"premiums","y"=>$premiums));
 array_push($type,array("name"=>"guest","y"=>$guest));

 $type_b=[];
 $arr_t=[];
 array_push($arr_t,"premiums");
 array_push($arr_t,$premiums);
 array_push($type_b,$arr_t);
 $arr_t=[];

 array_push($arr_t,"guest");
 array_push($arr_t,$guest);

 array_push($type_b,$arr_t);





$data=array();
$country_arr=array();
$c=$countries->pluck('pais');
$_users=$countries->pluck('data');
$c_arr=[];
$arr=[];
$meses=$users->pluck('mes');
$usuarios=$users->pluck('data');

    for ($i=0; $i <count($c) ; $i++) { 
       
        array_push($c_arr,$c[$i]);
        array_push($c_arr,$_users[$i]);
        array_push($country_arr,$c_arr);

        $c_arr=[];
    }

    for ($i=0; $i<count($meses);$i++) 
    {
            
        array_push($arr,$meses[$i]);
        array_push($arr,$usuarios[$i]);
        array_push($data,$arr);

        $arr=[];


        
    }

    //communities

 $com=DB::table('communities')->select(DB::raw('count(communities.id) as `data`'),'cities.name as ciudades')
->join('cities','cities.id','communities.city_id')
->join('states','states.id','cities.state_id')
->join('countries','countries.id','states.country_id')
->groupby('communities.city_id');
if ($request->city_id!=0) 
{
 $com=$com->where('city_id',$request->city_id);

}
$com=$com->where('countries.id',$request->country_id)
->get();

        $cities_arr=$com->pluck('ciudades');
        $_com=$com->pluck('data');
        $com_arr=array();
        $communities=array();

     for ($i=0; $i <count($cities_arr) ; $i++) { 
       
        array_push($com_arr,$cities_arr[$i]);
        array_push($com_arr,$_com[$i]);
        array_push($communities,$com_arr);

        $com_arr=[];
    }

 //productos publicados
 
  $prod=DB::table('products')->select(DB::raw('count(products.id) as `data`'),'cities.name as ciudades')
->join('cities','cities.id','products.city_id')
->groupby('products.city_id');
if ($request->city_id!=0) 
{
 $prod=$prod->where('cities.id',$request->city_id);

}
$prod=$prod->where('products.country_id',isset($request->country_id)?$request->country_id:43)
->get();

        $cities_arr=$prod->pluck('ciudades');
        $_prod=$prod->pluck('data');
        $prod_arr=array();
        $products=array();

     for ($i=0; $i <count($cities_arr) ; $i++) 
     { 
       
        array_push($prod_arr,$cities_arr[$i]);
        array_push($prod_arr,$_prod[$i]);
        array_push($products,$prod_arr);

        $prod_arr=[];
    }   


     //productos prestados
 
  $prod_p=DB::table('products')->select(DB::raw('count(products.id) as `data`'),'cities.name as ciudades')
->join('cities','cities.id','products.city_id')
->groupby('products.city_id')
->where('products.borrowed',1);
if ($request->city_id!=0) 
{
 $prod_p=$prod_p->where('cities.id',$request->city_id);

}
$prod_p=$prod_p->where('products.country_id',isset($request->country_id)?$request->country_id:43)
->get();

        $cities_arr_p=$prod_p->pluck('ciudades');
        $_prod_p=$prod_p->pluck('data');
        $prod_arr_p=array();
        $prestados=array();

     for ($i=0; $i <count($cities_arr_p) ; $i++) 
     { 
       
        array_push($prod_arr_p,$cities_arr_p[$i]);
        array_push($prod_arr_p,$_prod_p[$i]);
        array_push($prestados,$prod_arr_p);

        $prod_arr_p=[];
    }  

$users_all_country=DB::table('users')->select('countries.name',DB::raw('count(users.id) as `total`'))
->join('countries','countries.id','users.country_id')
->groupby('country_id')->get();
$a=[];
$all_countries=[];


foreach ($users_all_country as $key => $value) 
{
 array_push($a, $value->name);
 array_push($a,$value->total);
 array_push($all_countries,$a);

 $a=[];   
}




#return compact('meses','usuarios','data','country_arr','communities','products','prestados','type','type_b');

if ($request->ajax()) 
{
#return response()->json($request->all(),404);
return response()->json(compact('users_by_country','all','all_com','products_by_country','all_products','meses','usuarios','data','country_arr','communities','products','prestados','type','type_b','all_countries'));
}

return view('admin.estadisticas.index',compact('users_by_country','all','all_com','products_by_country','all_products','meses','usuarios','data','country_arr','communities','products','prestados','type','type_b','all_countries'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
