<?php

namespace App\Http\Controllers\Admin\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Config;
use Storage;
use Illuminate\Filesystem\Filesystem;

class ConfigController extends Controller
{
    public function __construct()
    {

        View::share('titulo', "Configuraciones");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $email          = Config::getConfig('email_notificaciones');
        $duracionSesion = Config::getConfig('duracion_sesion');
        $monedaSimbolo  = Config::getConfig('moneda_simbolo');
        $monedaNombre   = Config::getConfig('moneda_nombre');
        $porcentajeSalarioCuota = Config::getConfig('porcentaje_salario_cuota');
        $porcentajeOtrosIngresosCuota = Config::getConfig('porcentaje_otros_ingresos_cuota');
        $ipc   = Config::getConfig('ipc');
        $tasa_mvotma   = Config::getConfig('tasa_mvotma');
        $tasa_svvffaa   = Config::getConfig('tasa_svvffaa');
        return view('admin.config.index' , compact('email', 'duracionSesion', 'monedaSimbolo', "monedaNombre","porcentajeSalarioCuota","ipc","tasa_mvotma","tasa_svvffaa","porcentajeOtrosIngresosCuota"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file('imagen')) 
        {

            if(!Storage::exists('/public/configuracion')) {

    Storage::makeDirectory('/public/configuracion', 0775, true); //creates directory

}else{
    Storage::deleteDirectory('/public/configuracion');
}
          
            $file=$request->file('imagen');
            $ruta=$file->storeAs('configuracion', str_replace(' ','_',$file->getClientOriginalName()), 'public');
                    $request->merge(["email_pie_url"=>$ruta]);

        }

        foreach($request->except(["_token","imagen"]) as $key => $value){

            
            $config = Config::getConfig($key);
            $config->value = $value;
            $config->save();
        }
        flash('La configuracion ha sido actualizada con exito')->success();
        return redirect('admin/config');
    }

      private function borrarImagen($imagen) {
        $$imagen = str_replace("storage", "public", $imagen);
        Storage::delete($fotoU);
    }
}
