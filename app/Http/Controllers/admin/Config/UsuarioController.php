<?php

namespace App\Http\Controllers\Admin\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class UsuarioController extends Controller
{
    public function __construct()
    {
        $this->middleware("permission:usuarios_ver")->only("index","show");
        $this->middleware("permission:usuarios_crear")->only("create", "store");
        $this->middleware("permission:usuarios_editar")->only("edit", "update");
        $this->middleware("permission:usuarios_eliminar")->only("destroy");
        View::share('titulo', "Usuarios");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $usuario = [];
            $users = User::all();
            return response()->json($users);
        }
        
        return view('admin.config.usuarios.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.config.usuarios.create', compact('roles'));
    }

    public function editUser($user_id, Request $request)
    {
        $user = User::find($user_id);

        try {

            $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
            ]);

            if($request->filled('password')){
                
                $request->validate([
                    'password' => 'required|string|min:6'
                ]);
                $user->password = bcrypt( $request->password );
            }

            $user->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email
            ]);

            return response()->json([
                'success' => 'true',
                'mensaje' => "Datos actualizados!"
            ]);
            
        } catch (Exception $e) {
            
            return response()->json([
                 'error' => $e->getMessage()
            ]);    
        }        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    public function usuariosSide(Request $request)
    {

    $data= [
        'nombres' => 'required',
        'apellidos' => 'required',
    ];

    $this->validate($request, $data);

    $usuario = User::findOrFail(auth()->user()->id);

        if($request->habilitar==true)
        {
            $usuario->estado=true;
            $usuario->save();
            return response()->json([
                'success' => 'true',
                'mensaje' => "Se habilito el usuario correctamente"
            ]);
        }else{
            $data = [
                'nombres' => 'required',
                'apellidos' => 'required'
            ];

            if ($request->password != "") {
                $data['password'] = 'required|same:repClave';
            }

            if ($request->anterior_email != $request->email) {
                $data['email'] = 'required|email|unique:users';
            }

            try {
                $usuario->fill($request->except('repClave', 'rol', 'habilitar','anterior_email','password','foto_perfil', 'cambiar_imagen'));
          
                if ($request->password != "") {
                    $usuario->password = bcrypt($request->password);
                }

                $urlfinal="";
                if($request->cambiar_imagen == 1 && $request->_inicio == 1){
                    $this->borrarImagenUsuario($usuario);
                    if($request->input('foto_perfil')){
                        $foto_perfil = $request->file('foto_perfil');
                        $ruta = "img/foto_perfil/".uniqid().'.jpg';
                        Storage::put('public/'.$ruta,  $this->decode_imageCropit($request->input('foto_perfil')));
                        $urlfinal = "/storage/$ruta";
                      }
                  $usuario->foto_perfil = $urlfinal;
                }

                $usuario->save();

            

                flash("Los datos fueron actualizado con éxito")->success();

                return response()->json([
                    'success' => 'true',
                    'mensaje' => "Los datos fueron actualizado con éxito"
                ]);
                
            } catch (\Exception $e) {
               return response()->json([
                    'error' => $e->getMessage()
                ]);
            } 
        } 
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nombres' => 'required',
            'apellidos' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|same:repClave|min:6',
            'rol' => 'required'
        ]);

        try {
            $urlfinal="";
        

            if($request->input('foto_perfil')){
              $foto_perfil = $request->file('foto_perfil');
              $ruta = "img/foto_perfil/".uniqid().'.png';
              Storage::put('public/'.$ruta,  $this->decode_imageCropit($request->input('foto_perfil')));
              $urlfinal = "/storage/$ruta";
            }

            $user = new User();

            $user->fill($request->except('repClave', 'rol', 'foto_perfil'));
            $user->foto_perfil=$urlfinal;
            $user->password = bcrypt($request->password);

            $user->save();

            if($request->rol){
                $user->roles()->sync([$request->rol]);
            }else{
                $user->roles()->sync([]);
            }

            flash("El usuario <b>$request->nombres</b> fue creado con éxito")->success();

            return response()->json([
                'success' => true,
                'mensaje' => "El usuario <b>$request->nombres</b> fue creado con éxito"
            ]); 
        } catch (\Exception $e) {
            return response()->json([
                    'error' => $e->getMessage()
                ]);
        }
    }

    private function decode_imageCropit($imageCropit){
        $imageCropit = str_replace('data:image/jpeg;base64,', '', $imageCropit);
        $imageCropit = str_replace(' ', '+', $imageCropit);
        return base64_decode($imageCropit);
     }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $usuario = User::findOrFail($id);
        return view('admin.config.usuarios.edit', compact('roles','usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $usuario = User::findOrFail($id);

        if($request->habilitar==true)
        {
            $usuario->estado=true;
            $usuario->save();
            return response()->json([
                'success' => 'true',
                'mensaje' => "Se habilito el usuario correctamente"
            ]);
        }else{
            $data = [
                'nombres' => 'required',
                'apellidos' => 'required'
            ];

            if ($request->password != "") {
                $data['password'] = 'required|same:repClave';
            }

            if ($request->anterior_email != $request->email) {
                $data['email'] = 'required|email|unique:users';
            }

            $this->validate($request, $data); 
            try {
                $usuario->fill($request->except('repClave', 'rol', 'habilitar','anterior_email','password','foto_perfil', 'cambiar_imagen'));

                if ($request->password != "") {
                    $usuario->password = bcrypt($request->password);
                }
                $urlfinal="";
                if($request->cambiar_imagen == 1 && $request->_inicio == 1){
                    $this->borrarImagenUsuario($usuario);
                    if($request->input('foto_perfil')){
                        $foto_perfil = $request->file('foto_perfil');
                        $ruta = "img/foto_perfil/".uniqid().'.jpg';
                        Storage::put('public/'.$ruta,  $this->decode_imageCropit($request->input('foto_perfil')));
                        $urlfinal = "/storage/$ruta";
                      }
                  $usuario->foto_perfil = $urlfinal;
                }

                $usuario->save();

                if ($request->_inicio == 1) {
                    $usuario->roles()->sync([$request->rol]);
                }

                flash("Los datos fueron actualizado con éxito")->success();

                return response()->json([
                    'success' => 'true',
                    'mensaje' => "Los datos fueron actualizado con éxito"
                ]);


                
            } catch (\Exception $e) {
               return response()->json([
                    'error' => $e->getMessage()
                ]);
            } 
        } 
    }

    private function borrarImagenUsuario($usuario){
        $fotoU = $usuario->foto_perfil;
        if($fotoU != "" || $fotoU != null){

            $fotoU = str_replace("storage","public",$fotoU);
            Storage::delete($fotoU);
        }
    }

    public function cambiarFoto($user_id, Request $request)
    {
        $usuario = User::findOrFail($user_id);

        try {

            $urlfinal="";
            // $this->borrarImagenUsuario($usuario);
            if($request->hasFile('foto_perfil')){

              $foto_perfil = $request->file('foto_perfil');
              $imgName = $foto_perfil->getClientOriginalName();
              $ruta = "img/profile/".$imgName;
              $foto_perfil->move(public_path("img/profile/"), $imgName);
              // Storage::put('public/'.$ruta,  $this->decode_imageCropit($request->input('foto_perfil')));
               // $urlfinal = "/storage/$ruta";
               $urlfinal = $imgName;

            }

            $usuario->profile_img = $urlfinal;
            $usuario->save();

            return response()->json([
                'success' => 'true',
                'mensaje' => "Foto de perfil actualizada!",
                'foto_url'=> "/img/profile/".$usuario->profile_img
            ]);

        } catch (\Exception $e) {
           return response()->json([
                'error' => $e->getMessage()
            ]);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $usuario = User::findOrFail($id);
        try {
            $result = $usuario->delete();
            if ($result == 1) {
                $this->borrarImagenUsuario($usuario);
                if ($usuario->id == $user->id) {
                    Auth::logout();
                    return response()->json([
                        'success' => true,
                        'mensaje' => "Se ha eliminado correctamente",
                        'cerrar_sesion'=>true
                    ]);
                }else{
                   return response()->json([
                        'success' => true,
                        'mensaje' => "Se ha eliminado correctamente",
                        'cerrar_sesion'=>false
                    ]); 
                }
                
            } else {
                return response()->json([
                    'error' => "Error al eliminar el usuario"
                ]);
            }
        } catch (\Exception $e) {
            $usuario->estado=0;
            $usuario->save();
            return response()->json([
                'success' => true,
                'mensaje' => "Se ha deshabilitado el usuario correctamente",
                'cerrar_sesion'=>false
            ]);
        }
    }
}
