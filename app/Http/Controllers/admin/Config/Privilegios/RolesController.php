<?php

namespace App\Http\Controllers\Admin\Config\Privilegios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class RolesController extends Controller
{

    public function __construct()
    {
        $this->middleware("permission:privilegios_ver");
        $this->middleware("permission:privilegios_crear")->only("create", "store");
        $this->middleware("permission:privilegios_editar")->only("edit", "update");
        $this->middleware("permission:privilegios_eliminar")->only("destroy");
        View::share('titulo', "Roles");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $rol = Role::all();
            return response()->json($rol);
        } else {
            return view('admin.config.privilegios.roles.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permisos = Permission::all();
        return view('admin.config.privilegios.roles.create',compact('permisos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validation = [
            'name' => 'required',
        ];

        $this->validate($request, $validation);

        $slug_rol = str_slug($request->name, '_');

        $rol = Role::where("name", $slug_rol)->first();
        if ($rol) {
            flash('El nombre del rol no se encuentra disponible')->error();
            return Redirect::back()->withInput($request->all());
        }

        $rol = new Role();
        $rol->fill($request->except('permisos'));
        $rol->name = $slug_rol;
        $rol->save();

        $rol->perms()->sync($request->permisos);
        flash('Rol Creado Correctamente')->success();
        return redirect('admin/config/privilegios/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rol = Role::findOrFail($id);
        $pre_permisos = Permission::all();
        $permisos = [];
        foreach ($pre_permisos as $key => $permiso) {
            $check = DB::table('permission_role')->where('permission_id',$permiso->id)->where('role_id',$id)->count();
            if($check == 1){
                $permiso->checked = 1;
            }else{
                $permiso->checked = 0;
            }
            $permisos[] = $permiso;
        }
        return view('admin.config.privilegios.roles.edit', compact('rol','permisos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rol = Role::findOrFail($id);

        $validation = [
            'name' => 'required',
        ];
        $this->validate($request, $validation);

        $slug_rol = str_slug($request->name, '_');

        if ($rol->name != $slug_rol) {
            $role = Role::where("name", $slug_rol)->first();
            if ($role) {
                flash('El nombre del rol no se encuentra disponible')->error();
                return Redirect::back()->withInput($request->all());
            }
        }
        $rol->fill($request->except('permisos'));
        $rol->name = $slug_rol;
        $rol->save();

        $rol->perms()->sync($request->permisos);
        
        flash('El rol ha sido actualizado con exito')->success();
        return redirect('admin/config/privilegios/roles');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rol = Role::findOrFail($id);

        if($rol->perms){
            return response()->json([
                'success' => false,
                'mensaje' => "El rol posee permisos activos",
            ]);
        }
        $rol->delete();
        return response()->json([
            'success' => true,
            'mensaje' => "El rol ha sido eliminado con exito",
        ]);
    }
}
