<?php

namespace App\Http\Controllers\Admin\Config\Privilegios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Permission;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class PermissionsController extends Controller
{

    public function __construct()
    {
        $this->middleware("permission:privilegios_ver");
        $this->middleware("permission:privilegios_crear")->only("create", "store");
        $this->middleware("permission:privilegios_editar")->only("edit", "update");
        $this->middleware("permission:privilegios_eliminar")->only("destroy");
        View::share('titulo', "Permisos");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $permisos = Permission::all();
            return response()->json($permisos);
        } else {
            return view('admin.config.privilegios.permisos.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.config.privilegios.permisos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = [
            'name' => 'required',
        ];

        $this->validate($request, $validation);

        $slug_permiso = str_slug($request->name, '_');

        $permiso = Permission::where("name", $slug_permiso)->first();
        if ($permiso) {
            flash('El nombre del permiso no se encuentra disponible')->error();
            return Redirect::back()->withInput($request->all());
        }
        $permiso = new Permission();
        $permiso->fill($request->all());
        $permiso->name = $slug_permiso;
        $permiso->save();
        flash('Permiso Creado Correctamente')->success();
        return redirect('admin/config/privilegios/permisos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permiso = Permission::findOrFail($id);
        return view('admin.config.privilegios.permisos.edit', compact('permiso'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $permiso = Permission::findOrFail($id);

        $validation = [
            'name' => 'required',
        ];

        $this->validate($request, $validation);

        $slug_permiso = str_slug($request->name, '_');

        if ($permiso->name != $slug_permiso) {
            $permission = Permission::where("name", $slug_permiso)->first();
            if ($permission) {
                flash('El nombre del permiso no se encuentra disponible')->error();
                return Redirect::back()->withInput($request->all());
            }
        }
        $permiso->fill($request->all());
        $permiso->save();
        flash('El permiso ha sido creado con exito!')->success();
        return redirect('admin/config/privilegios/permisos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permiso = Permission::findOrFail($id);
        $permiso->delete();
        return response()->json([
            'success' => true,
            'mensaje' => "El permiso ha sido eliminado con exito",
        ]);
    }
}
