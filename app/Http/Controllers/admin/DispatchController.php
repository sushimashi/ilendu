<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ShippingRecord;
use App\Borrower;
use Mail;

class DispatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function get_info($id)
    {

        $s=ShippingRecord::where('borrow_id',$id)->first();
        return response()->json($s);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            "weight"=>"required",
            "price"=>"required",
            "dispatch_date"=>"required",
            "total"=>"required"
        ]);

        $s=ShippingRecord::create(
            [
            "weight"=>$request->weight,
            "price"=>$request->price,
            "dispatch_date"=>$request->dispatch_date,
            "borrow_id"=>$request->borrow_id,
            "total"=>$request->total
            ]);

        $b=Borrower::find($request->borrow_id);
        $b->status="Dispatching";
        $b->save();

          if ($s == true){


        $data = array('toEmail' => $s->borrower->user->email,
            'toName' => $s->borrower->user->name,
            'product_name' => $s->borrower->product->product_name,
            'product_img' => $s->borrower->product->product_img,
            'first_name' => $s->borrower->user->first_name,
            'last_name' => $s->borrower->user->last_name,
            'product_id'=>$s->borrower->product->id,
            'email' => $s->borrower->user->email,
            'weight'=>$s->weight,
            'price'=>$s->price,
            'dispatch_date'=>$s->dispatch_date,
            'total'=>$s->total
        );

        Mail::send('emails.dispatch_email', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['toName']);
            $message->from('noreply@ilendu.com','Ilendu');
            $message->subject('Ilendu order amount for '. $data['product_name']);
        });

            return response()->json(["success" => true]);
        } else{
            return response()->json(["success" => false]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
