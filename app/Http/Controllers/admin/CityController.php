<?php

namespace App\Http\Controllers\admin;

use App\Country;
use App\State;
use App\Product;
use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Community;

class CityController extends Controller
{
    public function __construct()
    {
        View::share('titulo', "Ciudades");
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   $countries = Country::all(['id', 'name']);
        return view('admin.cities.index', compact('countries'));
    }

    // tabla de ciudades
    public function Datatable($country_id)
    {
        $country = Country::find($country_id);
        $cities = City::where('country_id', $country->id)
                       // ->where('status', 1)
                        ->get(['id', 'name','status']);
        return response()->json($cities);
    }

    // ciudades deshabilitadas 
    public function disabledCities()
    {
        return view('admin.cities.disabledCities');
    }

    // ciudades deshabilitadas tabla
    public function disabledCitiesDatatable($country_id)
    {   
       $country = Country::find($country_id);
       $cities = City::where('country_id', $country->id)
                       ->where('status', 0)
                       ->get(['id', 'name']);
       return response()->json($cities);
    }

    // Habilitar/ deshabilitar ciudad
    public function enableCity($city_id)
    {
        $city = City::findOrFail($city_id);
        if ($city->status === 0){
            $city->update(['status' => 1]);
$c=Community::where('name','like','%'.$city->name.'%')->where('city_id',$city->id)->first();

if (empty($c)) 
{
    Community::firstorCreate([
                "name"=>ucfirst($city->name),
                "city_id"=>$city->id
               ],[
                "description"=>$city->name,
                "com_image"=>"city_icon.png",
                "status"=>1,
                "created_by"=>1,
                "public"=>1
            ]);
}else{
   $c->status=1;
   $c->save(); 
}

       

            return response()->json([
                'message' => 'ciudad habilitada!'
            ]);
        }else{
            $city->update(['status' => 0]);
            $c=Community::where('name','like','%'.$city->name.'%')->where('city_id',$city->id)->first();
            $c->status=0;
            $c->save();

            return response()->json([
                'message' => 'ciudad deshabilitada!'
            ]);
        }
       
    }

      public function productsByCountry($country_id){

        $count_products = Product::whereHas('city',function($q) use($country_id){

          $q->where('country_id',$country_id);
        })->count();

        return response()->json($count_products);
    }

    // estados por pais
    public function countrySelect(Request $request){

        if (isset($request->term))
         {
                 $c = Country::select('id as id','name as text')->where('name','like','%'.$request->term.'%')->get();

        }else{
            $c = Country::select('id as id','name as text')->get();
     
        }

    

        $list = ['results'=>collect($c)];
        return response()->json($list);
    }

    // estados por pais
    public function statesByCountry($country_id){
        $states = State::where('country_id', $country_id)->get(['id', 'name']);

    

        return response()->json($states);
    }

      // estados por pais
    public function statesByCountrySelect(Request $request,$country_id){

         if (isset($request->term))
         {


        $states = State::select('id as id','name as text')->where('country_id', $country_id)->where('name','like','%'.$request->term.'%')->get();

        }else{
        $states = State::select('id as id','name as text')->where('country_id', $country_id)->get();
     
        }

        $list = ['results'=>collect($states)];
        return response()->json($list);

    }

    

    // ciudades por estado
    public function cityByState($state_id){
        $cities = City::where([
                                ['state_id','=',$state_id]
                            ])->get(['id', 'name']);
        return response()->json($cities);
    }

      public function cityByStateSelect(Request $request,$state_id){


         if (isset($request->term))
         {
        $cities = City::select('id as id','name as text')->where('state_id', $state_id)->where('name','like','%'.$request->term.'%')->get();

        }else{
  $cities = City::select('id as id','name as text')->where([
                                ['state_id','=',$state_id]
                            ])->get();     
        }
      

        $list = ['results'=>collect($cities)];
        return response()->json($list);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'country' => 'required',
            'state' => 'required',
            'city' => 'required'
        ]);

        if( 
            City::where('name', $request->city)
                ->where('state_id', $request->state)
                ->where('country_id', $request->country)
                ->first() != null  
        ){
            return response()->json([
                "errors" => [
                    "CityAlreadyExists" => ['Ciudad ya se encuentra registrada.']
                ]
            ], 422);
        }

        City::create([
            'name' => strtolower($request->city),
            'state_id' => $request->state,
            'country_id' => $request->country,
            'status' => 1,
            'email' => $request->user()->email,
        ]);

        return response()->json(['success' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::find($id);
        $countries = Country::all(['id', 'name']);
        return view('admin.cities.edit', compact('city', 'countries'));
    }

    // editar ciudad
    public function editCity($city_id)
    {
        $city = City::find($city_id);
        $country = $city->country;
        $state = $city->state;

        return response()->json([
            'country_id' => $country->id,
            'state_id' => $state->id,
            'state_name' => $state->name,
            'city_id' => $city->id,
            'city_name' => $city->name,
       ]);
    }

    // actualizar ciudad
    public function updateCity( Request $request, $id )
    {
        City::find($id)->update([
            'name' => $request->newCity,
            'state_id' => $request->newState,
            'country_id' => $request->newCountry,
            'status' => 1,
            'email' => $request->user()->email,
        ]);
        return response()->json(['success' => true]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        City::destroy($id);

       return response()->json([
            'message' => 'Ciudad eliminada!'
        ]);
    }
}
