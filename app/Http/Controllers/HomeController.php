<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Borrower;
use App\Product;
use App\User;
use App\Membership;
use App\Category;
use App\Country;
use App\State;
use App\Community;
use App\Community_user;
use App\City;
use Config;
use Auth;
use File;
use Illuminate\Support\Facades\Validator;
use Redirect,DB;
use Session;
use Mail;
use URL,Response,View;
use App\BorrowerMessage;

class HomeController extends Controller
{



    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /** for language switch**/



    public function switchLang($lang)
    { 
       if (array_key_exists($lang, Config::get('languages'))) {
           Session::put('applocale', $lang);
       }
       return Redirect::back();
   }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function security()
    {
        return view('site.security');
    }

    public function about()
    {
        return view('site.about');
    }

    public function index( Request $request ) 
    {

        $isLoggedIn = Auth::check();
        $user = Auth::user();
        $only_country="cities";

        #return Session::get('countryvalue');

        if(Session::get('cheked'))
        {
            $request->session()->flash('registered', '1');
            $request->session()->forget(['cheked']);

        }
        $message = "";

        //obtenemos categorias activas
        $categories = Category::where('is_active',1)
        ->select('name','id','name_spanish')
        ->get()->toArray();



        $products=collect([]);
        //obtenemos productos activos que se vean en el front
       // $products = Product::with('category','user')->where('is_active',1)->where('in_front',1);

        $united_states=231;


        if (Auth::check()) 
        {
            if (empty(Session::get('cityvalue'))) 
            {
                session(['cityvalue'=> $user->city_id]);
                session(['countryvalue'=> $user->country_id]);
            }
            session(['countryvalue'=> $user->country_id]);


        }else{


            $ip=request()->server('HTTP_X_FORWARDED_FOR');

            if (!empty($ip)) 
            {
                $location =  \Location::get($ip);

                $country=Country::firstOrCreate(['name'=>$location->countryName]);

                $state=State::firstOrCreate(['name'=>$location->regionName,'country_id'=>$country->id]);

                $city=City::firstOrCreate(['name'=>$location->cityName,'state_id'=>$state->id]);



                session(['cityvalue'=> $city->id]);
                session(['countryvalue'=> $country->id]);
            }else{
                //united states products
                session(['countryvalue'=> $united_states]);
            }

        }

        




        $_paginate=9;
        $cid=Session::get('cityvalue');
        #$cid=5167;
        $user_country=Session::get('countryvalue');

        $cities = City::join('products','products.city_id','cities.id')
        ->join('states','states.id','cities.state_id')
        ->join('countries','countries.id','states.country_id')
        ->select('cities.id','cities.name')
        ->groupBy('products.city_id')
        ->where('countries.id',$user_country)
        ->havingRaw('COUNT(products.id) >= 1')
        ->get();


        if($cities){
            if ($cities->contains('id',$cid))
            {
                $only_country="cities";

                $products = Product::with('category','user')->where(['is_active'=>1,'in_front'=>1,'city_id'=>$cid,'country_id'=>$user_country])
                ->orWhere(['share_all'=>1])->where('country_id',$user_country)->orderBy('created_at','DESC')
                ->paginate($_paginate);
            }else
            {                   

                $products = Product::with('category','user')->where(['is_active'=>1,'in_front'=>1,'country_id'=>$user_country,'is_premium'=>1,'share_all'=>1])->orderBy('created_at','DESC')
                ->paginate($_paginate);
                $only_country="country";


                if (!count($products)) 
                {
                    $only_country="united";

                        //united states products
                    $products = Product::with('category','user')->where(['is_active'=>1,'in_front'=>1,'country_id'=>$united_states,'is_premium'=>1,'share_all'=>1])->paginate($_paginate);

                }

            }

        }

        if($user){


            foreach ($products as $key => $value) {

                $products[$key]->borrower = Borrower::where('user_id', Auth::user()->id)
                ->where('product_id', $value->id)
                ->orderBy('created_at','DESC')
                ->first();
                
            }
            $userproducts = Product::where('user_id', Auth::user()->id)->get();
            $user= User::find(Auth::user()->id);
            if((strtotime($user->expire_date)) < strtotime(date('Y-m-d'))){
                $user->expireDays = 0;
            }elseif($user->expire_date != null){
                $date1=date_create(date('Y-m-d'));
                $date2=date_create(date('Y-m-d',strtotime($user->expire_date)));
                $diff=date_diff($date1,$date2);
                $user->expireDays = $diff->days;
            }else{
                $user->expireDays = 0;
            }



            $membership_fees = Membership::orderBy('id', 'desc')->first();
            if(isset($membership_fees)){
                $user->membershipFees = $membership_fees->fees;
            }
        }
        if (request()->ajax()) 
        { 
           return Response::json(View::make('site.cityproduct', array('cities'=>$cities,'products'=>$products,'user'=>$user,'isLoggedIn'=>$isLoggedIn,'only_country'=>$only_country))->render());


       }

    

       return view('site.home', compact(
        'products',
        'userproducts',
        'user',
        'categories',
        'cities',
        'message1',
        'countries',
        'only_country'
    ))->with('isLoggedIn', $isLoggedIn);


   }

   public function logout(Request $request) {

    $request->session()->flush();

    Session::forget('cityvalue');
    Session::forget('countryvalue');

    $request->session()->invalidate();
    Auth::logout();

    return Redirect::home();
}

public function dump() {
    $isLoggedIn = Auth::check();
    return view('dump')
    ->with('isLoggedIn', $isLoggedIn)
    ->with('user', Auth::user()->getUserInfo())
    ->with('accessToken', Auth::user()->getAuthPassword());
}


public function send_message(Request $request)
{

    $user_id = Auth::user()->id;
    $id=$request->borrow_id;

    $message=BorrowerMessage::create(["borrow_id"=>$request->borrow_id,"user_id"=>$user_id,"message"=>$request->message]);


    $borrow_ids=Borrower::whereHas('product',function($q) use ($user_id){
        $q->where('user_id',$user_id);
    })->orWhere('user_id',$user_id)->where('status','Pending')->get();

    $m= array();
        // tab messages

    $data=BorrowerMessage::where('borrow_id',$id)->get();
    $m[$id]=$data;

    $borrow=Borrower::find($id);
    $messages_collection= $m;


    return view('site.tabs.dynamic_message',compact('messages_collection','borrow')); 
}

public function get_messages($id)
{
    $user_id = Auth::user()->id;

    $borrow_ids=Borrower::whereHas('product',function($q) use ($user_id){
        $q->where('user_id',$user_id);
    })->orWhere('user_id',$user_id)->where('status','Pending')->orWhere('status','Canceled')->get();


    $m= array();
        // tab messages

    $data=BorrowerMessage::where('borrow_id',$id)->get();
    $m[$id]=$data;
    $borrow=Borrower::find($id);

    $messages_collection= $m;


    return view('site.tabs.dynamic_message',compact('messages_collection','borrow')); 
}

/* Account Page */
public function account(Request $request) {

    $paginate=6;
    $isLoggedIn = Auth::check();
    $user_id = Auth::user()->id;
    $products = Product::where('borrowed',0)
    ->where('user_id', $user_id)->paginate($paginate);



    $communities =DB::table('communities')
    ->leftJoin('community_user', 'communities.id', '=', 'community_user.community_id')
    ->where('communities.created_by',$user_id)
    ->orWhere('community_user.user_id',$user_id)
    ->select('communities.id as id','communities.name')
    ->paginate($paginate);



    $user = User::find($user_id);
    #$countries = Country::get();
    #$allcommunity = Community::get();
    #$states = State::where('country_id',$user->country_id)->get();
    #$allcities = City::where('state_id',$user->state_id)->get();

    /*
    $cities = City::join('products','products.city_id','cities.id')->select('cities.id','cities.name')
    ->groupBy('products.city_id')
    ->select('cities.id','cities.name')
    ->havingRaw('COUNT(products.id) >= 5')
    ->paginate($paginate);*/


    $coll=Product::with(['user','borrower'])->whereHas('borrower',function($q) use($user_id){
        $q->where('user_id', $user_id);
    })->get();


    $accepted = $coll->where('borrower.status','Accept')->all();
    $sent = $coll->where('borrower.status','Sent')->all();
    $received = $coll->where('borrower.status','Received')->all();
    $pending = $coll->where('borrower.status','Pending')->all();
    $dispatch = $coll->where('borrower.status','Dispatching')->all();
    $paidOut = $coll->where('borrower.status','PaidOut')->all();

    $v=array_merge($accepted,$pending);
    $s=array_merge($v,$sent);
    $r=array_merge($s,$received);
    $d=array_merge($r,$dispatch);
    $p=array_merge($d,$paidOut);

    $collection=collect($p);

    $borrowed=$collection->pluck('borrower.product_id');

    $ordered_products = Product::with('user')->whereHas('borrower',function($q) use($user_id){
        $q->where('user_id', $user_id);
    })->whereIn('id', $borrowed)->paginate($paginate);



    $borrowed_products = Product::with('user')->wherehas('borrower',function($q) use ($user_id){
        $q->where('status','Accept');
    })
    ->where('borrowed',1)
    ->where('user_id',$user_id)
    ->paginate($paginate);


    $created_community = Community::where('created_by', $user_id)->paginate($paginate);
        //echo "<pre>";print_r($created_community);die;
         //$joined_community = Community_user::where('user_id', $user_id)->get();
    $join = Community_user::join('communities','communities.id','=','community_user.community_id')
    ->where('community_user.user_id',$user_id)
    ->select('communities.name','communities.description','communities.com_image','communities.id','communities.created_by')
    ->distinct()
    ->paginate($paginate);
    $categories=\App\Category::get();

        // countries
    #$countries = Country::all(['id', 'name']);
    $fees=Membership::all();

        //messages

    $borrow_ids=Borrower::whereHas('product',function($q) use ($user_id){
        $q->where('user_id',$user_id);
    })->orWhere('user_id',$user_id)->where('status','Pending')->orWhere('status','Canceled')->orderBy('created_at','DESC')->get();


    $m= array();
        // tab messages

    foreach ($borrow_ids as $key => &$value) 
    {


        $data=BorrowerMessage::where('borrow_id',$value->id)->get();

        $value->last_message=BorrowerMessage::where('borrow_id',$value->id)->latest('created_at')->first();

        if (count($data)>0) 
        {
          $m[$value->id]=$data;

      }
  }




  if (request()->ajax()) 
  { 

    switch ($request->type) {
        case 'my_communities':
        return Response::json(View::make('site.pagination.joinCommunity', array('join'=>$join))->render());         
        break;

        case 'my_product':
        return Response::json(View::make('site.pagination.product', array('products'=>$products))->render());         

        break;

        case 'borrowed_products':

        return Response::json(View::make('site.pagination.borrowed', array('borrowed_products'=>$borrowed_products))->render());         

        break;

        case 'ordered_products':
        return Response::json(View::make('site.pagination.ordered_products', array('ordered_products'=>$ordered_products))->render());         


        break;


    }

}

        //return $borrowed_products;
return view('site.account', compact(
    'products',
    'join',
    #'allcommunity',
    'borrowed_products',
    'user',
    #'countries',
    #'states',
    #'allcities',
    #'cities',
    'created_community',
    'categories',
    #'countries',
    'ordered_products',
    'fees',
    'communities',
    'borrow_ids',
    'messages_collection'
))->with('isLoggedIn', $isLoggedIn);
}

/* Profile Update */
public function image(Request $request)
{
    $user_id = Auth::user()->id;
    $user = User::find($user_id);
    $image = $request->file('profile_img');
    if (isset($image)) {
        $user->profile_img = $image->getClientOriginalName();
        $destinationPath = public_path('/img/profile');
        $image->move($destinationPath, $image->getClientOriginalName());
    }
    $user->save();

    $message;
    Session::get('applocale') == 'en'?
    $message = 'Profile Image successfully updated!':
    $message = changeLang('en', 'es', 'Profile Image successfully updated!');

    $notification = array(
        'message' => $message,
        'alert-type' => 'success',
    );
    return redirect(route('account'))->with($notification);
}


public function profile(Request $request) {


    $user_id = Auth::user()->id;
    $user = User::find($user_id);
    $validations=[];
    if (isset($request->change_password) && $request->change_password==1) 
    {
       $validations["password"]='required|string|min:6|confirmed';
   }
   $validations["first_name"]='required';
   $validations["last_name"]='required';

   $validator = Validator::make($request->all(),$validations)->validate();


        /*
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors($validator);
            }*/

            if( $user->city_id != $request->cityUser ){

            /*
            foreach ($user->products as $product) {
                $product->update(['city_id' => $request->cityUser,'country_id'=>$request->countryUser]);
            }*/
            
            session(['cityvalue'=> $request->cityUser]);
            session(['countryvalue'=> $request->countryUser]);



        }


        $detached=Community::where('city_id',$user->city_id)->first();

        if (!empty($detached)) 
        {
            $detached->communityUsers()->detach($user);
            $detached->products()->detach($user->products()->get()->pluck('id'));

        }

        Product::where('user_id',$user->id)->update(['city_id' => $request->cityUser,'country_id'=>$request->countryUser]);
        


        $community=Community::where('city_id',$request->cityUser)->first();
        if (!empty($community)) 
        {
            $community->communityUsers()->attach($user);
            $community->products()->attach($user->products()->get()->pluck('id'));
        }


        #$country=Country::find($request->countryUser);
        #$code=str_replace('+','',$country->phonecode);
        $phone=$request->phone;

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        // $user->credit_card = $request->credit_card;
        $user->phone=$phone;
        $user->zip_code=$request->zip_code;
        $user->country_id = $request->countryUser;
        $user->state_id = $request->stateUser;
        $user->direction=$request->direction;
        $user->city_id = $request->cityUser;
        if (isset($request->change_password) && $request->change_password==1) 
        {
            $user->password=bcrypt($request->password);
        }
        if( $request->community != ""){
            $user->community_id = $request->community;
        }

        $image = $request->file('profile_img');
        if (isset($image)) {
            $user->profile_img = $image->getClientOriginalName();
            $destinationPath = public_path('/img/profile');
            $image->move($destinationPath, $image->getClientOriginalName());
        }

        $user->save();
        
        $message;
        $message = 'Perfil actualizado Exitosamente!';
/*
        Session::get('applocale') == 'es'?
        $message = 'Perfil actualizado Exitosamente!':
        $message = translate('Perfil actualizado Exitosamente!');*/

        $notification = array(
            'message' => $message,
            'alert-type' => 'success',
        );

        if($request->community != ""){
            $community = new Community_user;
            $community->user_id = Auth::user()->id;
            $community->community_id = $request->community;
            $community->save();
        }


        return response()->json(["success"=>true,"message"=>"Account data updated successfully"]);
       // return redirect(route('account'))->with($notification);
    }

    public function privacy()
    {
        $isLoggedIn = Auth::check();
        $user = Auth::user();
        return view('site.privacy')->with('isLoggedIn', $isLoggedIn)->with('user', $user);
    }

    public function lenders()
    {
        return view('site.lenders');
    }
    public function borrows()
    {
        return view('site.borrows');
    }
    public function getState($id){
        $state = State::where('country_id',$id)->get();
        $option = "<option> Select State</option>";
        foreach ($state as $key => $value) {
            $option .= '<option value="'.$value->id.'">'.$value->name.'</option>';
        }
        return $option;
    }

    public function getCity($id){
        $city = City::where('state_id',$id)->get();


        $option = "<option> Select City</option>";
        foreach ($city as $key => $value) {
            $option .= '<option value="'.$value->id.'">'.$value->name.'</option>';
        }
        return $option;
    }

    ////////////////////////////////////////////////////
    //               Añadir Ciudad                    //
    ////////////////////////////////////////////////////

    public function addCity(Request $request)
    {

        if( 
            City::where('name',ucfirst($request->city))
            ->where('state_id', $request->state)
            ->where('country_id', $request->country)
            ->first() != null 
        ){
            $message;
            $message = 'La ciudad ya se encuentra registrada';
            /*
            Session::get('applocale') == 'es' ?
            $message = 'La ciudad ya se encuentra registrada':
            $message = translate('La ciudad ya se encuentra registrada');*/
            
            return response()->json([
                "errors" => [
                    "CityAlreadyExists" => [$message]
                ]
            ], 422);
        }

        $city= City::create([
            "name" => ucfirst($request->city),
            'email' => $request->user()->email,
            'country_id' =>$request->country,
            'state_id' => $request->state,
            'status'=> 0,
            'created_at' => now()
        ]);



        $message;
        Session::get('applocale') == 'en' ?
        $message = "City pending approval":
        // $message = changeLang('en', 'es', "We will let you know once your city will be listed.");
        $message = "City pending approval";

        return response()->json(['message' => $message]);
    }

    ////////////////////////////////////////////////////
    // Añadir correos   subcripcion de noticias ilendu //
    ////////////////////////////////////////////////////

    public function newsletter(Request $request)
    {   
        $request->validate([
            'email' => 'required | email'
        ]);

        $x = DB::table('newsletter')->where('email',$request->email)->exists();
        $a = DB::table('newsletter')->select('*')->where('email',$request->email)->get();

        $message;
        Session::get('applocale') == 'en'?
        $message = 'You are Successfully Subscribed for Ilendu newsletter':
        $message = 'Suscrito con éxito al boletín de Ilendu!';

        $data = array(
            'toEmail' => $request->email,
            'subscription' =>1,
        );

        if($x == '')
        {
            Mail::send('emails.newsletter', $data, function ($message) use ($data){
                $message->to($data['toEmail']);
                $message->subject('Ilendu newsletter subscription');
            });

            $newsletter = DB::table('newsletter')->insert([
                'email' =>$request->email ,
                'subscription' =>1,
            ]);

            return response()->json(['message' => $message ], 200); 

        }
        elseif( $a[0]->subscription == 0)
        {
            Mail::send('emails.newsletter', $data, function ($message) use ($data){
                $message->to($data['toEmail']);
                $message->subject('Ilendu newsletter subscription');
            });

            $newsletter = DB::table('newsletter')->update([
                'email' =>$request->email ,
                'subscription' =>1,
            ]);

            return response()->json(['message' => $message], 200); 

        }
        else
        {
          // return redirect('home')->with('message','You are already subscribed for ilendu newsletter');
            return response()->json(['message' => $message], 200); 
        }

    }

    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////

    public function unsubscribe($email)
    {
        $a = DB::table('newsletter')->select('*')->where('email',$email)->get();
        $data = array(
            'toEmail' => $email,
            'subscription' => 0,
        );
        Mail::send('emails.newsletter', $data, function ($message) use ($data){
            $message->to($data['toEmail']);
            $message->subject('Ilendu newsletter subscription');
        });
   // print_r($email);die;
        $newsletter = DB::table('newsletter')->where('email',$email)->update([
            'subscription' => 0 ,
        ]);

        return redirect('home')->with('message','You are successfully unsubscribed for ilendu newsletter');
    }

    public function prodsearch(Request $request,$search=null)
    {

        $s=$search;
        $isLoggedIn = Auth::check();
        $user = Auth::user();

        $categoryName;
        Session::get('applocale') === 'en'?
        $categoryName = 'categories.name':
        $categoryName = 'categories.name_spanish';

        $user_country=Session::get('countryvalue');
        $cid=Session::get('cityvalue');
        $united_states=231;

        if($search == ""){

            Session::forget('categorysearch');
            $products = 
            DB::table('products')
            ->join('categories','categories.id','=','products.product_category')
            ->join('users','users.id','=','products.user_id')
            ->select(
                'products.product_name',
                'products.id',
                'products.user_id',
                'products.product_img',
                'categories.name',
                'users.first_name',
                'users.last_name',
                'users.facebook_id',
                'users.profile_img',
                'products.is_premium',
                'products.give_away'
            )
            ->where('products.is_active',1)
            ->where('products.in_front',1);
            if ($request->search_type=="cities") 
            {
                $products=$products->where('products.city_id', $cid)
                ->where('products.country_id',$user_country);

            }elseif ($request->search_type=="country") 
            {
                $products=$products->where('products.country_id',$user_country);
            }elseif ($request->search_type=="united") 
            {

                $products=$products->where('products.country_id',$united_states);

            }

            $products=$products->where('products.city_id', Session::get('cityvalue'))
            ->orderBy('products.created_at','DESC')
            ->paginate(9);


            //if there is not products
            if (count($products)==0) 
            {


                $products = 
                DB::table('products')
                ->join('categories','categories.id','=','products.product_category')
                ->join('users','users.id','=','products.user_id')
                ->select(
                    'products.product_name',
                    'products.id',
                    'products.user_id',
                    'products.product_img',
                    'categories.name',
                    'users.first_name',
                    'users.last_name',
                    'users.facebook_id',
                    'users.profile_img',
                    'products.is_premium',
                    'products.give_away'
                )
                ->where('products.is_active',1)
                ->where('products.in_front',1);
                $products=$products->where('products.country_id',$united_states)
                ->orderBy('products.created_at','DESC')
                ->paginate(9);

            }

            return view('site.homecategory',compact('products','isLoggedIn','user'));
        }

        $products = 
        DB::table('products')
        ->join('categories','categories.id','=','products.product_category')
        ->join('users','users.id','=','products.user_id')
        ->select(
            'products.product_name',
            'products.id',
            'products.user_id',
            'products.product_img',
            'categories.name',
            'users.first_name',
            'users.last_name',
            'users.facebook_id',
            'users.profile_img',
            'products.is_premium',
            'products.give_away'

        )
        ->where('products.is_active',1)
        ->where('products.in_front',1);
        if ($request->search_type=="cities") 
        {
            $products=$products->where('products.country_id',$user_country);

        }elseif ($request->search_type=="country") 
        {

            $products=$products->where('products.country_id',$user_country);
        }elseif ($request->search_type=="united") 
        {

            $products=$products->where('products.country_id',$united_states);

        }
        $products=$products->where('products.product_name','like','%'.$s.'%')->orderBy('products.created_at','DESC')
        ->paginate(9);


             //if there is not products
        if (count($products)==0) 
        {
           $products = 
           DB::table('products')
           ->join('categories','categories.id','=','products.product_category')
           ->join('users','users.id','=','products.user_id')
           ->select(
            'products.product_name',
            'products.id',
            'products.user_id',
            'products.product_img',
            'categories.name',
            'users.first_name',
            'users.last_name',
            'users.facebook_id',
            'users.profile_img',
            'products.is_premium',
            'products.give_away'

        )
           ->where('products.is_active',1)
           ->where('products.in_front',1);
           $products=$products->where('products.country_id',$united_states);
           $products=$products->where('products.product_name','like','%'.$s.'%')
           ->orderBy('products.created_at','DESC')
           ->paginate(9);

       }


        // dd($products);
       $products->setPath(url('/').'/prodsearch/'.$search);

       return view('site.homecategory',compact('products','isLoggedIn','user', 'products'));
   }

   public function catsearch($search=null)
   {
    session(['catsearch'=> $search]);
    $s= explode(', ', $search);
    $isLoggedIn = Auth::check();
    $user = Auth::user();

    $categoryName;
    Session::get('applocale') === 'en'?
    $categoryName = 'categories.name':
    $categoryName = 'categories.name_spanish';

    if($search == ""){

        Session::forget('categorysearch');

        $products = 
        DB::table('products')
        ->join('categories','categories.id','=','products.product_category')
        ->join('users','users.id','=','products.user_id')
        ->select(
            'products.product_name',
            'products.id',
            'products.user_id',
            'products.product_img',
            'categories.name',
            'users.first_name',
            'users.last_name',
            'users.facebook_id',
            'users.profile_img'
        )
        ->where('products.is_active',1)
        ->where('products.in_front',1)
        ->where('products.city_id', Session::get('cityvalue'))
        ->paginate(9);

        return view('site.homecategory',compact('products','isLoggedIn','user'));
    }

    $products = 
    DB::table('products')
    ->join('categories','categories.id','=','products.product_category')
    ->join('users','users.id','=','products.user_id')
    ->select(
        'products.product_name',
        'products.id',
        'products.user_id',
        'products.product_img',
        'categories.name',
        'users.first_name',
        'users.last_name',
        'users.facebook_id',
        'users.profile_img'
    )
    ->where('products.is_active',1)
    ->where('products.in_front',1)
    ->where('products.city_id', Session::get('cityvalue'))
    ->whereIn($categoryName, $s)
    ->paginate(9);
        // dd($products);
    $products->setPath(url('/').'/catsearch/'.$search);

    return view('site.homecategory',compact('products','isLoggedIn','user', 'products'));
}

public function setcity($city_id){
    session(['cityvalue'=> $city_id]);

    return response()->json(['success'=>true,'data'=>session('cityvalue')]);
}
}
