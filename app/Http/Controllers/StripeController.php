<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use URL;
use Session;
use Redirect;
use Input;
use App\User;
use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;
use Auth,Carbon;
use App\Transaction;
use App\UserPlan;
use Crypt;
use App\ShippingRecord;
use App\Borrower;
use App\Membership;

use Mail;

class StripeController extends Controller
{


public function payWithStripe()
{
 return view('paywithstripe');
}


public function charge($id)
{

	  $id = Crypt::decrypt($id);    
	  $transaction=Transaction::find($id);


    return view('site.product.charge_success', ['id' => $transaction['id'],'date'=>$transaction['created_at'],'amount'=>$transaction['price']]);

}


public function postPaymentWithStripe(Request $request)
 {

 $validator = Validator::make($request->all(), [
 'card_no' => 'required',
 'ccExpiryMonth' => 'required',
 'ccExpiryYear' => 'required',
 'cvvNumber' => 'required',
 'amount' => 'required',
 ])->validate();
 $input = $request->all();



 $input = array_except($input,array('_token'));
 
 $stripe = Stripe::make('sk_test_hMHQb50MvSTceiF00kY1GeKQ');
 try {
$user=User::find(auth()->user()->id);

$customer = $stripe->customers()->create([
    'email' =>$user->email,
    'name'=>$user->first_name.' '.$user->last_name,
    'phone'=>$user->phone
]);


 $token = $stripe->tokens()->create([
 'card' => [
 'number' => $request->get('card_no'),
 'exp_month' => $request->get('ccExpiryMonth'),
 'exp_year' => $request->get('ccExpiryYear'),
 'cvc' => $request->get('cvvNumber'),
 ],
 ]);


 $card = $stripe->cards()->create($customer['id'], $token['id']);

 $user->customerId=$customer['id'];
 $user->save();
 
 // $token = $stripe->tokens()->create([
 // ‘card’ => [
 // ‘number’ => ‘4242424242424242’,
 // ‘exp_month’ => 10,
 // ‘cvc’ => 314,
 // ‘exp_year’ => 2020,
 // ],
 // ]);
if (!isset($token['id'])) {
$error = \Illuminate\Validation\ValidationException::withMessages([
   'error' => ['Ha ocurrido un error'],
]);
throw $error;
 }

$charge = $stripe->charges()->create([
    'customer' => $user->customerId,
    'currency' => 'USD',
    'amount' =>floatval($request->amount),
    'description' => 'Add in wallet',
]);


 
 if($charge['status'] == 'succeeded') {

      $user=User::find(auth()->user()->id);
      $user->is_premium=1;
      $user->save();

      $_expire_date=Carbon\Carbon::parse($user->expire_date)->addMonths(1);


      $user->expire_date=$_expire_date;
      $user->save();

      $user_plan=UserPlan::create([
        "user_id"=>auth()->user()->id,
        "plan_id"=>$request->plan_id
      ]);

      $transaction = new Transaction;
      $transaction->transaction_id = $charge["id"];
      $transaction->user_id = Auth::user()->id;
      $transaction->price = $charge["amount"]/100;
      $transaction->raw_data = json_encode($charge);
      $transaction->plan_id=$request->plan_id;
      $transaction->status = 1;
      $transaction->save();
      $date=date("Y-m-d H:i:s", $charge['created']);

      switch ($request->type) {
        case 'dispatch':
          
          $s=ShippingRecord::where("borrow_id",$request->borrow_id)->first();
          $s->transaction_id=$transaction->id;
          $s->status=1;
          $s->save();

          $b=Borrower::find($request->borrow_id);
          $b->status="PaidOut";
          $b->save();

          $data = array('toEmail' => $s->borrower->user->email,
            'toName' => $s->borrower->user->first_name,
            'product_name' => $s->borrower->product->product_name,
            'product_img' => $s->borrower->product->product_img,
            'first_name' => $s->borrower->user->first_name,
            'last_name' => $s->borrower->user->last_name,
            'product_id'=>$s->borrower->product->id,
            'email' => $s->borrower->user->email,
            'weight'=>$s->weight,
            'price'=>$s->price,
            'dispatch_date'=>$s->dispatch_date,
            'total'=>$s->total
        );



        Mail::send('emails.dispatch_receipt_email', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['toName']);
            $message->from('noreply@ilendu.com','Ilendu');
            $message->subject('receipt of payment');
        });

        Mail::send('emails.dispatch_receipt_admin_email', $data, function ($message) use ($data) {
            $message->to('admin@ilendu.com','Ilendu');
            $message->from('noreply@ilendu.com','Ilendu');
            $message->subject('receipt of payment');
        });

          break;
        
      
      }

      $plan=Membership::find($request->plan_id);

          $data = array('toEmail' => $user->email,
            'toName' => $user->first_name,
            'plan' => $plan,
            'expire_date'=>$user->expire_date,
            'total'=>$plan->fees
        );

  Mail::send('emails.premium_receipt_email', $data, function ($message) use ($data) {
            $message->to($data['toEmail'], $data['toName']);
            $message->from('noreply@ilendu.com','Ilendu');
            $message->subject('receipt of payment');
        });


      return response()->json(['success'=>true,'id'=>Crypt::encrypt($transaction->id)]);
 } else {

 $error = \Illuminate\Validation\ValidationException::withMessages([
   'error' => ['Money not add in wallet!!'],
]);
throw $error;
 }
 } catch (Exception $e) {

 $error = \Illuminate\Validation\ValidationException::withMessages([
   'error' => [$e->getMessage()],
]);
throw $error;

 } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {

  $error = \Illuminate\Validation\ValidationException::withMessages([
   'error' => [$e->getMessage()],
]);
throw $error;	
 } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
   $error = \Illuminate\Validation\ValidationException::withMessages([
   'error' => [$e->getMessage()],
]);
throw $error;	
 }
 
 }
}
