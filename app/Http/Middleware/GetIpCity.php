<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class GetIpCity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ip_address=$_SERVER['REMOTE_ADDR'];
        $geopluginURL='http://www.geoplugin.net/php.gp?ip='.$ip_address;
        $addrDetailsArr = unserialize(file_get_contents($geopluginURL));
        $city = $addrDetailsArr['geoplugin_city'];
        //$country = $addrDetailsArr['geoplugin_countryName'];
        session(['ipcity' => $city]);
        return $next($request);
    }
}
