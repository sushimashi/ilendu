<?php
/**
 * Created by PhpStorm.
 * User: smiith
 * Date: 15/7/2018
 * Time: 2:59 AM
 */

namespace App\Services\Menu;


use Illuminate\Support\Facades\Log;

class MenuServices
{
    public function isActive($url, $other, $exact = false)
    {
        $result = "";
        if ($exact) {
            if ($other === $url) {
                $result = "active";
            }
        } else {
            $url = substr($url, 1);
            $other = substr($other, 1);
            $other = str_replace("/", '\/', $other);
            $pattern = '/^' . $other . '/i';
            if (preg_match($pattern, $url)) {
                $result = "active";
            }
        }

        return $result;
    }
}