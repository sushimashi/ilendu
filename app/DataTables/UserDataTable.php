<?php

namespace App\DataTables;

use App\User;
use Yajra\DataTables\Services\DataTable;
use Yajra\Datatables\Datatables;

class UserDataTable extends DataTable
{
    public function query()
    {
        $users = User::query();
        return $this->ajax($users);
    }

    public function ajax($users)
    {
        return Datatables::of($users)
            ->addColumn('actions', function ($users)
            {
                $actions = 
                // ver usuario
                '<a data-toggle="modal" href="#showUserModal" data-user_id="'.$users->id.'" class="btn bg-white fa fa-eye showUser" title="Ver"></a>'.
                // modificar usuario
                '<a data-toggle="modal" href="#showUserModal" data-user_id="'.$users->id.'" class="btn bg-white fa fa-pencil modifyUser" title="Modificar"></a>'.
                // eliminar usuario
                '<a data-user_id="'.$users->id.'" class="btn btn-danger fa fa-close _delete" title="Eliminar"></a>';
                return $actions;
            })
            ->rawColumns(['id', 'email', 'name', 'actions'])
            ->make(true);
    }
}
