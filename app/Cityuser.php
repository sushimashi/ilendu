<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cityuser extends Model
{
  protected $table = 'city_user';
   public function country(){
    	return  $this->hasOne('App\Country','id','country_id');
    }

 public function state(){
    	return  $this->hasOne('App\State','id','state_id');
    }

 public function city(){
    	return  $this->hasOne('App\City','id','city_id');
    }

}
