<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	public $table = 'cities';
	protected $fillable = [
		'name',
		'state_id',
		'country_id',
		'email',
		'status'
	];
	protected $rules = [
		'name' => 'required',
		'state_id' => 'required'
	];

	
    public $timestamps = false;

    // Ciudad
	public function country()
	{
		return $this->belongsTo(Country::class, 'country_id', 'id');
	}

	// Estado
	public function state()
	{
		return $this->belongsTo(State::class, 'state_id', 'id');
	}
	
	// Productos
    public function products()
    {
        return $this->hasMany('App\Product','city_id');
    }

    // usuarios
    public function users()
    {
         return $this->hasMany(User::class, 'city_id', 'id');
    }

}
