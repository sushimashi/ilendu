<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrower extends Model
{

	protected $fillable=["expiration_notify","user_id","product_id","status","expire_date","notified","message"];
       public function user()
    {
    	return  $this->hasOne('App\User','id','user_id');
    }


         public function product()
    {
    	return  $this->hasOne('App\Product','id','product_id');
    }
}
