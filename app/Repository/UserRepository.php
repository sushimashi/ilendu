<?php
namespace App\Repository;
use App\User as User;
use Auth0\Login\Contract\Auth0UserRepository as Auth0UserRepository;

class UserRepository implements Auth0UserRepository {
	public function getUserByDecodedJWT($jwt) {
		$jwt->user_id = $jwt->sub;
		return $this->upsertUser($jwt);
	}
	public function getUserByUserInfo($userInfo) {
		return $this->upsertUser((object) $userInfo['profile']);
	}
	/**
	 * Check if user is in database, if not create.
	 *
	 * @return User
	 */
	protected function upsertUser($profile) {
		$sub_id = explode("|", $profile->sub);
		$user = User::where("email",$profile->email)->first();
		// create user if not in database
		if ($user === null) {
			$user = new User();
			if ($profile->email != "") {
				$user->email = $profile->email;
			} else {
				$user->email = $profile->name;
			}
			$user->auth0id = $profile->sub;
			$user->name = $profile->name;
			// random password, we dont need it
			$user->password = md5(time());
			$user->save();
		}
		return $user;
	}
	public function getUserByIdentifier($identifier) {
		//Get the user info of the user logged in (probably in session)
		$user = \App::make('auth0')->getUser();
		if ($user === null) {
			return null;
		}

		// build the user
		$user = $this->getUserByUserInfo($user);
		// it is not the same user as logged in, it is not valid
		if ($user && $user->id == $identifier) {
			return $user;
		}
	}
}