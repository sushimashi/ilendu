<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAmazon extends Model
{
    // Set primary key
    protected $primaryKey = 'product_id';
    
    public $table = 'products_amazon';

    // Assignable categories
    protected $fillable = [
        'ASIN', 'name', 'description', 'retail_price', 'offer_price', 'brand', 'category',
    ];
}
