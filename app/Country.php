<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $table = 'countries';
    protected $fillable = ['name','iso3','iso2','phone_code','capital','currency'];
    protected $rules = ['name' => 'required'];

         public $timestamps = false;


    public function users()
    {
    	return $this->hasMany(User::class, 'country_id', 'id');
    }

    // estados
    public function states()
    {
        return $this->hasMany('App\State','country_id');
    }

}
