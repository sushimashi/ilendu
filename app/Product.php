<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	public $fillable = [
        "user_id",
        "product_name",
        "product_desc",
        "product_img", 
        "product_category",
        "is_premium",
        "is_active",
        "give_away", 
        "in_front", 
        "city_id",
        "return_term",
        "borrowed"
    ];

    public function category()
    {
    	return  $this->hasOne('App\Category','id','product_category');
    }

       public function borrower()
    {
        return  $this->hasOne('App\Borrower','product_id','id')->latest();
    }

       public function borrowers()
    {
        return  $this->hasMany('App\Borrower','product_id','id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    public function user()
    {
    	return  $this->hasOne('App\User','id','user_id');
    }

     public function communities()
    {
        return $this->belongsToMany('App\Community','product_communities','product_id','community_id');
    }

}
