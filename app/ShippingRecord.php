<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingRecord extends Model
{
        protected $table = 'shipping_records';
        
        protected $fillable=[
        	"weight",
        	"price",
        	"transaction_id",
        	"borrow_id",
        	"status",
        	"dispatch_date",
        	"total"
        ];

    public function borrower()
    {
        return  $this->hasOne('App\Borrower','id','borrow_id');
    }
}
