<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Community_user extends Model
{
    protected $table = 'community_user';
     public $timestamps =false;

     protected $fillable= ['user_id','community_id','is_approve'];
}
