<?php 

return [
	'submittedBy' => 'Presentado por',
	'addedBy' => 'Añadido por',
	'noProductsFound' => 'No se encontraron productos',
	'addProduct' => 'Añadir producto',
	'editProduct' => 'Editar producto',
	'noProductsAvailable' => 'No hay productos disponibles',
	'productImage' => 'Imagen de producto',
	'productDetails' => 'Detalles de Producto',
	'premiumProduct' => 'Producto Premium',
	'borrowProductRequest' => 'Su solicitud de prestamo de producto',
	'yourProduct' => 'Es tu producto',
	'productList' => 'Lista de Productos',
	 "multiple_products"=>"¡Añade fotos de tus productos y dales una descripcion despues!"

];