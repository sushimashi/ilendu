<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'success'=>'¡Éxito!',
    'successful' => '¡Tu transacción se ha realizado exitosamente. Gracias por confiar en nosotros!',
    'transaction_detail' => 'Detalles de la transacción',
    'transaction_id' => 'ID de la transacción:',
    'transaction_date' => 'Fecha de la transacción:',
    'amount' => 'Monto:',
    'back' => 'Volver',

];
