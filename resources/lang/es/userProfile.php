<?php 

return [
	'messages'=>'Mensajes',
	'firstName' => 'Nombres',
	'lastName' => 'Apellidos',
	'email' => 'Correo Electronico',
	'profileImage' => 'Imagen de Perfil',
	'account' => 'Cuenta',
	'myProducts' => 'Mis Productos',
	'myCommunities' => 'Mis Comunidades',
	'orderedProducts' => 'Productos ordenados',
	'joinedCommunities' => 'Mis Comunidades (miembro)',
	'noJoinedCommunities' => 'No pertenece a ninguna comunidad',
    'plans'=>'Planes'
];