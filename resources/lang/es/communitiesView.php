<?php

return [
	'communities' =>'Comunidades', 
	'community' =>'Comunidad', 
	'addCommunity' =>'Añadir comunidad', 
	'joinedCommunities' =>'Comunidades (miembro)', 
	'noCommunitiesAvailable' =>'No has creado ninguna comunidad aun', 
	'noProductsAddedInCommunity' =>'No hay productos agregados en esta comunidad', 

];