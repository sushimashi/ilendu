<?php

return [
	'addCity'=>'Add City',
	'country'=>'Country',
	'state'=>'State',
	'city'=>'City',
	'selectCountry'=>'Select Country',
	'selectState'=>'Select State',
	'selectCity'=>'Select City',
	'requiredCity'=>'please, add a City',

];