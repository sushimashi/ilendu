<?php

return [
	'submittedBy' => 'Submitted by',
	'addedBy' => 'Added by',
	'noProductsFound' => 'No products found',
	'addProduct' => 'Add product',
	'editProduct' => 'Edit product',
	'noProductsAvailable' => 'No products available',
	'productImage' => 'Product image',
	'productDetails' => 'Product Details',
	'premiumProduct' => 'Premium Product',
	'borrowProductRequest' => 'Your Borrow Product Request',
	'yourProduct' => 'It\'s your own product',
	'productList' => 'Product List',
	"multiple_products"=>"¡Add photos of your products and give them a description later!"

];