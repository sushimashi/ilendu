<?php 

return [
	'messages'=>'Messages',
	'firstName' => 'first name',
	'lastName' => 'last name',
	'email' => 'email',
	'profileImage' => 'profile image',
	'account' => 'Account',
	'myProducts' => 'My products',
	'myCommunities' => 'My communities',
	'orderedProducts' => 'Ordered products',
	'joinedCommunities' => 'My communities (member)',
	'noJoinedCommunities' => 'Does not belong to any community',
	'plans'=>'Plans'
];