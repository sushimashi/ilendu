<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */
    'success'=>'¡Success!',
    'successful' => '¡Your transaction was successful. Thanks for trusting us!',
    'transaction_detail' => 'Transaction detail',
    'transaction_id' => 'Transaction id',
    'transaction_date' => 'Transaction Date',
    'amount' => 'Amount',
    'back' => 'Go back',
    

];
