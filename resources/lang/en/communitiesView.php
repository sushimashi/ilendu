<?php

return [
	'communities' =>'Communities', 
	'community' =>'Community', 
	'addCommunity' =>'Add community', 
	'joinedCommunities' =>'Communities (member)', 
	'noCommunitiesAvailable' =>'You don\'t have created any communities yet',
	'noProductsAddedInCommunity' =>'No Product(s) added in this community', 
 
];