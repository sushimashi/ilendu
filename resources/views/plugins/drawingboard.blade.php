@push('css')
<link rel="stylesheet" href="{{asset("plugins/drawingboard/drawingboard.min.css")}}">
   
@endpush

@push('js')
<script src="{{asset("plugins/drawingboard/drawingboard.min.js")}}"></script>
@endpush
