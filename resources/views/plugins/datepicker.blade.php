@push('css')
<link rel="stylesheet" href="{{asset('plugins/datepicker/css/bootstrap-datepicker3.min.css')}}">
@endpush

@push('js')
<script src="{{asset('plugins/datepicker/js/bootstrap-datepicker.js')}}"></script>
<script src="{{asset('plugins/datepicker/js/bootstrap-datepicker.es.min.js')}}"></script>
<script>
    $(function () {
        cargar_datepicker();

    });
    function cargar_datepicker()
    {
        var campos_fecha = $(".datepicker");
        var campos_fecha_mes = $(".datepicker_mes");
        var campos_fecha_años = $(".datepicker_años");
        var campos_fecha_decadas = $(".datepicker_decadas");
       

        if (campos_fecha.length > 0) {
            campos_fecha.datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                language: "es",
                autoclose: true,
                todayHighlight: true,
            });
        }
        if (campos_fecha_mes.length > 0) {
            campos_fecha_mes.datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                language: "es",
                autoclose: true,
                todayHighlight: true,
                startView: 1,
            });
        }
        if (campos_fecha_años.length > 0) {
            campos_fecha_años.datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                language: "es",
                autoclose: true,
                todayHighlight: true,
                startView: 2,
            });
        }
        if (campos_fecha_decadas.length > 0) {
            campos_fecha_decadas.datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                language: "es",
                autoclose: true,
                todayHighlight: true,
                startView: 3,
            });
        }


    }
</script>
@endpush