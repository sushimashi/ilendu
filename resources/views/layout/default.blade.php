<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">



    @section('meta')
    <meta property="og:title" content="¿Para qué comprar cosas que no usaras? En iLendu.co puedes compartir con tu comunidad las cosas que no siempre usas de forma segura">
    <meta property="og:description" content=" En iLendu.co puedes compartir y pedir prestado lo que quieras a tu comunidad, ahorrando y ayuda al medio ambiente">
    <meta property="og:type" content="article" />
    <meta property="og:image" content="https://ilendu.co/">
    <meta property="og:url" content="https://ilendu.co/">
    <meta property="og:site_name" content="iLendu.coInc.">
    @show

    <meta name="twitter:title" content="iLendu.co, Never buy, share... ">
    <meta name="twitter:description" content="En iLendu.co puedes compartir con tu comunidad las cosas que no siempre usas de forma segura, ahorrando y ayuda al medio ambiente” " >

    <meta name="twitter:image" content="http://ilendu.env/img/1200px-Wii_console.png">
    <meta name="twitter:image:alt" content="1200px-Wii_console.png">
    <meta name="twitter:card" content="http://ilendu.env/img/1200px-Wii_console.png">
    <meta name="twitter:site" content="@website-username">
    <meta property="fb:app_id" content="your_app_id" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" type="img/png" href="/favicon/favicon-96x96.png">

    <title>Ilendu</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome/css/all.css') }}">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.css') }}"  />
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/datepicker/css/bootstrap-datepicker3.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.feedBackBox.css')}}">
    <input type="hidden" id="feed_url" name="url" value="{{url()->current()}}" >

    <!-- Custom styles for this template -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    {{Html::style("css/theme.css")}}


        <!-- Bootstrap core CSS -->
        <!-- <link rel="stylesheet" href="css/bootstrap.min.css"> -->
        <link rel="stylesheet" type="text/css" href="{{url('/')}}/css/theme1.css">
        <link rel="stylesheet" type="text/css" href="{{url('/')}}/css/new_index.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <!-- Custom styles for this template -->
        <!-- <link href="https://ilendu.co/css/app.css" rel="stylesheet"> -->
    
<style>

@media screen and ( max-width: 400px ){

    li.page-item {

        display: none;
    }

    .page-item:first-child,
    .page-item:nth-child( 2 ),
    .page-item:nth-last-child( 2 ),
    .page-item:last-child,
    .page-item.active,
    .page-item.disabled {

        display: block;
    }
}
.hide{
    display: none
}
.search {
  overflow: hidden;
  background-color: #e9e9e9;
}

.search .search-container {
    float: right;
    border: 1px solid #8888812b;
}

.search input[type=text] {
  padding: 5px 15px !important;
  font-size: 17px !important; 
  border: none !important;
}

.search .search-container button {
  float: right !important;
  padding: 6px 10px !important;
  background: #ddd !important;
  font-size: 17px !important;
  border: none !important;
  cursor: pointer !important;
}

.search .search-container button:hover {
  background: #ccc !important;
}

@media screen and (max-width: 600px) {
  .search .search-container {
    float: none !important;
  }
  .search a, .search input[type=text], .search .search-container button {
    float: none !important;
    display: block !important;
    text-align: left !important;
    width: 100% !important;
    margin: 0 !important;
    padding: 14px !important;
  }
  .search input[type=text] {
    border: 1px solid #ccc !important;  
  }
}



.filter-button
{
    font-size: 18px;
    border: 1px solid #42B32F;
    border-radius: 5px;
    text-align: center;
    color: #42B32F;
    margin-bottom: 30px;

}
.filter-button:hover
{
    font-size: 18px;
    border: 1px solid #42B32F !important;
    border-radius: 5px;
    text-align: center;
    color: #ffffff;
    background-color: #42B32F ;

}
.btn-default.active
{
    background-color: #42B32F !important;
    color: white !important;
}
.filter{
    margin-bottom: 30px !important;
}

.product h5 {
    margin-top: 15px !important;
    display: inline-block !important;
}

/* active current category in header*/
.currentCategory{
  text-decoration: underline !important;
  text-decoration-color: #F10080 !important;
}

</style>
    @stack('css')

  </head>
  <body>
    <!-- Header/Menu start here -->
    @include('layout.header')
    <!-- Header/Menu end here -->

    <main role="main">

        <input type="hidden" name="msg" id="msg" value="@if(isset($message)){{$message}}@endif">
        @yield('content')

        <div class="modal fade" id="cityModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9999;">
            @include('site.addCityModal')
        </div>

        <div class="clearfix"></div>
        @include('layout.footer')

    </main>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script  type="text/javascript" src="{{ asset('js/jquery-slim.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}" ></script>
    <script  type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}" ></script>
    <script type="text/javascript"  src="{{ asset('js/toastr.min.js') }}"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script type="text/javascript" src="{{ asset('js/boostrap-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.blockUI.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.feedBackBox.js') }}"></script>


    <!-- <script src="js/jquery.min.js"></script> -->
        <!-- <script src="js/bootstrap.min.js"></script> -->
    <script src="{{ asset('js/jquery.fancybox.min.js') }}"></script>
  <script src="{{ asset('js/select2.js') }}"></script>



  @yield('customjs')
 <!-- ================================================== -->
 @include('site.js.addCity')
 @include('site.loader')

 <!-- ================================================== -->

<?php   preg_match('/([a-z]*)@/i', request()->route()->getActionName(), $matches);
        $controllerName = $matches[1]; ?>
    <div id="feed_box"></div>

  </body>

  @yield('pagejavascripts')
    <script>
           $(document).ready(function () {
            $('#feed_box').feedBackBox();
        });

      @if(Session::has('message'))
        var type = "{{ Session::get('alert-type', 'info') }}";
        switch(type){
            case 'info':
                toastr.info("{{ Session::get('message') }}");
                break;

            case 'warning':
                toastr.warning("{{ Session::get('message') }}");
                break;

            case 'success':
                toastr.success("{{ Session::get('message') }}");
                break;

            case 'error':
                toastr.error("{{ Session::get('message') }}");
                break;
        }
      @endif
    </script>

  <script type="text/javascript">

    $(document).ready(function(){

        $('#state').select2({
            dropdownParent: $('#cityModal')
        });

        $('#country').select2({
            dropdownParent: $('#cityModal')
        });

    });

    function myFunction() {
        var msg = $('#msg').val();
        if(msg != "")
        toastr.success(msg);
        $('#msg').val('');
    }
    window.myFunction(); 

    $('.btn-number').click(function(e){
       
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
    });
    $('.input-number').focusin(function(){
       $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {
        
        minValue =  parseInt($(this).attr('min'));
        maxValue =  parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());
        
        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        
        
    });
        $(".input-number").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                 // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) || 
                 // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126210826-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-126210826-1');
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->

<script>


$('#country').change(function(){
    id = $(this).val();
    $.ajax({
        url: "{{url('/')}}/getstate/"+id,
        type: "get",
        success: function (response) {
           $('#state').html(response);
        }

    });
});

$('#state').change(function(){
    id = $(this).val();
    $.ajax({
        url: "{{url('/')}}/getcity/"+id,
        type: "get",
        success: function (response) {
           $('#citybyid').html(response);
        }

    });
});

$('#city').change(function(){

        id = $(this).val();
        var current_fun = '{{request()->route()->getActionMethod()}}';
        var current_con = '{{$controllerName}}';

        if(id == 'add'){
            $('#cityModal').modal('show');
            return false;
        }

        if(current_fun == 'index' && current_con == 'HomeController'){

            $.ajax({
                url: "{{url('/')}}/cityproduct/"+id,
                type: "get",
                success: function (response) {                    
                    $('.filterbtn').removeClass('active');
                    $('.check').prop('checked', false);
                    $('#myList > a:first-child').addClass('active');
                    $('#myInput').val('');
                    $('#cityproduct').html(response);
                }

            });
        }else{


            $.ajax({
                url: "{{url('/')}}/setcity/"+id,
                type: "get",
                success: function (response) {
                    location.reload(true);
                }

            });
        }

});

// seleccionar opcion "---" al cerra modal de añadir ciudad
$('#cityModal').on('hide.bs.modal', function(){   

    let citySelect = document.getElementById('city'),
        valueExists = false;

    Array.from(citySelect.options).forEach(function(element){
      if(element.value == '{{ Session::get('cityvalue')}}')
        valueExists = true;
    });
    
    if (valueExists){
      citySelect.value = '{{ Session::get('cityvalue')}}';
    }else{
      if ( citySelect.options >= 3){
        citySelect.options[3].selected = true;
      }else{
        citySelect.options[0].selected = true;              
      }
    }
});



// tildar checkbox de categoria activada al cargar pagina
/*let checkbox = $('.filterbtn.active').attr('data-filter');
$(`#${checkbox}`).prop('checked', true);
*/

////////////////////////////////////////////////////
// Añadir correos a subcripcion de noticias ilendu //
////////////////////////////////////////////////////


$('.suscribeNews').click(function(e){
    e.preventDefault();

    let data = $('#suscribeNewsForm').serialize();
    console.log(data);

    toastr.options.timeOut = 2500;
   $.ajax({
        url: $('#suscribeNewsForm').attr('action'),
        method: $('#suscribeNewsForm').attr('method'),
        data: data,
        success: function(result){
            toastr.success(result.message);
            $('#suscribeNewsForm').trigger('reset');
        },
        error: function (e) {
            console.log(e.responseJSON);
            $.each(e.responseJSON.errors, function (index, element) {
                if ($.isArray(element)) {
                    toastr.error(element[0]);
                }
            });
        }
    });

});


</script>
    <script src="{{asset('plugins/datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('plugins/datepicker/js/bootstrap-datepicker.es.min.js')}}"></script> 
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v6.0&appId=391610381767727&autoLogAppEvents=1"></script>
    @stack('js')

</html>
