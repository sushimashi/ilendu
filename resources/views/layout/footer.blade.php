
    <div class="signin_block_color" style="margin-top: 2%;">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-sm-6 text-center">
            <span class="signup_font">@lang('sentence.Newsletter')</span>
          </div>
          <div class="col-lg-6 col-sm-6 text-center">
            <div class="col-lg-8 offset-lg-2 mmt_15">
              <form id="suscribeNewsForm" action="{{url('newsletter')}}" method="post">
                {{ csrf_field() }}
                <div class="input-group">
                  <input type="email" name="email" placeholder="@lang('sentence.addEmail')" class="form-control email_text" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn_common suscribeNews" type="submit"><i class="fas fa-play"></i></button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- FOOTER -->
    <footer  class="footer_color">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-3 col-6">
                    <div class="ul_title">@lang('sentence.Our company')</div>
                    <ul class="list-inline footer_ul">
                        <li><a href="{{url('about')}}" class="footer_a">@lang('sentence.About us')</a></li>
                        <li><a href="{{url('security')}}" class="footer_a">@lang('sentence.Security')</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-sm-3 col-6">
                    <div class="ul_title">@lang('sentence.Support')</div>
                    <ul class="list-inline footer_ul">
                        <li><a href="{{url('faq')}}" class="footer_a">FAQ</a></li>
                        <li><a href="{{URL::to('privacy')}}" class="footer_a">@lang('sentence.Terms and Conditions')</a></li>
                        <li><a href="{{url('contact')}}" class="footer_a">@lang('sentence.Contact')</a></li>
                    </ul>
                </div>
                <div class="col-lg-6 col-sm-6 col-12 mm_text_center">
                    <div class="col-lg-8 offset-lg-2">
                        <img src="https://ilendu.co/img/ilendulogo.svg" alt=""><br/><br/>
                        <ul class="list-inline footer_social">
                            <li class="list-inline-item"><a href="#" target="_blank" class="footer_a"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="list-inline-item"><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <li class="list-inline-item"><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>

