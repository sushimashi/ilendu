<header>
      <nav class="navbar navbar-expand-md navbar-light fixed-top bg-light">
        <a class="navbar-brand" href="{{URL::to('/')}}"><img src="{{url('img/ilendulogo.svg')}}" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <!-- <li class="nav-item">
                  <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
            
                <li class="nav-item">
                  <a class="nav-link" href="{{route('product')}}">Catalog</a>
                </li>

                <li class="nav-item search">
                    <div class="search-container">
                        <form action="{{url('search')}}" method="get">
                            <input type="text" placeholder="Search.." name="search" value="@if(isset($search)) {{$search}} @endif">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </li> -->
                 <!--@if(Session::get('ipcity') != "")
                @endif -->

                <!-- PHP -->
                <?php 
                // Ciudades de usuario logueado con articulos
                  use Illuminate\Support\Facades\DB;

                   function citiesOfCurrentUser($user)
                  {
                    /*$sql = 'SELECT id, `name` FROM cities WHERE id IN
                          (SELECT city_id FROM products) 
                          AND id IN 
                          (SELECT id FROM cities WHERE country_id IN 
                            (SELECT country_id FROM users WHERE id = :user_id)
                          )';

                    $cities = DB::select( $sql, ['user_id' => $user_id]);*/

                    $user_country=$user["country_id"];
                    $cities=App\City::join("products","products.city_id","cities.id")->join("users","users.city_id","cities.id")->select('cities.id','cities.name')->where('users.country_id',$user_country)->groupBy('cities.name')->get();

                    $add=App\City::where('id',$user["city_id"])->first();

                    $cities->push($add);
                    $unique=$cities->unique();
                    return $unique;
                  }


                  if (auth()->user())
                  {
                                      $cities = citiesOfCurrentUser(auth()->user()); 

                  }
                  else
                  {
                    $cities=[];
                  }

                  if(empty($cities))
                  {
                    $cities=[];
                  }
                   
                ?>
                <!-- --- -->
                {{-- {{ dd(translate()) }} --}}

                @if(Auth::user())
                  <li>
                      <div class="selectm">

                          <select class="header_select" name="city" id="city" style="margin-left: 12px;margin-top: 5px">

                              {{--<option value="-" disabled>----------</option>
                              <option value="add">@lang('sentence.addCity')</option>
                              <option  disabled>----------</option>--}}
                              
                              @if(count($cities) > 0)

                                @for($i = 0; $i < count($cities) ;$i++)

                                    <option value="{{$cities[$i]->id}}" 
                                      <?php 
                                      echo (request()->route()->getActionMethod() == 'getProductsByCategory' ? "class='city_change'" : '');?>   
                                       >
                                        {{ $cities[$i]->name }}
                                    </option>

                                @endfor
                             
                              @endif
                              
                          </select>

                      </div>
                  </li>
                @endif

            </ul>

    <ul class="nav navbar-nav navbar-right">

         <li class="dropdown">
             <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">
                 <span class="caret"></span>&nbsp;
                 <span>
                     {{ Config::get('languages')[App::getLocale()] }}
                 </span>
             </a>
             <ul class="dropdown-menu language_ul">
                 <li>
                     <a href="{{ route('lang.switch', 'en') }}" class="dropdown-item" alt="English" id="en" > English
                     </a>
                 </li>
                 <li>
                     <a href="{{ route('lang.switch', 'es') }}" class="dropdown-item" alt="Spanish" id="es" > Spanish
                     </a>
                 </li>
             </ul>
         </li>

        @if(!@Auth::user()->id)     

             <li class="nav-item"><a href="{{ route('login') }}" class="nav-link signin-button login">@lang('sentence.Log in')</a></li>

             <li class="nav-item"><a href="{{ route('register') }}" class="nav-link signin-button login">@lang('sentence.Check in')</a></li> 

        @else

                <li class="nav-item"><a href="{{URL::to('communities')}}" class="nav-link signin-button login">
                @lang('sentence.Joincommunity')</a></li>

                <li class="nav-item"><a href="{{route('createproduct')}}" class="nav-link signin-button login">@lang('sentence.addProduct')</a></li>

                @if(Auth::user()->isAdmin())
                    {{-- <li class="nav-item"><a href="{{URL::to('city')}}" class="nav-link">User City</a></li> --}}
                    <li class="nav-item"><a href="{{ url('/admin') }}" class="nav-link signin-button login">@lang('keywords.dashboard')</a></li>
                    {{-- <li class="nav-item"><a href="{{URL::to('productlist')}}" class="nav-link signin-button login">Product List</a></li> --}}
                @endif

                <li class="nav-item"><a href="{{ route('account') }}" class="nav-link signin-button login">@lang('sentence.profile')</a></li>

                <li class="nav-item"><a href="{{ route('logout') }}" class="nav-link signin-button login">@lang('sentence.logout')</a></li>

          @endif

    </ul>
    
        </div>
      </nav>

      <?php 
        $categories = get_category_list();
        $city_id = @session('cityvalue') ? session('cityvalue') : '11111';
      ?>

      @if(count($categories) > 0)

          <ul class="navbar list-inline mb-1 index_ul">
           @if(Session::get('applocale') == 'es')
            @foreach($categories as $category)
                  <li class="list-inline-item index_li mr-3"><a class="@if( request()->path() == 'category/'.$category['id'])currentCategory @endif" href="{{URL::to('category')}}/{{$category['id']}}">{{$category['name_spanish']}}</a></li>
              @endforeach
              @else
              @foreach($categories as $category)
                  <li class="list-inline-item index_li mr-3"><a class="@if( request()->path() == 'category/'.$category['id'])currentCategory @endif" href="{{URL::to('category')}}/{{$category['id']}}">{{$category['name']}}</a></li>
              @endforeach
              @endif
          </ul>

      @endif

    </header>

@push('js')
    <script type="text/javascript">

      (function(){
          let citySelect = document.getElementById('city'),
              valueExists = false;

          Array.from(citySelect.options).forEach(function(element){
         //   console.log(element.value)
            if(element.value == '{{ Session::get('cityvalue')}}')
              valueExists = true;
          });
          
          if (valueExists){
            citySelect.value = '{{ Session::get('cityvalue')}}';
          }else{
            if ( citySelect.options >= 3){
              citySelect.options[3].selected = true;
              $('#city').trigger('change');              
            }else{
              citySelect.options[0].selected = true;              
            }
          }

      })();  
      
    </script>
@endpush