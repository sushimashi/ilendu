<script type="text/javascript">
    $(document).ready(function(){

    	// select2
$('#countryOfNewUser').select2({
  ajax: {
    url: '/api/country-select',
    dataType: 'json'
    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
  }
});




$('#cityOfNewUser').select2({
  ajax: {
    url: 'https://api.github.com/search/repositories',
    dataType: 'json'
    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
  }
});


        // agregar estados segun pais seleccionado
        $("#countryOfNewUser").on("change",function(){

            $('#stateOfNewUser').select2({
  ajax: {
    url: '/api/country-select/'+$(this).val()+'/states',
    dataType: 'json'
    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
  }
});

     
        });

        // agregar ciudades segun estado seleccionado
        $("#stateOfNewUser").on("change",function(){


                $('#cityOfNewUser').select2({
                  ajax: {
                    url: '/api/state-select/'+$(this).val()+'/cities',
                    dataType: 'json'
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                  }
                });


         

    
        });


        $("#register_form").on('submit',function(e){
            e.preventDefault();

              var formData = new FormData($(this)[0]);
$.blockUI({ message: '<h1><img src="/img/loader.gif" /></h1>' });

        $.ajax({
                cache: false,
                contentType: false,
                 processData: false,
            type: 'POST',
            url: $(this).attr('action'),
            data:formData,
            success: function (response) {

                if(response.success==true)
                {
            $.unblockUI();

                 toastr.success(response.message,"Exito!");

                 window.location.replace("/home");         
                }


            },
            error: function (e) {
                console.log(e);
                $.unblockUI();

                $.each(e.responseJSON.errors, function (index, element) {
                    if ($.isArray(element)) {
                        toastr.error(element[0]);
                    }
                });


            },
            complete: function(response)
            {

            }
        });

        })

    });        
</script>