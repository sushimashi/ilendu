@extends('layout.default')

@section('content')



<style type="text/css">
    .container{
        width: 100% !important;
        padding-right: 0px !important; 
        padding-left: 15px !important;
        margin-right: 5px !important;
    }
</style>
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 m-auto" style="background-image: url('/img/1.png'); background-repeat: no-repeat; background-size: cover;">
            <div class="col-md-4 m-auto">
            <div class="panel panel-default mt-0" style="margin-top: 2% !important; margin-bottom: 2%;">
                <div class="panel-heading">
                        <a href="{{ url('auth/facebook') }}" ><button class="btn btn-primary" style="width: 94%; background-color: #3B5998; border-color: #3B5998;"><i class="fab fa-facebook-f mr-2"></i> @lang('sentence.Facebook')</button> </a>
                       
                    </div>

                <div class="panel-body " >
                    <p style="text-align: center;">@lang('sentence.or')</p>
                    <form class="form-horizontal" id="register_form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        {{--  Name --}}
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-6 control-label">@lang('userProfile.firstName')</label>

                            <div class="col-md-12">
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}"  autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="help-block error">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         {{--Last  Name --}}
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-6 control-label">@lang('userProfile.lastName')</label>

                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}"  autofocus>

                                @if ($errors->has('last_name'))
                                    <span class="help-block error">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{-- Email --}}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-7 control-label">@lang('sentence.Emailaddress')</label>

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" >

                                @if ($errors->has('email'))
                                    <span class="help-block error">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                          {{-- phone --}}
                                 <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-7 control-label">Phone</label>

                            <div class="col-md-12">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" >

                                @if ($errors->has('phone'))
                                    <span class="help-block error">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>  


                         {{-- zip code --}}
                                 <div class="form-group{{ $errors->has('zip_code') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-7 control-label">Zip Code</label>

                            <div class="col-md-12">
                                <input id="phone" type="text" class="form-control" name="zip_code" value="{{ old('zip_code') }}" >

                                @if ($errors->has('zip_code'))
                                    <span class="help-block error">
                                        <strong>{{ $errors->first('zip_code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>  
                       

                        {{--  Country --}}
                        <div class="form-group">
                               <label class="col-md-8 control-label" for="countryOfNewUser">@lang('keywords.country')</label>
                               
                               <div class="col-md-12">
                                   <select class="form-control " name="countryOfNewUser" id="countryOfNewUser" >
                                         
                                   </select>
                                   <span class="help-block error">
                                       <strong>{{ $errors->first('countryOfNewUser') }}</strong>
                                   </span>
                               </div>

                           </div>
                        {{-- State  --}}
                           <div class="form-group">
                               <label class="col-md-8 control-label" for="stateOfNewUser">@lang('keywords.state')</label>

                               <div class="col-md-12">
                                   <select class="form-control " name="stateOfNewUser" id="stateOfNewUser" >
                                   </select>
                                   <span class="help-block error">
                                       <strong>{{ $errors->first('stateOfNewUser') }}</strong>
                                   </span>
                               </div>

                           </div>
                        {{-- City --}}
                           <div class="form-group">

                               <label class="col-md-8 control-label" for="cityOfNewUser">@lang('keywords.city')</label>
                               <div class="col-md-12">
                                   <select class="form-control " name="cityOfNewUser" id="cityOfNewUser" >
                                   </select>
                                   <span class="help-block error">
                                       <strong>{{ $errors->first('cityOfNewUser') }}</strong>
                                   </span>
                               </div>

                           </div>

                            {{-- Direction --}}
                           <div class="form-group">

                               <label class="col-md-8 control-label" for="direction">@lang('keywords.direction')</label>
                               <div class="col-md-12">
                                  
                                  <textarea  name="direction" class="form-control" id="direction"></textarea> 
                                   <span class="help-block error">
                                       <strong>{{ $errors->first('direction') }}</strong>
                                   </span>
                               </div>

                           </div>


                        {{-- Password --}}
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-6 control-label">@lang('sentence.Password')</label>

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control" name="password" >

                                @if ($errors->has('password'))
                                    <span class="help-block error">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        {{-- Password Confirm --}}
                        <div class="form-group">
                            <label for="password-confirm" class="col-md-8 control-label">@lang('sentence.confirm')</label>

                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" >
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-6">
                                <button type="submit" class="btn btn-primary" style="width: 100%">
                                    @lang('sentence.Check in')
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>
        <!--<div class="col-md-6 p-0">
            <img src="https://apod.nasa.gov/apod/fap/image/1604/MRS_6459schukar1024.jpg" width="100%" height="650px" class="img-fluid login_bg">
        </div>-->
    </div>
</div>
@endsection

@push('js')

   @include('auth.js.register')
@endpush
