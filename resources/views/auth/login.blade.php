@extends('layout.default')

@section('content')
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 m-auto" style="background-image: url('/img/1.png'); background-repeat: no-repeat; background-size: cover;">
            <div class="col-md-4 m-auto">
            <div class="panel panel-default mt-0" style="margin-top: 2% !important; margin-bottom: 2%;">
                <div class="panel-heading">
                        <a href="{{ url('auth/facebook') }}" ><button class="btn btn-primary" style="width: 94%; background-color: #3B5998; border-color: #3B5998;"><i class="fab fa-facebook-f mr-2"></i> @lang('sentence.Facebook')</button> </a>
                       
                    </div>

                <div class="panel-body" >
                    <p style="text-align: center;">@lang('sentence.or')</p>
                     <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            @if ($errors->has('error'))
                            <div class="col-md-12">
                               <span class="help-block error text-center">
                                <p>
                                                                                <strong>{{ $errors->first('error') }}</strong>

                                    
                                </p>
                                        </span>    
                            </div>
                                    
                                    @endif

                            <div class="form-group{{ $errors->has('error') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-7 control-label">@lang('sentence.Emailaddress')</label>

                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                    
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('error') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-6 control-label">@lang('sentence.Password')</label>

                                <div class="col-md-12">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-7 col-md-offset-6">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> @lang('sentence.remind')
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12 col-md-offset-6">
                                    <button type="submit" class="btn btn-primary" style="width: 100%">
                                       @lang('sentence.Login')
                                    </button>

                                    <a style="margin-top: 10px;display: none;" href="{{ url('auth/facebook') }}" class="btn btn-primary">
                                        <i class="fa fa-facebook"></i>@lang('sentence.Facebook')
                                    </a>  

                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                       @lang('sentence.Forgot')
                                    </a>
                                </div>
                            </div>
                           
                        </form>
                </div>
            </div>
            </div>
        </div>
        <!--<div class="col-md-6 p-0">
            <img src="https://apod.nasa.gov/apod/fap/image/1604/MRS_6459schukar1024.jpg" width="100%" height="650px" class="img-fluid login_bg">
        </div>-->
    </div>
</div>
@endsection
