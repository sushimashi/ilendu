<script type="text/javascript">
$(function(){



	$("#search_form").submit(function(e)
{
$(".loader").removeClass('hide');
$(".content-box").addClass('hide');

e.preventDefault();
$.ajax({
            async:true,   
            cache:false, 
            dataType: 'json',
            type: 'get',  
            url: $(this).attr('action'),
            data:$(this).serialize(),
            success: function(response)
            {
          $(".loader").addClass('hide');
		$(".content-box").removeClass('hide');

            $('#caja-busqueda').html(response);    

            },complete: function(response) 
            {
            
               
            }
            
        });
});

 $(document).on('click', '.pagination a', function (e) {
         e.preventDefault();

         page=$(this).attr('href').split('page=')[1];
         pageRaiz=$(this).attr('href');
         console.log(pageRaiz);
       	$(".loader").removeClass('hide');
		$(".content-box").addClass('hide');

          $.ajax({
            url :  pageRaiz,
            dataType: 'json',
            beforeSend: function(){
             
            },
        }).done(function (data) {
           $(".loader").addClass('hide');
		$(".content-box").removeClass('hide');

          //$(".loader").hide();
           console.log(data);
            $('#caja-busqueda').html(data);
           
            location.hash = page;
        }).fail(function () {
           // alert('Posts could not be loaded.');
        });
          
        });
})

</script>