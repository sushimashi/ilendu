@extends('layout.default')

@section('content')

<style type="text/css">
    a.white{
        color: white !important;
    }
    .center{
        width: 150px;
        margin: 40px auto;
    }
    .plusbutton{
        height: 38px;
        border-radius: 5px 0px 0px 5px;
        width: 40px;
    }
    .minusbutton{
        height: 38px;
        border-radius: 0px 5px 5px 0px;
        width: 40px;
    }
@media screen and ( max-width: 520px ){

    li.page-item {

        display: none;
    }

    .page-item:first-child,
    .page-item:last-child,
    .page-item.active {

        display: block;
    }
}
    
</style>
<br/>

<div class="product-grid">

  <div class="container">
        <div class="card"></br>  
             <div class="row">
                
                <h3 style="font-weight:bold;">&nbsp;&nbsp;&nbsp;@lang('productsView.productList'): {{  @session('applocale') === 'en'? $category->name : $category->name_spanish  }}</h3>                    
               
            </div><hr>
            <div class="card-body"><br/>

                <form id="search_form" action="{{url('/search-name')}}" method="get" >
                <div class="row">
                <input type="hidden" name="category_id" id="category_id" value="{{$category->id}}">
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="@lang('keywords.search')...">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <button class="btn btn-primary col-btn" type="submit" >@lang('keywords.search')</button>
                    </div>
                </div> 


                     <div class="col-md-5 col-sm-5 col-xs-12 form-group">
              <label class="labeltext">@lang('keywords.select_filter')</label><br>
                  <div class="form-check-inline">

                    <label class="customradio"><span class="radiotextsty">@lang('keywords.filter_premium')</span>
                      <input type="radio" class="filter_radio" name="filter_radio" value="premium">
                      <span class="checkmark"></span>
                    </label>        
                    <label class="customradio"><span class="radiotextsty">@lang('keywords.filter_gift')</span>
                      <input type="radio" class="filter_radio" name="filter_radio" value="gift">
                      <span class="checkmark"></span>
                    </label>
                    &nbsp; &nbsp; &nbsp; &nbsp;
                    
                    <label class="customradio"><span class="radiotextsty">@lang('keywords.filter_all')</span>
                      <input type="radio" class="filter_radio" name="filter_radio" checked value="all">
                      <span class="checkmark"></span>
                    </label>

                </div>
          </div>
               {{-- <div class="col-md-5">
                    <div class="form-group">
                    <label class="radio-inline"><input type="radio" name="filter_radio" value="premium" >Premium</label>
                     <label class="radio-inline"><input type="radio" name="filter_radio" value="gift">Gift</label>

                    </div>
                </div>--}}

                
                </div>

            </form>
               
                                 
                <br>

                <!-- pagination goes here-->
                <div id="caja-busqueda" >
                    @include('category.pagination.index')
                </div>
              
                <!-- end pagination-->

            </div>    
        </div>
    </div>
</div>

<style type="text/css">
  .morelink
  {
    display: none;
  }
</style>

@endsection

@section('pagejavascripts')
@include ('category.js.index')
<script type="text/javascript">
    
    $(function(){

        $(".filter_radio").on("click",function(){

        $(".loader").removeClass('hide');
        $(".content-box").addClass('hide');
            $.ajax({
            url:  '/category-filter',
            method:'get',
            data: {'filter':$(this).val(),'category_id':$("#category_id").val()},
            success: function(result){
             $(".loader").addClass('hide');
             $(".content-box").removeClass('hide');

            $('#caja-busqueda').html(result);    
            }, 
            error: function(e){
                console.log(e.responseJSON);
                $.each(e.responseJSON.errors, function (index, element) {
                    if ($.isArray(element)) {
                        toastr.error(element[0]);
                    }
                });
            }, complete: function(){
            }
        });
        })
    })
</script>
@stop