@foreach($product_communities as $product_community)

  <div class="col-lg-4 col-sm-6">

    <div class="card">

      <a href="{{ url('products/'.$product_community->id)}}">
        @if($product_community->product_img != "")
          <img class="card-img-top"  src="{{url('/')}}/img/thumbnail/{{ $product_community->product_img}}" alt="">
        @elseif($product_community->product_img == "")
          <img class="card-img-top"  src="{{url('/')}}/img/thumbnail/{{ $product_community->product_img}}" alt="">
        @endif
      </a>
      
      <div class="card-body">
          
        <p class="card-text">{{$product_community->product_name}}</p>

        <table>
          <td>
            @if( isset($product_community->profile_img) && file_exists(public_path("/img/profile/".$product_community->profile_img)))
            
              <img src="{{url('/')}}/img/profile/{{$product_community->profile_img}}" class="small_image">

            @else
              <img src="{{url('/')}}/img/profile/user.png" class="small_image">
            @endif
          </td>

          <td><span class="text-uppercase sub_text">@lang('productsView.submittedBy')</span><br/>{{$product_community->first_name}} {{$product_community->last_name}}</td>
        </table>

      </div>

    </div>

  </div>

@endforeach