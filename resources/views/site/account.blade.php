@extends('layout.default')

@section('content')

@push('css')
<!-- Account page content start -->
<style type="text/css">
.product {
  position: relative;
}

.cross-img {
  position: absolute;
  top: 0px;
  right: 59px;
  font-size: 18px;
  width: 15px;
  background-color: #ffff;
}

img { 
  opacity: 1;
}

  .cash-symbol{font-size: 50%}
  .time-symbol{font-size: 45%; color: gray;}
  ul{
     list-style-type: none;

  }

  .font-big{font-size: 35px !important; color: #F10482;}
</style>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
@endpush


<div class="container">
        <div class="row">
            <div class="col-sm-3  mt-3" >
                <div class="card">
                    <div class="card-body text-center">


                     @if($user->profile_img != "" && file_exists(public_path('img/profile/'.$user->profile_img)))

                        <img id="imgFileUpload" alt="{{$user->first_name}}" width="40%" title="Select File" src="{{url('/')}}/img/profile/{{$user->profile_img}}" style="cursor: pointer" class="rounded-circle" />
                        {{  Form::open(array('url' => URL::to('profileimage'),'id'=> 'imagechange', 'files' => true)) }}
                        {{ csrf_field() }}
                         <input Input::old('profile_img') type="file" name="profile_img" id="FileUpload1" value='{{$user->profile_img}}' style="display:none;">
                          <input type="hidden" name="old_file" value="{{$user->profile_img}}" />
                           {{Form::close()}}

                      @else

                        <img src="{{url('/')}}/img/avatar.jpg" width="40%" alt="{{$user->first_name}}" class="rounded-circle">

                      @endif

                       <br>
                        <h6>{{$user->first_name}} {{$user->last_name}}</h6>
                    </div>
                </div>
                 </a>

                <ul class="list-group">
                  <li class="list-group-item active tabs clicked"  id="myprofile" data-href="my_profile">@lang('userProfile.account')</li>
                  <li class="list-group-item tabs clicked"   data-href="my_communities">@lang('userProfile.myCommunities')</li>

                  {{--messages--}}
                  <li class="list-group-item tabs clicked"   data-href="messages_tab"> @lang('userProfile.messages')</li>

                  <li class="list-group-item tabs clicked"    id="myproducts" data-href="my_product">@lang('userProfile.myProducts')</li>
                  <li class="list-group-item tabs clicked"   data-href="borrowed_products"> @lang('sentence.lendered_products')</li>
            
                  <li class="list-group-item tabs clicked"   data-href="ordered_products"> @lang('userProfile.orderedProducts')</li>

                   <li class="list-group-item tabs clicked"   data-href="plans_tab"> @lang('userProfile.plans')</li>

                  {{-- <li class="list-group-item tabs" data-href="join_communities">
                    @lang('userProfile.joinedCommunities')
                  </li> --}}
                 <!--  <li class="list-group-item" disabled="">Account Details</li>
                  <li class="list-group-item" disabled="">Settings</li> -->
                </ul>
            </div>
              <input type="hidden" id="selected_tab" value="" name="">
          <!-- My Profile start -->
            <div class="col-sm-9 tab mt-3" id="my_profile">
                <div class="card add_product_padding">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>@lang('userProfile.account')</h3>
                                <hr/>
                            </div>
                        </div>
                        <div class="row">
                    	   <div class="col-sm-8">
               <form method="POST" action="{{route('profile')}}" role="form" id="account_form" name="account_form" enctype="multipart/form-data">
                     {{ csrf_field()}}


                            <!-- First Name -->
    			                     <div class="form-group">
                			            <label for="first_name">@lang('userProfile.firstName')</label>
                			            <input type="text" class="form-control form-control-lg" name="first_name" id="first_name" placeholder="" value="{{$user->first_name ? $user->first_name : old('first_name')}}">
                                        @if ($errors->has('first_name'))
                                        <div class="error">{{ $errors->first('first_name') }}</div>
                                        @endif
                                </div>

                                <!-- Last Name-->
                                <div class="form-group">
                  			            <label for="last_name">@lang('userProfile.lastName')</label>
                  			            <input type="text" class="form-control form-control-lg" name="last_name" id="last_name" placeholder="" value="{{$user->last_name ? $user->last_name : old('first_name')}}">
                                          @if ($errors->has('last_name'))
                                              <div class="error">{{ $errors->first('last_name') }}</div>
                                          @endif
    			                      </div>

                               <!-- Email -->
                                <div class="form-group">
        			                     <label for="product_name">@lang('userProfile.email')</label>
        			                     <input type="text" class="form-control form-control-lg" name="email" id="email" placeholder="" value="{{$user->email ? $user->email : ''}}" readonly="">
                                </div>

                                 <div class="form-group">
                                   <label for="phone">phone</label>
                                   <input type="text" class="form-control form-control-lg" name="phone" id="phone" placeholder="" value="{{$user->phone ? $user->phone : ''}}" >
                                </div>


                                 <div class="form-group">
                                   <label for="phone">Zip Code</label>
                                   <input type="text" class="form-control form-control-lg" name="zip_code" id="zip_code" placeholder="" value="{{$user->zip_code ? $user->zip_code : ''}}" >
                                </div>

            			           <div class="form-group">
                			            <label for="profile_img">@lang('userProfile.profileImage')</label>

                                   <?php if(file_exists(public_path('img/profile/'.$user->profile_img)) && $user->profile_img !=""){ ?>

                			            <img class="col-md-3 rounded-circle" src="{{URL::to('')}}/img/profile/{{$user->profile_img}}" style="width: 13%;" >
                               <?php }?>
                                  <div class="imageholder">
                                    <input Input::old('profile_img') type="file" name="profile_img" value='{{$user->profile_img}}'>
                                    <input type="hidden" name="old_file" value="{{$user->profile_img}}" />
            			                </div>
                              </div>


                              <div class="form-group">
                              <label for="change">Change Password: </label>
                              <input value="1"  id="change_password"   type="checkbox" name="change_password" class="checkbox" >
                              </div>


                                <div class="form-group pass hide">
                                   <label for="password">Password</label>
                                   <input type="password" class="form-control form-control-lg" name="password" id="password"  placeholder=""  >
                                </div>
                               
                                 <div class="form-group pass hide">
                                   <label for="phone">Confirm Password</label>
                                   <input type="password" class="form-control form-control-lg" name="password_confirmation" id="password_confirmation" placeholder="" value="" >
                                </div>

                                    {{-- Country --}}
                                    <div class="form-group">
                                        <label for="countryUser">@lang('citiesView.country')</label>
                                        <select class="form-control form-control-lg" name="countryUser" id="countryUser">
                                          <option value="{{$user->country->id}}" selected >{{$user->country->name}}</option>
                                        </select>
                                    </div>

                                    {{-- <script type="text/javascript">
                                        document.getElementById('countryUser').dispatchEvent('change');
                                    </script> --}}

                                    {{-- State --}}
                                    <div class="form-group">
                                        <label for="stateUser">@lang('citiesView.state')</label>
                                        <select class="form-control form-control-lg" name="stateUser" id="stateUser">
                                        <option value="{{$user->state->id}}" selected >{{$user->state->name}}</option>

                                        </select>
                                    </div>

                                    {{-- City --}}
                                    <div class="form-group">
                                        <label for="cityUser">@lang('citiesView.city')</label>
                                        <select class="form-control form-control-lg" name="cityUser" id="cityUser">
                                          <option value="{{$user->city->id}}" selected >{{$user->city->name}}</option>

                                        </select>
                                    </div>

                                          {{-- Direction --}}
                           <div class="form-group">

                               <label class="control-label" for="direction">@lang('keywords.direction')</label>
                                  
                                  <textarea  name="direction" class="form-control" id="direction">{{$user->direction}}</textarea> 
                                 

                           </div>

                                    <!-- <div class="form-group">
                                        <label for="communities">Community</label>
                                        <select class="form-control form-control-lg" name="community" id="community">
                                           <option value=" ">---  Select Community you want to join  ---</option>
                                            @foreach($allcommunity as $community )
                                                <option value="{{$community->id}}" @if($user->community_id == $community->id) selected @endif>{{$community->name}}</option>
                                            @endforeach
                                        </select>
                                    </div> -->
    			                     <button type="submit" class="btn btn-primary btn-lg btn-block">@lang('keywords.submit')</button>

                            </form>
    			             </div>
                        </div>
                    </div>
                </div>
            </div>
          <!-- My Profile End -->

            <!-- Messages start -->
              <div class="col-sm-9 tab mt-3" id="messages_tab" style="display:none;">
                <div class="card add_product_padding">
                  <div class="card-body">
                    <div class="row">
                     @include('site.tabs.messages')
 
                    
                  </div>
                </div>
              </div>
               </div>
          <!-- Messages End -->


          <!-- My Product start -->
              <div class="col-sm-9 tab mt-3" id="my_product" style="display:none;">
                <div class="card add_product_padding">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">
                      	<div class="csrf_token" data_content="{{ csrf_token() }}"></div>
                        <h3>@lang('sentence.My products')</h3>
                        <a href="{{route('createproduct')}}"  class="btn btn-primary product_add float-right">@lang('productsView.addProduct')</a>
                      </div>
                    </div>
                    <div class="row caja-busqueda">
                        
                      @include('site.pagination.product')

                    </div>
                               
                  </div>
                </div>
              </div>

              <!-- borrowed products-->
               <div class="col-sm-9 tab mt-3" id="borrowed_products" style="display:none;">
                <div class="card add_product_padding">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="csrf_token" data_content="{{ csrf_token() }}"></div>
                        <h3>@lang('sentence.lendered_products')</h3>
                        
                      </div>
                    </div>
                    <div class="row caja-busqueda">
                     
                        @include('site.pagination.borrowed')

                    </div>
              

                  </div>
                </div>
              </div>
          <!-- Borrowed products End -->


          <!-- My Product End -->
<!-- My Communities start -->
                <div class="col-sm-9 tab" id="my_communities" style="display:none;"><br>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-9"><br>
                                    <h3 style="margin-left:30px;">@lang('userProfile.myCommunities')</h3></div>
                                   <div class="col-sm-3"><br>
                                    <input type="button" class="btn btn-success float-center"  id="community_model" value="@lang('communitiesView.addCommunity')">
                                </div>
                            </div><hr>
                          </br>
                        
                            @if(@count($join) < 1)
                            <div style="text-align: center; margin-bottom: 2%;">
                                    <span style=" width: 100%; border-bottom: 1px solid #F10482">@lang('communitiesView.noCommunitiesAvailable')</span>
                            </div>
                      
                            @endif
                            <div class="row caja-busqueda" >
                             
                            @include('site.pagination.joinCommunity')

                            </div>
                                          </div>
                    </div>
                </div>
          

  <!-- My Communities End -->
          <!-- Borrowed Product start -->
              <div class="col-sm-9 tab mt-3" id="ordered_products" style="display:none;">
                <div class="card add_product_padding">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">
                        <h3>@lang('userProfile.orderedProducts')</h3>
                        <!-- <a href="{{route('createproduct')}}"  class="btn btn-primary product_add float-right">Add Product</a> -->
                      </div>
                    </div>
                    <div class="row caja-busqueda">

                  @include('site.pagination.ordered_products')
                    </div>

                             
                  </div>
                </div>
              </div>
          <!-- Borrowed Product End -->

            <!-- Borrowed Product start -->
              <div class="col-sm-9 tab mt-3" id="plans_tab" style="display:none;">
                <div class="card add_product_padding">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-12">
                        <h3>@lang('userProfile.plans')</h3>
                        <!-- <a href="{{route('createproduct')}}"  class="btn btn-primary product_add float-right">Add Product</a> -->
                      </div>
                    </div>
                    <div class="row">
                    
                        @foreach($fees as $f)
    <div class="col-md-4 card" style="border: 1px solid #F10080; border-radius: 5%">
{{--auth()->user()->country->name!='Chile'?'stripe':'qvo'--}}
<form class=" stripe payment" method="post"  action="{{url('charge/pay')}}">
  
      <input type="hidden" name="amount" value="{{$f->fees}}">
      <input type="hidden" name="plan_id" value="{{$f->id}}">
      {{  Form::hidden('url',URL::previous())  }}

      <h1 class="text-center" style="font-size: 60px"><span class="cash-symbol">$</span>{{$f->fees}}<span class="time-symbol">/mo</span></h1>
      <div class="content" style="min-height: 200px;">
        <ul class="">
          <li><i class="fa fa-check-circle-o font-big" aria-hidden="true"></i> <span>Accede a productos Premium</span> </li>
        </ul>

     <div class="form-group  " style="margin-bottom: 10%; margin-top: 20%;">
                                <button type="button" class="btn btn-primary" style="width: 100%">
                                  Choose Plan
                                </button>
                        </div>
               
      </div>
</form>


    </div>

    @endforeach


                    </div>
                  </div>
                </div>
              </div>
          <!-- Borrowed Product End -->


           <!-- Joined Communities start -->

           
          </div>
  <!-- Joined Communities End -->
<!---community add form-->

      @include('site.community.addCommunityModal')
      @include('site.stripeModal')

  </div>


<div class="modal fade bd-example-modal-md" id="edit_com" tabindex="-1" role="dialog" aria-labelledby="edit_com" aria-hidden="true">
  
</div>
<!-- add new Communities End -->


<div class="modal fade bd-example-modal-md" id="edit_product" tabindex="-1" role="dialog" aria-labelledby="edit_product" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="view_orderModal">@lang('productsView.editProduct')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
             <div class="col-md-12">
                 <div class="modal-body">
                     <form method="POST" action="" role="form" id="newModalForm" name="newModalForm1" enctype="multipart/form-data">
                     {{ csrf_field()}}
                       
                        <input name="product_id" type="hidden" id="productid" value="">
                       
                        <div class="form-group">
                            <label for="product_name">@lang('keywords.product')</label>
                            <input type="text" name="product_name" class="form-control" id="productname" value="">
                           
                        </div>
                         <div class="form-group">
                            <label>@lang('productsView.productImage')</label>
                                <img src="" height="30px" width="30px" id="productimg" >
                                <div class="imageholder">
                                  <input  type="file"  name="product_img[]" id="img"  multiple="multiple">
                                  <input type="hidden" id="productimg1" name="old_file" value="" />
                         </div>
                       </div>
                        <div class="form-group">
                            <label for="product_desc">@lang('keywords.category')</label>
                            <select class="form-control form-control-lg" name="category" id="productcat">
                                @foreach($categories as $category )
                                     <option value="{{$category->id}}" >{{$category->name}}</option>
                                  @endforeach
                             </select>    
                        </div>

                        
                            <div class="form-group">
            <label for="return_term">@lang('keywords.return_term')</label>
            
            <select class="form-control" id="return_term" name="return_term">
                <option value="1" >@lang('keywords.1_week')</option>
                <option value="2" >@lang('keywords.2_week')</option>
                <option value="3" >@lang('keywords.3_week')</option>
                <option value="4" >@lang('keywords.4_week')</option>
            </select>
              </div>
                        
                        <div class="form-group">
                            <label for="product_desc">@lang('keywords.description')</label>
                            <textarea name='product_desc' id="productdesc" class="form-control"  id="product_desc"></textarea>
                        </div>
                             <div class="form-group">
                <label for="premium">Premium: </label>
                <input style="margin-left: 10px;" value="1" id="is_premium"   type="checkbox" class="checkbox" name='is_premium'>

             <p style="display: inline-block; font-size: 13px; ">*@lang('keywords.premium_add')</p>

              </div>
                

              <div class="form-group">
                <label for="is_active">@lang('keywords.active'): </label>
               <input style="margin-left: 10px;" id="is_active" value="1" type="checkbox" class="checkbox" name='is_active'>
              <p style="display: inline-block; font-size: 13px; ">*@lang('keywords.active_add')</p>

             </div>

              <div class="form-group">
              <label  for="premium">@lang('keywords.give_away') </label>
              <input style="margin-left: 10px;" value="1" id="give_away" type="checkbox" class="checkbox" name='give_away'>

             <p style="display: inline-block; font-size: 13px; ">*@lang('keywords.give_away_add')</p>

              </div>

                          <div class="form-group" id="box">
                  <label for="communities">@lang('keywords.communities')</label><br>
                   <select class="form-control" name="coms[]" id="community" style="width: 100%" multiple="multiple">
              
                    </select>
                  </div>
                        <div class="form-group1">
                        <button type="submit" class="btn btn_color btn-sm" id="newModalForm1">@lang('sentence.saveChanges')</button>
                       </div>
                    </form>
                </div>
            </div>
    </div>
</div>
</div>
<!-- Account page content end -->
@endsection

@section('pagejavascripts')

@include('site.community.js.addCommunityModal')
@include('site.dispatch_user_modal')

    <script type="text/javascript">

      $(function(){

      $("#dispatch_user_form").on('submit',function(e){
      e.preventDefault();

      $(".show_dispatch_modal").prop('disabled',true);
      $(".delete_order").prop('disabled',true);

      var formData = new FormData($(this)[0]);
      $('#dispatch_user_modal').modal('hide');
      $('#loader_modal').modal('show');


      $.ajax({
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        url: $(this).attr('action'),
        data:formData,
        success: function (response) {

          if(response.success==true)
          {
          


                    $('#loader_modal').modal('hide');
                      toastr.success("sucess");


               }


             },
             error: function (e) {
              console.log(e);
               // $.unblockUI();
      $(".show_dispatch_modal").prop('disabled',false);
      $(".delete_order").prop('disabled',false);
               $.each(e.responseJSON.errors, function (index, element) {
                if ($.isArray(element)) {
                  toastr.error(element[0]);
                }
              });


             },
             complete: function(response)
             {

             }
           });

    })



        $(".show_dispatch_modal").on("click",function(){


      id=$(this).attr('data-id');
      
  
     
      document.getElementById("dispatch_user_form").reset();
 
      $.get("/get-dispatch-info/"+id, function(data, status){
        $(".weight").val(data.weight)
        $(".price").val(data.price)
        $(".dispatch_date").val(data.dispatch_date)
        $(".total_payment").val(data.total)
        $(".total_payment_span").html(data.total)

        
        $("#borrow_id").val(id);
       $('#dispatch_user_modal').modal('show');
  });
        })
      })
    </script>
<script type="text/javascript"
    src="https://viralpatel.net/blogs/demo/jquery/jquery.shorten.1.0.js"></script>
    <!--file upload-->
<script type="text/javascript">
    $(function () {

      $('#countryUser').select2({
  ajax: {
    url: '/api/country-select',
    dataType: 'json'
    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
  }
});


   $("#countryUser").on("change",function(){

            $('#stateUser').select2({
  ajax: {
    url: '/api/country-select/'+$(this).val()+'/states',
    dataType: 'json'
    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
  }
});

     
        });


 toastr.options.onHidden = function() { location.reload();};
 toastr.options.onclick = function() { location.reload(); };
  $(".datepicker_month").datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                language: "es",
                autoclose: true,
                todayHighlight: true,
                startView: 1,
            });


 $(".datepicker_year").datepicker({
                format: "yyyy-mm-dd",
                todayBtn: "linked",
                language: "es",
                autoclose: true,
                todayHighlight: true,
                startView: 2,
            });


  $(document).on('click','.payment',function(){


    if($(this).hasClass('stripe'))
    {
     $('#stripe_modal').modal('show');
    }else{
         // $(this).unbind(e);

    }

  })

 $("#payment-form").on('submit',function(e){
            e.preventDefault();

              var formData = new FormData($(this)[0]);
//$.blockUI({ message: '<h1><img src="/img/loader.gif" /></h1>' });

        $.ajax({
                cache: false,
                contentType: false,
                 processData: false,
            type: 'POST',
            url: $(this).attr('action'),
            data:formData,
            success: function (response) {

                if(response.success==true)
                {
           // $.unblockUI();
                  var id=response.id;
                 toastr.success(response.message,"Success!");
              
                  window.location = "{{url('/charge/success')}}/"+id

                // location.reload();
                 //document.getElementById('account_form').reset();

                }


            },
            error: function (e) {
                console.log(e);
               // $.unblockUI();

                $.each(e.responseJSON.errors, function (index, element) {
                    if ($.isArray(element)) {
                        toastr.error(element[0]);
                    }
                });


            },
            complete: function(response)
            {

            }
        });

        })


 $("#account_form").on('submit',function(e){
            e.preventDefault();

              var formData = new FormData($(this)[0]);
//$.blockUI({ message: '<h1><img src="/img/loader.gif" /></h1>' });

        $.ajax({
                cache: false,
                contentType: false,
                 processData: false,
            type: 'POST',
            url: $(this).attr('action'),
            data:formData,
            success: function (response) {

                if(response.success==true)
                {
           // $.unblockUI();

                 toastr.success(response.message,"Success!");
                 location.reload();
                 //document.getElementById('account_form').reset();

                }


            },
            error: function (e) {
                console.log(e);
               // $.unblockUI();

                $.each(e.responseJSON.errors, function (index, element) {
                    if ($.isArray(element)) {
                        toastr.error(element[0]);
                    }
                });


            },
            complete: function(response)
            {

            }
        });

        })

        $("#create_comunity_form").on("submit",function(e){
     

        e.preventDefault();
        var formData = new FormData(this);

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
         $('#communitymodel').modal('hide');

            toastr.success(data.message);
           // location.reload();
            },
           error: function (e) {
                console.log(e);
                $.each(e.responseJSON.errors, function (index, element) {
                    if ($.isArray(element)) {
                        toastr.error(element[0]);
                    }
                });


            }
        });


    })


          $(".clicked").on("click",function(){
            $("#selected_tab").val($(this).attr('data-href'));
          })

         $(document).on('click', '.pagination a', function (e) {
         e.preventDefault();
         page=$(this).attr('href').split('page=')[1];
         pageRaiz=$(this).attr('href');
         console.log(pageRaiz);
        $(".loader").removeClass('hide');
    $('#'+$("#selected_tab").val()+' '+".content-box").addClass('hide');

          $.ajax({
            url :  pageRaiz+'&type='+$("#selected_tab").val(),
            dataType: 'json',
            beforeSend: function(){
             
            },
        }).done(function (data) {
           $(".loader").addClass('hide');
           $(".content-box").removeClass('hide');

          //$(".loader").hide();
            $('#'+$("#selected_tab").val()+' '+'.caja-busqueda').html(data);
           
            location.hash = page;
        }).fail(function () {
           // alert('Posts could not be loaded.');
        });
          
        });

       var fileupload = $("#FileUpload1");
        var filePath = $("#spnFilePath");
        var image = $("#imgFileUpload");

        image.click(function () {
            fileupload.click();
        });


        fileupload.change(function () {
            var fileName = $(this).val().split('\\')[$(this).val().split('\\').length - 1];
            // filePath.html("<b>Selected File: </b>" + fileName);
            $('#imagechange').submit();
        });
    });

</script>


<script type="text/javascript">

$(document).ready(function(){


       $("#change_password").on("click",function(){
        if( $(this).is(':checked')){
          $(".pass").removeClass('hide');
        }else{
          $(".pass").addClass('hide');
        }
       })
     
   var queryString = window.location.search.substring(1);
    var varArray = queryString.split("="); //eg. index.html?msg=1

    var param1 = varArray[0];
    var param2 = varArray[1]; 
  if(param2 == 1){
    $('#myprofile').removeClass('active');
     $('#myproducts').addClass('active');
     $('#my_profile').attr('style','display:none');
    $('#my_product').attr('style','display:block');
                $("#selected_tab").val("my_product");

  }
});


$(document).on("click",'.received_order',function(){

      var id = $(this).attr('data-id');

      var retVal = confirm("Has the product been received successfully?");

      if(retVal)
      {
              $('#loader_modal').modal('show');

          $.ajax({
    type: 'POST',
    url: $(this).attr('data-href'),
    data: { 
        'id':id, 
        "_token": $('meta[name="csrf-token"]').attr('content')
    },
    success: function(response){
      if(response.success==true)
      {
            $('#loader_modal').modal('hide');

           toastr.success("Product received successfully.");
           

            location.reload();
   
      }
    }
});  
      }

  


  });



$(document).on("click",'.returned_order',function(){

      var id = $(this).attr('data-id');

      var retVal = confirm("Has the product been returned successfully?");

      if(retVal)
      {
            $('#loader_modal').modal('show');

          $.ajax({
    type: 'POST',
    url: $(this).attr('data-href'),
    data: { 
        'id':id, 
        "_token": $('meta[name="csrf-token"]').attr('content')
    },
    success: function(response){
      if(response.success==true)
      {
            $('#loader_modal').modal('hide');

           toastr.success("Product returned successfully.");
           

           window.location.href = "/product/request/"+response.borrow_id;

   
      }
    }
});  
      }

  


  });

  $(document).on("click",'.delete_order',function(){

      var id = $(this).attr('data-id');

      var retVal = confirm("Do you want to delete it?");

      if(retVal)
      {
                $('#loader_modal').modal('show');

          $.ajax({
    type: 'POST',
    url: $(this).attr('data-href'),
    data: { 
        'id':id, 
        "_token": $('meta[name="csrf-token"]').attr('content')
    },
    success: function(response){
      if(response.success==true)
      {
            $('#loader_modal').modal('hide');

           toastr.success("Order successfully deleted.");

           location.reload();
   
      }
    }
});  
      }

  


  });



  $('.tabs').on("click",function(){
      var active_id = $(this).attr('data-href');
      $('.list-group-item').removeClass('active');
      $(this).addClass('active');

      $('.tab').hide();
      $('#'+active_id).show();
  });

/** delete product*/
    $(document).ready(function() {
          
         $(".comment").shorten({
          "showChars" : 60,
          "moreText"  : "<b>See More</b>",
          "lessText"  : "<b>Less</b>",
      });
       });

  $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('.csrf_token').attr('data_content')
        }
	});


      $(document).on('click', '.edit_community', function(e) 
  {
      e.preventDefault(); 

      var $this = $(this);
        $.get({
            url: $this.attr('data-href')
        }).done(function (data) {
            //alert(data);
            $("#edit_com").html("");
            $("#edit_com").html(data);

            $('#edit_com').modal('show');

        });
    
  });

      $(document).on('submit','#edit_community_form', function(e) 
  {
      e.preventDefault(); 

           var formData = new FormData(this);

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
              toastr.success(data.message);

                //location.reload();
            },
            error: function(e){
                $.each(e.responseJSON.errors, function (index, element) {
                    if ($.isArray(element)) {
                        toastr.error(element[0]);
                    }
                });

            }
        });
    
  });

	$(document).on('click', '.del_link', function(e) 
  {
	    e.preventDefault(); 

	    var $this = $(this);
	    var retVal = confirm("Do you want to delete it?");
        var dataId = $this.attr('data-id');
        if( retVal == true ){
		    $.post({
		        type: 'DELETE',
		        url: $this.attr('data-href')
		    }).done(function (data) {
		        //alert(data);
            if(data.success == true){
             // $(".data"+dataId).fadeOut();
              toastr.success(data.message);
              //location.reload();
            }
		    });
		}
	});
</script>

<script type="text/javascript">
    $("#community_model").on("click", function(){
        $('#communitymodel').modal('show');
    });
  </script>
<!-- edit product -->
  <script type="text/javascript">
  $(document).on('click','.edit_link',function(){
              var id = $(this).data('id');
              var name = $('#pname'+id).text();
              var desc =$('#pdesc'+id).attr('value');
              var premium =$('#pdesc'+id).attr('data-premium');
              var active=$('#pdesc'+id).attr('data-active');
              var give_away=$('#pdesc'+id).attr('data-away');
              var img = $('#pimg'+id).attr('src');
              var cat = $(this).data('category');
              var term=$('#pdesc'+id).attr('data-term');
              var communities=JSON.parse($('#pdesc'+id).attr('data-communities'));
              var options="";


              for (i = 0; i < communities.length; i++)
             {
                  options =options+"<option value="+communities[i].id+" selected='selected'>"+communities[i].name+"</option>";

             }

             

             $("#community").html("");
             $("#community").append(options);
               $('#community').select2({
                        ajax: {
                            url: '/user_communities',
                            dataType: 'json'
                        },
                        dropdownParent: $("#box")

                    });
                  $
                  $('#edit_product').modal('show');
                  $('#productid').attr('value',id);
                  $('#productname').attr('value',name);
                  $('#productdesc').text(desc);
                  $('#productimg').attr('src',img); 
                  $('#productcat').value = cat;
                  $('#productcat option[value="' + cat +'"]').attr("selected", "selected");
                  $('#productimg1').attr('value',img);

           $('#return_term option[value="' + term +'"]').attr("selected", "selected");

                  if(premium==1)
                  {
                    $("#is_premium").prop('checked', true);
                  }else{
                    $("#is_premium").prop('checked', false);

                  }

                   if(give_away==1)
                  {
                    $("#give_away").prop('checked', true);
                  }else{
                    $("#give_away").prop('checked', false);

                  }

                    if(active==1)
                  {
                    $("#is_active").prop('checked', true);
                  }else{
                    $("#is_active").prop('checked', false);

                  }


              var url = $(this).data('href');
              $('#newModalForm').attr('action',url);
            $('#newModalForm1').on('click',function(){
             var a = $('#newModalForm').serialize();
             $('#edit_product').submit();
            });
         });
</script>
@stop

@push('js')
  @include('site.js.account')
@endpush