<div class="modal fade" id="dispatch_user_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog"  role="document">

    <div class="modal-content">

      <div class="modal-header text-center">
        <h5 class="modal-title">Información de despacho</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form class="" method="POST" id="dispatch_user_form" role="form" action="{!!route('addmoney.stripe')!!}" >
        {{ csrf_field() }}
        <input type="hidden" name="borrow_id" id="borrow_id" value="">
        <input type="hidden" name="type" id="type" value="dispatch">
      
        <div class="modal-body">

          <div class="form-group">
            <p class="text-center"><b>Dear user, this is the shipping information</b></p>
            <label class="control-label">Peso</label>
            <input class="form-control weight"  type="number" disabled name="weight" required>

          </div>


          <div class="form-group">
           <label class="control-label">Precio</label>
           <input autocomplete="off" class="form-control price" placeholder="price" disabled  type="text" name="price" required>

         </div>





         <div class="form-group ">
           <label class="control-label">Fecha estimada de recibo </label>
           <input class="form-control card-expiry-year dispatch_date datepicker" placeholder="YYYY" disabled size="4" type="text" name="dispatch_date" required>
         </div>




         <div class="form-group">
          <p><h4 class="text-right"><b>Total:<b>$<span class="total_payment_span"></span></h4></p>
         <input type="hidden" class="total_payment" name="amount" value="">
          </div>
          <div class="form-group">
           <label class="control-label">Card Number</label>
           <input autocomplete="off" class="form-control card-number" size="20" type="text" name="card_no">

         </div>


         <div class="form-group">
           <label class="control-label">CVV</label>
           <input autocomplete="off" class="form-control card-cvc" placeholder="ex. 311" size="4" type="text" name="cvvNumber">

         </div>



         <div class="form-group col-lg-4" style="display: inline-block;">
          <label class="control-label">Expiration</label>
          <input class="form-control card-expiry-month" placeholder="MM" size="2" type="text" name="ccExpiryMonth">

        </div>

        <div class="form-group col-lg-4"  style="display: inline-block;">
         <label class="control-label"> </label>
         <input class="form-control card-expiry-year" placeholder="YYYY" size="4" type="text" name="ccExpiryYear">
       </div>

       <div class=" col-lg-2"  style="display: inline-block;">
        <button  type="submit" id="dispatch_user_button" class="btn btn-primary">@lang('keywords.save')</button>
      </div>


    </div>



  </form>
</div>

</div>
</div>