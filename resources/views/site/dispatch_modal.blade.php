<div class="modal fade" id="dispatch_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog"  role="document">

    <div class="modal-content">

      <div class="modal-header text-center">
        <h5 class="modal-title">Información de despacho</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form class="" method="POST" id="dispatch_form" role="form" action="{!!url('admin/shipping-records')!!}" >
        {{ csrf_field() }}
        <input type="hidden" name="borrow_id" id="borrow_id" value="">
        <div class="modal-body">

          <div class="form-group">
           <label class="control-label">Peso</label>
           <input class="form-control weight"  type="number" name="weight" required>
           
         </div>

         
         <div class="form-group">
           <label class="control-label">Precio</label>
           <input autocomplete="off" class="form-control price" placeholder="price"  type="text" name="price" required>

         </div>

         

         

         <div class="form-group ">
           <label class="control-label">Fecha estimada de recibo </label>
           <input class="form-control card-expiry-year datepicker" placeholder="YYYY" size="4" type="text" name="dispatch_date" required>
         </div>
         <div class="form-group">
           <label class="control-label">Total</label>
           <input autocomplete="off" class="form-control total" placeholder="price"  type="text" name="total" required>

         </div>
         
         <div class=" form-group"  >
          <button id="sendCreateCommunityForm" type="submit" class="btn btn-primary">@lang('keywords.save')</button>
        </div>

      </div>






    </form>
  </div>

</div>
</div>