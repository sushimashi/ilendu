@extends('layout.default')

@section('content')
<!-- Home page content start -->
<!-- <div class="jumbotron">
  <div class="container">
    <h1 class="text-center">Lend and borrow products and save upto 80%</h1>
  </div>
</div> -->
<style type="text/css">

/*
  ##Device = Most of the Smartphones Mobiles (Portrait)
  ##Screen = B/w 320px to 479px
  */

  @media (min-width: 320px) and (max-width: 480px) {

   .banners{
    margin-bottom: 3%;
  }

}

.overlay .playWrapper_2
{
  opacity: 1;
  position: absolute;
  z-index: 1;
  top: 0;
  width: 192px;
  height: 109px;
  background: rgba(0,0,0,0.6) url("https://wptf.com/wp-content/uploads/2014/05/play-button.png") no-repeat scroll center center / 50px 50px;
}
.pagination > li > a, .pagination > li > span {
  background-color: #FFFFFF;
  border: 1px solid #DDDDDD;
  color: inherit;
  float: left;
  line-height: 1.42857;
  margin-left: -1px;
  padding: 4px 10px;
  position: relative;
  text-decoration: none;
}
.card-img-top{
  padding: 5%;
}
.centered-1 {
  position: absolute;
  top: 50%;
  left: 70%;
  transform: translate(-50%, -50%);
}
.white{color: white;}
.centered-2 {
  position: absolute;
  top: 50%;
  left: 75%;
  transform: translate(-50%, -50%);
}
.text-center {
  text-align: center!important;
}
header
{
  margin-bottom: 0px;
}

#myList .active{
 background-color: #b4cce68c;
}

</style>

<div id="demo" class="carousel slide" data-ride="carousel">
  <!-- <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active d-none d-sm-block"></li>
    <li data-target="#demo" data-slide-to="1" class="d-none d-sm-block"></li>
    <li data-target="#demo" data-slide-to="2" class="d-none d-sm-block"></li>
  </ul> -->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="{{url('img/2.png')}}" class="d-block w-100">
        <!-- <div class="carousel-caption">
          <div class="overlay text-uppercase">
            <h1 class="h1_media">accede gratis</h1>
            <p><b>a miles de herramientas</b></p>
          </div>
        </div> -->
      </div>
    <!-- <div class="carousel-item">
      <img src="{{url('img/1.png')}}" class="d-block w-100">
      <div class="carousel-caption">
        <div class="overlay text-uppercase">
          <h1 class="h1_media">para que comprar? <br/> comparte!</h1>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <img src="{{url('img/2.png')}}" class="d-block w-100">
      <div class="carousel-caption">
        <div class="overlay text-uppercase">
          <h1 class="h1_media">accede gratis</h1>
          <p><b>a miles de libros</b></p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a> -->
  </div>
</div>
<!--  -->

@if(Session::has('registered'))
<input type="hidden" id="registered" value="{{Session::get('registered')}}" name="">
<input type="hidden" id="user_name" value="{{(isset($user))?$user->getFullName():''}}" name="">

@endif
<div class="container">
  <div class="row">
    <div class="col-lg-4">
    {{--  <div class="video_main">
        <div class="row">
          <div class="col-lg-6 col-sm-6 pr-0">
            <span class="video_text">@lang('sentence.Learn more')</span>
          </div>
          <div class="col-lg-6 col-sm-6">
            <div class="overlay">
              <a href="#"><img class="thumbnail" src="https://ilendu.co/img/1.png" width="192" height="109" alt=""></a>
              <a data-fancybox href="https://www.youtube.com/watch?v=rzJB7WlNJaI&t=4s" class="playWrapper_2"></a>
            </div>
          </div>
        </div>
      </div>--}}
    </div>
    <div class="col-lg-8">
      <div class="box_banner" style=" ">

        <h1 style="font-size: 35px;"><b>@lang('sentence.shareandborrow')<span class="and_font">&</span><br> @lang('sentence.shareandborrow1')</b></h1>
        <br>
        <h5>@lang('sentence.Save saving the planet')</h5>                 <br>
        @if(Auth::check())
        <a class="btn banner_btn" href="/communities" data-keyboard="false">@lang('sentence.join up')</a>
        @else
        <a class="btn banner_btn" href="/register" data-keyboard="false">@lang('sentence.join up')</a>
        @endif
      </div>

    </div>

  </div>
  <div style="clear:both"></div>
  <div class="row">
    <div class="col-lg-6 col-sm-6 banners">
      @if(!Auth::check())
      <a href="{{URL::to('/lender')}}">
        <img src="{{url('/')}}/img/lender.png" class="img-fluid"/>
        <div class="centered-1">

          <div class="card-body coll_card-body white">
           <h4 class="white">@lang('sentence.lender-header')</h4>

           @lang('sentence.text-lender')
         </div>
       </div>
     </a>
     @endif

   </div>
   <div class="col-lg-6 col-sm-6">
    @if(!Auth::check())

    <a href="{{URL::to('/borrow')}}">
      <img src="{{url('/')}}/img/borrower.png" style="height: 360px;" class="img-fluid"/>
      <div class="centered-2">
        <div class="card-body coll_card-body white">
          <h4 class="white">@lang('sentence.borrowers-header')</h4>

          @lang('sentence.text-borrow')
        </div>

      </div>
    </a>
    @endif

  </div>
</div>

<div class="row">
  <div class="col-lg-12 mt50 mb13">
    <h4 class="color_black">@lang('sentence.How it works?')</h4>
  </div>
  <div class="col-lg-4 col-sm-4">
    <div class="card index_card text-center">
      <div class="card-body">
        <img class="mt-5" src="{{url('/')}}/img/borrow.png"/>
        <div class="b3_text">@lang('sentence.Findneed')</div>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-sm-4">
    <div class="card index_card text-center">
      <div class="card-body">
        <img class="mt-5" src="{{url('/')}}/img/find.png"/>
        <div class="b3_text">@lang('sentence.Sharecommunities')</div>
      </div>
    </div>
  </div>
  <div class="col-lg-4 col-sm-4">
    <div class="card index_card text-center">
      <div class="card-body">
        <img class="mt-5" src="{{url('/')}}/img/rating.png"/>
        <div class="b3_text">@lang('sentence.continuesharing')</div>
      </div>
    </div>
  </div>
</div>
<!--  -->
</div>
<div class="product-grid">

  <div class="container home">
    <div class="row">
      <div class="col-lg-3">

       <div class="bg_white">

        <h6 class="text-uppercase diff_color"  align="center">
          @lang('categoriesView.categories')
        </h6>

        <input type="hidden" name="category[]" id="myInput1" value="@if(isset($search)) {{$search}} @endif"/>

        <input type="hidden" name="seach_type" id="seach_type" value="{{$only_country}}">

        <input type="text" id="myInput" class="form-control input_border" placeholder="@lang('sentence.Search for')..." name="search" value=""/>

        <div class=" form-group">
          <br>
          <div class="form-check-inline" style="padding-left: 12px !important; padding-right: 12px !important;">

            <label class="customradio" style="margin-right: -13px !important;"><span class="radiotextsty">@lang('keywords.filter_premium')</span>
              <input type="radio" class="filter_radio" name="filter_radio" value="premium">
              <span class="checkmark"></span>
            </label>        
            <label class="customradio"><span class="radiotextsty">@lang('keywords.filter_gift')</span>
              <input type="radio" class="filter_radio" name="filter_radio" value="gift">
              <span class="checkmark"></span>
            </label>
            &nbsp; &nbsp;

            <label class="customradio"><span class="radiotextsty">@lang('keywords.filter_all')</span>
              <input type="radio" class="filter_radio" name="filter_radio" checked value="all">
              <span class="checkmark"></span>
            </label>

          </div>
        </div>
        <div class="form-control"  style="border: none;" id="myList" >
          <!-- <input type="checkbox" ng-model="checked"> -->


          @if(Session::get('applocale') == 'es')

          @foreach($categories as $category)
          <div>

           <a class="btn filterbtn @if((@Session::get(category)) == $category['id']) active @endif" data-id="{{$category['id']}}" data-filter="{{$category['name_spanish']}}"  style="border: none;">{{$category['name_spanish']}}</a>
           </div>
           @endforeach

           @else

           @foreach($categories as $category)
           <div>
           <a class="btn filterbtn @if((@Session::get(category)) == $category['id']) active @endif" data-id="{{$category['id']}}" data-filter="{{$category['name']}}"  style="border: none;">{{$category['name']}}</a>
           </div>
           @endforeach

           @endif
           </div>

           </div>
           </div>
           <!--   <div align="center">
            <h5 style="color: #C63131">{{@$message1}}</h5>
            </div><br> -->

            <div class="col-lg-9">
   <div class="col-md-12 col-sm-12 loader text-center hide">
                <img src="{{asset('img/loader.gif')}}">
        </div>
            @if(count($products))

            <div class="row" id="cityproduct">
    
              @foreach($products as $product)

              <div class="col-lg-4 col-sm-6">
                <div class="card">

                  <a href="{{ url('products/'.$product->id)}}">
                   @if($product->product_img != "")
                   <?php if(file_exists(public_path('img/thumbnail/'.$product->product_img)) && $product->product_img !=""){ ?>
                   <img class="card-img-top" src="{{url('/')}}/img/thumbnail/{{$product->product_img}}" alt="">
                   <?php }else{ ?>
                   <img class="card-img-top" src="https://www.freeiconspng.com/uploads/no-image-icon-6.png" alt="">
                   <?php } ?>
                   @else
                   <img class="card-img-top" src="{{url('/')}}/img/no-image.png" alt="">
                   @endif
                 </a>

                 <div class="card-body">

                  <a href="{{ url('products/'.$product->id)}}" class="card-text">
                    <p>{{$product->product_name}}</p>
                  </a>

                  <table>
                    <td>

                      <a style="text-decoration:none;" href="{{ url('products/'.$product->id)}}" class="card-text">

                        <?php

                        $urlImg;

                        if( isset($product['user']->profile_img) && file_exists(public_path('img/profile/'.$product['user']->profile_img)))
                        {

                          $urlImg = url('/')."/img/profile/".$product['user']->profile_img;

                        }else{

                          $urlImg = url('/')."/img/profile/new_user.png";

                        }

                        ?>

                        <img class="small_image" src="{{ $urlImg }}" alt="">

                      </a>
                    </td>

                    <td>
                      <a style="text-decoration:none;" href="{{ url('products/'.$product->id)}}" class="card-text">
                       <span class="text-uppercase sub_text">@lang('productsView.submittedBy')</span><br/>
                       <span><a style="color: black" href="{{url('reputation/'.$product['user']->id)}}">{{ !empty($product['user'])?$product['user']->getFullName():'' }}</a></span>
                     </a>
                   </td>

                 </table>

                 @if($product['is_premium']==1 && $product['give_away']==1)
                 <div style=" border-color: #F10080 !important; background: #F10080 !important; " class="topright">@lang('keywords.gift')</div>

                 @elseif($product['is_premium']==1)
                 <div style=" border-color: #F10080 !important; background: #F10080 !important; " class="topright">@lang('keywords.premium')</div>
                 @endif
               </div>
             </div>
           </div>
           @endforeach

           <div class="col-lg-12"></div>
           <div class="clearfix"></div>
           <div class="div_center mb50"><br>
            {!!  $products->links() !!}

          </div>
        </div>

        @else
        <div class="row">
         <div class="col-md-12 card index_card text-center">
           {{-- @if(!auth()->user())
             <h3 style="margin-top: 10%;"><a href="{{url('/login')}}" class="diff_color"> @lang('sentence.loginRequire')</a> @lang('sentence.borrowProducts')</h3>
             @else
              <h3 style="margin-top: 10%;"><span  class="diff_color">@lang('productsView.noProductsFound')</span></h3>
              @endif--}}

              <h3 style="margin-top: 10%;"><span  class="diff_color">@lang('productsView.noProductsFound')</span></h3>

            </div>
          </div>
          @endif
        </div>



      </div>
    </div>
  </div>

        <!-- <div class="ajax-load text-center" style="display:none">
          <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More post</p>-->
          <!-- </div> -->
          <!-- Home page content end -->

          @endsection

          @section('pagejavascripts')

          @include('site.js.productsByCategoryInHome')

          <script type="text/javascript">

            $(document).on('click','.pagination a',function(e){
              e.preventDefault();

              $('#cityproduct').addClass('hide');

             $(".loader").removeClass('hide');

              href = $(this).attr('href');
              $.ajax({
                url: href,
                type: "get",
                success: function (response) {
             $(".loader").addClass('hide');
              $('#cityproduct').removeClass('hide');

                 $('#cityproduct').html(response);
               }

             });
            });

          </script>
          <script type="text/javascript">

            var page = 1;
            var test = 0;
           


            function loadMoreData(page){
              $(".loader").removeClass('hide');

              $.ajax(
              {
                url: '?page=' + page,
                type: "get",
                beforeSend: function()
                {

                  $('.ajax-load').show();
                }

              })
              .done(function(data)
              {
                $(".loader").addClass('hide');

                if(data.html == " "){

                  test = 1;
                  return;

                }else if(page <= data.total){

                  $('.ajax-load').hide();
                  $("#cityproduct").append(data.html);

                }
                if(page == data.total){
                  $(".loader").addClass('hide');

                  test = 1;
                  $('.ajax-load').hide();
                  return;

                }
              });
            }


            $(document).ready(function(){

              if($("#registered").val()==1)
              {
                var name=$("#user_name").val()
                toastr.success("Welcome "+name);
              }

              var Url = "<?php echo URL('/'); ?>";
              var userid = "<?php echo Auth::id(); ?>";

              $(".borrowed").click(function(e){
                e.preventDefault();
                var id = $(this).data('id');
                if(userid != ""){
                  $.blockUI({ css: { fontSize: '17px'},message: 'Processing request ...'});
                  var $this = $(this);
                  if($(this).attr('data_count') > 0  || $(this).attr('data_expire') > 0){
                 //$("#product_borrowed").html('<span class="btn-lg btn-block">Processing request ...<span>');
                 $.get(Url+'/borrow/'+id, function(data, status){
                  if(data == 'Sended' || status == 'success'){
                    $.unblockUI();
                    $this.hide();
                    toastr.success("Awaiting reply from the lender.");
                  }
                });
               } else {
                $(this).parent('#payment').submit();
              }
            }else{
              window.location.href = Url+'/products/'+id;
            }
          });


              /**Resize product images***/
              $(".image_width").each(function(){
               var setWidth =  $(this).width();
               if(setWidth > 300)
               {
                 $(this).css("width","100%");
               }
             })
            })

            /***category search***/
            $(document).ready(function(){

              $('#myList').on('click',function(){
                var arr=[];

                $('input:checkbox[name="category[]"]:checked').each(function() {
                  arr.push($(this).attr('id'));
                });

    //$('#myInput').val(arr.join(', '));
  });

  /*$("#myInput").on("keyup", function(e) {

    var value = $(this).val().toLowerCase();

    if(e.which > 48 && e.which <122) {

       $("#myList a").filter(function() {

            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)

            if( $(this).css('display') == 'none')
            {
                if( $(this).text() != 'All')
                  $(this).parent().find('.check').hide();
             // console.log($(this).parent().html());
            }
          //console.log($(this).text().toLowerCase().indexOf(value) > -1);
        });

    }else {

        $('.filterbtn').css('display',"initial");
        $('.check').show();

    }

  });*/

});

            $('#myInput').on('keypress',function(e) {
              var only=$("#seach_type").val();


              if(e.which == 13) {

                $('.check').show();
                let search = $(this).val(),
                request_url = "{{URL::to('prodsearch')}}";

                if(search !== "")
                  request_url = request_url + '/' + search+"?search_type="+only;
                $.ajax({
                  url: request_url,
                  type: "get",
                  success: function (response) {



                   $('#cityproduct').html(response);

                 }
               });
              }
            });

/*
   $('#myInput').on('keypress',function(e) {
        if(e.which == 13) {

          $('.check').show();
          let search = $(this).val(),
              request_url = "{{URL::to('catsearch')}}";

           if(search !== "")
              request_url = request_url + '/' + search;
            $.ajax({
                url: request_url,
                type: "get",
                success: function (response) {

                  let categorySelected = $(`[data-filter = ${search.charAt(0).toUpperCase() + search.slice(1)}]`);

                  $('.filterbtn').removeClass('active');
                  $('.check').prop('checked', false);

                  if(categorySelected.attr('data-id') != undefined){

                    $(`#${categorySelected.text()}`).prop('checked', true);
                    categorySelected.addClass('active');

                  }

                   $('#cityproduct').html(response);

                }
            });
           }
         });*/

         $('.check').on('change',function(e) {
           search =  $('#myInput').val();
           var request_url = "{{URL::to('catsearch')}}";
           if(search !== "")
            request_url = request_url + '/' + search;
          $.ajax({
            url: request_url,
            type: "get",
            success: function (response) {
             $('#cityproduct').html(response);

           }
         });
        });

      </script>
      @stop