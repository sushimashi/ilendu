@extends('layout.default')

@section('content')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<br/>
<div class="product-grid">
	<div class="container">
        <div class="card mt-3"></br>  
              <h3 style="font-weight:bold;">&nbsp;User City</h3><hr>
            <div class="card-body">
                <?php $i = 1 ?>
        	    <table class="table table-bordered display" id="example" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Email</th>
                            <th>Country</th>
                            <th>State</th>
                            <th>City</th>
                        </tr>
                    </thead>
                    <tbody id="e1">
                      
                    </tbody>
                </table>
               
            </div>
        </div>
    </div>
</div>

@endsection

@section('pagejavascripts')

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
   var table =  $('#example').DataTable({
        serverSide: true,
        serverMethod: 'get',
        processing: true,
        pagingType: 'full_numbers',
        ajax: '{{URL::to("/getuserlist")}}',
        columns: [
                        { data: 'id', name: 'id' },
                        { data: 'email', name: 'email' },
                        { data: 'countryname', name: 'countryname' },
                        { data: "statename", name: 'statename' },
                        { data: 'cityname', name: 'cityname' },
                        ]
          });
    });
</script>

@stop