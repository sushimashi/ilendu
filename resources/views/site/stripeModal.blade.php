<div class="modal fade" id="stripe_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog"  role="document">

    <div class="modal-content">

        <div class="modal-header text-center">
            <h5 class="modal-title">Stripe Payment</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

 <form class="" method="POST" id="payment-form" role="form" action="{!!route('addmoney.stripe')!!}" >
{{ csrf_field() }}
<input type="hidden" name="plan_id" value="{{$f->id}}">
<input type="hidden" name="amount" value="{{$f->fees}}">

 <div class="modal-body">

                  <div class="form-group">
 <label class="control-label">Card Number</label>
 <input autocomplete="off" class="form-control card-number" size="20" type="text" name="card_no">
 
                  </div>

                 
                  <div class="form-group">
                 <label class="control-label">CVV</label>
 <input autocomplete="off" class="form-control card-cvc" placeholder="ex. 311" size="4" type="text" name="cvvNumber">

                  </div>

          

             <div class="form-group col-lg-4" style="display: inline-block;">
              <label class="control-label">Expiration</label>
 <input class="form-control card-expiry-month" placeholder="MM" size="2" type="text" name="ccExpiryMonth">

                  </div>

             <div class="form-group col-lg-4"  style="display: inline-block;">
           <label class="control-label"> </label>
 <input class="form-control card-expiry-year" placeholder="YYYY" size="4" type="text" name="ccExpiryYear">
                  </div>
                  
                  <div class=" col-lg-2"  style="display: inline-block;">
                      <button id="sendCreateCommunityForm" type="submit" class="btn btn-primary">@lang('keywords.save')</button>
                  </div>

            </div>






 </form>
    </div>

</div>
    </div>
