@extends('layout.default')

@section('content')
<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script> -->

<!-- Add product page content start -->
<div class="container">
	<div class="row">
	  <div class="col-sm-12" style="text-align: center;">
	    <div class="card">
	      	<div class="card-body" style="padding-top: 18%;padding-bottom: 15%;">
	      		<div >
		        	<h5>@lang('payment.successful')</h5>
		        	<p>@lang('payment.check')</p>
		        	<p>@lang('payment.click')</p>
		        	<a href="{{url('/')}}"><p>Click here</p></a>
	      		</div>
	      	</div>
	    </div>
	 </div>


	</div>
</div>
<!-- Add Product page content end -->
@endsection

@section('pagejavascripts')

@stop