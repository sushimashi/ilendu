@extends('layout.default')
@section('content')
<style type="text/css">
	body
	{
		background-color: #fff;
	}
	.btn_change
	{
		font-size: 30px!important;
		margin-top: 45px!important;
	}
</style>
<div id="demo" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner">
		<div class="carousel-item active">
			<img src="{{url('/')}}/img/2b_i2.png" class="d-block w-100">
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-10 div_center">
			<div class="row faq_top">
				<div class="col-lg-6">
					<img src="{{url('/')}}/img/faq.png" class="img-fluid">
				</div>
				<div class="col-lg-1"></div>
				<div class="col-lg-5" style="font-size: 19px;color: gray;">
				<p>@lang('sentence.borrowers')</p>
				<p>@lang('sentence.borrowers1')</p>
				<p>@lang('sentence.borrowers2')</p>
				<p>@lang('sentence.borrowers3')</p>
				<p>@lang('sentence.borrowers4')</p>
				<p>@lang('sentence.borrowers5')</p>
				<p>@lang('sentence.borrowers6')</p>
				<p>@lang('sentence.borrowers7')</p>
					<a class="btn btn_change banner_btn" href="/communities" data-keyboard="false">@lang('sentence.Signup')</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-10 div_center">
			<div class="faq_top">
				<h4>FAQ's</h4>
				<div id="accordion" class="mt-4">
					<div class="card card_css">
						<div class="card-header card_hcss" id="headingOne">
							<h5 class="mb-0">
								<button class="btn btn-link collapsed w-100 acc_btn p-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
								<div class="float-left">@lang('sentence.borrow')</div>
									<div class="float-right"><i class="fa fa-plus"></i></div>
								</button>
							</h5>
						</div>
						<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
							<div class="card-body coll_card-body">
							First of all you must register on the platform and join a community (you will be by default in the community of your city).

Once registered, you should look for products available in your community and you can order the one you want, waiting for a response from the lender.

You must agree with who lends regarding the place of delivery of the product.

If you request a Premium product, you just have to indicate where you want it to be sent to you without waiting for approval from iLendu (You must be a Premium member)

Remember that the more products you share, the more products you can order, all following the sense of community.

							</div>
						</div>
					</div>
					<div class="card card_css">
						<div class="card-header card_hcss" id="headingTwo">
							<h5 class="mb-0">
								<button class="btn btn-link collapsed w-100 acc_btn p-0" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
									<div class="float-left">@lang('sentence.problem')</div>
									<div class="float-right"><i class="fa fa-plus"></i></div>
								</button>
							</h5>
						</div>
						<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
							<div class="card-body coll_card-body">
								When you borrow something, you should check it well since by accepting it you are accepting that it is in optimal condition.

In that situation you should not receive it and inform iLendu that the product was defective.

							</div>
						</div>
					</div>

					<!--

					<div class="card card_css">
						<div class="card-header card_hcss" id="headingThree">
							<h5 class="mb-0">
								<button class="btn btn-link collapsed w-100 acc_btn p-0" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									<div class="float-left">@lang('sentence.located')</div>
									<div class="float-right"><i class="fa fa-plus"></i></div>
								</button>
							</h5>
						</div>
						<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
							<div class="card-body coll_card-body">
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries
							</div>
						</div>
					</div>

					<div class="card card_css">
						<div class="card-header card_hcss" id="headingFourth">
							<h5 class="mb-0">
								<button class="btn btn-link collapsed w-100 acc_btn p-0" data-toggle="collapse" data-target="#collapseFourth" aria-expanded="false" aria-controls="collapseFourth">
									<div class="float-left">@lang('sentence.chargeoffice')</div>
									<div class="float-right"><i class="fa fa-plus"></i></div>
								</button>
							</h5>
						</div>
						<div id="collapseFourth" class="collapse" aria-labelledby="headingFourth" data-parent="#accordion">
							<div class="card-body coll_card-body">
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries
							</div>
						</div>
					</div>

					<div class="card card_css card_lastb">
						<div class="card-header card_hcss" id="headingFifth">
							<h5 class="mb-0">
								<button class="btn btn-link collapsed w-100 acc_btn p-0" data-toggle="collapse" data-target="#collapseFifth" aria-expanded="false" aria-controls="collapseFifth">
									<div class="float-left">@lang('sentence.Whereborrow')</div>
									<div class="float-right"><i class="fa fa-plus"></i></div>
								</button>
							</h5>
						</div>
						<div id="collapseFifth" class="collapse" aria-labelledby="headingFifth" data-parent="#accordion">
							<div class="card-body coll_card-body">
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries
							</div>
						</div>
					</div>
					-->
				</div>

			</div>
		</div>
	</div>
</div>
@endsection

@section('pagejavascripts')
<script type="text/javascript">
$(document).ready(function(){
    // Add minus icon for collapse element which is open by default
    $(".collapse.show").each(function(){
    	$(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });

    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function(){
    	$(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function(){
    	$(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
});
</script>
@endsection