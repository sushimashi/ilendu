@extends('layout.default')
@section('content')
<style type="text/css">
	body
	{
		background-color: #fff;
	}
	.btn_change
	{
		font-size: 30px!important;
		margin-top: 45px!important;
	}
</style>
<div id="demo" class="carousel slide" data-ride="carousel">
	<div class="carousel-inner">
		<div class="carousel-item active">
			<img src="{{url('/')}}/img/2b_i1.png" class="d-block w-100">
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-10 div_center">
			<div class="row faq_top">
				<div class="col-lg-6">
					<img src="{{url('/')}}/img/faq.png" class="img-fluid">
				</div>
				<div class="col-lg-1"></div>
				<div class="col-lg-5" style="font-size: 19px;color: gray;">
					<p>@lang('sentence.Lenders')</p>
					<p>@lang('sentence.Lenders1')</p>
					<p>@lang('sentence.Lenders3')</p>
					<p>@lang('sentence.Lenders4')</p>
					<p>@lang('sentence.Lenders5')</p>
					<p>@lang('sentence.Lenders6')</p>
					<a class="btn btn_change banner_btn" href="/communities" data-keyboard="false">@lang('sentence.Signup')</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-10 div_center">
			<div class="faq_top">
				<h4>FAQ's</h4>
				<div id="accordion" class="mt-4">
					<div class="card card_css">
						<div class="card-header card_hcss" id="headingOne">
							<h5 class="mb-0">
								<button class="btn btn-link collapsed w-100 acc_btn p-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
									<div class="float-left">@lang('sentence.Ilend')</div>
									<div class="float-right"><i class="fa fa-plus"></i></div>
								</button>
							</h5>
						</div>
						<div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
							<div class="card-body coll_card-body">
								First of all you must upload the products that you want to make available to the community. You must go to the "Add Product" button (If you do not find it, look at the top right) where you must upload the product information,  what type of user you want to provide and In what communities.
							</div>
						</div>
					</div>
					<div class="card card_css">
						<div class="card-header card_hcss" id="headingTwo">
							<h5 class="mb-0">
								<button class="btn btn-link collapsed w-100 acc_btn p-0" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
									<div class="float-left">@lang('sentence.returnme')</div>
									<div class="float-right"><i class="fa fa-plus"></i></div>
								</button>
							</h5>
						</div>
						<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
							<div class="card-body coll_card-body">
								We recommend you to share among people known or with a good reputation in the community, this gives you greater security.
								On the other hand, the products provided to premium customers are guaranteed by iLendy, which is established through the dispute process where the background is reviewed to deliver a refund for the value of the product.

							</div>
						</div>
					</div>
					<div class="card card_css">
						<div class="card-header card_hcss" id="headingThree">
							<h5 class="mb-0">
								<button class="btn btn-link collapsed w-100 acc_btn p-0" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
									<div class="float-left">@lang('sentence.returned')</div>
									<div class="float-right"><i class="fa fa-plus"></i></div>
								</button>
							</h5>
						</div>
						<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
							<div class="card-body coll_card-body">
								As if the product was not returned to you, a review of the matter is placed. In the event that this occurs, you must immediately inform iLendu for the review of the situation.

								Products lent to premium customers are guaranteed by iLendu.

							</div>
						</div>
					</div>
					<div class="card card_css">
						<div class="card-header card_hcss" id="headingFourth">
							<h5 class="mb-0">
								<button class="btn btn-link collapsed w-100 acc_btn p-0" data-toggle="collapse" data-target="#collapseFourth" aria-expanded="false" aria-controls="collapseFourth">
									<div class="float-left">@lang('sentence.dispatches')</div>
									<div class="float-right"><i class="fa fa-plus"></i></div>
								</button>
							</h5>
						</div>
						<div id="collapseFourth" class="collapse" aria-labelledby="headingFourth" data-parent="#accordion">
							<div class="card-body coll_card-body">
								The shipment of the products is agreed between the requestor and the Lender. Many times who asks for the product will look for it and go back to return it.
							</div>
						</div>
					</div>
					<div class="card card_css card_lastb">
						<div class="card-header card_hcss" id="headingFifth">
							<h5 class="mb-0">
								<button class="btn btn-link collapsed w-100 acc_btn p-0" data-toggle="collapse" data-target="#collapseFifth" aria-expanded="false" aria-controls="collapseFifth">
									<div class="float-left">@lang('sentence.productsplaces')</div>
									<div class="float-right"><i class="fa fa-plus"></i></div>
								</button>
							</h5>
						</div>
						<div id="collapseFifth" class="collapse" aria-labelledby="headingFifth" data-parent="#accordion">
							<div class="card-body coll_card-body">
								First of all you must register on the platform and join a community (you will be by default in the community of your city).

Once registered, you should look for products available in your community and you can order the one you want, waiting for a response from the lender.

You must agree with who lends regarding the place of delivery of the product.

If you request a Premium product, you just have to indicate where you want it to be sent to you without waiting for approval from iLendu (You must be a Premium member)

Remember that the more products you share, the more products you can order, all following the sense of community.

							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
@endsection

@section('pagejavascripts')
<script type="text/javascript">
$(document).ready(function(){
    // Add minus icon for collapse element which is open by default
    $(".collapse.show").each(function(){
    	$(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });

    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function(){
    	$(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function(){
    	$(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
});
</script>
@endsection