@extends('layout.default')

@section('content')
<style>
.container {
    position: relative;
}

.topright {
    position: absolute;
    top: 8px;
    right: 82px;
    font-size: 18px;
    border: 1px solid #c63131;
    background: #c63131;
    color: white;
    font-size: 0.7em;
    font-weight: bold;
    padding: 3px
}

.pagination > li > a, .pagination > li > span {
    background-color: #FFFFFF;
    border: 1px solid #DDDDDD;
    color: inherit;
    float: left;
    line-height: 1.42857;
    margin-left: -1px;
    padding: 4px 10px;
    position: relative;
    text-decoration: none;
}

.text-center {
    text-align: center!important;
}

/*img { 
    width: 100%;
    height: auto;
    opacity: 0.3;
}*/
</style>
<!-- Product page content start -->
<div class="product-grid">
  <div class="container">
    <input type="hidden" name="msg" id="msg" value="@if(isset($message)){{$message}}@endif">
    <div class="row" id="cityproduct">
      <h4>Sorry,Product was not more than 100 For Your City. </h4>
    </div>
    <div class="ajax-load text-center" style="display:none">
      <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More post</p>
    </div>
  </div>
</div>
<!-- Product page content end -->
@endsection

@section('pagejavascripts')
@stop