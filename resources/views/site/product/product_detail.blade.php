@extends('layout.default')

@section('content')
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" type="text/css" href="{{asset('css/zoom.css')}}">
@push('css')

<link rel="stylesheet" type="text/css" href="{{asset('css/starts.css')}}">
<style type="text/css">

  .botao-wpp {
    height: 20px !important;
    text-decoration: none;
    color: #eee;
    display: inline-block;
    background-color: #25d366;
    font-weight: bold;
    font-size: 13px;
    border-radius: 3px;
  }

  .payment_stripe{
    color: #e83e8c;
  }

  .botao-wpp:hover {
    background-color: darken(#25d366, 5%);
  }

  .botao-wpp:focus {
    background-color: darken(#25d366, 15%);
  }


  @media (min-width: 668px) {

    .whatsapp{display: none}
  }

  body > main > div.container > div > div.col-sm-7.mt-3 > div > div.gc-display-area > div.gc-icon.gc-icon-download{display: none !important}
  a{color: black;}


</style>
@endpush

@section('meta')
<meta property="og:url"                content="{{url()->current()}}" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="{{$product->product_name}}" />
<meta property="og:description"        content="{{$product->product_desc}}" />
<meta property="og:image" content="{{url('img/'.$product->product_img)}}" />
<meta property="og:image:secure_url"   content="{{url('img/'.$product->product_img)}}" />
@endsection


<!-- Product detail page content start -->
<div class="container"><br>
  <h3 style="font-weight:bold;">&nbsp;@lang('productsView.productDetails')</h3><hr>

  <div class="row">
    <div class="col-sm-7 mt-3">
      <ul id="glasscase" class="gc-start">
        @foreach($products as $p)
        <li><img src="{{url('/')}}/img/thumbnail/{{$p->photo}}" alt="" /></li>

        @endforeach
      </ul>



      <div class="fb-share-button" data-href="{{url()->current()}}" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Compartir</a>

<!-- twitter

<a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-text="Ilendu, never buy,share...{{$product->product_name}}:" data-url="https://ilendu.co/products/{{$product->id}}" data-show-count="false">Tweet</a>
-->
</div>

<div style="margin-top: 4px; display: inline-block;" >

  <!-- twitter-->

  <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-text="Ilendu, never buy,share...{{$product->product_name}}:" data-url="https://ilendu.co/products/{{$product->id}}" data-show-count="false">Tweet</a>

</div>
<div style="margin-top: 0px; display: inline-block;" >
  <!-- só o conteúdo do href já é suficiente -->
  <a href="whatsapp://send?text=Ilendu, never buy,share...{{$product->product_name}}: {{url()->current()}}" class="botao-wpp whatsapp">
    <!-- ícone -->
    <!--<i class="fa fa-whatsapp"></i>-->
    whatsapp
  </a>
</div>

</div>
<div class="col-sm-5 mt-3">

  @if (Auth::check())
  <?php if($product->user_id == Auth::user()->id){?>
  <a style="cursor:pointer;" data-toggle="modal" id="data1" data-target="#edit_product" data-category="{{$product->product_category}}"><i class="far fa-edit"></i></a>
  <?php }?>
  @endif

  <h2>{{$product->product_name}}</h2>
  <p>{{$product->product_desc}}</p>

  <?php $product_owner = $userimg->first_name.' '.$userimg->last_name; ?>

  @if( $userimg->profile_img != "" && file_exists(public_path('/img/profile/'.$userimg->profile_img)))
  <?php $owner_img = $userimg->profile_img; ?>
  @else
  <?php $owner_img = "new_user.png"; ?>
  @endif

  <?php 
  if ( isset($userimg->facebook_id) && strpos($userimg->profile_img, $userimg->facebook_id) != false ) {
    $urlImg = url('/')."/img/profile/".$owner_img; 
  }else{
    $urlImg = url('/')."/img/profile/".$owner_img; 
  }
  ?>

  <table>
    <tbody>
      <tr>
        <td>
          <img class="small_image rounded-circle" src="{{ $urlImg }}" alt="{{$product_owner}}" title="{{$product_owner}}">
        </td>
        <td>
          <span class="text-uppercase sub_text">@lang('productsView.addedBy')</span><br>
          <span><a href="{{url('/reputation/'.$userimg->id)}}">{{ $userimg->getFullName() }}</a></span>
        </td>
      </tr>
    </tbody>
  </table>
  <small>
    @if($product->is_premium)
    @lang('productsView.premiumProduct')
    @endif
  </small>
  <br> <br>

  @if (Auth::check())

  @if($product->user_id != $user->id)

  @if($product->share_all==1)

  @if(!empty($user_product_status))

  @if($user_product_status->user_id==$user->id)


  @switch($user_product_status->status)


  @case('Dispatching')

  @include('site.product.responses.request_dispatching_share')


  @break  

  @case('Pending')
  @include('site.product.responses.request_pending_share')

  @break

  @case('Sent')
  @include('site.product.responses.request_sent_share')

  @break

  @case('Received')
  @include('site.product.responses.request_accepted')

  @break

  @case('Accept')

  @include('site.product.responses.request_accepted')
  @break


  @case('Canceled')

  @include('site.product.contact_data_share_product')


  @break

  @case('Returned')
  @include('site.product.contact_data_share_product')


  @break

  @case('Reject')

  @include('site.product.contact_data_share_product')


  @break    

  @case('PaidOut')

  @include('site.product.responses.request_PaidOut_share')


  @break      

  @endswitch

  @else
  <div id="product_borrowed">
    <div class="payment">
      @include('site.product.contact_data')

    </div>
  </div>
  @endif

  @else

  <div id="product_borrowed">
    <div class="payment">
     @include('site.product.contact_data_share_product')

   </div>
 </div>

 @endif

 @elseif(empty($borrow))

 <div id="product_borrowed">
  <div class="payment">
   @include('site.product.contact_data')

 </div>
</div>


@elseif(!empty($borrow))


@switch($borrow->status)

@case('Pending')
@include('site.product.responses.request_status')

@break

@case('Accept')

@include('site.product.responses.borrowed_to')
@break

@case('Returned')
@include('site.product.contact_data')


@break

@case('Canceled')
@include('site.product.contact_data')


@break

@case('Reject')

@include('site.product.contact_data')


@break        

@endswitch


@if(!empty($product->borrower))
@if($product->borrower->status=="Accept")
@include('site.product.responses.borrower_info')

@endif

@endif
@endif


<br>
@else

<!-- owner user-->

<h5 class="text-center">@lang('productsView.yourProduct').</h5>


@endif
{{--login user--}}
@else
<div style="background-color: #F10482; border-radius: 10px; padding: 10px;">
 <a  class="" style="cursor: pointer; text-decoration: none;" href="{{url('/login')}}"><h6 style="color:white;" class="text-center">Please log in to borrow this product </h6></a>

</div>

@endif




</div>

<?php echo $product->status ?>




</div>
</div>
@include('site.product.editProductModal')

</div>


@include('site.stripeModal')


<div class="modal fade"
id="xModal"
tabindex="-1"
role="dialog"
aria-labelledby="xModal"
aria-hidden="true"
onClick="">
<!--<div class="modal fade pantalla" id="modal"  onClick="modalClose()">-->
</div>
<!-- Product detail page content end -->
@endsection

@section('pagejavascripts')

<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<script type="text/javascript" src="{{asset('js/zoom.js')}}"></script>
<script type="text/javascript">
  $(document).ready( function () {


   $(document).on('click','.payment_stripe',function(){

     $('#stripe_modal').modal('show');
     

   });




   $(document).on('click','#borrowed',function(){
/*
      if($(this).hasClass('stripe'))
      {
       $('#stripe_modal').modal('show');
     }else{
         // $(this).unbind(e);

       }*/


       $('#borrowed').prop('disabled', true);

                      $('#loader_modal').modal('show');

                //borrow request

                if("{{$product->share_all==0}}")
                {


                  var category_id=$("#category_id").val()
                  var message=$("#message").val();

                  var Url = "<?php echo URL('/borrow/' . $product->id); ?>";

                  $.ajax({
                    type: 'GET',
                    url: Url,
                    data:{message:message,category_id:category_id},
                    success: function (data) {

                     if(data.message == 'sended'){
                      $("#product_borrowed").html(data.body);
                      $('#loader_modal').modal('hide');
                      toastr.success("Success!!");

                      setTimeout(location.reload.bind(location), 3000);
                    }

                  },
                  error: function (e) {
                    console.log(e);
               // $.unblockUI();
       $('#borrowed').prop('disabled', false);

               $.each(e.responseJSON.errors, function (index, element) {
                if ($.isArray(element)) {
                  toastr.error(element[0]);
                }

                                      $('#loader_modal').modal('hide');

              });


             },
             complete: function(response)
             {

             }
           });



                  

                }else{

                  var message=$("#message").val();
                  var country_id=$("#country_id").val()
                  var state_id=$("#state_id").val()
                  var direction=$("#direction").val()
                  var postal_code=$("#postal_code").val()
                  var category_id=$("#category_id").val()




                  var Url = "<?php echo URL('/borrow-share/' . $product->id); ?>";


                  $.ajax({
                    type: 'GET',
                    url: Url,
                    data:{message:message,country_id:country_id,state_id:state_id,direction:direction,postal_code:postal_code,category_id:category_id},
                    success: function (data) {

                      if(data.message == 'sended'){
                        $("#product_borrowed").html(data.body);
                        $('#loader_modal').modal('hide');

                        toastr.success("Success!!");

                        setTimeout(location.reload.bind(location), 3000);
                      }

                    },
                    error: function (e) {
                      console.log(e);
               // $.unblockUI();
       $('#borrowed').prop('disabled', false);

               $.each(e.responseJSON.errors, function (index, element) {
                if ($.isArray(element)) {
                  toastr.error(element[0]);
                $('#loader_modal').modal('hide');

                }
              });


             },
             complete: function(response)
             {

             }
           });




                }

              })

   $("#payment-form").on('submit',function(e){
    e.preventDefault();

    var formData = new FormData($(this)[0]);
    $('#stripe_modal').modal('hide');
    $('#loader_modal').modal('show');


    $.ajax({
      cache: false,
      contentType: false,
      processData: false,
      type: 'POST',
      url: $(this).attr('action'),
      data:formData,
      success: function (response) {

        if(response.success==true)
        {

       toastr.success("Success!!");
       $('#loader_modal').modal('hide');

       setTimeout(location.reload.bind(location), 3000);   



        }


      },
      error: function (e) {
        console.log(e);
               // $.unblockUI();

               $.each(e.responseJSON.errors, function (index, element) {
                if ($.isArray(element)) {
                  toastr.error(element[0]);
               $('#loader_modal').modal('hide');

                }
              });


             },
             complete: function(response)
             {

             }
           });

  })


   $('#country_id').select2({
    ajax: {
      url: '/countries-ajax',
      dataType: 'json'
    }
  });


   $('#country_id').on('change', function () {

    id = $(this).val();

    $.get(`/getstate/`+id, function(result){
     $("#state_id").html(result);
   });

  });

   $('#community').select2({
    ajax: {
      url: '/user_communities',
      dataType: 'json'
    },
    dropdownParent: $("#box")

  });


 //toastr.options.onHidden = function() { location.reload();};
 //toastr.options.onclick = function() { location.reload(); };

 
 $(".start_value").on("click",function(){
  $("#start_score").val($(this).attr('value'))

})
            //If your <ul> has the id "glasscase"
            $('#glasscase').glassCase({ 'thumbsPosition': 'bottom', 'widthDisplay' : 560});
          });
        </script>
        <script type="text/javascript">












         $("#expired_link").click(function(){

           $.get
           (
            '/subscription/create',function(result)
            {
              $('#xModal').empty();
              $('#xModal').append(result);
              $('#xModal').modal('show');

            }
            );



         });


         $('#score_form').submit(function(e){
                      $('#loader_modal').modal('show');


          e.preventDefault();



          let data = $(this).serialize();


          $.ajax({
            url:  $(this).attr('action'),
            method:  $(this).attr('method'),
            data: data,
            success: function(result){
              toastr.info(result.message);
                      $('#loader_modal').modal('hide');

              $("#score_form input").prop("disabled", true);
              $("#score_form button").prop("disabled", true);



            }, 
            error: function(e){
              console.log(e.responseJSON);
              $.each(e.responseJSON.errors, function (index, element) {
                if ($.isArray(element)) {
                  toastr.error(element[0]);
                }
              });
            }, complete: function(){
     //   $('#sendAddCityForm').removeClass('disabled');
   }
 });
        });
         $("#changeStatus").click(function(){
          var Url = "<?php echo URL('/productstatuschange/' . $product->id.'/'.$product->is_active); ?>";
          $.get(Url, function(data, status){
              //alert("Data: " + data + "\nStatus: " + status);
              if(status == 'success'){
                $.unblockUI();
                toastr.success("Successfully Change Status.");
                window.location.href = "<?php echo URL('/productlist/'); ?>";

              }
            });
        });

         $('#data1').on('click',function(e){ 
          var cat = $(this).data('category');
     // $('#productcat').value = cat;
     $('#productcat option[value="' + cat +'"]').attr("selected", "selected");
     $('#edit_product').submit();
   });

 </script>
 @stop