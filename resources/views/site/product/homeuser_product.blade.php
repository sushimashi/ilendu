@extends('layout.default')
@section('content')
<div class="container">
    <div class="row">
<div class="col-lg-12 mt-3">
  <h3>Product List - <span style="color:black;">{{$user->first_name}}
             {{$user->last_name}}</span></h3><hr/>
</div>
@if(count($products) > 0)
@foreach ($products as $product)
  <div class="col-lg-3 col-sm-6">
    <div class="card">
      <a href="{{ url('products/'.$product->id)}}">
         @if($product->product_img != "")
             <?php if(file_exists(public_path('img/thumbnail/'.$product->product_img))){ ?>
                <img class="card-img-top" src="{{url('/')}}/img/thumbnail/{{$product->product_img}}" alt="">
              <?php }else{ ?>
                <img class="card-img-top" src="{{url('/')}}/img/no-image-icon.png" alt="">
              <?php } ?>
         @else
            <img class="card-img-top" src="{{url('/')}}/img/no-image.png" alt="">
         @endif
      </a>
      <div class="card-body">
        <a href="{{ url('products/'.$product->id)}}" class="card-text">
            <p>{{$product->product_name}}</p>
        </a>
       
      </div>
    </div>
  </div>
  @endforeach
  <div class="col-lg-12"></div>
  <div class="clearfix"></div>
  <div class="div_center mb50"><br>
   {!!  $products->links() !!}
  </div>
@else
  <div class="col-lg-12 col-sm-6">
      <h6><center>No Product available in the selected category !!</center></h6>
  </div>
@endif
</div>
</div>
@endsection