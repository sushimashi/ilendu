@extends('layout.default')

@section('content')
<style>
.container {
    position: relative;
}

.topright {
    position: absolute;
    top: 8px;
    right: 82px;
    font-size: 18px;
    border: 1px solid #c63131;
    background: #c63131;
    color: white;
    font-size: 0.7em;
    font-weight: bold;
    padding: 3px
}

.pagination > li > a, .pagination > li > span {
    background-color: #FFFFFF;
    border: 1px solid #DDDDDD;
    color: inherit;
    float: left;
    line-height: 1.42857;
    margin-left: -1px;
    padding: 4px 10px;
    position: relative;
    text-decoration: none;
}

.text-center {
    text-align: center!important;
}

/*img { 
    width: 100%;
    height: auto;
    opacity: 0.3;
}*/
</style>
<!-- Product page content start -->
<div class="product-grid">
  <div class="container">
    <br>
        <!-- <div align="center">
            <h5 style="color: #C63131">{{@$message1}}</h5>
        </div><br> -->
    <!-- <input type="hidden" name="msg" id="msg" value="@if(isset($message)){{$message}}@endif"> -->
    <div class="row" id="cityproduct">
      <?php $i = 1;?>
          @foreach($products as $product)
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <div class="card" >
                    <a href="{{ url('products/'.$product->id)}}">
                        @if($product->product_img != "")
                          <?php if(file_exists(public_path('/img/thumbnail/'.$product->product_img))){ ?>
                            <img class="card-img-top" src="{{url('/')}}/img/thumbnail/{{$product->product_img}}" alt="">
                          <?php }else{ ?>
                            <img class="card-img-top" src="https://www.freeiconspng.com/uploads/no-image-icon-6.png" alt="">
                          <?php } ?>
                        @else
                            <img class="card-img-top" src="https://www.freeiconspng.com/uploads/no-image-icon-6.png" alt="">
                        @endif
                    </a>
                    <div class="card-body">
                        <a href="{{ url('products/'.$product->id)}}" class="card-text">
                          <p>{{$product->product_name}}</p>
                        </a>
                        @if($isLoggedIn)
                          <table>
                            <td>
                              @if($product['user']->profile_img != "")
                                <?php if(file_exists(public_path('/img/profile/'.$product['user']->profile_img))){ ?>
                                    <img class="small_image" src="{{url('/')}}/img/profile/{{$product['user']->profile_img}}" alt="">
                                <?php }else{ ?>
                                  <img class="small_image" src="{{url('/')}}/img/profile/new_user.png">
                                <?php } ?>
                              @else
                              <img class="small_image" src="{{url('/')}}/img/profile/new_user.png">
                              @endif
                            </td>
                            <td>
                              <span class="text-uppercase sub_text">Submitted By</span><br/>
                              <span>{{$product['user']->name}}</span>
                            </td>
                          </table>
                        @endif
                    </div>
                    @if($product->is_premium == 1)
                      <div class="topright">PREMIUM</div>
                    @endif
                </div>
            </div>
          @endforeach
          <div class="col-lg-12"></div>
          <div class="clearfix"></div>
          <div class="div_center mb50">
            {!!  $products->links() !!}
          </div>
    </div>
    <div class="ajax-load text-center" style="display:none">
      <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More post</p>
    </div>
  </div>
</div>
<!-- Product page content end -->
@endsection

@section('pagejavascripts')
<script type="text/javascript">
    /*function myFunction() {
        var msg = $('#msg').val();
        if(msg != "")
        toastr.success(msg);
        $('#msg').val('');
    }
    window.myFunction(); */
</script>
@stop