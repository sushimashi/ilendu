<div class="card add_product_padding" style="background-color: white; padding: 5%;">
	      <div class="card-body">
@if(auth()->user()->id==$product->borrower->user->id)


<span class=" btn-block" >You have this product now<br>
</span>
@else

<h5 class="text-center">This product was borrowed By:</h5><br>
<table border="0" cellpadding="4" width="100%" style="text-align: center;">
            <tr>
              <th>Borrower Name: </th>
              <td><a href="{{url('reputation/'.$product->borrower->user->id)}}">{{$product->borrower->user->first_name}} {{$product->borrower->user->last_name}}</a></td>
            </tr>
            <tr><th>Borrower Email: </th><td>{{$product->borrower->user->email}}</td></tr> 
            <tr><th>Phone: </th><td>{{$product->borrower->user->phone}}</td></tr> 
            
          
 </table>
</span>


@endif

</div>
</div>