
 <div class="row bootstrap snippets">
    <div class="col-md-12  col-sm-12">
        <div class="comment-wrapper">
          <form method="post" id="score_form" action="{{url('/score')}}">
                    <input type="hidden" name="product_id" value="{{$product->id}}">
                    <input type="hidden" name="borrower_id" value="{{$borrow->id}}">
                    {{ csrf_field() }}

            <div class="panel panel-info" style="padding: 30px">
                <div class="panel-heading">
                    Comment panel
                </div>
                <div class="panel-body">
                    <input type="hidden" id="start_score" name="score">
                    <textarea class="form-control" id="comment" placeholder="write a comment..." name="comment" rows="3"></textarea>
                    @if(auth()->user()->id==$product->user_id)
                    <input type="hidden" name="commentator_id" value="{{auth()->user()->id}}">
                    <input type="hidden" name="receiver_id" value="{{$borrow->user_id}}">
                    @else
                    
                    <input type="hidden" name="commentator_id" value="{{auth()->user()->id}}">
                    <input type="hidden" name="receiver_id" value="{{$product->user_id}}">

                    @endif

                  <div class="rating" style="float: right;">


      
      <input type="radio" id="star5" class="start_value" name="rating" value="5" /><label for="star5" title="Meh">5 stars</label>
      <input type="radio" id="star4" class="start_value" name="rating" value="4" /><label for="star4" title="Kinda bad">4 stars</label>
      <input type="radio" id="star3" class="start_value" name="rating" value="3" /><label for="star3" title="Kinda bad">3 stars</label>
      <input type="radio" id="star2" class="start_value" name="rating" value="2" /><label for="star2" title="Sucks big tim">2 stars</label>
      <input type="radio" id="star1" class="start_value" name="rating" value="1" /><label for="star1" title="Sucks big time">1 star</label>
    </div>
                        <br>

                    <hr>
            <button type="submit" class="btn btn-info pull-right">Send</button>
                    <div class="clearfix"></div>

              

   
                </div>
            </div>

            </form>
        </div>

    </div>

   
</div>

