@extends('layout.default')

@section('content')

<style type="text/css">
    
    .checkbox
    {
        /* Double-sized Checkboxes */
        -ms-transform: scale(2); /* IE */
        -moz-transform: scale(2); /* FF */
        -webkit-transform: scale(2); /* Safari and Chrome */
        -o-transform: scale(2); /* Opera */
        transform: scale(2);
        padding: 10px;

      /* Checkbox text */
      font-size: 110%;
      display: inline;
      cursor:pointer;
    }
   
</style>
<link href="{{ asset('css/dropzone.css') }}" rel="stylesheet">
<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script> -->
<!-- Latest compiled and minified CSS -->
  


<!-- Add product page content start -->
<div class="container">
	
	<div style="margin-top: 30px;">
		@include('flash::message')
	</div>
        {{--<div class="row">
        	<div class="col-md-2"></div>
        	<div class="col-md-7">
        		  <div class="block-title">
            <h2 class="text-center">@lang('productsView.multiple_products')</h2>
        </div>
        	   <form  method="POST" action="{{route('addproduct')}}"
                      id="form_nueva_foto"  class="dropzone" >
                       {{ csrf_field() }}
                       <input type="hidden" name="only_img" value="1">
    </form>	
        	</div>

  </div>--}}



	<div class="row">
		        	<div class="col-md-2"></div>

	  <div class="col-sm-7 mt-3">
	    <div class="card add_product_padding">
             <h3 style="font-weight:bold;">@lang('productsView.addProduct')</h3><hr>
	      <div class="card-body">
	        <form class="create_form" action="{{route('addproduct')}}" method="post"  enctype="multipart/form-data">
	        	 {{ csrf_field() }}
	          <div class="form-group">
	            <label for="product_name">@lang('keywords.product')</label>
	            <input type="text" class="form-control form-control-lg" required name="product_name" id="product_name" aria-describedby="emailHelp" placeholder="" value="{{ old('product_name') }}" >
	            <small id="" class="form-text text-muted">@lang('sentence.lending')</small>
	            <small class="red">{{ $errors->first('product_name') }}</small>
	          </div>
	          <div class="form-group">
	            <label for="product_img">@lang('productsView.productImage')</label>
	            <input type="file" name="product_img[]" multiple="multiple"  required class="form-control-file" id="product_img">
	            <small class="red">{{ $errors->first('product_img') }}</small>
	          </div>
	          <div class="form-group">
	            <label for="product_category">@lang('keywords.category')</label>
	            <select class="form-control" id="product_category" name="product_category">

	              <option value="">@lang('keywords.select')</option>
	               @if(Session::get('applocale') == 'es')
	              @foreach($category as $value)
	              <option value = "{{$value->id}}" @if(old('product_category', @$product_category) == $value->id) selected @endif >{{$value->name_spanish}}</option>
	              @endforeach
	              @else
	               @foreach($category as $value)
	              <option value = "{{$value->id}}" @if(old('product_category', @$product_category) == $value->id) selected @endif >{{$value->name}}</option>
	              @endforeach
	              @endif
	            </select>
	            <small class="red">{{ $errors->first('product_category') }}</small>
	          </div>

	           <div class="form-group">
	        <label for="return_term">@lang('keywords.return_term')</label>
	    	
	    	<select class="form-control" name="return_term">
	    		<option value="1">@lang('keywords.1_week')</option>
	    		<option value="2">@lang('keywords.2_week')</option>
	    		<option value="3">@lang('keywords.3_week')</option>
	    		<option value="4">@lang('keywords.4_week')</option>
	    	</select>
	          </div>

	          <div class="form-group">
	            <label for="product_desc">@lang('keywords.description')</label>
	            <textarea class="form-control" id="product_desc" name="product_desc" rows="3" ></textarea>
	            <small id="" class="form-text text-muted">@lang('keywords.optional')</small>
	          </div>
	          <div class="form-group">
	            <label for="premium">Premium: </label>
	            <input style="margin-left: 10px;margin-right:2%;" value="1" type="checkbox" class="checkbox" name='is_premium'>
              <p style="display: inline-block; font-size: 13px;">*@lang('keywords.premium_add')</p>
            </div>

	  
             <div class="form-group">
              <label for="premium">@lang('keywords.give_away') </label>
	            <input style="margin-left: 10px;" value="1" type="checkbox" class="checkbox" name='give_away'>
              <p style="display: inline-block; font-size: 13px; ">*@lang('keywords.give_away_add')</p>

	          </div>
	          
	          <!--
	          <div class="form-group">
	            <label for="place">@lang('sentence.preferredTransactionPlaces')</label>
	            <input type="text" class="form-control form-control-lg" id="place" aria-describedby="emailHelp" placeholder="">

	            <input type="hidden" class="form-control form-control-lg" name="place[]" value="" id="placearr">
	          </div>-->
	          	<!-- <div class="form-group">
                    <label for="credit_card">City</label>
                    <select class="form-control form-control-lg" name="city" id="city" @if(old('city', @$city) == $value->id) selected @endif>
                    	<option value="">Select City</option>
                    	@if(@$allcities)
                        @foreach(@$allcities as $city )
                            <option value="{{$city->id}}" >{{$city->name}}</option>
                        @endforeach
                        @endif
                    </select>
                    {{ $errors->first('city') }}
                </div> -->
	          <div class="form-group" id="placeinput">

	          </div>
	          <div class="form-group" id="placename">

	          </div>
	          
	         
              <div class="form-group" id="box">
                  <label for="communities">@lang('keywords.communities')</label><br>
                   <select class="form-control" name="coms[]" id="community" multiple="multiple">
                   	@foreach($communities as $c)

                   	  <option value="{{$c->id}}" selected="selected">{{$c->name}}</option>

                   	@endforeach

                    </select>
                  </div>
                
	          <button type="submit" class="btn btn-primary btn-lg btn-block">@lang('keywords.publish')</button>

	     </form>
	      </div>
	    </div>
	  </div>


	</div>
</div>
<!-- Add Product page content end -->
@endsection

@section('pagejavascripts')
<script src="{{ asset('js/dropzone.js') }}"></script>

<script>
Dropzone.autoDiscover = false;


    $(function () {


   var myDropzone = new Dropzone("#form_nueva_foto",{ autoProcessQueue: true,parallelUploads: 50 });


  myDropzone.on("complete", function(file) {

 toastr.success('foto almacenada con exito');
  });
 });
</script>

<script type="text/javascript">
$(function(){

	 $('#community').select2({
                        ajax: {
                            url: '/user_communities',
                            dataType: 'json'
                        },
                        dropdownParent: $("#box")

                    });



   //store
    $('body').on('submit','.create_form', function (e) {
        e.preventDefault();
  var formData = new FormData($(this)[0]);
$.blockUI({ message: '<h1><img src="/img/loader.gif" /></h1>' });

        $.ajax({
        	    cache: false,
                contentType: false,
                 processData: false,
            type: 'POST',
            url: $(this).attr('action'),
            data:formData,
            success: function (response) {

            	if(response.success==true)
            	{
            $.unblockUI();

           toastr.success(response.message,"Exito!");
                        $(".create_form").trigger("reset");

                 window.location.replace("/account?key=1"); 		
            	}


            },
            error: function (e) {
                console.log(e);
                $.unblockUI();

                $.each(e.responseJSON.errors, function (index, element) {
                    if ($.isArray(element)) {
                        toastr.error(element[0]);
                    }
                });


            },
            complete: function(response)
            {

            }
        });
    });

})


</script>



<script>
	$('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>

@stop