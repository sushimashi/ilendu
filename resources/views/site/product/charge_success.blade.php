@extends('layout.default')

@section('content')
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>

@push('scripts')
<style type="text/css">
  
  a{color: white;}
</style>
@endpush

<!-- Product detail page content start -->
<div class="container"><br>
      <h3 style="font-weight:bold;">&nbsp;Detail</h3><hr>
    
  <div class="row">
     
 <div class="col-md-12 card text-center">
    <h1 class="text-center">@lang('payment.success') </h1>

     <p>@lang('payment.successful')
</p>
  <h2>@lang('payment.transaction_detail')</h2>

  <p>
    <strong>@lang('payment.transaction_date') </strong>
    {{$date}}
  </p>
  <p>
    <strong>@lang('payment.charge') </strong>
    {{ $amount }}$
  </p>

    <div class="col-lg-12 text-center">
        <a class="btn btn-primary" href="{{url('home')}}">@lang('payment.back')</a>

    </div>
  <hr>
 
 </div>
  
  </div>
</div>
</div>

<!-- Product detail page content end -->
@endsection

@section('pagejavascripts')

@stop