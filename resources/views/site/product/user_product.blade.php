@extends('layout.default')

@section('content')
<!-- Product page content start -->
<div class="product-grid">
    <div class="container">
        <ul class="nav nav-tabs mt-3" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="lend-tab" data-toggle="tab" href="#lend" role="tab" aria-controls="lend" aria-selected="true">Lend Product</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="borrow-tab" data-toggle="tab" href="#borrow" role="tab" aria-controls="borrow" aria-selected="false">Borrow Product</a>
            </li>
        </ul><br>

        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="lend" role="tabpanel" aria-labelledby="lend-tab">
                <div class="row">
                    @if(@count($lendproducts) > 0)
                        @foreach($lendproducts as $product)

                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="card" >
                                <a href="{{ url('products/'.$product->id)}}">
                                    @if($product->product_img != "")
                                      <?php if(file_exists(public_path('/img/thumbnail/'.$product->product_img)) && $product->product_img !=""){ ?>
                                        <img class="card-img-top" src="{{url('/')}}/img/thumbnail/{{$product->product_img}}" alt="">
                                      <?php }else{ ?>
                                        <img class="card-img-top" src="https://www.freeiconspng.com/uploads/no-image-icon-6.png" alt="">
                                      <?php } ?>
                                    @else
                                        <img class="card-img-top" src="{{url('/')}}/img/no-image.png" alt="">
                                    @endif
                                </a>
                                <div class="card-body">
                                    <a href="{{ url('products/'.$product->id)}}" class="card-text">
                                      <p>{{$product->product_name}}</p>
                                    </a>
                                </div>
                                @if($product->is_premium == 1)
                                  <div class="topright">PREMIUM</div>
                                @endif
                            </div>
                        </div>
                      @endforeach
                  @else
                    <span style="text-align: center; width: 100%; margin-top: 10px">No Products Available</span>
                @endif
                </div>
            </div>
            <div class="tab-pane fade" id="borrow" role="tabpanel" aria-labelledby="borrow-tab">
                <div class="row">
                    @if(@count($borrowproducts) > 0)
                    @foreach($borrowproducts as $product)

                    <!-- <div class="col-md-4">
                        <div class="product">
                          <a href="{{ url('products/'.$product->id)}}">
                            <img height="182px" src="../img/thumbnail/{{$product->product_img}}" alt="">
                            <h4>{{$product->product_name}}</h4>
                          </a>
                        </div>
                    </div> -->
                   <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                            <div class="card" >
                                <a href="{{ url('products/'.$product->id)}}">
                                    @if($product->product_img != "")
                                      <?php if(file_exists(public_path('/img/thumbnail/'.$product->product_img))){ ?>
                                        <img class="card-img-top" src="{{url('/')}}/img/thumbnail/{{$product->product_img}}" alt="">
                                      <?php }else{ ?>
                                        <img class="card-img-top" src="{{url('/')}}/img/no-image.png" alt="">
                                      <?php } ?>
                                    @else
                                        <img class="card-img-top" src="{{url('/')}}/img/no-image.png" alt="">
                                    @endif
                                </a>
                                <div class="card-body">
                                    <a href="{{ url('products/'.$product->id)}}" class="card-text">
                                      <p>{{$product->product_name}}</p>
                                    </a>
                                    @if($isLoggedIn)
                                      <table>
                                        <td>
                                          @if($product->profile_img != "")
                                            <?php if(file_exists(public_path('/img/profile/'.$product->profile_img))){ ?>
                                                <img class="small_image" src="{{url('/')}}/img/profile/{{$product->profile_img}}" alt="">
                                            <?php }else{ ?>
                                              <img class="small_image" src="{{url('/')}}/img/profile/new_user.png">
                                            <?php } ?>
                                          @else
                                          <img class="small_image" src="{{url('/')}}/img/profile/new_user.png">
                                          @endif
                                        </td>
                                        <td>
                                          <span class="text-uppercase sub_text">Submitted By</span><br/>
                                          <span>{{$product->username}}</span>
                                        </td>
                                      </table>
                                    @endif
                                </div>
                                @if($product->is_premium == 1)
                                  <div class="topright">PREMIUM</div>
                                @endif
                            </div>
                        </div>
                    
                  @endforeach
                  @else
                    <span style="text-align: center; width: 100%; margin-top: 10px;">No Products Available</span>
                @endif
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Product page content end -->
@endsection

@section('pagejavascripts')

@stop