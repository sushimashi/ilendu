@if(auth()->user()->is_premium)   

<input type="hidden" name="category_id" id="category_id" value="{{$product->product_category}}">
<div class="form-group">
  <label>Name</label>
  <input type="text" name="name" class="form-control" value="{{auth()->user()->first_name.' '.auth()->user()->last_name}}">
</div>
<div class="form-group">
  <label>Postal code</label>
  <textarea class="form-control" name="postal_code" id="postal_code" required></textarea>
</div>
<div class="form-group">
  <label>Country</label>
  <select class="form-control" id="country_id">
   <option value="{{auth()->user()->country->id}}" selected>{{auth()->user()->country->name}}</option>
 </select>
</div>

<div class="form-group">
  <label>State</label>
  <select class="form-control" id="state_id">
   <option value="{{auth()->user()->state->id}}" selected>{{auth()->user()->state->name}}</option>
 </select>
</div>

<div class="form-group">
  <label>Direction</label>
  <textarea class="form-control" name="direction" id="direction" required>{{auth()->user()->direction}}</textarea>
</div>

<div class="form-group">
  <label for="message">Message:</label>

  <textarea class="form-control" id="message"  name="message" ></textarea><br>
  <button data_count="{{@count($products)}}"  class="btn btn-lg btn-primary btn-block stripe" id="borrowed" type="button">Borrow</button>
</div>


@else

<div class="form-group">
  <p>You need to be a premium users to access to this product, please <a href="javascript:void(0)" class="payment_stripe">click here</a> to become one</p></div>
  @endif