@extends('layout.default')

@section('content')
<!-- Product page content start -->
<div class="product-grid">
  <div class="container">
    <input type="hidden" name="msg" id="msg" value="@if(isset($message)){{$message}}@endif">
    @if(count($products) > 0)
    <div class="row" id="cityproduct">
      <?php $i = 1;?>
          @foreach($products as $product)
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                <div class="product" >
                    <a href="{{ url('products/'.$product->id)}}">
                        <!-- <img height="182px" src="img/{{$product->product_img}}" alt="" class="image_width"> -->
                        <div style="border-color: black;height: auto;width: auto;">
                            @if($product->product_img != "")
                              <img height="182px" src="img/thumbnail/{{$product->product_img}}" alt="">
                           @else
                              <img height="182px" src="https://www.freeiconspng.com/uploads/no-image-icon-6.png" alt="">
                           @endif
                      </div>
                      <br>
                        @if(!$isLoggedIn)
                          <p style="float:left; margin-left: 20px"><strong>{{$product->product_name}}</strong></p>
                        @elseif($isLoggedIn)
                        
                          <p style="float:left; margin-left: 20px"><strong>{{$product->product_name}}</strong></p>
                          <div class="clearfix"></div>
                          <div class="col-md-12">

                            <div class="col-md-3" style="float: left; margin-left: -18px">

                              @if($product['user']->profile_img != "")

                                  

                                  @if( file_exists(url('/').'/img/profile/'.$product['user']->profile_img))

                                      <img class="submittedbyimg small_image" src="{{url('/')}}/img/profile/{{$product['user']->profile_img}}" alt="">

                                  @else

                                      <img class="submittedbyimg small_image" src="{{url('/')}}/img/profile/profile_user.jpg">

                                  @endif

                              @else

                                <img class="submittedbyimg" src="{{url('/')}}/img/profile/user.png">

                              @endif

                            </div>
             </a>
                            <div class="col-md-9 submittedbytext">
                              <p class="submitted">@lang('sentence.submittedBy')</p>
                              <p class="ml23"><b><a href="{{url('reputation/'.$product['user']->id)}}">{{ $product['user']->getFullName() }}</a></b></p>
                            </div>

                          </div>
                        @endif
       
                    @if($product->is_premium == 1)
                      <div class="topright">PREMIUM</div>
                    @endif
                </div><br>
            </div>
          @endforeach
          <br>
          <div class="clearfix"></div>
          <div class=" col-md-12 text-center mb50 ml40"><br>
            {!!  $products->links() !!}
          </div>
    </div>
    @else
    <div class="row">
      <div class="col-md-12" style="height: 200px; margin-top: 200px; text-align: center">
          <h5>No <?php echo ucfirst($_GET['search']); ?> Product Found</h5>
          <!-- <img style="margin-left: 300px" height="182px"  width="100%" src="img/no_product.png" alt=""> -->
      </div>
    </div>
    @endif
  </div>
</div>
<!-- Product page content end -->
@endsection

@section('pagejavascripts')
<script type="text/javascript">
    function myFunction() {
        var msg = $('#msg').val();
        if(msg != "")
        toastr.success(msg);
        $('#msg').val('');
    }
    window.myFunction(); 
</script>
@stop