 <!-- <?php $i = 1;?> -->
@foreach($products as $product)

      <div class="col-md-4">
        <div class="product" style="top: 1465px; left: 482px; width: 347px; height: 280px;margin-top: 10px;">
          <a href="{{ url('products/'.$product->id)}}">
             @if($product->product_img != "")
              <img height="182px" src="img/thumbnail/{{$product->product_img}}" alt="">
             @else
              <img height="182px" src="https://www.freeiconspng.com/uploads/no-image-icon-6.png" alt="">
             @endif
            <h5 style="float:left;">{{$product->product_name}}</h5></br>
            <div class="col-md-12">
                <div class="col-md-3 small_image" style="float: left;">
                  @if($product['user']->profile_img != "")
                  <img src="{{url('/')}}/img/profile/{{$product['user']->profile_img}}" style="border-radius: 50%; width: 50px; height: 50px">
                  @else
                  <img src="{{url('/')}}/img/profile/user.png" style="border-radius: 50%; width: 50px; height: 50px">
                  @endif
                </div>
                <div class="col-md-9" style="float: right; text-align: left;padding-left: 0px">
                  <p style="margin-bottom: 0px; color: #9B9B9B;font-size: 0.8rem">SUBMITTED BY</p>
                  <p><b>{{$product['user']->name}}</b></p>
                </div>
              </div>
          </a>
            @if($product->is_premium == 1)
             <div class="topright">PREMIUM</div>
            @endif
        </div>
      </div>
        <!-- @if($i % 3 == 0)
        </div>
        <div class="row">
        @endif
        <?php $i++;?> -->
      @endforeach