<div class="modal fade bd-example-modal-md" id="edit_product" tabindex="-1" role="dialog" aria-labelledby="edit_product" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="view_orderModal">@lang('productsView.editProduct')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
             <div class="col-md-12">
                 <div class="modal-body">
                     <form method="POST" action="{{route('editproduct', $product->id)}}" role="form" id="newModalForm" name="newModalForm1" enctype="multipart/form-data">
                     {{ csrf_field()}}
                       
                        <input name="product_id" type="hidden" id="productid" value="{{$product->id}}">
                       
                        <div class="form-group">
                            <label for="product_name">@lang('keywords.product')</label>
                            <input type="text" name="product_name" class="form-control" id="product_name" value="{{$product->product_name}}">
                             <span class="text-danger">
                                        <strong id="c_add2-error-edit"></strong>
                              </span>
                        </div>
                         <div class="form-group">
                            <label>@lang('sentence.Product image')</label>

                                <img src="{{URL::to('')}}/img/thumbnail/{{$product->product_img}}" height="30px" width="30px" >
                                <div class="imageholder">

                                <input type="file" multiple="multiple" name="product_img[]">
                                 
                         </div>
                       </div>
                     <div class="form-group">
                            <label for="product_desc">@lang('keywords.category')</label>
                            <select class="form-control" name="category" id="productcat">
                               
                                @php
                                    $categoryName;
                                    Session::get('applocale') === 'en'?
                                    $categoryName = 'name':
                                    $categoryName = 'name_spanish';
                                @endphp

                                @foreach($categories as $category )
                                     <option value="{{$category->id}}" @if($category->category_id == $category->id) selected @endif>{{$category->$categoryName}}</option>
                                  @endforeach
                             </select>    
                        </div>

                            <div class="form-group">
            <label for="return_term">@lang('keywords.return_term')</label>
            
            <select class="form-control" name="return_term">
                <option value="1" {{$product->return_term==1?"selected":''}}>@lang('keywords.1_week')</option>
                <option value="2" {{$product->return_term==2?"selected":''}}>@lang('keywords.2_week')</option>
                <option value="3" {{$product->return_term==3?"selected":''}}>@lang('keywords.3_week')</option>
                <option value="4" {{$product->return_term==4?"selected":''}}>@lang('keywords.4_week')</option>
            </select>
              </div>

                        <div class="form-group">
                            <label for="product_desc">@lang('keywords.description')</label>
                            <textarea name='product_desc' class="form-control"  id="product_desc">{{$product->product_desc}}</textarea>
                        </div>
                <div class="form-group">
                <label for="premium">Premium: </label>
                <input style="margin-left: 10px;" value="1" {{($product->is_premium==1)?'checked=checked':''}} type="checkbox" class="checkbox" name='is_premium'>
                
                <p style="display: inline-block; font-size: 13px;">*@lang('keywords.premium_add')</p>

               </div>
                
                <div class="form-group">

                <label for="premium">@lang('keywords.active'): </label>
                <input style="margin-left: 10px;" value="1" {{($product->is_active==1)?'checked=checked':''}} type="checkbox" class="checkbox" name='is_active'>
                           <p style="display: inline-block; font-size: 13px; ">*@lang('keywords.active_add')</p>

              </div>
              
                <div class="form-group">

              <label  for="premium">@lang('keywords.give_away') </label>
              <input style="margin-left: 10px;" value="1" {{($product->give_away==1)?'checked=checked':''}} id="give_away" type="checkbox" class="checkbox" name='give_away'>
             
              <p style="display: inline-block; font-size: 13px; ">*@lang('keywords.give_away_add')</p>

              </div>


            <div class="form-group" id="box">
                  <label for="communities">@lang('keywords.communities')</label><br>
                   <select class="form-control" name="coms[]" id="community" style="width: 100%" multiple="multiple">
                    @foreach($communities as $c)

                      <option value="{{$c->id}}" selected="selected">{{$c->name}}</option>

                    @endforeach

                    </select>
                  </div>
                        <div class="form-group1">
                        <button type="submit" class="btn btn_color btn-sm pull-right" id="newModalForm1">@lang('sentence.saveChanges')</button>
                       </div>
                    </form>
                </div>
            </div>
    </div>
</div>