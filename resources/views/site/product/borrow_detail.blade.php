@extends('layout.default')

@section('content')
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
<link rel="stylesheet" type="text/css" href="{{asset('css/zoom.css')}}">
@push('css')

<link rel="stylesheet" type="text/css" href="{{asset('css/starts.css')}}">
<style type="text/css">
body > main > div.container > div > div.col-sm-7.mt-3 > div > div.gc-display-area > div.gc-icon.gc-icon-download{display: none !important}
  a{color: black;}
</style>
@endpush


<!-- Product detail page content start -->
<div class="container"><br>
      <h3 style="font-weight:bold;">&nbsp;@lang('productsView.productDetails')</h3><hr>
    
  <div class="row">
        <div class="col-sm-7 mt-3">
            <ul id="glasscase" class="gc-start">
              @foreach($products as $p)
                    <li><img src="{{url('/')}}/img/{{$p->photo}}" alt="" /></li>

              @endforeach
            </ul>
             
        </div>
        <div class="col-sm-5 mt-3">

           

            <h2>{{$product->product_name}}</h2>
            <p>{{$product->product_desc}}</p>

            <?php $product_owner = $userimg->first_name.' '.$userimg->last_name; ?>

            @if( $userimg->profile_img != "" && file_exists(public_path('/img/profile/'.$userimg->profile_img)))
                <?php $owner_img = $userimg->profile_img; ?>
            @else
              <?php $owner_img = "new_user.png"; ?>
            @endif

            <?php 
              if ( isset($userimg->facebook_id) && strpos($userimg->profile_img, $userimg->facebook_id) != false ) {
                $urlImg = url('/')."/img/profile/".$owner_img; 
              }else{
                $urlImg = url('/')."/img/profile/".$owner_img; 
              }
            ?>

            <table>
              <tbody>
                <tr>
                  <td>
                    <img class="small_image rounded-circle" src="{{ $urlImg }}" alt="{{$product_owner}}" title="{{$product_owner}}">
                  </td>
                  <td>
                    <span class="text-uppercase sub_text">@lang('productsView.addedBy')</span><br>
                    <span><a href="{{url('/reputation/'.$userimg->id)}}">{{ $userimg->getFullName() }}</a></span>
                  </td>
                </tr>
              </tbody>
            </table>
            <small>
              @if($product->is_premium)
                @lang('productsView.premiumProduct')
              @endif
            </small>
          <br> <br>


          
            @if(!$has_commented)


                @include('site.product.responses.add_comment');

             @if($product->borrower->status=="Returned")

              @if(auth()->user()->id==$product->user_id)
              @include('site.product.responses.returned_info');
              @else
                @include('site.product.responses.returned_to');

              @endif
             @endif
             @else
            <h4 class="text-center">@lang('keywords.thanks')</h4>
            
            @endif


         
        </div>
        

  </div>
</div>
</div>

<!-- Product detail page content end -->
@endsection

@section('pagejavascripts')
<script type="text/javascript" src="{{asset('js/zoom.js')}}"></script>
<script type="text/javascript">
        $(document).ready( function () {
// toastr.options.onHidden = function() { location.reload();};
 //toastr.options.onclick = function() { location.reload(); };

 
          $(".start_value").on("click",function(){
$("#start_score").val($(this).attr('value'))

})
            //If your <ul> has the id "glasscase"
            $('#glasscase').glassCase({ 'thumbsPosition': 'bottom', 'widthDisplay' : 560});
        });
    </script>
<script type="text/javascript">
  var Url = "<?php echo URL('/borrow/' . $product->id); ?>";
 

  $("#borrowed").click(function(){
      //  $.blockUI({ css: { fontSize: '17px'},message: 'Processing request ...'});

$.get(Url, function(data, status){
                if(data.message == 'sended'){
                    $.unblockUI();
                    $("#product_borrowed").html(data.body);
                    toastr.success("Awaiting reply from the lender.");
setTimeout(location.reload.bind(location), 3000);
                }
            });



});


 $("#expired_link").click(function(){

 $.get
                (
                    '/subscription/create',function(result)
                    {
                        $('#xModal').empty();
                        $('#xModal').append(result);
                        $('#xModal').modal('show');

                    }
                );



});

/*
  $("#borrowed").click(function(){

        let processingText = "@lang('sentence.processingRequest')",
            awaitingReplyText = "@lang('sentence.awaitingReply')";

        $.blockUI({ css: { fontSize: '17px'},message: processingText + '...'});

        if($(this).attr('data_count') > 0  || $(this).attr('data_expire') > 0){
            
            $("#product_borrowed").html('<span class="btn-lg btn-block">'+ processingText +'...<span>');

            $.get(Url, function(data, status){
              //alert("Data: " + data + "\nStatus: " + status);
                if(data == 'Sended' || status == 'success'){
                    $.unblockUI();
                    $("#product_borrowed").html('<span class="btn-lg btn-block">'+awaitingReplyText+'<span>');
                    toastr.success(awaitingReplyText);
                }
            });
        } else {
            $('#payment').submit();
        }
    });
*/

$('#score_form').submit(function(e){


    e.preventDefault();

   

    let data = $(this).serialize();


    $.ajax({
      url:  $(this).attr('action'),
      method:  $(this).attr('method'),
      data: data,
      success: function(result){
        toastr.info(result.message);

$("#score_form input").prop("disabled", true);
$("#score_form button").prop("disabled", true);
setTimeout(location.reload.bind(location), 3000);


        
      }, 
      error: function(e){
        console.log(e.responseJSON);
              $.each(e.responseJSON.errors, function (index, element) {
                  if ($.isArray(element)) {
                      toastr.error(element[0]);
                  }
              });
      }, complete: function(){
     //   $('#sendAddCityForm').removeClass('disabled');
      }
    });
  });
    $("#changeStatus").click(function(){
      var Url = "<?php echo URL('/productstatuschange/' . $product->id.'/'.$product->is_active); ?>";
        $.get(Url, function(data, status){
              //alert("Data: " + data + "\nStatus: " + status);
                if(status == 'success'){
                    $.unblockUI();
                    toastr.success("Successfully Change Status.");
                    window.location.href = "<?php echo URL('/productlist/'); ?>";

                }
            });
    });

    $('#data1').on('click',function(e){ 
      var cat = $(this).data('category');
     // $('#productcat').value = cat;
      $('#productcat option[value="' + cat +'"]').attr("selected", "selected");
      $('#edit_product').submit();
      });

</script>
@stop