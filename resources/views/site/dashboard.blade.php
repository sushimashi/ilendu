@extends('layout.default')

@section('content')
<style type="text/css">
    a.white{
        color: white !important;
    }
    .center{
        width: 150px;
        margin: 40px auto;
    }
    .plusbutton{
        height: 38px;
        border-radius: 5px 0px 0px 5px;
        width: 40px;
    }
    .minusbutton{
        height: 38px;
        border-radius: 0px 5px 5px 0px;
        width: 40px;
    }
</style>
<br/>
<div class="product-grid">
	<div class="container">
        <div class="card mt-3"></br>  
             <h3 style="font-weight:bold;">&nbsp;Dashboard</h3><hr>
            <div class="card-body">
                <div>
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#membershipmodel">Add Membership Fees</button>
                </div>
                <br>
                <?php $i = 1 ?>
        	    <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Firstname</th>
                            <th>Lastname</th>
                            <th>Email</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $userrecord)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$userrecord->first_name}}</td>
                            <td>{{$userrecord->last_name}}</td>
                            <td>{{$userrecord->email}}</td>
                            <td width="5%"><button type="button" class="btn btn-danger" data-toggle="modal" onclick="showModal({{$userrecord->id}},{{$userrecord->expireDays}})">Membership</button></td>
                            <td width="5%"><a class="white" href="{{URL::to('/userproduct/'.$userrecord->id)}}"><button type="button" class="btn btn-info" >View Detail</button></a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="clear"></div>
                <div class="div_center mb50">
                    {!!  $users->links() !!}
                  </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-sm"  tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="mySmallModalLabel">Membership</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{  Form::open(array('action' => ['DashboardController@membership'], 'files' => true, 'method'=>"post")) }}
                 {{ csrf_field() }}
                 <input type="hidden" value="" name="userid" id="userid">
            <div class="modal-body">
                    <div class="center">
                        <div class="input-group">
                          <span class="input-group-btn">
                                <button type="button" class="btn btn-danger btn-number plusbutton"  data-type="minus" data-field="days">
                                    <span class="fa fa-minus"></span>
                                </button>
                            </span>
                            <input type="text" name="days" class="form-control input-number text-center" id="days" value="" min="1" max="100">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success btn-number minusbutton" data-type="plus" data-field="days">
                                    <span class="fa fa-plus"></span>
                                </button>
                            </span>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
                {{ Form::close() }}
        </div>
    </div>
</div>
<div class="modal fade" id="membershipmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Membership Fees</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{  Form::open(array('action' => ['DashboardController@store'], 'files' => true, 'method'=>"post")) }}
                 {{ csrf_field() }}
                <div class="modal-body">
                    <div class="form-group">
                        <label>Membership Fees:</label>
                        <input type="text" class="form-control" id="fees" placeholder="Enter Fees" name="fees" required="" value="@if(isset($membershipfees)) {{$membershipfees->fees}} @endif">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
<script type="text/javascript">
    function showModal(id,days) {
        $('#userid').val(id);
        $("#days").val(days);
        $('.bd-example-modal-sm').modal();
    }
</script>
@endsection

@section('pagejavascripts')

@stop