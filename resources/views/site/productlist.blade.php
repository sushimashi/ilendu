@extends('layout.default')

@section('content')
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<!-- Datatable CSS -->
<link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

<br/>
<div class="product-grid">
	<div class="container">
        <div class="card mt-3"></br>  
             <h3 style="font-weight:bold;">&nbsp;Product List</h3><hr>
            <div class="card-body">
                <?php $i = 1 ?>
                <table class="table table-bordered display" id="example"  cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Sr No.</th>
                            <th>Product Name</th>
                            <th>Image</th>
                            <th>Category</th>
                            <th>Status</th>
                            <th>In Front Page</th>
                            <th></th>
                     
                        </tr>
                    </thead>
                    <tbody id="e1">
                      
                    </tbody>
                </table>
             
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-md" id="edit_product" tabindex="-1" role="dialog" aria-labelledby="edit_product" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="view_orderModal">Edit Product Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
             <div class="col-md-12">
                 <div class="modal-body">
                     <form method="POST" action="" role="form" id="newModalForm" name="newModalForm1" enctype="multipart/form-data">
                     {{ csrf_field()}}
                       
                        <input name="product_id" type="hidden" id="productid" value="{{$product->id}}">
                       
                        <div class="form-group">
                            <label for="product_name">Product Name</label>
                            <input type="text" name="product_name" class="form-control" id="productname" disabled="disabled" value=" ">
                         
                        </div>
                         <div class="form-group">
                            <label>Image:</label>
                                <img src="{{URL::to('')}}/img/{{$product->product_img}}"  id="productimg" height="30px" width="30px" >
                                <div class="imageholder">
                                  <input Input::old('product_img') type="file" name="product_img" value=''>
                                 
                                  <input type="hidden" name="old_file" value="{{$product->product_img}}" />
                         </div>
                       </div>
                        <div class="form-group">
                            <label for="product_desc">Category</label>
                            <select class="form-control form-control-lg" name="category" id="productcat">
                                @foreach($categories as $category )
                                     <option value="{{$category->id}}">{{$category->name}}</option>
                                  @endforeach
                             </select>    
                        </div>
                        <div class="form-group1">
                        <button type="submit" class="btn btn_color btn-sm" id="newModalForm1">Save changes</button>
                       </div>
                    </form>
                </div>
            </div>
    </div>
</div>
</div>



@section('pagejavascripts')
<!-- jQuery Library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Datatable JS -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".changeStatus").on('change',function(){
            $.blockUI({ css: { fontSize: '17px'},message: 'Processing request ...'});
            $id = $(this).data('id');
            $this = $(this);
            if($(this).prop("checked") == true){
               $status = 1;
            }else{
               $status = 0;
            }
            var Url = "<?php echo URL('/productstatuschange/'); ?>"+'/'+$id+'/'+$status;
            $.get(Url, function(data){
                        $.unblockUI();
                        if(data.status == 1){
                            toastr.success("Successfully Activated Product.");
                            $this.attr('checked');
                        }else{
                            toastr.success("Successfully Inactivated Product.");
                           $this.removeAttr('checked'); 
                        }
                });
        });

        $(".productFrontPage").on('change',function(){
            $.blockUI({ css: { fontSize: '17px'},message: 'Processing request ...'});
            $id = $(this).data('id');
            $this = $(this);
            if($(this).prop("checked") == true){
               $status = 1;
            }else{
               $status = 0;
            }
            var Url = "<?php echo URL('/productfrontpage/'); ?>"+'/'+$id+'/'+$status;
            $.get(Url, function(status){
                        $.unblockUI();
                        toastr.success("Successfully Change.");
                        if(status == 1){
                            $this.attr('checked');
                        }else{
                           $this.removeAttr('checked'); 
                        }
                });
        });
    });
</script>
<script type="text/javascript" src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
   function getMessage(){
  $('.edit_link').on('click',function(){
              var id = $(this).data('id');
              var name = $(this).data('name');
              var cat =$('#pcat'+id).attr('value');
              var img = $('#pimg'+id).attr('src');
              var cat = $(this).data('category');
                  $('#productid').attr('value',id);
                  $('#productname').attr('value',name);
                  $('#productimg').attr('src',img); 
                  $('#productcat option[value="' + cat +'"]').attr("selected", "selected");
              var url = $(this).data('href');
              $('#newModalForm').attr('action',url);
             var a = $('#newModalForm').serialize();
             console.log(a);
            $('#newModalForm1').on('click',function(){
             $('#edit_product').submit();
            });
         });
    }
</script>
<script type="text/javascript">
  $(document).ready(function() {
   var table =  $('#example').DataTable({
        serverSide: true,
        serverMethod: 'get',
        processing: true,
        pagingType: 'full_numbers',
        ajax: '{{URL::to("/getproductlist")}}',
        columns: [
                        { data: 'id', name: 'id' },
                        { data: 'product_name', name: 'product_name' },
                        { data: 'product_img', name: 'product_img' },
                        { data: "product_category", name: 'product_category' },
                        { data: 'is_active', name: 'is_active' },
                        { data: 'in_front', name: 'in_front' },
                        { data: 'id', name: 'id' },
                        ],
        columnDefs: [
                       { targets: 2,
                        render: function(data, type, full, meta) {
                          // var url = "{{URL::to('/img/')}}"+'/' +data;
                          var action ="{{URL::to('productlist/edit')}}"+'/' +full.id;
                          $('#newModalForm').attr('action',action);
                          return '<img id="pimg'+full.id+'" src="'+data+'"  height="50px">'
                        }
                      } ,
                      { targets:6,
                        render: function(data, type, full, meta) {
                          var url = "{{URL::to('productdetails')}}"+'/' +data;
                          return '<a class="white" href="'+url+'"><button type="button" class="btn btn-info" >View Detail</button></a>&nbsp;<a data-toggle="modal"  data-target="#edit_product"><button type="button" id="edit_it" data-id="'+data+'" data-name="'+full.product_name+'"class="btn btn-primary edit_link"  data-category="'+full.category_id+'" onclick="'+getMessage()+'">Edit</button></a>'
                        }
                  } ,
                  { targets: 4,
                        render: function(data) {
                         return data == '1' ? ' <input class = "changeStatus" type="checkbox" name="is_active" data-toggle="toggle" data-on="Active" data-off="In Active" data-onstyle="success" data-offstyle="danger" data-id="'+data+'" checked>':'<input class = "changeStatus" type="checkbox" name="is_active" data-toggle="toggle" data-on="Active" data-off="Inactive" data-onstyle="success" data-id="'+data+'" data-offstyle="danger">'
                        }
                      } ,
                  { targets: 5,
                        render: function(data) {
                           return data == '1' ? '<input class="productFrontPage" type="checkbox" name="in_front" data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-offstyle="danger" data-id="'+data+'" checked >' : '<input class = "productFrontPage" type="checkbox" name="in_front" data-toggle="toggle" data-on="Yes" data-off="No" data-onstyle="success" data-id="'+data+'" data-offstyle="danger" >'
                        }
                      }  
               ],
               "fnDrawCallback": function() {
                   $('.changeStatus').bootstrapToggle();
                   $('.productFrontPage').bootstrapToggle();
            },
        });
    });
</script>

@stop
@endsection