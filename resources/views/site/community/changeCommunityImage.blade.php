<div class="modal" id="myModal" tabindex="-1" role="dialog" style="z-index: 9999;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h6 class="modal-title">@lang('sentence.CropImageAndUpload')</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <div id="resizer"></div>
                <button class="btn rotate float-lef" data-deg="90" > 
                <i class="fas fa-undo"></i></button>
                <button class="btn rotate float-right" data-deg="-90" > 
                <i class="fas fa-redo"></i></button>
                <hr>
                <button class="btn btn-block btn-dark" id="upload" > 
                @lang('sentence.CropAndUpload')</button>
            </div>
        </div>
    </div>
</div>