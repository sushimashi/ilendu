<script type="text/javascript">
		
	$('#sendCreateCommunityForm').click(function(e){

		if($('#communityName').val() === ''){
			e.preventDefault();
			toastr.options.timeOut = 2500;
			return toastr['warning']('@lang('sentence.nameRequired')');
		}

		if($('#com_image').val() === ''){
			e.preventDefault();
			toastr.options.timeOut = 2500;
			return toastr['warning']('@lang('sentence.imageRequired')');
		}
		
		if($('#communityDescription').val() === ''){
			e.preventDefault();
			toastr.options.timeOut = 2500;
			return toastr['warning']('@lang('sentence.descriptionRequired')');
		}

	});

</script>