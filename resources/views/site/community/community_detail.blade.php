@extends('layout.default')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Croppie css 
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">

-->
<style type="text/css">

.MultiCarousel { float: left; overflow: hidden; padding: 15px; width: 100%; position:relative; }
    .MultiCarousel .MultiCarousel-inner { transition: 1s ease all; float: left; }
        .MultiCarousel .MultiCarousel-inner .item { float: left;}
        .MultiCarousel .MultiCarousel-inner .item > div { text-align: center; padding:10px; margin:10px; background:white; color:#666;}
    .MultiCarousel .leftLst, .MultiCarousel .rightLst { position:absolute; border-radius:50%;top:calc(50% - 20px); }
    .MultiCarousel .leftLst { left:0; }
    .MultiCarousel .rightLst { right:0; }
    
        .MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over { pointer-events: none; background:#ccc; }

    input[type="file"] {
    display: none;
}
.custom-file-upload {
    display: inline-block;
    object-fit: scale-down;
    cursor: pointer;
}
.carousel-inner image
{
  width : 100%;  
  height : 470px;
  object-fit: scale-down; 
}
main
{
    background:  #f1f2f2;
}
</style>


@push('css')

<style type="text/css">

.botao-wpp {
  height: 20px !important;
  text-decoration: none;
  color: #eee;
  display: inline-block;
  background-color: #25d366;
  font-weight: bold;
  font-size: 13px;
  border-radius: 3px;
}

.botao-wpp:hover {
  background-color: darken(#25d366, 5%);
}

.botao-wpp:focus {
  background-color: darken(#25d366, 15%);
}


 @media (min-width: 668px) {

.whatsapp{display: none}
}

body > main > div.container > div > div.col-sm-7.mt-3 > div > div.gc-display-area > div.gc-icon.gc-icon-download{display: none !important}
  a{color: black;}


</style>
@endpush

@section('meta')
<meta property="og:url"                content="{{url()->current()}}" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="{{$product->product_name}}" />
<meta property="og:description"        content="{{$product->product_desc}}" />
<meta property="og:image" content="{{url('img/'.$product->product_img)}}" />
<meta property="og:image:secure_url"   content="{{url('img/'.$product->product_img)}}" />
@endsection

<div id="{{$user->id}}" class="user" style="display: none;" ></div>
<meta name="csrf-token" content="{{ csrf_token() }}">


<div class="slider_div">
         <!--
                <ul class="list-iline slider_ul">
                    <li class="list-inline-item"><a href="#">Books</a></li>
                    <li class="list-inline-item"><a href="#">Sports</a></li>
                    <li class="list-inline-item"><a href="#">Movies</a></li>
                    <li class="list-inline-item"><a href="#">Others</a></li>
                </ul> -->
</div>


            <div id="demo" class="carousel slide" data-ride="carousel">
                <!-- <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active d-none d-sm-block"></li>
                    <li data-target="#demo" data-slide-to="1" class="d-none d-sm-block"></li>
                    <li data-target="#demo" data-slide-to="2" class="d-none d-sm-block"></li>
                </ul> -->
                <div class="carousel-inner">
               
                  <div class="carousel-item active">
                       @if(@$image[0]->com_image != "")
                         <div class="jumbotron" style="
    background-image: url('/community_image/{{$image[0]->com_image}}');
    background-attachment: fixed; background-size: cover; background-repeat: no-repeat;"></div>
                        @else
                            <img style="height: 470px;object-fit:cover;" class="d-block w-100" src="{{url('/')}}/img/no-image.png" alt="">
                        @endif
                         <input type="hidden" id="old" name="old_file" value="{{$image[0]->com_image}}"/>
                        @if($community[0]->created_by == $user->id) 
                         {{--<div class="btn btn-light">

                            <label for="file-upload" class="custom-file-upload">
                                <i class="fa fa-cloud-upload"></i>
                                @lang('sentence.changeImage')
                            </label>

                            <input type="file" class="file-upload" id="file-upload" 
                            name="profile_picture" accept="image/*">

                          </div>--}}
                        @endif
                  </div>

                </div>
            </div>

            <div class="container">
                <div class="row">

                    <div class="col-lg-4">
                        <div class="video_main"> </div>
                    </div>

                    <div class="col-lg-8">
                        <div class="box_banner" style="margin-bottom: -5px;">
                            @foreach($community as $communities)
                                
                                <h1>{{$communities->name}}</h1>
                                <input type="hidden" id="community_id" value="{{$communities->id}}">

                              <div class="cropit" id="{{$communities->id}}" style="display: none;"></div>

                              <h4>
                                <div class="comment">{{$communities->description}}</div>
                              </h4>

                                <!--social networks-->

                              <div class="fb-share-button" data-href="{{url()->current()}}" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Compartir</a>

                                  <!-- twitter

                                  <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-text="Ilendu, never buy,share...{{$product->product_name}}:" data-url="https://ilendu.co/products/{{$product->id}}" data-show-count="false">Tweet</a>
                                  -->
                                  </div>

                                  <div style="margin-top: 4px; display: inline-block;" >

                                  <!-- twitter-->

                                  <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-text="Ilendu, never buy,share...{{$product->product_name}}:" data-url="https://ilendu.co/products/{{$product->id}}" data-show-count="false">Tweet</a>

                                  </div>
                                  <div style="margin-top: 0px;display: inline-block;" >
                                  <!-- só o conteúdo do href já é suficiente -->
                                  <a href="whatsapp://send?text=Ilendu, never buy,share...{{$product->product_name}}: {{url()->current()}}" class="botao-wpp whatsapp">
                                    <!-- ícone -->
                                    <!--<i class="fa fa-whatsapp"></i>-->
                                    whatsapp
                                  </a>
                                  </div>

                              <div id="status" value="{{$communities->public}}">
                                <b>@lang('keywords.status')</b>: @if($communities->public == 0) @lang('keywords.private') @else @lang('keywords.public') @endif
                              </div>

                                @if(Auth::user()->id != $communities->created_by)
                                  <input style="cursor: pointer" type="button"  id="{{$communities->id}}"  data-backdrop="static" data-keyboard="false"  class="btn banner_btn @if($is_join == 0) join @else unjoin @endif" @if($pending==1 && $is_join==0) disabled @endif  value="@if($pending == 1  && $is_join==0) @lang('keywords.pending') @elseif($is_join==0) @lang('keywords.join') @else @lang('keywords.joined') @endif" @if($is_join == 1) style="background-color: #3b824b;width: 500px; font-size: 28px;" @endif style="width: 500px;font-size: 28px;"/>
                                @endif

                            @endforeach                        
                        </div>
                    </div>

                </div>

                <div class="row">
                              @if(count($members))
       <div class="col-md-12 col-lg-12 col-sm-12">
              <h4 class="text-center"  style="background-color: white; padding: 1%; margin-top: 2%;"><strong>Members</strong> </h4>
              </div>
<div class="MultiCarousel" data-items="{{$items}}"  data-slide="1" id="MultiCarousel"  data-interval="1000">
            <div class="MultiCarousel-inner">
             
              @foreach($members as $m)
                <div class="item">
                    <div class="pad15" >
                    <a href="{{url('reputation/'.$m->id)}}">
                        @if($m->profile_img!="")
                        <img style="height: 150px; width: 150px" src="{{asset('img/profile/'.$m->profile_img)}}" class="rounded-circle"> 
                        @else
                        <img src="{{url('/')}}/img/avatar.jpg"  style="height: 150px; width: 150px" alt="{{$m->first_name}}" class="rounded-circle">
                        @endif
                      </a>
                        <p><b> <a href="{{url('reputation/'.$m->id)}}">{{$m->first_name.' '.$m->last_name}}</a></b></p>
                    </div>
                </div>

                @endforeach
               
            </div>
            @if(count($members)>3)
            <button class="btn btn-primary leftLst" ><</button>
            <button class="btn btn-primary rightLst" >></button>
         
            @endif
        </div>

        @else
        <div class="col-md-12">
          <h4 class="text-center" style="background-color: white; padding: 1%; margin-top: 2%;"><strong>This community doesn't have members yet</strong></h4>
        </div>
  @endif  
  </div>





                <div style="clear:both"></div>

                 @if(count($product_communities) == 0 && $is_join == 0)
                 <div style="margin-top: 5%;">
                   {{-- <h5><center>@lang('communitiesView.noProductsAddedInCommunity')</center></h5>--}}
                   
                 </div>   
                 @endif
                 <!-- if user has joined-->
                @if(@$is_join == 1 || @$community[0]->public == 1)
                <div class="row" style="margin-top: 3%;">
                <!-- <div class="row" style="margin-top: 3%;background-color:#F5F5F5;"> -->
                    <div class="col-lg-3 col-sm-3 mb-3 bg_white">
                      <div class="col-lg-9 col-sm-9 mb-9" style="display: inline-block;padding: 0px"> 
                        <div class=" ">
                            
                            <input type="text" id="catsearch" class="form-control input_border" placeholder="@lang('keywords.search')..." data-id="{{$community[0]->id}}" name="search" value="@if(isset($search)) {{$search}}@endif"/>
                       
                           {{-- <h6 class="text-uppercase diff_color">@lang('keywords.categories')</h6>--}}
                        </div>
</div>

  <div class="col-lg-1 col-sm-1 mb-1 text-left" style="display: inline-block; padding: 0px">

                      <button type="button" id="search_button" class="btn btn-default">Search</button>

                    </div> 
  <div class="form-check-inline" style="padding-left: 12px !important; padding-right: 12px !important;">
<label class="customradio"><span class="radiotextsty">@lang('keywords.filter_all')</span>
<input type="radio" class="cat_filter" data-value="all" name="filter_radio" checked value="all">
<span class="checkmark"></span>
</label>
&nbsp; &nbsp;

<label class="customradio" style="margin-right: -13px !important;"><span class="radiotextsty">@lang('keywords.filter_premium')</span>
<input type="radio" class="cat_filter" data-value="premium" name="filter_radio" value="premium">
<span class="checkmark"></span>
</label>
&nbsp; &nbsp;

<label class="customradio" style="margin-left: 5%;"><span class="radiotextsty">@lang('keywords.filter_gift')</span>
<input type="radio" class="cat_filter"  data-value="gift"  name="filter_radio" value="gift">
<span class="checkmark"></span>
</label>
                    
                   
                </div>
                <hr>

                         @foreach($categories as $category) 
              <div class="radio" style="margin-left: 4%;">
        
                    <label class="customradio" style="margin-right: -13px !important;"><span class="radiotextsty">{{$category['name']}}</span>
                      <input type="radio" class="cat_radio" id="category_{{$category->name}}" name="category_id" value="{{$category->id}}">
                      <span class="checkmark"></span>
                    </label>   
              
               
              </div>
            @endforeach
                    </div>
                   
                    <div class="col-lg-9 col-sm-9"><br>



                          <div id="caja-busqueda">
                          @include('site.community.pagination.index')
                          </div>

                      </div>

                    </div>
                @endif
            </div> </div><br/>

    <!-- Change Community Image Modal -->
    @include('site.community.changeCommunityImage')
    <br/>

@endsection

@section('pagejavascripts')

{{-- <!--  jQuery and Popper.js  -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" 
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" 
    crossorigin="anonymous"></script> --}}

<!-- Croppie js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.js"></script>

<script type="text/javascript">
  
$(document).ready(function () {
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";

    $('.leftLst, .rightLst').click(function () {
        var condition = $(this).hasClass("leftLst");
        if (condition)
            click(0, this);
        else
            click(1, this)
    });

    ResCarouselSize();




    $(window).resize(function () {
        ResCarouselSize();
    });

    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);


            if (bodyWidth >= 1200) {
                incno = itemsSplit[3];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 992) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 768) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").addClass("over");
            $(".rightLst").removeClass("over");

        });
    }


    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }

    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    }

});


 $(document).on('click','.cat_filter', function (e) {

          var value=$(this).attr('data-value');

          $.ajax({
            url :  '/community_categories',
            data:{
              'filter_radio':value,
              'id':$("#community_id").val()
            },
            dataType: 'json',
            beforeSend: function(){
             
            },
        }).done(function (data) {
           $(".loader").addClass('hide');
            $(".content-box").removeClass('hide');

          //$(".loader").hide();
            $('#caja-busqueda').html(data);
           
        }).fail(function () {
           // alert('Posts could not be loaded.');
        });
          
        });
 $(document).on('click','.cat_radio', function (e) {


          $.ajax({
            url :  '/community_categories',
            data:{
              'category_id':$(this).val(),
              'id':$("#community_id").val()
            },
            dataType: 'json',
            beforeSend: function(){
             
            },
        }).done(function (data) {
           $(".loader").addClass('hide');
            $(".content-box").removeClass('hide');

          //$(".loader").hide();
            $('#caja-busqueda').html(data);
           
        }).fail(function () {
           // alert('Posts could not be loaded.');
        });
          
        });

 $(document).on('click','#search_button', function (e) {
         e.preventDefault();

          $.ajax({
            url :  '/community_products',
            data:{
              'busqueda':$("#catsearch").val(),
              'id':$("#community_id").val()
            },
            dataType: 'json',
            beforeSend: function(){
             
            },
        }).done(function (data) {
           $(".loader").addClass('hide');
            $(".content-box").removeClass('hide');
           $('.cat_radio').prop('checked', false);

          //$(".loader").hide();
            $('#caja-busqueda').html(data);
           
        }).fail(function () {
           // alert('Posts could not be loaded.');
        });
          
        });

   $(document).on('click', '.pagination a', function (e) {
         e.preventDefault();

         page=$(this).attr('href').split('page=')[1];
         pageRaiz=$(this).attr('href');
         console.log(pageRaiz);
        $(".loader").removeClass('hide');
    $(".content-box").addClass('hide');

          $.ajax({
            url :  pageRaiz,
            dataType: 'json',
            beforeSend: function(){
             
            },
        }).done(function (data) {
           $(".loader").addClass('hide');
    $(".content-box").removeClass('hide');

          //$(".loader").hide();
           console.log(data);
            $('#caja-busqueda').html(data);
           
            location.hash = page;
        }).fail(function () {
           // alert('Posts could not be loaded.');
        });
          
        });
</script>

<script type="text/javascript">
$(function() {
    var croppie = null;
    var el = document.getElementById('resizer');

    $.base64ImageToBlob = function(str) {
        // extract content type and base64 payload from original string
        var pos = str.indexOf(';base64,');
        var type = str.substring(5, pos);
        var b64 = str.substr(pos + 8);
      
        // decode base64
        var imageContent = atob(b64);
      
        // create an ArrayBuffer and a view (as unsigned 8-bit)
        var buffer = new ArrayBuffer(imageContent.length);
        var view = new Uint8Array(buffer);
      
        // fill the view, using the decoded base64
        for (var n = 0; n < imageContent.length; n++) {
          view[n] = imageContent.charCodeAt(n);
        }
      
        // convert ArrayBuffer to Blob
        var blob = new Blob([buffer], { type: type });
      
        return blob;
    }

    $.getImage = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {  
                 $('#resizer').croppie('bind',{
                    url: e.target.result,
                });
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#file-upload").on("change", function(event) {
        $('#myModal').on('shown.bs.modal', function () {
    $(this).find('.modal-dialog').css({ position: 'relative',
    display: 'table', /* <-- This makes the trick */
    'overflow-y': 'auto',    
    'overflow-x': 'auto',
     width: 'auto',
    'min-width': '300px'});
});
        $("#myModal").modal();
        $("#myModal").focus();  
        // Initailize croppie instance and assign it to global variable
        $('#resizer').croppie({
            enableExif: true,
                viewport: {
                    width: 1000,
                    height: 1000,
                    type: 'square'
                },
                boundary: {
                    width: 1000,
                    height: 1000
                },
                enableOrientation: true,
                enableResize: true,
                mouseWheelZoom: 'ctrl',
                showZoomer: true
            });

/*

        croppie = new Croppie(el, {
            enableExif: true,
                viewport: {
                    width: 1920,
                    height: 860,
                    type: 'square'
                },
                boundary: {
                    width: 1920,
                    height: 860
                },
                enableOrientation: true,
                enableResize: true,
                mouseWheelZoom: 'ctrl',
                showZoomer: true
            });*/
        $.getImage(event.target); 
    });

    $("#upload").on("click", function() {

 $('#resizer').croppie('result',{circle: false, size: "original", type:"rawcanvas"}).then(function (rawcanv) {
   //$("#myModal").modal("hide"); 

  var url = "{{ url('/demos/jquery-image-upload') }}";
  var comid = $('.cropit').attr('id');
  var old = $('#old').attr('value');

  $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

  resample_single(rawcanv, 1000, 1000, true);
  var canvasBase64 = rawcanv.toDataURL();

    $.ajax({
                type: 'POST',
                url: url +"/"+comid+"/"+old,
                data:{"profile_picture":canvasBase64.substring(22)},
                success: function(data) {
                    if (data.success==true) {
                         document.location.reload(true);
                    } else {
                        $("#profile-pic").attr("src","/image/no-image.png"); 
                        console.log(data['profile_picture']);
                    }
                },
                error: function(error) {
                    console.log(error);
                    $("#profile-pic").attr("src","/image/no-image.png"); 
                }
            });
        


})
    /*
        croppie.result('base64','original').then(function(base64) {
            $("#myModal").modal("hide"); 
            //$("#profile-pic").attr("src","/images/ajax-loader.gif");

            var url = "{{ url('/demos/jquery-image-upload') }}";
            var formData = new FormData();
            formData.append("profile_picture", $.base64ImageToBlob(base64));

            // This step is only needed if you are using Laravel
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var comid = $('.cropit').attr('id');
            var old = $('#old').attr('value');
            $.ajax({
                type: 'POST',
                url: url +"/"+comid+"/"+old,
                data: formData,
                processData: false,
                contentType: false,
                success: function(data) {
                    if (data == "uploaded") {
                        $("#profile-pic").attr("src", base64);
                         $("#profile-pic").attr("style","height: 470px;object-fit:scale-down;"); 
                         document.location.reload(true);
                    } else {
                        $("#profile-pic").attr("src","/image/no-image.png"); 
                        console.log(data['profile_picture']);
                    }
                },
                error: function(error) {
                    console.log(error);
                    $("#profile-pic").attr("src","/image/no-image.png"); 
                }
            });
        });*/
    });

    // To Rotate Image Left or Right
    $(".rotate").on("click", function() {
        croppie.rotate(parseInt($(this).data('deg'))); 
    });

    $('#myModal').on('hidden.bs.modal', function (e) {
        // This function will call immediately after model close
        // To ensure that old croppie instance is destroyed on every model close
        setTimeout(function() { croppie.destroy(); }, 100);
    })

});


function resample_single(canvas, width, height, resize_canvas) {
  var width_source = canvas.width;
  var height_source = canvas.height;
  width = Math.round(width);
  height = Math.round(height);

  var ratio_w = width_source / width;
  var ratio_h = height_source / height;
  var ratio_w_half = Math.ceil(ratio_w / 2);
  var ratio_h_half = Math.ceil(ratio_h / 2);

  var ctx = canvas.getContext("2d");
  var img = ctx.getImageData(0, 0, width_source, height_source);
  var img2 = ctx.createImageData(width, height);
  var data = img.data;
  var data2 = img2.data;

  for (var j = 0; j < height; j++) {
    for (var i = 0; i < width; i++) {
      var x2 = (i + j * width) * 4;
      var weight = 0;
      var weights = 0;
      var weights_alpha = 0;
      var gx_r = 0;
      var gx_g = 0;
      var gx_b = 0;
      var gx_a = 0;
      var center_y = (j + 0.5) * ratio_h;
      var yy_start = Math.floor(j * ratio_h);
      var yy_stop = Math.ceil((j + 1) * ratio_h);
      for (var yy = yy_start; yy < yy_stop; yy++) {
        var dy = Math.abs(center_y - (yy + 0.5)) / ratio_h_half;
        var center_x = (i + 0.5) * ratio_w;
        var w0 = dy * dy; //pre-calc part of w
    var xx_start = Math.floor(i * ratio_w);
    var xx_stop = Math.ceil((i + 1) * ratio_w);
    for (var xx = xx_start; xx < xx_stop; xx++) {
      var dx = Math.abs(center_x - (xx + 0.5)) / ratio_w_half;
      var w = Math.sqrt(w0 + dx * dx);
      if (w >= 1) {
        //pixel too far
        continue;
      }
      //hermite filter
      weight = 2 * w * w * w - 3 * w * w + 1;
      var pos_x = 4 * (xx + yy * width_source);
      //alpha
      gx_a += weight * data[pos_x + 3];
      weights_alpha += weight;
      //colors
          if (data[pos_x + 3] < 255)
            weight = weight * data[pos_x + 3] / 250;
          gx_r += weight * data[pos_x];
          gx_g += weight * data[pos_x + 1];
          gx_b += weight * data[pos_x + 2];
          weights += weight;
        }
      }
      data2[x2] = gx_r / weights;
      data2[x2 + 1] = gx_g / weights;
      data2[x2 + 2] = gx_b / weights;
      data2[x2 + 3] = gx_a / weights_alpha;
    }
  }
  //clear and resize canvas
  if (resize_canvas === true) {
    canvas.width = width;
    canvas.height = height;
  } else {
    ctx.clearRect(0, 0, width_source, height_source);
  }

  //draw
  ctx.putImageData(img2, 0, 0);
}
</script>

<script type="text/javascript" src="https://viralpatel.net/blogs/demo/jquery/jquery.shorten.1.0.js"></script>
<script language="javascript">
  $(document).ready(function() {  
           $(".comment").shorten({
            "showChars" : 60,
            "moreText"  : "See More",
            "lessText"  : "Less",
        });
    });

</script>

<script >

   toastr.options.onHidden = function() { location.reload(true) };
   toastr.options.onHidden = function() { location.reload(true) };

  /////////////////////////////////
  $(document).on('click','.unjoin',function(){

     $this = $(this);
     var id = this.id;

     if ($this.hasClass('disabled'))
      return;

     var val=$(this).val().replace(/ /g,'')
     if(val=='Joined' || val=='Unido'){

        if (confirm("do you want to unjoin from this community?")==true) 
        {

           var status =  document.getElementById('status').getAttribute('value');
           var userid = document.querySelector('.user').id;
           $this.addClass('disabled');
           $.ajax({
              type: "get",
              url:'/unjoincommunity/'+id+"/"+status,
                    success: function (data) {
                        document.getElementById(id).value="join up";
                       $(".join").css({'background-color': '#3b824b','width': '500px'});
                        $this.removeClass('unjoin disabled');
                        toastr.success("Successfully Unjoined Community.");

              }
          });
        }
     }
   
  });

  /////////////////////////////////
  $(".join").on('click',function(){
   
   $this = $(this);

   if($this.hasClass('disabled'))
    return;

   var id = this.id;
   
   if(document.getElementById(id).value == 'Joined' || document.getElementById(id).value == 'Unido'){
    return false;
   }

   var public =  document.getElementById('status').getAttribute('value');
   var userid = document.querySelector('.user').id;
   $this.addClass('disabled');

     $.ajax({
        type: "get",
        url: (public==1 ? '/joincommunity/'+id+"/"+public : '/requestemail/'+id+"/"+userid),
              success: function (data) {
                  document.getElementById(id).value="Joined";
                  $(".join").css({'background-color': '#3b824b','width': '500px'});
                  $this.removeClass('join disabled');
                  toastr.success("Successfully Joined Community.");
          
        }
      });

  });

  /////////////////////////////////
  $('#catsearch').on('keypress',function(e) {
      if(e.which == 13) {
          id = $(this).data('id');
          search = $(this).val();
          $.ajax({
              url: "{{url('categorysearch')}}/"+id+"/"+search,
              type: "get",
              success: function (response) {
                 $('#categorydata').html(response);
              }
          });
      }
  });

</script>

@stop