<div class="modal fade" id="communitymodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">@lang('communitiesView.community')</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
         
            <form id="create_comunity_form" enctype="multipart/form-data" method="post" action="{{url('communities')}}">
            {{ csrf_field() }}

              <div class="modal-body">

                  <div class="form-group">
                      <label>@lang('communitiesView.community'):</label>
                      <input type="text" class="form-control" placeholder="@lang('keywords.name')" id="communityName" name="name" value="">
                  </div>

                   <div class="form-group">
                      <label>@lang('keywords.image'):</label>

                           <input  type='file' name='com_image' id='com_image' class='inputfile '>
                              
                          <div class='avatar_preview'></div>
                      <div><i>@lang('keywords.cover')</i></div>

                   </div>
                  <div class="form-group">
                      <label>@lang('keywords.description'):</label>
                      <textarea  id="communityDescription" class="form-control" id="description" type="text"  placeholder="@lang('sentence.addDescriptionDetails')" name="description" ></textarea>

                  </div>
                      <div class="form-group">
                          <label>@lang('keywords.status'):</label>
                          <select name="status" class="form-control" >
                              <option value="0">@lang('keywords.private')</option>
                              <option value="1">@lang('keywords.public')</option>
                          </select>
                      </div>
                  <div class="modal-footer">
                      <button id="sendCreateCommunityForm" type="submit" class="btn btn-primary">@lang('keywords.save')</button>
                  </div>
              </form>
          </div>
      </div>
  </div>
</div>