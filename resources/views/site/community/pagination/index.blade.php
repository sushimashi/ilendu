@if(count($product_communities)>0)

    <div class="loader text-center hide" style="min-height: 700px">
        <img src="{{asset('img/loader.gif')}}">
    </div>

    <div class="row content-box" >

                              @foreach($product_communities as $product_community)

                              <div class="col-lg-4 col-sm-6" style="padding-top: 2%">
                                <div class="card" style="height: 100%">
                                  <a href="{{ url('products/'.$product_community->id)}}">

                                  @if($product_community->product_img != "")
                                  <img class="card-img-top" style="padding: 5%"  src="{{url('/')}}/img/thumbnail/{{ $product_community->product_img}}" alt="">
                                  @else
                                  <img class="card-img-top"  style="padding: 5%"  src="https://www.freeiconspng.com/uploads/no-image-icon-6.png" alt="">
                                  @endif

                                  <p class="card-text text-center">{{$product_community->product_name}}</p>
                                  </a>

                                <table>
                                  
                                  <td class="text-center">
                                    @if( isset($product_community->profile_img) && file_exists(public_path("/img/profile/".$product_community->profile_img)))
                                    
                                      <img src="{{url('/')}}/img/profile/{{$product_community->profile_img}}" class="small_image">

                                    @else

                                      <img src="{{url('/')}}/img/profile/user.png" class="small_image">
                                    
                                    @endif
                                  </td>
                                  <br>
                                  <td><span class="text-uppercase sub_text">@lang('productsView.submittedBy')</span><br/>
                                    <a href="{{url('reputation/'.$product_community->user_id)}}">{{$product_community->first_name}} {{$product_community->last_name}}</a></td>
                                </table>
                                 @if($product_community->is_premium==1 && $product_community->give_away==1)
                 <div style=" border-color: #F10080 !important; background: #F10080 !important; " class="topright">@lang('keywords.gift')</div>

                @elseif($product_community->is_premium==1)
                 <div style=" border-color: #F10080 !important; background: #F10080 !important; " class="topright">@lang('keywords.premium')</div>
                 @endif
                                </div>
                              </div>

       
        @endforeach

           
            
  <div class="text-center col-md-12 col-sm-12" style="margin-top: 3%">
  {{ $product_communities->appends(Request::except('page'))->render() }}
  </div>

    </div>

@else

    <div class="row">
        <div class="col-md-12">
            <p class="text-center"><b >@lang('sentence.noResultsFound')</b></p>
        </div>
    </div>

@endif

