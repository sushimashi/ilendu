  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">@lang('communitiesView.community')</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
            <form id="edit_community_form" enctype="multipart/form-data" method="post" action="{{url('communities/'.$community->id)}}">
                            {{ csrf_field() }}

              <input name="_method" type="hidden" value="PUT">
                <input type="hidden" name="id" value="{{$community->id}}">
              <div class="modal-body">

                  <div class="form-group">
                      <label>@lang('communitiesView.community'):</label>
                      <input type="text" class="form-control" placeholder="@lang('keywords.name')" id="communityName" name="name" value="{{$community->name}}">
                  </div>

                   <div class="form-group">
                      <label>@lang('keywords.image'):</label>
                          <img src="{{URL::to('')}}/community_image/{{$community->com_image}}" height="30px" width="30px" >

                           <input  type='file' name='com_image' id='com_image' class='inputfile '>
                              
                          <div class='avatar_preview'></div>
                      <div><i>@lang('keywords.cover')</i></div>

                   </div>
                  <div class="form-group">
                      <label>@lang('keywords.description'):</label>
                      <textarea  id="communityDescription" class="form-control" id="description" type="text"  placeholder="@lang('sentence.addDescriptionDetails')" name="description" >{{$community->description}}</textarea>

                  </div>
                      <div class="form-group">
                          <label>@lang('keywords.status'):</label>
                          <select name="status" class="form-control" >
                           
                              <option value="0" {{$community->public==0?'selected':''}}>@lang('keywords.private')</option>
                              <option value="1" {{$community->public==1?'selected':''}}>@lang('keywords.public')</option>
                          

                          </select>
                      </div>
                  <div class="modal-footer">
                      <button id="sendCreateCommunityForm" type="submit" class="btn btn-primary">@lang('keywords.save')</button>
                  </div>
                </form>
          </div>
      </div>
  </div>