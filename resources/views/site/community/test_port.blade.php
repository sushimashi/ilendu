@extends('layout.default')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!-- Croppie css -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('css/cover_image.css')}}">

<style type="text/css">
    input[type="file"] {
    display: none;
}
.custom-file-upload {
    display: inline-block;
    object-fit: scale-down;
    cursor: pointer;
}
.carousel-inner image
{
  width : 100%;  
  height : 470px;
  object-fit: scale-down; 
}
main
{
    background:  #f1f2f2;
}

.background-img{
  background-image: url('http://localhost:8000/community_image/kyc57.png');
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;
  height: 100vh;
  width: 100%;
}

</style>
<div id="{{$user->id}}" class="user" style="display: none;" ></div>
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="slider_div">
         <!--
                <ul class="list-iline slider_ul">
                    <li class="list-inline-item"><a href="#">Books</a></li>
                    <li class="list-inline-item"><a href="#">Sports</a></li>
                    <li class="list-inline-item"><a href="#">Movies</a></li>
                    <li class="list-inline-item"><a href="#">Others</a></li>
                </ul> -->
            </div>

<div class="hovercover text-center" id="bgimage" style="width: 50%;">

                    <img src="{{asset('community_image').'/'.$image[0]->com_image}}" class="img-responsive "    style="margin-top: {{$image[0]->position_image}}; width: 100% !important;">
                        <div class="hover-div"  >
                           <form method="post" id="hoverform" action="{{url('/upload')}}" enctype="multipart/form-data">
                           {{ csrf_field() }}
                                <label for="file-upload" class="custom-file-upload" title="Change Cover Image">
                                    <i class="fa fa-file-image-o"></i>&nbsp; Change Cover
                                </label>



                                <input id="file-upload" name="file" type="file" />
                             </form>
                        </div>
                </div>

                <div class="hovercover1 text-center" id="adjimage" style="display: none; ">
                   
                </div>
                </div>
           {{-- <div id="demo" class="carousel slide" data-ride="carousel">
            
                <div class="carousel-inner">
               
                  <div class="carousel-item active">
                    <div class="row background-img"></div>
           
                         <input type="hidden" id="old" name="old_file" value="{{$image[0]->com_image}}"/>
                        @if($community[0]->created_by == $user->id) 
                         <div class="btn btn-light">

                            <label for="file-upload" class="custom-file-upload">
                                <i class="fa fa-cloud-upload"></i>
                                @lang('sentence.changeImage')
                            </label>

                            <input type="file" class="file-upload" id="file-upload" 
                            name="profile_picture" accept="image/*">

                          </div>
                        @endif
                  </div>

                </div>
            </div>--}}



    <!-- Change Community Image Modal -->
    @include('site.community.changeCommunityImage')
    <br/>

@endsection

@section('pagejavascripts')
<script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.wallform.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jwincrop.js')}}"></script>
<script type="text/javascript" >
 

     // submit the form soon after change image
     $('#file-upload').on('change',function(){
        
        $("#hoverform").ajaxForm({target: '#adjimage',
            success:function(){
            $(".hover-div").hide();
                
                $("#bgimage").hide();
                $("#adjimage").show();
                
                
            }}).submit();
     });
    
   
     
  $('.hovercove').each(function() {
    //set size
    var th = $(this).height(),//box height
        tw = $(this).width(),//box width
        im = $(this).children('img'),//image
        ih = im.height(),//inital image height
        iw = im.width();//initial image width
    if (ih>iw) {//if portrait
        im.addClass('ww').removeClass('wh');//set width 100%
    } else {//if landscape
        im.addClass('wh').removeClass('ww');//set height 100%
    }
    //set offset
    var nh = im.height(),//new image height
        nw = im.width(),//new image width
        hd = (nh-th)/2,//half dif img/box height
        wd = (nw-tw)/2;//half dif img/box width
    if (nh<nw) {//if portrait
        im.css({marginLeft: '-'+wd+'px', marginTop: 0});//offset left
    } else {//if landscape
        im.css({marginTop: '-'+hd+'px', marginLeft: 0});//offset top
    }
});


</script>


{{-- <!--  jQuery and Popper.js  -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" 
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" 
    crossorigin="anonymous"></script> --}}

<!-- Croppie js -->

<script type="text/javascript">
  
   $(document).on('click', '.pagination a', function (e) {
         e.preventDefault();

         page=$(this).attr('href').split('page=')[1];
         pageRaiz=$(this).attr('href');
         console.log(pageRaiz);
        $(".loader").removeClass('hide');
    $(".content-box").addClass('hide');

          $.ajax({
            url :  pageRaiz,
            dataType: 'json',
            beforeSend: function(){
             
            },
        }).done(function (data) {
           $(".loader").addClass('hide');
    $(".content-box").removeClass('hide');

          //$(".loader").hide();
           console.log(data);
            $('#caja-busqueda').html(data);
           
            location.hash = page;
        }).fail(function () {
           // alert('Posts could not be loaded.');
        });
          
        });
</script>


</script>

<script type="text/javascript" src="https://viralpatel.net/blogs/demo/jquery/jquery.shorten.1.0.js"></script>
<script language="javascript">
  $(document).ready(function() {  
           $(".comment").shorten({
            "showChars" : 60,
            "moreText"  : "See More",
            "lessText"  : "Less",
        });
    });

</script>

<script >

   toastr.options.onHidden = function() { location.reload(true) };
   toastr.options.onHidden = function() { location.reload(true) };

  /////////////////////////////////
  $(document).on('click','.unjoin',function(){

     $this = $(this);
     var id = this.id;

     if ($this.hasClass('disabled'))
      return;

     var val=$(this).val().replace(/ /g,'')
     if(val=='Joined'){

        if (confirm("do you want to unjoin from this community?")==true) 
        {

           var status =  document.getElementById('status').getAttribute('value');
           var userid = document.querySelector('.user').id;
           $this.addClass('disabled');
           $.ajax({
              type: "get",
              url:'/unjoincommunity/'+id+"/"+status,
                    success: function (data) {
                        document.getElementById(id).value="join up";
                       $(".join").css({'background-color': '#3b824b','width': '500px'});
                        $this.removeClass('unjoin disabled');
                        toastr.success("Successfully Unjoined Community.");

              }
          });
        }
     }
   
  });

  /////////////////////////////////
  $(".join").on('click',function(){
   
   $this = $(this);

   if($this.hasClass('disabled'))
    return;

   var id = this.id;
   
   if(document.getElementById(id).value == 'Joined'){
    return false;
   }

   var public =  document.getElementById('status').getAttribute('value');
   var userid = document.querySelector('.user').id;
   $this.addClass('disabled');

     $.ajax({
        type: "get",
        url: (public==1 ? '/joincommunity/'+id+"/"+public : '/requestemail/'+id+"/"+userid),
              success: function (data) {
                  document.getElementById(id).value="Joined";
                  $(".join").css({'background-color': '#3b824b','width': '500px'});
                  $this.removeClass('join disabled');
                  toastr.success("Successfully Joined Community.");
          
        }
      });

  });

  /////////////////////////////////
  $('#catsearch').on('keypress',function(e) {
      if(e.which == 13) {
          id = $(this).data('id');
          search = $(this).val();
          $.ajax({
              url: "{{url('categorysearch')}}/"+id+"/"+search,
              type: "get",
              success: function (response) {
                 $('#categorydata').html(response);
              }
          });
      }
  });

</script>

@stop