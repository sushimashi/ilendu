<div class="modal-dialog" role="document">

    <div class="modal-content">

        <div class="modal-header text-center">
            <h5 class="modal-title">@lang('citiesView.addCity')</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        {{  Form::open(array('id' => 'addCityForm', 'action' => ['HomeController@addCity'], 'files' => true, 'method'=>"post")) }}

             {{ csrf_field() }}

            <div class="modal-body">

                {{-- Countries --}}
                 <div class="form-group">
                        <label for="credit_card">@lang('citiesView.country')</label>
                        <select class="form-control" style="width: 100%" name="country" id="country" >
                            <option value="">@lang('keywords.select')</option>
                            @if(@$countries)
                            @foreach(@$countries as $country )
                                <option value="{{$country->id}}">{{$country->name}}</option>
                            @endforeach
                            @endif
                        </select>
                    </div>

                {{-- States --}}
                    <div class="form-group">
                        <label for="credit_card">@lang('citiesView.state')</label>
                        <select class="form-control" style="width: 100%" name="state" id="state" >
                            <option value="" >@lang('keywords.select')</option>
                            @if(@$states)
                            @foreach(@$states as $state )
                                <option value="{{$state->id}}"</option>
                            @endforeach
                            @endif
                        </select>
                    </div>

                {{-- Cities --}}
                    <div class="form-group">
                        <label for="credit_card">@lang('citiesView.addCity')</label>
                        <input type="text" class="form-control" style="width: 100%" name="city" id="citybyid" placeholder="@lang('citiesView.city')">
                    </div>
            </div>
             
            <div class="modal-footer">
                <button type="submit" id="sendAddCityForm" class="btn btn-primary">@lang('keywords.save')</button>
            </div>

        {{ Form::close() }}

    </div>

</div>
