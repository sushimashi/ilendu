       <div class="col-md-12 col-sm-12 loader text-center hide">
        <img src="{{asset('img/loader.gif')}}">
      </div>

      <div class="row content-box" style="width: 100%;margin:0px;" >

       @foreach($ordered_products as $borrowed_product)
       <div class="col-md-4">
        <div class="product">
          <a href="{{ url('products/'.$borrowed_product->id)}}">
            <img class="img rounded" width="50%" height="50%" src="{{url('/img/thumbnail/'.$borrowed_product->product_img)}}" alt="">
            <h6>{{$borrowed_product->product_name}}</h6>
          </a>

          @switch($borrowed_product->borrowers()->where('user_id',auth()->user()->id)->latest()->first()->status)
          @case("Pending")

          <button class=" btn-danger delete_order" data-id="{{$borrowed_product->borrower->id}}" data-href="{{url('deleteOrder')}}"  type="button" style="cursor: pointer;">@lang('keywords.delete')</button>

          @break

          @case("Sent")
          <button class=" btn-danger received_order" data-id="{{$borrowed_product->id}}" data-href="{{url('received-order/'.$borrowed_product->id)}}"  type="button" style="cursor: pointer;">@lang('keywords.received')</button>
          @break

          @case("Received")

          <button class=" btn-danger returned_order" data-id="{{$borrowed_product->id}}" data-href="{{url('returnedOrder/'.$borrowed_product->id)}}"  type="button" style="cursor: pointer;">@lang('keywords.returned')</button>

          @break

          @case("Dispatching")
          <button class=" btn-danger delete_order" data-id="{{$borrowed_product->borrower->id}}" data-href="{{url('deleteOrder')}}"  type="button" style="cursor: pointer;">@lang('keywords.delete')</button>&nbsp;
          
          <button class=" btn-danger show_dispatch_modal" data-id="{{$borrowed_product->borrower->id}}" data-product="{{$borrowed_product->id}}"  type="button" style="cursor: pointer;">@lang('keywords.dispatch')</button>

          @break

          @case("PaidOut")
        
          <button class=" btn-success" disabled type="button" style="cursor: pointer;">@lang('keywords.proccesing')</button>

          @break



          @endswitch

        </div>


      </div>
      @endforeach
      @if(@count($ordered_products) < 1)
      <span style="text-align: center; width: 100%">@lang('productsView.noProductsAvailable')</span>
      @else

      @endif
      @if(count($ordered_products)>0)
      <div class="col-md-12 col-sm-12 text-center" >
        {{ $ordered_products->appends(Request::except('page'))->render() }}
      </div>
      @endif
    </div>


