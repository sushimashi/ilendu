    @if(count($communities)>0)

        <div class="loader text-center hide">
                <img src="{{asset('img/loader.gif')}}">
        </div>

        <div class="row content-box" >
        
            @foreach($communities as $community)
                <div class="col-md-3">
                    <div class="product">
                        <a href="{{ url('communities/'.$community->id)}}">
                        @if(@$community['com_image'] != "")
                          <?php if(file_exists(public_path('/community_image/'.@$community['com_image']))){ ?>
                            <img class="img-fluid rounded" src="community_image/{{ $community['com_image']}}" alt="" style="width: 50%; height: 50%">
                          <?php }else{ ?>
                            <img style="width: 50%; height: 50%" class="img-fluid rounded" src="{{url('/')}}/img/city_icon.png" alt="">
                          <?php } ?>
                        @else
                            <img style="width: 50%; height: 50%" class="img-fluid rounded" src="{{url('/')}}/img/city_icon.png" alt="">
                        @endif
                        <h4>{{$community->name}}</h4></a><br>
                        <p class="comment">{{$community->description}}</p>
                    </div>
                <br>
                </div>
            @endforeach
        </div>
    @else

    <div class="row">
        <div class="col-md-12">
            <p class="text-center"><b >@lang('sentence.noResultsFound')</b></p>
        </div>
    </div>

    @endif

    <div class="center col-sm-12 col-lg-12 col-md-12">
        {{ $communities->appends(Request::except('page'))->render() }}
    </div>
