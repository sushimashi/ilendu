       <div class="col-md-12 col-sm-12 loader text-center hide">
                <img src="{{asset('img/loader.gif')}}">
        </div>
              <div class="row content-box" style="width: 100%; margin:0px;" >

 @if(@count($products) < 1)
                        <span style="text-align: center; width: 100%">@lang('productsView.noProductsAvailable')</span>
                      @endif
                      @foreach($products as $product)
                      <div class="col-md-4 data{{$product->id}}" >
                        <!-- <div class="del_link" data_id="{{$product->id}}" data_href="{{route('deleteproduct', $product->id)}}"  style="cursor: pointer;"><img title="Delete Product" src="{{url('/img/delete-icon.png')}}" ></div> -->
                        <div class="product">
                          <a href="{{ url('products/'.$product->id)}}">
                            @if($product->product_img!="")
                            <img class="img rounded" width="50%" height="50%" id="pimg{{$product->id}}" src="{{url('/img/thumbnail/'.$product->product_img)}}" alt="">
                            @elseif($product->product_img=="")
                             <img class="img rounded" width="50%" height="50%" src="https://www.freeiconspng.com/uploads/no-image-icon-6.png" alt="">
                             @endif
                            <h6 id="pname{{$product->id}}">{{$product->product_name}}</h6>
                          </a>
                          <input type="hidden" name="" data-away="{{$product->give_away}}" data-premium="{{$product->is_premium}}" data-communities="{{$product->communities()->select('name')->get()}}" data-term="{{$product->return_term}}" data-active="{{$product->is_active}}" id="pdesc{{$product->id}}" value="{{$product->product_desc}}"/>


                            <button class="del_link btn-danger" data-id="{{$product->id}}" data-href="{{route('deleteproduct', $product->id)}}"  style="cursor: pointer;">@lang('keywords.delete')</button>

                            <button class="edit_link btn-primary" id="edit_it" data-id="{{$product->id}}" data-category="{{$product->product_category}}" data-href="{{route('editproduct', $product->id)}}" data-toggle="modal" style="cursor: pointer;">@lang('keywords.edit')</button>
                        </div>
                    
                      </div>

                      @endforeach
          @if(count($products)>0)
                             <div class="col-md-12 col-sm-12 text-center" >
        {{ $products->appends(Request::except('page'))->render() }}
    </div>
    @endif
      </div>