       <div class="col-md-12 col-sm-12 loader text-center hide">
                <img src="{{asset('img/loader.gif')}}">
        </div>
          <div class="row content-box" style="width: 100%;margin:0px;" >

 @if(@count($borrowed_products) < 1)
                        <span style="text-align: center; width: 100%">@lang('productsView.noProductsAvailable')</span>
                      @endif
                      @foreach($borrowed_products as $product)
                      <div class="col-md-4 data{{$product->id}}" >
                      
                        <div class="product">
                          <a href="{{ url('products/'.$product->id)}}">
                            @if($product->product_img!="")
                            <img class="img rounded" width="50%" height="50%" id="pimg{{$product->id}}" src="{{url('/img/thumbnail/'.$product->product_img)}}" alt="">
                            @elseif($product->product_img=="")
                             <img class="img rounded" width="50%" height="50%" src="https://www.freeiconspng.com/uploads/no-image-icon-6.png" alt="">
                             @endif
                            <h6 id="pname{{$product->id}}">{{$product->product_name}}</h6>
                          </a>
                            @if($product->borrower->status!="Returned")
                            <button class=" btn-danger returned_order" data-id="{{$product->id}}" data-href="{{url('returnedOrder/'.$product->id)}}"  type="button" style="cursor: pointer;">@lang('keywords.returned')</button>
                          @endif
                          <input type="hidden" name="" id="pdesc{{$product->id}}" value="{{$product->product_desc}}"/>
                       
                        </div>
                    
                      </div>

                      @endforeach
     @if(count($borrowed_products)>0)
                             <div class="col-md-12 col-sm-12 text-center" >
        {{ $borrowed_products->appends(Request::except('page'))->render() }}
    </div>
    @endif
</div>