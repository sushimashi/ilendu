       <div class="col-md-12 col-sm-12 loader text-center hide">
                <img src="{{asset('img/loader.gif')}}">
        </div>
          <div class="row content-box" style="width: 100%;margin:0px;" >

 @foreach($join as $join_community)

                              <div class="col-md-4">
                                <div class="product">
                                  <a href="{{ url('communities/'.$join_community->id)}}">
                                    @if($join_community['com_image'] != "")
                                      <img class="rounded" width="50%" height="50%" src="community_image/{{ $join_community['com_image']}}"  alt="">
                                     @else
                                     <img class="rounded" width="50%" height="50%" src="img/no-image.png" alt="">
                                     @endif
                                    <h4>{{$join_community->name}}</h4>
                                    </a>


                              @if($join_community->created_by == Auth::user()->id)

                                    <div>


                                     <button class="del_link btn-danger" data-id="{{$join_community->id}}" data-href="{{url('communities/'.$join_community->id)}}"  style="cursor: pointer;">@lang('keywords.delete')</button>

                            <button class="edit_community btn-primary" id="edit_it" data-id="{{$join_community->id}}"  data-href="{{url('communities/'.$join_community->id.'/edit')}}" data-toggle="modal" style="cursor: pointer;">@lang('keywords.edit')</button>           
                                    </div>
                               @endif
                                    @if($join_community->created_by != Auth::user()->id)
                                  
                                      <div style=" border-color: #007bff !important; background: #007bff !important; " class="topright">@lang('keywords.member')</div>
                                      @else

                                   <div style=" border-color: green !important; background: green !important; " class="topright">@lang('keywords.admin')</div>

                                    @endif

                                </div>
                              </div>
                             @endforeach
                              @if(count($join)>0)
                             <div class=" col-md-12 col-sm-12 text-center" >
        {{ $join->appends(Request::except('page'))->render() }}
    </div>
    @endif
  </div>
             
