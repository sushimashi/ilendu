@foreach($messages_collection as $i=>$b)

<div class="tab-pane message-body px" id="inbox-message-{{$i}}">
                <div class="message-top">

                    {{--<div class="product-message-detail text-center">
                            <img class="rounded-circle" src="{{url('/')}}/img/{{$b->borrow->product->product_img}}">
                            <p class="product-detail-title"><span>{{$b->borrow->product->product_name}}</span></p>
                            <div class="col-md-12">
                                <div class="col-md-4" style="display: inline-block;">
                                     <button  class="btn btn-lg btn-primary btn-block aprove_button" data-borrow-id="{{$b->borrow->product->id}}"  type="button">Approve</button>

                                </div>
                                <div class="col-md-4" style="display: inline-block;">
                         <button  class="btn btn-lg btn-primary btn-block decline_button" data-borrow-id="{{$b->borrow->product->id}}"  type="button">Decline</button>

                                </div>

                            </div>
                    </div>--}}
                    {{--<a class="btn btn btn-success new-message"> <i class="fa fa-envelope"></i> New Message </a>--}}

                    <div class="new-message-wrapper">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Send message to...">
                            <a class="btn btn-danger close-new-message" href="#"><i class="fa fa-times"></i></a>
                        </div>

                        <div class="chat-footer new-message-textarea">
                            <textarea class="send-message-text" name="message" ></textarea>
                            <label class="upload-file">
                                <input type="file" required="">
                                <i class="fa fa-paperclip"></i>
                            </label>
                            <button type="button" class="send-message-button btn-info"> <i class="fa fa-send"></i> </button>
                        </div>
                    </div>
                </div>

                <div class="message-chat">
                    <div class="chat-body">

            @foreach($b as $k=>$m)


                @if(auth()->user()->id==$m->user_id)



                        <div class="message info">
                            @if($m->user->profile_img != "" && file_exists(public_path('img/profile/'.$m->user->profile_img)))

                                <img alt="" class="img-circle medium-image" src="{{asset('img/profile/'.$m->user->profile_img)}}">
                            @else
                                <img alt="" class="img-circle medium-image" src="{{url('/')}}/img/avatar.jpg">

                            @endif
                            <div class="message-body">
                                <div class="message-info">
                                    <h4> {{$m->user->first_name.' '.$m->user->last_name}} </h4>
                                    <h5> <i class="fa fa-clock-o"></i> {{$m->created_at->format('h:i A')}} </h5>
                                </div>
                                <hr>
                                <div class="message-text">
                                    {{$m->message}}
                                </div>
                            </div>
                            <br>
                        </div>
                    @else

<div class="message my-message">

    @if($m->user->profile_img != "" && file_exists(public_path('img/profile/'.$m->user->profile_img)))

                                <img alt="" class="img-circle medium-image" src="{{asset('img/profile/'.$m->user->profile_img)}}">
                            @else
                                <img alt="" class="img-circle medium-image" src="{{url('/')}}/img/avatar.jpg">

                            @endif

                            <div class="message-body">
                                <div class="message-body-inner">
                                    <div class="message-info">
                                        <h4> {{$m->user->first_name.' '.$m->user->last_name}} </h4>
                                        <h5> <i class="fa fa-clock-o"></i> 2:28 PM </h5>
                                    </div>
                                    <hr>
                                    <div class="message-text">
                                    {{$m->message}}
                                    </div>
                                </div>
                            </div>
                            <br>
                        </div>

                    @endif

             @endforeach
                        

                      
                    </div>

                    <div class="chat-footer">
                        <textarea class="send-message-text" id="message_box"></textarea>
                        {{--<label class="upload-file">
                            <input type="file" required="">
                            <i class="fa fa-paperclip"></i>
                        </label>--}}
                        <button type="button" class="send-message-button btn-info"> <i class="fa fa-send"></i> </button>
                    </div>
                </div>
            </div>

@endforeach

