@extends('layout.default')

@section('content')

<style type="text/css">
    a.white{
        color: white !important;
    }
    .center{
        width: 100%;
        margin: 40px auto;
    }
    .plusbutton{
        height: 38px;
        border-radius: 5px 0px 0px 5px;
        width: 40px;
    }
    .minusbutton{
        height: 38px;
        border-radius: 0px 5px 5px 0px;
        width: 40px;
    }
</style>
<br/>

<div class="product-grid">
	<div class="container">
        <div class="card"></br>  
             <div class="row">
                
                <h3 style="font-weight:bold;">&nbsp;&nbsp;&nbsp;@lang('keywords.communities')</h3>
               
                    <input style="margin-left: 65%;" type="button" class="btn btn-success" id="community"  value="@lang('communitiesView.addCommunity')">
               
            </div><hr>
            <div class="card-body"><br/>
     <form id="search_form" action="{{url('/search-communities/name')}}" method="get" >
                <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="@lang('keywords.search')...">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <button class="btn btn-primary col-btn" type="submit" >@lang('keywords.search')</button>
                    </div>
                </div> 
                </div>

            </form>
               <form id="search_form_location" action="{{url('/search-communities')}}" method="get" >
                <div class="row">
                    
                <div class="col-md-3">
                       <div class="form-group">
                                        <label for="country_id">@lang('keywords.country')</label>
                                        <select class="form-control text-center" name="country_id" id="country_id">
                                         <option value="" >-- Seleccione --</option>
       
                                        </select>
                                    </div>
                </div>


                    <div class="col-md-3">
                       <div class="form-group">
                                        <label for="city_id">@lang('keywords.state')</label>
                                        <select class="form-control text-center" name="state_id" id="state_id">
                                        <option value="" >-- Seleccione --</option>
 
                                        </select>
                                  </div>
                   </div> 

                
                <div class="col-md-3">
                       <div class="form-group">
                                        <label for="city_id">@lang('keywords.city')</label>
                                        <select class="form-control text-center" name="city_id" id="city_id">
                                        <option value="" >-- Seleccione --</option>
     
                                        </select>
                                    </div>
                </div> 

                <div class="col-md-2">
                    <div class="form-group">
                        <button class="btn btn-primary col-btn" type="submit" style="margin-top: 20%" >@lang('keywords.search')</button>
                    </div>
                </div> 
                </div>

            </form>
               
                                 
                <br>

                <!-- pagination goes here-->
                <div id="caja-busqueda">
                    
                    @include('site.pagination.index')
                </div>
              
                <!-- end pagination-->

            </div>    
        </div>
    </div>
</div>
<div class="modal fade" id="communitymodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">@lang('keywords.community')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
           

               <form method="POST" action="{{url('communities')}}" role="form" id="create_comunity_form" name="create_comunity_form" enctype="multipart/form-data">
                     {{ csrf_field()}}

                <div class="modal-body">
                    <div class="form-group">
                        <label>@lang('keywords.community'):</label>
                        <input type="text" class="form-control" placeholder="@lang('keywords.name')" name="name" required="" value="">
                    </div>

                     <div class="form-group">
                        <label>@lang('keywords.image'):</label>
                         <input  type='file' name='com_image' id='com_image' class='inputfile ' required="">
                            <!-- <label for='avatar'><i class='fa fa-upload'></i> Choose a file</label> -->
                        <div class='avatar_preview'></div>
                        <div><i>@lang('keywords.cover')</i></div>
                     </div>

                     <div class="form-group">
                        <label>@lang('keywords.description'):</label>
                        <textarea  class="form-control" id="description" type="text"  placeholder="@lang('sentence.addDescriptionDetails')" name="description" ></textarea>
                    </div>
                    <div class="form-group">
                        <label>@lang('keywords.status'):</label>
                        <select name="status" class="form-control" required="" >
                            <option value="0">@lang('keywords.private')</option>
                            <option value="1">@lang('keywords.public')</option>
                        </select>
                    </div>
                </div>
                 
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">@lang('keywords.save')</button>
                </div>
        </form>

        </div>
    </div>
</div>
<style type="text/css">
  .morelink
  {
    display: none;
  }
</style>
@endsection

@section('pagejavascripts')
@include ('site.js.index')
<script type="text/javascript">

$(function(){
toastr.options.onHidden = function() { location.reload();};
 toastr.options.onclick = function() { location.reload(); };
     // select2
        $('#country_id').select2();
        $('#state_id').select2();
        $('#city_id').select2();

        $('#country_id').select2({
  ajax: {
    url: '/country-ajax',
    dataType: 'json'
    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
  }
});

        // agregar estados segun pais seleccionado
        $("#country_id").on("change",function(){


  $('#state_id').select2({
  ajax: {
    url: '/api/country-select/'+$(this).val()+'/states',
    dataType: 'json'
    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
  }
});


          

        });



        // agregar ciudades segun estado seleccionado
        $("#state_id").on("change",function(){

           
                $('#city_id').select2({
                  ajax: {
                    url: '/api/state-select/'+$(this).val()+'/cities',
                    dataType: 'json'
                    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
                  }
                });



        });
})

    $("#create_comunity_form").on("submit",function(e){
     

        e.preventDefault();
        var formData = new FormData(this);

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
         $('#communitymodel').modal('hide');

            toastr.success(data.message);
            //location.reload();
            },
           error: function (e) {
                console.log(e);
                $.each(e.responseJSON.errors, function (index, element) {
                    if ($.isArray(element)) {
                        toastr.error(element[0]);
                    }
                });


            }
        });


    })

    $("#community").on("click", function(){
        $('#communitymodel').modal('show');
    });
</script>
<script type="text/javascript" src="https://viralpatel.net/blogs/demo/jquery/jquery.shorten.1.0.js"></script>
<script language="javascript">
$(document).ready(function() {
    
   $(".comment").shorten({
    "showChars" : 80
});
 });
</script>
@stop