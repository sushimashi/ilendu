@if(count($products))

  @foreach($products as $product)
  <div class="col-lg-4 col-sm-6">
    <div class="card">

      <a href="{{ url('products/'.$product->id)}}">
         @if($product->product_img != "")
             <?php if(file_exists(public_path('img/thumbnail/'.$product->product_img)) && $product->product_img !=""){ ?>
                <img class="card-img-top" src="{{url('/')}}/img/thumbnail/{{$product->product_img}}" alt="">
              <?php }else{ ?>
                <img class="card-img-top" src="https://www.freeiconspng.com/uploads/no-image-icon-6.png" alt="">
              <?php } ?>
         @else
            <img class="card-img-top" src="{{url('/')}}/img/no-image.png" alt="">
         @endif
      </a>

      <div class="card-body">

        <a href="{{ url('products/'.$product->id)}}" class="card-text">
          <p>{{$product->product_name}}</p>
        </a>
        <table>

          <td>
            @if($product['user']->profile_img != "")

              @if(file_exists(public_path('img/profile/'.$product['user']->profile_img)))

                  <img class="small_image" src="{{url('/')}}/img/profile/{{$product['user']->profile_img}}" alt="">


              @else

                  <img class="small_image" src="{{url('/')}}/img/profile/new_user.png">

              @endif

            @else

              <img class="small_image" src="{{url('/')}}/img/profile/new_user.png">

            @endif
          </td>

          <td>
            <span class="text-uppercase sub_text">@lang('productsView.submittedBy')</span><br/>
            <span>{{ !empty($product['user'])?$product['user']->getFullName():'' }}</span>
          </td>

        </table>

         @if($product['is_premium']==1 && $product['give_away']==1)
                 <div style=" border-color: #F10080 !important; background: #F10080 !important; " class="topright">@lang('keywords.gift')</div>

                @elseif($product['is_premium']==1)
                 <div style=" border-color: #F10080 !important; background: #F10080 !important; " class="topright">@lang('keywords.premium')</div>
                 @endif
      </div>
    </div>
  </div>
  @endforeach

    <div class="col-lg-12"></div>
    <div class="clearfix"></div>
    <div class="div_center mb50"><br>
      {!!  $products->links() !!}
    </div>

@else
    <div class="row" style="width: 100%; margin-left:1% ">
      <div class="col-md-12 card index_card text-center">
        <h3 style="margin-top: 10%;">
          <span class="diff_color">@lang('productsView.noProductsFound')</span>
        </h3>
      </div>
    </div>
@endif

