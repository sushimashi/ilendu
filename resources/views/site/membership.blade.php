@extends('layout.default')

@section('content')

<div class="product-grid">
	<div class="container">
	   <h2>Membership form</h2>
        {{  Form::open(array('action' => ['DashboardController@store'], 'files' => true, 'method'=>"post")) }}
                 {{ csrf_field() }}
            <div class="form-group">
                <label>Membership Fees:</label>
                <input type="text" class="form-control" id="fees" placeholder="Enter Fees" name="fees" required="">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        {{ Form::close() }}
    </div>
</div>

@endsection

@section('pagejavascripts')

@stop