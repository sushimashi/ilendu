@extends('layout.default')

@section('content')
<div class="container">
  <div class="row">
	<form action='https://sandbox.2checkout.com/checkout/purchase' method='post'>
		<input type='hidden' name='sid' value='901382184' >
		<input type='hidden' name='mode' value='2CO' >
		<input type='hidden' name='li_0_type' value='product' >
		<input type='hidden' name='li_0_name' value='Example Product Name' >
		<input type='hidden' name='li_0_product_id' value='Example Product ID' >
		<input type='hidden' name='li_0__description' value='Example Product Description' >
		<input type='hidden' name='li_0_price' value='10.00' >
		<input type='hidden' name='li_0_quantity' value='2' >
		<input name='submit' type='submit' value='Checkout' >
	</form>
</div>
</div>

@endsection

@section('pagejavascripts')

@stop