@push('css')
<style type="text/css">
 .card-img-top{
padding: 5%;
} 
.gray_boxes .card{
  border: 1px solid #F2F2F2;
  background-color: #F2F2F2;
}
</style>

@endpush
@if(count($products)>0)

    <div class="loader text-center hide">
        <img src="{{asset('img/loader.gif')}}">
    </div>

    <div class="row content-box" >

        @foreach($products as $product)
        
          <div class="col-lg-3 col-sm-6 gray_boxes" >
            <div class="card">

              <a href="{{ url('products/'.$product->id)}}">
                 @if($product->product_img != "")
                     <?php if(file_exists(public_path('img/thumbnail/'.$product->product_img))){ ?>
                        <img class="card-img-top rounded" src="{{url('/')}}/img/thumbnail/{{$product->product_img}}" alt="">
                      <?php }else{ ?>
                        <img class="card-img-top rounded" src="{{url('/')}}/img/no-image-icon.png" alt="">
                      <?php } ?>
                 @else
                    <img class="card-img-top rounded" src="{{url('/')}}/img/no-image.png" alt="">
                 @endif
              </a>

              <div class="card-body">

                <a style="text-decoration:none;" href="{{ url('products/'.$product->id)}}" class="card-text text-center">
                    <p>{{$product->product_name}}</p>
                </a>

                 

                <table>
                  <td>
                     <a style="text-decoration:none;" href="{{ url('user/products/'.$product->user_id)}}">

                        @if( isset($product->user->profile_img) &&
                             file_exists(public_path('img/profile/'.$product->user->profile_img)) 
                            )

                              <img class="small_image rounded" src="{{url('/')}}/img/profile/{{$product->user->profile_img}}" alt="">


                        @else

                            <img class="small_image rounded" src="{{url('/')}}/img/profile/new_user.png">

                        @endif

                      </a>
                  </td>

                  <td>
                      <a style="text-decoration:none;" href="{{ url('reputation/'.$product->user_id)}}">
                       <span class="text-uppercase sub_text">@lang('productsView.submittedBy')</span>
                       <br/>
                        <span style="color:black;">{{$product->user->first_name}} {{$product->user->last_name}}</span>
                       </a>
                  </td>

                </table>

                @if($product->is_premium==1 && $product->give_away==1)
                 <div style=" border-color: #F10080 !important; background: #F10080 !important; " class="topright">@lang('keywords.gift')</div>

                @elseif($product->is_premium==1)
                 <div style=" border-color: #F10080 !important; background: #F10080 !important; " class="topright">@lang('keywords.premium')</div>
                 @endif
              </div>
            </div>
        </div>

        @endforeach
  <div class="center col-lg-12 col-sm-12 col-md-12" style="width: 100%;" >
  {{ $products->appends(Request::except('page'))->render() }}
  </div>

    </div>

@else

    <div class="row">
        <div class="col-md-12">
            <p class="text-center"><b >@lang('sentence.noResultsFound')</b></p>
        </div>
    </div>

@endif

