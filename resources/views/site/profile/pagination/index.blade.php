       <div class="loader text-center hide">
                <img src="{{asset('img/loader.gif')}}">
        </div>
<div class="card content-box">
	    <div class="card-body ">
            <h6 class="text-center"><strong> Users reviews</strong></h6>
            <br>
            @foreach($comments as $c)
	        <div class="row" style="margin-bottom: 2%">
        	    <div class="col-md-2">


            <div class="text-center">
                           @if($c->commentator->profile_img != "" && file_exists(public_path('img/profile/'.$c->commentator->profile_img)))
               <img src="{{url('/')}}/img/profile/{{$c->commentator->profile_img}}" width="40%"class="img rounded-circle img-fluid"/>
        @else

                        <img src="{{url('/')}}/img/avatar.jpg" width="40%" alt="{{ $c->commentator->first_name}}" class="rounded-circle">

                      @endif

        </div>
        	        <p class="text-secondary text-center"></p>
        	    </div>
        	    <div class="col-md-10">
        	        <p>
        	            <a class="float-left" href="{{url('reputation/'.$c->commentator_id)}}"><strong>{{$c->commentator->getFullName()}}</strong></a> &nbsp;<b>published : {{$c->commentator->created_at}}</b>
                        <?php $empty=5-$c->score; ?>
                        @for($y=0; $y<$empty;$y++)
                        <span class="float-right"><i class="text-warning fa fa-star-o"></i></span>

                        @endfor
                        @for($i=0; $i<$c->score;$i++)
        	            <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                        @endfor


        	       </p>
        	       <div class="clearfix"></div>
        	        <p>{{$c->comment}}</p>
        	    
        	    </div>
	        </div>
            <hr style="border: 1px solid #F10482;">
            @endforeach
               <div class="clearfix"></div>
          <div class=" col-md-12 text-center mb50 ml40"><br>
            {!!  $comments->links() !!}
          </div>
	        	{{--<div class="card card-inner">
            	    <div class="card-body">
            	        <div class="row">
                    	    <div class="col-md-2">
                    	        <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                    	        <p class="text-secondary text-center">15 Minutes Ago</p>
                    	    </div>
                    	    <div class="col-md-10">
                    	        <p><a href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>Maniruzzaman Akash</strong></a></p>
                    	        <p>Lorem Ipsum is simply dummy text of the pr make  but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                    	        <p>
                    	            <a class="float-right btn btn-outline-primary ml-2">  <i class="fa fa-reply"></i> Reply</a>
                    	            <a class="float-right btn text-white btn-danger"> <i class="fa fa-heart"></i> Like</a>
                    	       </p>
                    	    </div>
            	        </div>
            	    </div>
	            </div>--}}
	    </div>
	</div>