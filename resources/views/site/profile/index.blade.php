@extends('layout.default')

@section('content')
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/profile_starts.css')}}">

<style type="text/css">
	.card-inner{
    margin-left: 4rem;
}
</style>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

@endpush
<div class="container card" style="min-height: 800px;">
    <div class="col-md-12 text-center" style="margin-top: 15%;">

         @if($user->profile_img != "" && file_exists(public_path('img/profile/'.$user->profile_img)))
                   <img src="{{url('/')}}/img/profile/{{$user->profile_img}}" style="height: 160px;" class="rounded-circle"/>
        @else

                        <img src="{{url('/')}}/img/avatar.jpg" width="15%" alt="{{$user->first_name}}" class="rounded-circle">

                      @endif
    </div>
	<h4 class="text-center">{{$user->getFullName()}}</h2>

          <div class="col-md-12">
                <div class=" text-center">
                    <h6>Average user rating</h6>
                    <h2 class="bold padding-bottom-7">{{$rating}} <small>/ 5</small></h2>

                    @if($rating!=0)
                    @for($i=0; $i<$rating;$i++)
                    <button type="button" class="btn btn-warning btn-sm" aria-label="Left Align">
                   <i class="fa fa-star" aria-hidden="true"></i>
                    </button>

                    @endfor
                   
                    @for($i=0; $i<(5-$rating);$i++)


                    <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </button>

                     @endfor
                     @else
                       <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </button>
                      <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </button>

                      <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </button>

                      <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </button>

                      <button type="button" class="btn btn-default btn-grey btn-sm" aria-label="Left Align">
                        <i class="fa fa-star" aria-hidden="true"></i>
                    </button>
                     @endif
                
                </div>
        </div>

        <ul class="nav nav-tabs" id="myTab" role="tablist" style="justify-content: center;margin-top: 5%;">
  <li class="nav-item">
    <a class="nav-link active" id="comments-tab" data-toggle="tab" href="#comments" role="tab" aria-controls="home" aria-selected="true">@lang('keywords.comments')</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="products-tab" data-toggle="tab" href="#products" role="tab" aria-controls="profile" aria-selected="false">@lang('keywords.products')</a>
  </li>
 
</ul>
     

<div class="tab-content" id="myTabContent">
<div class="tab-pane fade show active col-md-12" id="comments" role="tabpanel" aria-labelledby="comments-tab" >
        @if(count($comments))
	      <div id="caja-busqueda">
                    
                    @include('site.profile.pagination.index')
        </div>
        @else


        <div class="row">
                
         


               <div class="col-md-12" >

             

                <div class="card content-box">
        <div class="card-body ">
                <h4 class="text-center">@lang('keywords.no_comments')</h4>

        </div>
            </div>
            </div>
        </div>
     

         
	   @endif
  </div>

  <div class="tab-pane fade" id="products" role="tabpanel" aria-labelledby="products-tab">
      @if(count($products))
        <div id="caja-busqueda-2" style="margin-top: 3%;">
                    
                    @include('site.profile.pagination.products')
        </div>
        @else


        <div class="row">
                
         


               <div class="col-md-12" >


                <div class="card content-box">
        <div class="card-body ">
                <h4 class="text-center">@lang('keywords.no_products')</h4>

        </div>
            </div>
            </div>
        </div>
     

         
     @endif

  </div>
  </div>


</div>


@endsection

@push('js')
<script type="text/javascript">
    var selected_tab="comments-tab";
    $(function(){
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var currId = $(e.target).attr("id");
  
  //just for demo
  selected_tab=currId;
})
         $(document).on('click', '.pagination a', function (e) {
         e.preventDefault();

         page=$(this).attr('href').split('page=')[1];
         pageRaiz=$(this).attr('href');
         console.log(pageRaiz);
        $(".loader").removeClass('hide');
        $(".content-box").addClass('hide');
          $.ajax({
            url :  pageRaiz+'&pagination='+selected_tab,
            dataType: 'json',
            beforeSend: function(){
             
            },
        }).done(function (data) {
           $(".loader").addClass('hide');
        $(".content-box").removeClass('hide');

          //$(".loader").hide();
           console.log(data);

           if(data.pagination=="products-tab")
           {
           $('#caja-busqueda-2').html(data.view);

           }else{
                      $('#caja-busqueda').html(data.view);
  
           }
  
            location.hash = page;
        }).fail(function () 
        {
           // alert('Posts could not be loaded.');
        });
          
        });
    })
</script>
@endpush