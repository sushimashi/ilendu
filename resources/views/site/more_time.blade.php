@extends('layout.default')

@section('content')
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
<div class="container">
    <div class="main_padding">
    <div class="row">
        <div class="col-md-5 col-xs-12 div_center">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('sentence.more_time')</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('add-more-time') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="borrow_id" value="{{$borrow_id}}">
                        <div class="form-group">
                            <label for="email" class="col-md-6 control-label">@lang('sentence.add_more_days')</label>

                           <select class="form-control" name="days">
                               <option value="2">2 days</option>
                               <option value="5">5 days</option>
                               <option value="7">7 days</option>

                           </select>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-6 control-label">@lang('sentence.add_more_message')</label>

                            <textarea class="form-control" name="message"></textarea>
                        </div>

                        

                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-6">
                                <button type="submit" class="btn btn-primary">
                                    Send
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
