<script type="text/javascript">
	
	$('#sendAddCityForm').click(function(e){

		if ($(this).hasClass('disabled'))
			return;

		e.preventDefault();
		toastr.options.timeOut = 2500;

		if($('#country').val() === "" || $('#country').val() === "Select Country")
			return toastr.warning('@lang("citiesView.selectCountry")');

		if($('#state').val() === "" || $('#state').val() === "Select State")
			return toastr.warning('@lang("citiesView.selectState")');

		if($('#citybyid').val() === "")
			return toastr.warning('@lang("citiesView.requiredCity")');

		let data = $('#addCityForm').serialize();
		console.log(data);

		$(this).addClass('disabled');

		$.ajax({
			url:  $('#addCityForm').attr('action'),
			method:  $('#addCityForm').attr('method'),
			data: data,
			success: function(result){
				toastr.info(result.message);
				$('#cityModal').modal('hide');
				$('#country').select2('destroy');
				$('#state').select2('destroy');
				// $('#citybyid').select2('destroy');
				$('#addCityForm').trigger('reset');
				$('#country').select2({dropdownParent: $('#cityModal')});
				$('#state').empty()
							.append('<option value="">Select State</option>')
							.select2({dropdownParent: $('#cityModal')});
				// $('#citybyid').empty()
				// 			.append('<option value="">Select City</option>')
				// 			.select2({dropdownParent: $('#cityModal'),tags: true});
			}, 
			error: function(e){
				console.log(e.responseJSON);
	            $.each(e.responseJSON.errors, function (index, element) {
	                if ($.isArray(element)) {
	                    toastr.error(element[0]);
	                }
	            });
			}, complete: function(){
				$('#sendAddCityForm').removeClass('disabled');
			}
		});
	});

</script>