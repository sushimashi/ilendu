<script type="text/javascript">  

    let firstChange = true; 	

        $.get(`/api/country/${ $('#countryUser').val() }/states`, function(result){

              if( result.length > 0 )
              {
                  result.forEach(element => {

                      $("#stateUser").append(
                          `<option value="${element.id}">
                              ${element.name}
                          </option>`
                      );

                  });

                  $('#stateUser').val('{{ $user->state_id }}').trigger('change');

              } else {

                  $("#stateUser").append(`<option value=""> - @lang('keywords.none')  - </option>`)
                             .trigger('change'); 

              }

          });

        // agregar estados segun pais seleccionado
        $("#countryUser").on("change",function(){

          $('#stateUser').empty();
            if ( $(this).val() === '' ){
                $("#stateUser").append(`<option value=""> - @lang('keywords.none')  - </option>`);
                $("#cityUser").empty().append(`<option value=""> - @lang('keywords.none')  - </option>`);
                return;
            } 

          $.get(`/api/country/${ $("#countryUser").val() }/states`, function(result){

                if( result.length > 0 )
                {
                    result.forEach(element => {

                        $("#stateUser").append(
                            `<option value="${element.id}">
                                ${element.name}
                            </option>`
                        );

                    });

                    $("#stateUser").trigger('change');

                } else {

                    $("#stateUser").append(`<option value=""> - @lang('keywords.none')  - </option>`)
                                   .trigger('change'); 

                }

            });

        });

        // agregar ciudades segun estado seleccionado
        $("#stateUser").on("change",function(){

            $('#cityUser').empty();

            if ($(this).val() === ''){
                $("#cityUser").append(`<option value=""> - @lang('keywords.none')  - </option>`);
                return;
            }

            $.get(`/api/state/${ $('#stateUser').val() }/cities`, function(result){

                if( result.length > 0 )
                {
                    result.forEach(element => {
                        
                        $("#cityUser").append(
                            `<option value="${element.id}">
                                ${element.name}
                            </option>`
                        );

                    });

                    if (firstChange === true ){
                        $("#cityUser").val('{{ $user->city_id }}');
                        firstChange = false;
                    }

                } else {

                    $("#cityUser").append(`<option value=""> - @lang('keywords.none')  - </option>`);

                }

            });
        });

        // select2
        $('#countryUser').select2();
        $('#stateUser').select2();
        $('#cityUser').select2();

</script>