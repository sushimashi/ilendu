@extends('layout.default')

@section('content')
@push('css')
<style type="text/css">
	.cash-symbol{font-size: 50%}
	.time-symbol{font-size: 45%; color: gray;}
	ul{
		 list-style-type: none;

	}

	.font-big{font-size: 35px !important; color: #F10482;}
</style>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

@endpush

<div class="container" style="min-height: 400px">
		<div class="row">
				<div class="col-md-12" style="margin-bottom: 5%">
		<h1 class="text-center">Pricing</h1>
		<p class="text-center" style="font-size: 17px;">Sign up in less than 30 seconds, Try out our 7 day risk<br>free trial, upgrade at anytime, no questions, no hassle</p>
        </div>
		</div>
	<div class="row">

		@foreach($fees as $f)
		<div class="col-md-4 card">
           {{  Form::open(array('url' => '/charge/pay', 'method'=>"post", "id" => "payment")) }}
			<input type="hidden" name="amount" value="{{$f->fees}}">
			<input type="hidden" name="plan_id" value="{{$f->id}}">
            {{  Form::hidden('url',URL::previous())  }}

			<h1 class="text-center" style="font-size: 60px"><span class="cash-symbol">$</span>{{$f->fees}}<span class="time-symbol">/mo</span></h1>
			<div class="content" style="min-height: 200px;">
				<ul class="">
					<li><i class="fa fa-check-circle-o font-big" aria-hidden="true"></i> <span>Accede a productos Premium</span> </li>
				</ul>

     <div class="form-group" style="margin-bottom: 10%; margin-top: 20%;">
                                <button type="submit" class="btn btn-primary" style="width: 100%">
                                	Choose Plan
                                </button>
                        </div>
               
			</div>

           {{ Form::close() }}

		</div>

		@endforeach
	</div>
</div>
@endsection
@push('js')

@endpush