@extends('layout.default')

@section('content')
<!-- Product detail page content start -->
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

<div class="container">
  <div class="row">
        <div class="col-sm-7 mt-3">
          <div class="card">
            <h3 style="font-weight:bold;">&nbsp;Product Detail</h3><hr>
            <div class="card-body">
              <?php if(file_exists(public_path('/img/'.$product->product_img)) && $product->product_img!= ""){ ?>
                  <img class="img-fluid" src="{{url('/')}}/img/{{$product->product_img}}" alt="">
              <?php }else{ ?>
                <img class="img-fluid" src="https://www.freeiconspng.com/uploads/no-image-icon-6.png">
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="col-sm-5 mt-3">
            <h2>{{$product->product_name}}</h2>
            <p>{{$product->product_desc}}</p>

            <small>Added by &nbsp;
              @if($userimg->profile_img != "")
                <?php if(file_exists(public_path('/img/profile/'.$userimg->profile_img))){ ?>
                    <img title="{{$userimg->first_name}}" alt="{{$userimg->first_name}}"  height="24px" class="rounded-circle" src="{{url('/')}}/img/profile/{{$userimg->profile_img}}" alt="">
                <?php }else{ ?>
                  <img title="{{$userimg->first_name}}" alt="{{$userimg->first_name}}"  height="24px" class="rounded-circle" src="{{url('/')}}/img/profile/new_user.png">
                <?php } ?>
              @else
              <img title="{{$userimg->first_name}}" alt="{{$userimg->first_name}}"  height="24px" class="rounded-circle" src="{{url('/')}}/img/profile/new_user.png">
              @endif
            <br>
            <br>
            <small>
            <form action="{{url('productcomment')}}" method="post">
              {{ csrf_field() }}
                <input type="hidden" name="id" value="{{$product->id}}">
              <div class="form-group">
                <input id = "changeStatus" type="checkbox" name='is_active' data-toggle="toggle" data-on="Active" data-off="Inactive" @if($product->is_active == 1) checked @endif data-onstyle="success" data-offstyle="danger"  >
              </div>
              <div class="form-group">
                <label for="premium">Premium </label>
                <input type="checkbox" name='is_premium' @if($product->is_premium == 1) checked @endif>
              </div>
              <div class="form-group">
                <label for="comment">Comment </label>
                <textarea type="text" class="form-control" name="comment" id="comment" aria-describedby="emailHelp" placeholder="Enter Comment" required="">{{$product->comment}} </textarea>
              </div>
              <button  type="submit" class="btn btn-primary btn-lg btn-block">Submit</button>
            </form>
           <br>
        </div>
        
        <?php echo $product->status ?>;
  </div>
</div>
<!-- Product detail page content end -->
@endsection

@section('pagejavascripts')
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@stop