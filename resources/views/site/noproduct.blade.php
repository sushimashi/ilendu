@extends('layout.default')

@section('content')
<!-- Home page content start -->
<!-- <div class="jumbotron">
	<div class="container">
	  <h1 class="text-center">Lend and borrow products and save upto 80%</h1>
	</div>
</div> -->
<style type="text/css">
  .pagination > li > a, .pagination > li > span {
    background-color: #FFFFFF;
    border: 1px solid #DDDDDD;
    color: inherit;
    float: left;
    line-height: 1.42857;
    margin-left: -1px;
    padding: 4px 10px;
    position: relative;
    text-decoration: none;
}

.text-center {
    text-align: center!important;
}
</style>
<div id="demo" class="carousel slide" data-ride="carousel">
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active d-none d-sm-block"></li>
    <li data-target="#demo" data-slide-to="1" class="d-none d-sm-block"></li>
    <li data-target="#demo" data-slide-to="2" class="d-none d-sm-block"></li>
  </ul>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="{{url('img/3.png')}}" class="d-block w-100">
        <div class="carousel-caption">
          <div class="overlay text-uppercase">
            <h1 class="h1_media">accede gratis</h1>
            <p><b>a miles de herramientas</b></p>
          </div>
        </div>
    </div>
    <div class="carousel-item">
      <img src="{{url('img/1.png')}}" class="d-block w-100">
      <div class="carousel-caption">
        <div class="overlay text-uppercase">
          <h1 class="h1_media">para que comprar? <br/> comparte!</h1>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <img src="{{url('img/2.png')}}" class="d-block w-100">
      <div class="carousel-caption">
        <div class="overlay text-uppercase">
          <h1 class="h1_media">accede gratis</h1>
          <p><b>a miles de libros</b></p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>
<div class="product-grid">
	<div class="container">
		<div align="center">
            <button class="btn btn-default filter-button active" data-filter="all">All</button>
            @foreach($category as $cat)
            <button class="btn btn-default filter-button" data-filter="{{$cat['name']}}">{{$cat['name']}}</button>
            @endforeach
        </div>
	      <div class="row" id="cityproduct">
			<h4>Product was not more than 100 For Your City.. </h4>
         
        </div>
	</div>
</div>
<!-- Home page content end -->

@endsection

@section('pagejavascripts')
<script type="text/javascript">
  var page = 1;
  var test = 0;
  $(window).scroll(function() {
      if($(window).scrollTop() + $(window).height() >= $(document).height() && test == 0) {
          page++;
          loadMoreData(page);
      }
  });


  function loadMoreData(page){
    $.ajax(
          {
              url: '?page=' + page,
              type: "get",
              beforeSend: function()
              {
                  $('.ajax-load').show();
              }
             
          })
          .done(function(data)
          {
             if(data.html == " "){

                  test = 1;
                  return;

              }else if(page <= data.total){ 

                  $('.ajax-load').hide();
                  $("#cityproduct").append(data.html);

              }
                if(page == data.total){
        
                  test = 1;
                  $('.ajax-load').hide(); 
                  return;

              }
          });
  }
  $(document).ready(function(){
    var Url = "<?php echo URL('/'); ?>";
    var userid = "<?php echo Auth::id(); ?>";
    $(".borrowed").click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        if(userid != ""){
            $.blockUI({ css: { fontSize: '17px'},message: 'Processing request ...'});
            var $this = $(this);
            if($(this).attr('data_count') > 0  || $(this).attr('data_expire') > 0){
                 //$("#product_borrowed").html('<span class="btn-lg btn-block">Processing request ...<span>');
                $.get(Url+'/borrow/'+id, function(data, status){
                    if(data == 'Sended' || status == 'success'){
                        $.unblockUI();
                        $this.hide();
                        toastr.success("Awaiting reply from the lender.");

                    }
                });
            } else {
                $(this).parent('#payment').submit();
            }
        }else{
            window.location.href = Url+'/products/'+id;
        }
    });

    /**Resize product images***/
      $(".image_width").each(function(){
         var setWidth =  $(this).width();
         if(setWidth > 300)
         {
           $(this).css("width","100%");
         }
      }) 
  })  
</script>

@stop