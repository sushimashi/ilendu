
<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="utf-8">
  <title>Telegenetic Admin</title>
  <style type="text/css">

    .btn 
    {
      display: inline-block;
      margin-bottom: 0;
      font-weight: 400;
      line-height: 1.42857143;
      text-align: center;
      white-space: nowrap;
      vertical-align: middle;
      -ms-touch-action: manipulation;
      touch-action: manipulation;
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      background-image: none;
      border: 1px solid transparent;
      border-radius: 4px;
    }

    .btn_color
    {
      border-radius: 50px;
      background-color: #2425a4;
      /* height: 34px; */
      width: 220px;
      color: #ffffff;
      text-decoration: none;
      margin-top: 20px;
      margin-bottom: 20px;
      padding-top: 9px;
      padding-bottom: 9px;
    }
    .btn_color:hover
    {
      background-color: #333399;
      color: white;
    }
    .btn_color.focus, .btn_color:focus, .btn_color:hover 
    {
      background-color: #333399;
      color: white;
    }

    .btn_color:disabled
    {
      color: black !important;
    }
    .mail_btext_color
    {
      color: #8E8E8E;
    }
    .a_color
    {
      color: #003c82; 
      text-decoration: none;
    }
  </style>
</head>
<body class="" style="background-color:#f6f6f6;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;">
  <?php $lang = 'en'; ?>
  <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;">
    <tr>
      <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
      <td class="container" style="font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;">
        <div class="content" style="box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;">
          <!-- START CENTERED WHITE CONTAINER -->
          <span class="preheader" style="color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;"></span>
          <table class="main" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;border-radius:3px;width:100%;">
            <!-- START MAIN CONTENT AREA -->
            <tr>
            <td>
              <table style="background-color:transparent;box-shadow: 1px 6px 10px #ddd;padding-left:15px!important;padding:10px;width:100%;">
                <td>
                  <img src= "{{asset('img/logo.png')}}" /> 
                </td>
              </table>
            </td>
            </tr>
              <tr>
              <td class="wrapper" style="font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;padding-top:0px">
                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;">
                  
                  <tr>
                   <td style="text-align: left;">

                   	<div style="padding:20px;max-width:600px;margin:0 auto;display:block;"> 
					           <p>Hello, {{$toName}}</p>

          <table>
						<tr><th>Community Name: {{$community_name}}</th></tr>
            @if($first_name && $last_name)<tr><th>Requester Name: {{$first_name}} {{$last_name}}</th></tr>@endif
            <tr><th>Requester Email: {{$email}}</th></tr>
						<tr>
							<td><a class="white" href="{{URL::to('/communitychangeStatus/'.$communityId.'/'.$userid.'/1')}}"><button value="Accept"  style="color:white ;background-color: #28a745; border-color: #28a745;border-radius: .25rem;border: 1px solid transparent;padding: .375rem .75rem; ">Accept</button></a></td> 
							<td><a class="white" href="{{URL::to('/communitychangeStatus/'.$communityId.'/'.$userid.'/2')}}"><button value="Reject"  style="color:white ;background-color: #dc3545; border-color: #dc3545;border-radius: .25rem;border: 1px solid transparent;padding: .375rem .75rem; ">Reject</button></a></td>
						</tr>
						
					</table>
					<p>Thank you</p>
					</div>

                    </td>
                  </tr>
                  <tr>
                    <td style="font-family:sans-serif;font-size:14px;vertical-align:top;"></td>
                  </tr>
            </table>
          </td>
        </tr>
        <!-- END MAIN CONTENT AREA -->
      </table>
      <!-- END CENTERED WHITE CONTAINER -->
    </div>
  </td>
  <td style="font-family:sans-serif;font-size:14px;vertical-align:top;">&nbsp;</td>
</tr>
</table>
</body>
</html>  
