<script>
    $(function () {


            $('#country_id').select2({
              ajax: {
                url: '/countries-ajax',
                dataType: 'json'
              }
            });



           $('#country_id').on('change', function () {
                
            id = $(this).val();
            status= $('#status').val();
            $.get(`/api/borrows-country?id=${id}&status=${status}`, function(result){
                _table.DataTable().clear();
                _table.DataTable().rows.add(result);
                _table.DataTable().draw();    
            });

            });

        var _table = $("#_table");
        
        var obj_datatable = {
            'ajax': {
                "url": '{{ url("/admin/borrows") }}',
                "type": "GET",
                dataSrc: '',
            },
            "responsive": true,
            "autoWidth": false,
            'columns': [
                  {
                     render: function (data, type, row) {

                  
                        return "<a href='/products/"+row.product_id+"'>"+row.producto+"</a>";
                       

                    }
                },
                {data: 'usuario',className: "text-center"},
                {data: 'pais',className: "text-center"},
                {data: 'mensaje',className: "text-center"},
                {data: 'status',className: "text-center"},
                {data: 'fecha',className: "text-center"},

                {
                     render: function (data, type, row) {

                        btn_habilitar="";
                        btn_eliminar="";
                        btn_despacho="";

                        switch (row.status) {
                            case 'Pending':
                               btn_despacho=`<a href="${row.id}" title="Despachar"  class=" btn btn-sm dispatch">
                            <i class="fa fa-car"></i>
                        </a>`;

                        
                      //ruta destroy
                        btn_eliminar = '<a href="' + row.id + '" class="btn btn-sm btn-danger _delete " title="Rechazar"><i class="fa fa-times"></i></a>';
                      
                                break;
                         case 'Dispatching':
                         
                        
                      //ruta destroy
                        btn_eliminar = '<a href="' + row.id + '" class="btn btn-sm btn-danger _delete " title="Rechazar"><i class="fa fa-times"></i></a>';
                      
                                break;

                        case 'PaidOut':
                              
                        //Habilitar / Deshabilitar Categoria
                        btn_habilitar = `<a href="${row.id}" title="Enviar"  class=" btn btn-sm send">
                            <i class="fa fa-arrow-up"></i>
                        </a>`;
                        
                      
                                break;
                       
                        }

                      

                            return  btn_habilitar+" "+btn_despacho+" "+btn_eliminar;
                    }
                }
            ]
        };
       
        _table.DataTable(obj_datatable);
            
         $("#dispatch_form").on("submit",function(e){
        e.preventDefault();

            var data = $(this).serialize();
           $(".dispatch").prop('disabled',true);
            $.ajax({
                url: $(this).attr('action'),
                type:$(this).attr('method'),
                data: data,
                success: function (result) {

                    swal("Exito!", "Informacion de despacho enviada exitosamente", "success")
                            .then((value) => {
                             _table.DataTable().ajax.reload();

                                 document.getElementById("dispatch_form").reset();
                                $("#dispatch_modal").modal('hide');
                            })
                           

                },
                error: function (e) {
                    console.log(e);
                    $.each(e.responseJSON.errors, function (index, element) {
                        if ($.isArray(element)) {
                            toastr.error(element[0]);
                        }
                    });
                },
                complete: function(){
                }
            });
      })
           $('#status').on('change', function () {
                      $('#loader_modal').modal('show');
            status = $(this).val();
            $.get(`/api/borrows/${status}/borrowsDatatable`, function(result){
                              $('#loader_modal').modal('hide');

                _table.DataTable().clear();
                _table.DataTable().rows.add(result);
                _table.DataTable().draw();    
            });

            });


         $('body').on('click', 'tbody ._delete', function (e) {
            e.preventDefault();
            
            category_id = $(this).attr('href');
            token = $("input[name=_token]").val();

            swal({
                title: '',
                text: '¿Desea rechazar la solicitud?',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    url: "{{url('/admin/borrows/')}}/" + category_id,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    datatype: 'json',
                    success: function (result) {
                        if (result.success) {
                            _table.DataTable().ajax.reload();
                            toastr.success(result.message);
                        } else {
                            toastr.error(result.error);
                        }
                    },
                    error: function (e) {
                        console.log(e);
                        $.each(e.responseJSON.errors, function (index, element) {
                            if ($.isArray(element)) {
                                toastr.error(element[0]);
                            }
                        });
                    }
                });
            }).catch(swal.noop);
        });

         //Habilitar / Deshabilitar Categoria
         $('body').on('click', 'tbody .send', function (e) {
            e.preventDefault();
            
            category_id = $(this).attr('href');

            swal({
                title: '',
                text: '¿Desea enviar el producto?',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(function () {
            $.ajax({
                url: "{{url('/admin/borrows-status/')}}/" + category_id+"?case=Sent",
                datatype: 'json',
                success: function (result) {
                    _table.DataTable().ajax.reload();
                    toastr.success(result.message);
                },
                error: function (e) {
                    console.log(e);
                    $.each(e.responseJSON.errors, function (index, element) {
                        if ($.isArray(element)) {
                            toastr.error(element[0]);
                        }
                    });
                }
                   });
            }).catch(swal.noop);

         });

    });
</script>
