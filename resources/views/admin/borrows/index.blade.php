@extends('admin.layouts.layout')

@section('content')
    <div class="block full">

        <div class="block-title">
            <h2>Solicitudes</h2>
        </div>

    

        <div class="row">
          <div class="row">
              <div class="col-md-2 text-left">
              <div class="text-center form-group">
                   <div class="form-group">
                              <div>
                                  
                                 <select class="form-control" style="width: 70%" name="status" id="status" >
                                  <option value="Sent" >Enviados</option>
                                  <option value="Received" >Recibidos</option>
                                  <option value="Returned" >Retornados</option>

                              </select>

                              </div>
                             

                            </div>
               </div>
            </div>

            <div class="col-md-2 text-left">
              <div class="text-center form-group">
                   <div class="form-group">
                              <div>
                              <label for="country_id">Pais</label>
                              <select class="form-control" style="width: 70%" name="country_id" id="country_id" >
                                <option value="44" selected>Chile</option>
                              </select>

                              </div>
                             

                            </div>
               </div>
            </div>
          </div>
             
            <div class="col-md-12">
                <br/><br/>
                <div class="table-responsive">
                    <div class="dataTables_wrapper form-inline no-footer">
                        <div class="_tabla">
                            <table class="table responsive table-vcenter dataTable no-footer" id="_table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">Producto</th>
                                        <th class="text-center">Usuario</th>
                                        <th class="text-center">Pais</th>
                                        <th class="text-center">Mensaje</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Fecha</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@include('site.dispatch_modal')
 @include('site.loader')


                                {{-- Ver Categoria --}}
@include('plugins.datepicker')
@endsection

@push('js')
<script type="text/javascript">
    $(function(){
                    $('#status').select2();

      $(document).on('click','.dispatch',function(e){
      e.preventDefault();

      id=$(this).attr('href');
      $("#borrow_id").val(id);
       $('#dispatch_modal').modal('show');
     document.getElementById("dispatch_form").reset();


 });


     

    })
       
</script>


    @include ('plugins.datepicker')
    @include ('plugins.datatable')
    @include('admin.borrows.js.index')


@endpush