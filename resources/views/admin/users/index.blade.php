@extends('admin.layouts.layout')

@section('content')
    <div class="block full">
        <div class="block-title">
            <h2>Listado de Usuarios</h2>
        </div>
        <div class="row">
            {{-- @permission('') --}}
            <div class="col-md-12 text-right">
                <a data-toggle="modal" href="#createUserModal" class="btn btn-info btn-effect-ripple">
                    <i class="fa fa-plus"></i> 
                    &nbsp Nuevo
                </a>
            </div>
            {{-- @endpermission --}}
        </div>
        <br>
        <div class="row">
           <div class="col-md-12">
        <div class="col-md-6">
                 <div class="form-group" style="display: inline-block; width: 40%;">
                      <label>Country:</label>
                     <select class="form-control country_id" id="country_id" name="country_id">
                         <option value="43" selected> Chile</option>
                     </select>
                  </div>

                    <div class="form-group" style="display: inline-block; width: 40%;">
                      <label>City:</label>
                     <select class="form-control city_id" id="city_id"  name="city_id"></select>
                  </div>

                     <div class="form-group" id="filter_button" style="display: inline-block; width: 10%;">
                    <button class="btn btn-primary">Filtrar</button>
                  </div>
        </div>
    </div>


            <div class="col-md-12">
                <div class="table-responsive">
                    <div class="dataTables_wrapper form-inline no-footer">
                        <div class="_tabla">
                            <table class="table responsive table-vcenter dataTable no-footer" id="users_table" cellspacing="0" width="100%">
                                <thead>
                                   <tr>
                                       <th class="text-center">Nombre</th>
                                       <th class="text-center">E-mail</th>
                                       <th class="text-center">Ciudad</th>
                                       <th class="text-center">Pais</th>
                                       <th class="text-center">Acciones</th>
                                   </tr>
                                </thead>
                                <tbody></tbody>
                            </table>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
                                        <!-- Modals -->
        
                                    {{-- nuevo usuario --}}
        <div class="modal fade" id="createUserModal" tabindex="-1" role="dialog" aria-labelledby="createUserModal" aria-hidden="true">
           @include('admin.users.create')
        </div>

                                    {{-- Ver Usuario --}}
        <div class="modal fade" id="showUserModal" tabindex="-1" role="dialog" aria-labelledby="showUserModal" aria-hidden="true">
            {{-- <div class="lds-spinner" style=" left: 50%; top: 50%; ">
                <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
            </div> --}}
        </div>

@endsection

@push('js')
 <script type="text/javascript">
        
        $(function(){
              $(".country_id").select2({
  ajax: {
    url: '/countries-ajax',
    dataType: 'json'
    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
  }
});

                 $(".city_id").select2({
  ajax: {
    url: '/cities-ajax/'+$(".country_id").val(),
    dataType: 'json'
    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
  }
}); 

$(".country_id").on("change",function(){

      $(".city_id").select2({
  ajax: {
    url: '/cities-ajax/'+$(".country_id").val(),
    dataType: 'json'
    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
  }
}); 
})

 
        })
    </script>
    @include ('plugins.datatable')
    @include('admin.users.js.index')
    @include('admin.users.js.create')
    @include('admin.users.js.edit')

@endpush