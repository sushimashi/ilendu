<div class="modal-dialog" role="document">

    <div class="modal-content">

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="text-center modal-title">
                {{$user->getNombreCompletoAttribute()}}
            </h4>
        </div>

            <div class="modal-body">
                <form autocomplete="off">
                            
                     <div class="mt-1 row text-center">
                         {{-- Nombre --}}
                         <div class="col-xs-6">
                             <div class="form-group">
                                 {!! Form::label('firstName', 'Nombre:') !!}
                                 <input disabled class="form-control" type="text" name="firstName" value="{{$user->first_name}}">
                             </div>
                         </div>
                         {{-- Apellidos --}}
                         <div class="col-xs-6">
                             <div class="form-group">
                                 {!! Form::label('lastName', 'Apellido:') !!}
                                 <input class="form-control" type="text" name="lastName" disabled value="{{$user->last_name}}">
                             </div>
                         </div>
                     </div>

                             
                     <div class="mt-1 row text-center">
                         {{-- Email --}}
                         <div class="col-xs-6">
                             <div class="form-group">
                                 {!! Form::label('userEmail', 'Email:') !!}
                                 <input class="form-control" disabled type="email" name="email"  value="{{$user->email}} " >
                             </div>
                         </div>
                         {{-- Pais --}}
                         <div class="col-xs-6">
                             <div class="form-group">
                                {!! Form::label('country', 'Pais:') !!}
                                 <select disabled name="country" class="form-control" style="width: 100%">
                                            <option>
                                                {{$country->name}}
                                         </option>
                                 </select>
                             </div>
                         </div>

                     </div>

                     <div class="mt-1 row text-center">

                         {{-- Estado --}}
                         <div class=" col-xs-6">
                             <div class="form-group">
                                 {!! Form::label('state', 'Estado:') !!}
                                 <select disabled name="state" class="form-control" style="width: 100%">
                                        <option>{{$state->name}}</option>
                                 </select>
                             </div>
                         </div>

                         {{-- Ciudad --}}
                         <div class=" col-xs-6">
                             <div class="form-group">
                             {!! Form::label('city', 'Ciudad:') !!}
                             <select name="city" disabled class="form-control" style="width: 100%">
                                    <option>{{$city->name}}</option>
                                 </select>                                
                             </div>
                         </div>

                     </div>
                     
                     <div class="mt-1 row text-center">
                         {{-- Foto --}}
                         <div class="col-xs-12">
                             <div class="form-group">

                                 {!! Form::label('userImage', 'Foto:') !!}
                                <div class="row image-editor-profile">

                                     <div class="col-md-12">

                                       <center>
                                         <div class="cropit-preview">
                                             
                                         </div>
                                         <br><br>
                                         <span class="fa fa-file-picture-o pic-small"></span>

                                         <input type="range" class="cropit-image-zoom-input">

                                         <span class="fa fa-file-picture-o pic-big"></span>
                                         <br><br>

                                       </center>

                                     </div>
                                </div>

                             </div>
                         </div>
                     </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                            Cerrar
                        </button>
                        <button type="submit" class="btn btn-primary modifyUser" 
                            data-user_id="{{$user->id}}" >
                            Modificar
                        </button>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>

<script type="text/javascript">

    $(".image-editor-profile").cropit({
        exportZoom: 1.25,
        imageBackground: true,
        imageBackgroundBorderWidth: 20,
        imageState: { src: "{{ isset($user->profile_img)?
                            asset('/img/profile/'.$user->profile_img):
                            '/images/user-img.png' }}" },
    });

</script>
