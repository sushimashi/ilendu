<script type="text/javascript">
	$(function () {

		$('#createUserModal').on('shown.bs.modal', function (e) {

		    $(".image-editor-createUser").cropit({
		        exportZoom: 1.25,
		        imageBackground: true,
		        imageBackgroundBorderWidth: 20,
		        imageState: { src: "/images/user-img.png"}
		    });

		});

		$('.image-editor-createUser .rotate-cw').click(function() {
		    $(".image-editor-createUser").cropit('rotateCW');
		});
		
		$('.image-editor-createUser .rotate-ccw').click(function() {
		    $(".image-editor-createUser").cropit('rotateCCW');
		});

		// select2
		$('#countryUser').select2({
		    dropdownParent: $('#country_select'),
		    // tags:true
		});

		$('#stateUser').select2({
		    dropdownParent: $('#state_select'),
		    // tags:true
		});

		$('#cityUser').select2({
		    dropdownParent: $('#city_select'),
		    // tags:true
		});
		//

		$("#countryUser").on("change",function(){

		  $('#stateUser').empty();
		    if ( $(this).val() === '' ){
		        $("#stateUser").append(`<option value=""> - Ninguno  - </option>`);
		        $("#cityUser").empty().append(`<option value=""> - Ninguno  - </option>`);
		        $('#sendCreateUserForm').addClass('disabled');
		        return;
		    } 

		  $.get(`/api/country/${ $(this).val() }/states`, function(result){

		        if( result.length > 0 )
		        {
		            result.forEach(element => {
		                
		                var data = {
		                    id: element.id,
		                    text: element.name
		                };

		                var newOption = new Option(data.text, data.id, false, false);
		                $("#stateUser").append(newOption);
		            });

		            $("#stateUser").trigger('change');

		        } else {

		            $("#stateUser").append(`<option value=""> - Ninguno  - </option>`)
		                       .trigger('change'); 

		        }

		    });

		})

		$("#stateUser").on("change",function(){

		    $('#cityUser').empty();

		    if ($(this).val() === ''){
		        $("#cityUser").append(`<option value=""> - Ninguno  - </option>`);
		        $('#sendCreateUserForm').addClass('disabled');
		        return;
		    }

		    $.get(`/api/state/${ $(this).val() }/cities`, function(result){

		        if( result.length > 0 )
		        {
		            result.forEach(element => {
		            var data = {
		            id: element.id,
		            text: element.name
		            };

		            var newOption = new Option(data.text, data.id, false, false);
		            $("#cityUser").append(newOption);
		        });
		            $('#sendCreateUserForm').removeClass('disabled');

		        } else {

		            $("#cityUser").append(`<option value=""> - Ninguno  - </option>`)
		                      .trigger('change');
		        }

		    });
		})

		function clearForm() {
            $('#users_table').DataTable().ajax.reload(null, false);
			$('#createUserModal').modal('hide');
            $('#countryUser').select2('destroy');
            $('#stateUser').empty().append('<option value="">-- Ninguno --</option>');
            $('#cityUser').empty().append('<option value="">-- Ninguno --</option>');
			$('#createUserForm').trigger('reset');
			$('#countryUser').select2({
			    dropdownParent: $('#createUserModal'),
			});
			$('.image-editor-createUser').cropit('destroy');
			$('.cropit-preview').empty();
		}

		// crear usuario
		$(document).on('click', '#sendCreateUserForm', function(e){
			e.preventDefault();

			if ($(this).hasClass('disabled'))
				return;

			let imageData = $(".image-editor-profile").cropit('export',{
			                    type: 'image/jpeg',
			                    quality: .9,
			                    originalSize:true
			                    });

			let	data = new FormData($('#createUserForm')[0]);
			$(this).addClass('disabled');

			console.log($('#createUserForm').serialize());
			$.ajax({
				processData: false,
				contentType: false,
				enctype: 'multipart/form-data',
				url: $('#createUserForm').attr('action'),
				type: $('#createUserForm').attr('method'),
				data: data,
	            success: function (result) {

	                swal("Exito!", "Usuario creado exitosamente", "success")
	                        .then((value) => {
	                        	clearForm();
	                        })
	                        .catch( function(){
	                        	clearForm();
	                        });

	            },
	            error: function (e) {
	                console.log(e);
	                $.each(e.responseJSON.errors, function (index, element) {
	                    if ($.isArray(element)) {
	                        toastr.error(element[0]);
	                    }
	                });
	            }, 
	            complete: function(){
	            	$('#sendCreateUserForm').removeClass('disabled');
	            }
	          });
		});
	});
</script>