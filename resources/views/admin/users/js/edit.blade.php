<script type="text/javascript">
	$(function () {
		
			// actualizar usuario
			$(document).on('click', '.modifyUser', function(e){
				e.preventDefault();

				let user_id = $(this).attr('data-user_id');
				$.get(`/admin/users/${user_id}/edit`, function(result){

					$('#showUserModal').empty().append(result).modal('show');
					$('#newCountryUser').select2({
					    dropdownParent: $('#new_country_select'),
					});

					$('#newStateUser').select2({
					    dropdownParent: $('#new_state_select'),
					});

					$('#newCityUser').select2({
					    dropdownParent: $('#new_city_select'),
					    // tags:true
					});

				});

			});

			$(document).on("change", "#newCountryUser",function(){

			  $('#newStateUser').empty();
			    if ( $(this).val() === '' ){
			        $("#newStateUser").append(`<option value=""> - Ninguno  - </option>`);
			        $("#newCityUser").empty().append(`<option value=""> - Ninguno  - </option>`);
			        $('#sendUpdateUserForm').addClass('disabled');
			        return;
			    } 

			  $.get(`/api/country/${ $(this).val() }/states`, function(result){

			        if( result.length > 0 )
			        {
			            result.forEach(element => {
			                
			                var data = {
			                    id: element.id,
			                    text: element.name
			                };

			                var newOption = new Option(data.text, data.id, false, false);
			                $("#newStateUser").append(newOption);
			            });

			            $("#newStateUser").trigger('change');

			        } else {

			            $("#newStateUser").append(`<option value=""> - Ninguno  - </option>`)
			                       .trigger('change'); 

			        }

			    });

			})

			$(document).on("change", "#newStateUser",function(){

			    $('#newCityUser').empty();

			    if ($(this).val() === ''){
			        $("#newCityUser").append(`<option value=""> - Ninguno  - </option>`);
			        $('#sendUpdateUserForm').addClass('disabled');
			        return;
			    }

			    $.get(`/api/state/${ $(this).val() }/cities`, function(result){

			        if( result.length > 0 )
			        {
			            result.forEach(element => {
			            var data = {
			            id: element.id,
			            text: element.name
			            };

			            var newOption = new Option(data.text, data.id, false, false);
			            $("#newCityUser").append(newOption);
			        });
			            $('#sendUpdateUserForm').removeClass('disabled');

			        } else {

			            $("#newCityUser").append(`<option value=""> - Ninguno  - </option>`)
			                      .trigger('change');

			        }

			    });
			})

			$(document).on('click', '#sendUpdateUserForm', function(e){
				e.preventDefault();

				if($(this).hasClass('disabled'))
					return;

				let imageData = $(".image-editor-profile").cropit('export',{
				                    type: 'image/jpeg',
				                    quality: .9,
				                    originalSize:true
				                    });

				let	data = new FormData($('#updateUserForm')[0]);
				console.log($('#updateUserForm').serialize());

				$(this).addClass('disabled');

				   $.ajax({
				   	   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				   	   processData: false,
				   	   contentType: false,
				   	   enctype: 'multipart/form-data',
				       url: $('#updateUserForm').attr('action'),
				       type: 'POST',
				       data: data,
				       success: function (result){

							swal(result.state, result.message, result.type)
							.then((value) => {
								$('#users_table').DataTable().ajax.reload(null, false);
				              	$('#showUserModal').modal('hide');
							})
							.catch( function(){
								$('#users_table').DataTable().ajax.reload(null, false);
				              	$('#showUserModal').modal('hide');
							});

				       },
				       error: function (e) {
				           console.log(e.responseJSON);
				           $.each(e.responseJSON.errors, function (index, element) {
				               if ($.isArray(element)) {
				                   toastr.error(element[0]);
				               }
				           });
				       },
				       complete: function(){
				       	$('#sendUpdateUserForm').removeClass('disabled');
				       }
				   });
		   });

	});
</script>