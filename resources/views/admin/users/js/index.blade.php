<script type="text/javascript">
	$(function () {

	


	    var _table = $("#users_table");
	    var obj_datatable = {
	        "ajax": {
	        	"url": '/api/usersTable',
	            "type": "GET",
	            dataSrc: '',
	             data: function ( d ) {
        d.country_id = $('#country_id').val();
        d.city_id = $('#city_id').val();

    }
	        },
	        "responsive": true,
	        "autoWidth": false,
	         columns: [
	            { 
	            	render: function(data, type, row){
	            	return `${row.first_name} ${row.last_name}`; 
	            	},
	            	 className: "text-center"
	            },
	            { data: 'email', className: "text-center"},
	            { data: 'city.name', className: "text-center"},
	            { data: 'country.name', className: "text-center"},
	            {
	                 render: function (data, type, row) {
	                    
	                    let btn_showUser,
	                        btn_modifyUser,
	                        btn_deleteUser;

	                        // ver usuario
	                        btn_showUser = '<a data-toggle="modal" href="#showUserModal" data-user_id="'+row.id+'" class="btn bg-white fa fa-eye showUser" title="Ver"></a>';

	                        // modificar usuario
	                        btn_modifyUser = '<a data-toggle="modal" href="#showUserModal" data-user_id="'+row.id+'" class="btn btn-success fa fa-pencil modifyUser" title="Modificar"></a>';

	                        // eliminar usuario
	                        btn_deleteUser = '<a data-user_id="'+row.id+'" class="btn btn-danger fa fa-close _delete" title="Eliminar"></a>';

	                        if(row.rol=="Administrador")
	                        {
	                            btn_admin = '<a data-user_id="'+row.id+'" class="btn btn-default fa fa-hand-scissors-o _leave" title="Quitar rol"></a>'; 	

	                        }else
	                        {
	                            btn_admin = '<a data-user_id="'+row.id+'" class="btn btn-default fa fa-user _admin" title="Añadir rol"></a>'; 	
	                        }
	                   


	                        return  btn_showUser+" " + btn_modifyUser +" "+btn_deleteUser+" "+btn_admin;
	                }
	            }
	        ]
	    };

		_table.DataTable(obj_datatable);

		// ver usuario
		$(document).on('click', '.showUser', function(e){
			e.preventDefault();
			let user_id = $(this).attr('data-user_id');
			$.get(`/admin/users/${user_id}`, function(result){
				$('#showUserModal').empty().append(result).modal('show');
			});
		});

		//
		$('#showUserModal').on('hidden.bs.modal', function () {
		    $(this).empty();
		});
// quitar admin
		 $('body').on('click', 'tbody ._leave', function (e) {
		    
		    userID = $(this).attr('data-user_id');
		    token = $("input[name=_token]").val();
		    e.preventDefault();
		    swal({
		        title: '',
		        text: 'Quitar rol de administrador?',
		        showCancelButton: true,
		        confirmButtonText: 'Si',
		        cancelButtonText: 'No'
		    }).then(function () {
		        $.ajax({
		        	url: "{{ url('/admin/users/role-admin')}}/" + userID+'?role=leave',
		            headers: {'X-CSRF-TOKEN': token},
		            type: 'POST',
		            datatype: 'json',
		            success: function (result) {
		            	$('#users_table').DataTable().ajax.reload(null, false);
		                toastr.success(result.message);
		            },
		            error: function (e) {
		                console.log(e);
		                $.each(e.responseJSON.errors, function (index, element) {
		                    if ($.isArray(element)) {
		                        toastr.error(element[0]);
		                    }
		                });
		            }
		        });
		    }).catch(swal.noop);
		});


	$("#filter_button").on("click",function(){

		            	$('#users_table').DataTable().ajax.reload();


});

// eliminar usuario
		 $('body').on('click', 'tbody ._admin', function (e) {
		    
		    userID = $(this).attr('data-user_id');
		    token = $("input[name=_token]").val();
		    e.preventDefault();
		    swal({
		        title: '',
		        text: '¿Otorgar rol de administrador?',
		        showCancelButton: true,
		        confirmButtonText: 'Si',
		        cancelButtonText: 'No'
		    }).then(function () {
		        $.ajax({
		        	url: "{{ url('/admin/users/role-admin')}}/" + userID,
		            headers: {'X-CSRF-TOKEN': token},
		            type: 'POST',
		            datatype: 'json',
		            success: function (result) {
		            	$('#users_table').DataTable().ajax.reload(null, false);
		                toastr.success(result.message);
		            },
		            error: function (e) {
		                console.log(e);
		                $.each(e.responseJSON.errors, function (index, element) {
		                    if ($.isArray(element)) {
		                        toastr.error(element[0]);
		                    }
		                });
		            }
		        });
		    }).catch(swal.noop);
		});


		// eliminar usuario
		 $('body').on('click', 'tbody ._delete', function (e) {
		    
		    userID = $(this).attr('data-user_id');
		    token = $("input[name=_token]").val();
		    e.preventDefault();
		    swal({
		        title: '',
		        text: '¿Eliminar Usuario?',
		        showCancelButton: true,
		        confirmButtonText: 'Si',
		        cancelButtonText: 'No'
		    }).then(function () {
		        $.ajax({
		        	url: "{{ url('/admin/users/')}}/" + userID,
		            headers: {'X-CSRF-TOKEN': token},
		            type: 'DELETE',
		            datatype: 'json',
		            success: function (result) {
		            	$('#users_table').DataTable().ajax.reload(null, false);
		                toastr.success(result.message);
		            },
		            error: function (e) {
		                console.log(e);
		                $.each(e.responseJSON.errors, function (index, element) {
		                    if ($.isArray(element)) {
		                        toastr.error(element[0]);
		                    }
		                });
		            }
		        });
		    }).catch(swal.noop);
		});

	});
</script>