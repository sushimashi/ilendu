<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="text-center modal-title">
                Editar Usuario
            </h4>
        </div>

            <div class="modal-body">

                <form id="updateUserForm" enctype="multipart/form-data" action="{{ route('updateUser', ['id' => $user->id]) }}" method="POST" autocomplete="off">

                    {{ csrf_field() }}

                    <div class="mt-1 row text-center">
                        {{-- Nombre --}}
                        <div class="col-xs-6">
                            <div class="form-group">
                                {!! Form::label('firstName', 'Nombre:') !!}
                                <input class="form-control" type="text" name="firstName" value="{{$user->first_name}}">
                            </div>
                        </div>
                        {{-- Apellidos --}}
                        <div class="col-xs-6">
                            <div class="form-group">
                                {!! Form::label('lastName', 'Apellido:') !!}
                                <input class="form-control" type="text" name="lastName" value="{{$user->last_name}}">
                            </div>
                        </div>
                    </div>

                            
                    <div class="mt-1 row text-center">
                        {{-- Email --}}
                        <div class="col-xs-6">
                            <div class="form-group">
                                {!! Form::label('userEmail', 'Email:') !!}
                                <input class="form-control" type="email" name="email"  value="{{$user->email}} " >
                            </div>
                        </div>
                        {{-- Pais --}}
                        <div class="col-xs-6">
                            <div class="form-group " id="new_country_select">
                         <label>Country:</label>
                                <select id="newCountryUser" name="country" class="form-control" style="width: 100%">
                                        @foreach($countries as $country)
                                            <option 
                                            @if ($user->country_id == $country->id)
                                                selected 
                                            @endif
                                            value="{{ $country->id }}">{{$country->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="mt-1 row text-center">

                        {{-- Estado --}}
                        <div class=" col-xs-6">
                            <div class="form-group" id="new_state_select">
                                <label>State:</label>
                                <select id="newStateUser" name="state" class="form-control" style="width: 100%">
                                        @foreach($states as $state)
                                            <option
                                            @if ($user->state_id == $state->id)
                                                selected 
                                            @endif
                                             value="{{ $state->id }}">{{$state->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>

                        {{-- Ciudad --}}
                        <div class=" col-xs-6">
                            <div class="form-group" id="new_city_select">
                                <label>City:</label>
                            <select name="city" id="newCityUser" class="form-control" style="width: 100%">
                                    @foreach($cities as $city)
                                        <option 
                                        @if ($user->city_id == $city->id)
                                            selected 
                                        @endif
                                        value="{{ $city->id }}">{{$city->name}}</option>
                                    @endforeach
                                </select>                                
                            </div>
                        </div>

                    </div>

                    <div class="mt-1 row text-center">
                        {{-- Foto --}}
                        <div class="col-xs-12">

                            <div class="form-group">
                                {!! Form::label('userImage', 'Foto:') !!}

                               <div class="row image-editor-profile">
                                    <div class="col-md-12">
                                      <center>
                                        <div class="cropit-preview">
                                        </div>
                                        <br><br>

                                        <div class="rotate">
                                          <span  class="fa fa-repeat rotate-cw icon-rotate-right"></span>
                                          <span class="fa fa-repeat rotate-ccw icon-rotate-left"></span>
                                        </div>

                                        <span class="fa fa-file-picture-o pic-small"></span>

                                        <input type="range" class="cropit-image-zoom-input">

                                        <span class="fa fa-file-picture-o pic-big"></span>

                                        <input type="hidden" name="profile_photo" class="hidden-image-data"/><br><br>

                                      </center>
                                    </div>

                                    <div class="col-md-4 col-md-offset-4 col-xs-offset-4">
                                        <span class="btn btn-default btn-file">
                                            Subir archivo <input name="userImage" class="cropit-image-input" type="file"
                                            value="">
                                        </span>
                                    </div>

                                </div>

                            </div>

                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">
                            Cancelar
                        </button>
                        <button type="submit" class="btn btn-info" id="sendUpdateUserForm">
                            Salvar
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(".image-editor-profile").cropit({
        exportZoom: 1.25,
        imageBackground: true,
        imageBackgroundBorderWidth: 20,
        imageState: { src: "{{ isset($user->profile_img)?
                            asset('/img/profile/'.$user->profile_img):
                            '/images/user-img.png' }}" }
    });

    $('.image-editor-profile .rotate-cw').click(function() {
        $(".image-editor-profile").cropit('rotateCW');
    });
    
    $('.image-editor-profile .rotate-ccw').click(function() {
        $(".image-editor-profile").cropit('rotateCCW');
    });

</script>

