<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="text-center modal-title">
                Nuevo Usuario
            </h4>
        </div>

            <div class="modal-body">
                <form enctype="multipart/form-data" id="createUserForm" action="{{ route('users.store') }}" method="POST" autocomplete="off">

                    {{ csrf_field() }}
                            
                    <div class="mt-1 row text-center">
                        {{-- Nombre --}}
                        <div class="col-xs-6">
                            <div class="form-group">
                                {!! Form::label('firstName', 'Nombre:') !!}
                                <input class="form-control" type="text" name="firstName" placeholder="Nombres">
                            </div>
                        </div>
                        {{-- Apellidos --}}
                        <div class="col-xs-6">
                            <div class="form-group">
                                {!! Form::label('lastName', 'Apellido:') !!}
                                <input class="form-control" type="text" name="lastName" placeholder="Apellidos">
                            </div>
                        </div>
                    </div>

                    <div class="mt-1 row text-center">
                        {{-- Email --}}
                        <div class="col-xs-6">
                            <div class="form-group">
                                {!! Form::label('email', 'Email:') !!}
                                <input class="form-control" type="email" name="email" placeholder="ejemplo@email.com">
                            </div>
                        </div>
                        {{-- Password --}}
                        {{-- <div class="col-xs-6">
                            <div class="form-group">
                                {!! Form::label('password', 'Contraseña:') !!}
                                <input class="form-control" type="password" name="password">
                            </div>
                        </div> --}}
                        {{-- Pais --}}
                        <div class="col-xs-6">
                            <div class="form-group" id="country_select">
                         <label>Country:</label>

                                <select  id="countryUser" name="country" class="form-control" style="width: 100%">
                                    <option value="">-- Seleccione --</option>
                                        @foreach($countries as $country)
                                            <option value="{{ $country->id }}">{{$country->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="mt-1 row text-center">
                        {{-- Estado --}}
                        <div class=" col-xs-6">
                            <div class="form-group" id="state_select">
                                <label>State:</label>

                                <select  id="stateUser" name="state" class="form-control" style="width: 100%">
                                    <option value="">-- Ninguno --</option>
                                </select>
                            </div>
                        </div>

                        {{-- Ciudad --}}
                        <div class=" col-xs-6">
                            <div class="form-group" id="city_select">
                                <label>City:</label>
                            <select  id="cityUser" name="city" class="form-control" style="width: 100%">
                                <option value="">-- Ninguno --</option>
                                </select>                                
                            </div>
                        </div>
                    </div>

                    <div class="mt-1 row text-center">
                        {{-- Foto --}}
                        <div class="col-xs-12">
                            <div class="form-group">

                                <label for='userImage'>Foto:</label>
                               <div class="row image-editor-createUser">

                                    <div class="col-md-12">

                                      <center>
                                        <div class="cropit-preview">
                                            
                                        </div>
                                        <br><br>

                                        <div class="rotate">
                                          <span  class="fa fa-repeat rotate-cw icon-rotate-right"></span>
                                          <span class="fa fa-repeat rotate-ccw icon-rotate-left"></span>
                                        </div>

                                        <span class="fa fa-file-picture-o pic-small"></span>

                                        <input type="range" class="cropit-image-zoom-input">

                                        <span class="fa fa-file-picture-o pic-big"></span>
                                        <br><br>

                                      </center>

                                    </div>

                                    <div class="col-md-4 col-md-offset-4 col-xs-offset-4">
                                        <span class="btn btn-default btn-file">
                                            Subir archivo <input name="userImage" class="cropit-image-input" type="file">
                                        </span>
                                    </div>

                               </div>

                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                            Cerrar
                        </button>
                        <button type="submit" class="btn btn-primary" id="sendCreateUserForm">
                            Crear
                        </button>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>

