<script>
    $(function () {

    $('#checkall').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.products').each(function() { //loop through each checkbox
        //alert("check");
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
           $('.products').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
        //alert("uncheck");
            });        
        }
  }); 


 $(document).on("click",".delete_multiple",function(e){
      
      e.preventDefault();
      
    if ( !$(".products").is(':checked') )
    {
         alert("Por favor selecciona al menos una caja!..");
        
        return false; 
    }
    else
    {
      if(confirm('Estas seguro de querer eliminar a estos productos?') == true)
      {
          $("#listing_form").attr("action", "/admin/products/delete_multi");
          var url  = $("#listing_form").attr("action");
          var data = $("#listing_form").serialize();
          $.ajax({
            url : url ,
            method : 'post',
            data : data,
            success : function(data){
                //Reload Table Data 
                $("#undisplayedProductsTable").DataTable().ajax.reload(null, false);
                $("#checkall").prop("checked","");
                
                var json_data = $.parseJSON(data);
              
                if( json_data.status == 200 )
                {
                   msg_display( 200, json_data.message );
                }
                else
                {
                  msg_display( 500, json_data.message );

                }
            }
          });
        }
        else
        {
          return false;
        }
      }
      
    });
        $('#undisplayedProducts').addClass('active');

        var _table = $("#undisplayedProductsTable");
        var obj_datatable = {
            'ajax': {
                "url" : `{{ route('undisplayedProductsDataTable') }}`,
                "type": "GET",
                dataSrc: '',
            },
            "responsive": true,
            "autoWidth": false,
            'columns': [
            {
                    render:function(data,type,row)
                    {
                        return '<input type="checkbox"  id="chk" data-id="'+row.id+'" name="chk[]" class="product_check  products text-center" value="'+row.id+'">';
                    },
                                         className: "text-center"

                },
                {data: 'product_name',className: "text-center"},
                {
                     render: function (data, type, row) {
                        
                        let product_img =  
                            
                            `<img style="width:150px; height:150px;" data-product_id="${row.id}" title="${row.product_name}" src= "/img/thumbnail/${row.product_img}">
                            </img>`;

                            return  product_img;
                    }, className: "text-center"
                },
                { 
                    render: function(data, type, row){

                    if(row.borrowed==1)
                    {
                      return "Prestado";
                    }else{
                      return "Disponible";
                    }
                    },
                     className: "text-center"
                },
                {
                     render: function (data, type, row) {
                        
                        let exhibit, exhibitTitle;

                            if(row.in_front == 1){
                                exhibit = 'fa fa-arrow-down';
                                exhibitTitle = 'Quitar de Exhibicion';
                            } else{
                                exhibit = 'fa fa-arrow-up';
                                exhibitTitle = 'Exhibir';
                            }

                            // exhibir producto
                            btn_habilitar = `<a href="#" class=" ${exhibit} btn btn-sm btn-secondary btn_habilitar" title="${exhibitTitle}" id="product_${row.id}" data-product_id="${row.id}"><i class=" "></i></a>`;

                            return  btn_habilitar;
                    }
                }
            ]
        };

        _table.DataTable(obj_datatable);

        // exhibir producto
        $(document).on('click', '.btn_habilitar', function(e){
            e.preventDefault();
            
            let product_id = $(this).attr('data-product_id');
            $.get(`/api/product/${product_id}/productDisplay`, function(result){
               
               _table.DataTable().ajax.reload(null, false);
                toastr.success(result.message);

            });
        });

    });
</script>
