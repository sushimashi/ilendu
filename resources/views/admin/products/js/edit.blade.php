<script type="text/javascript">
    $(function () {
  

       $('#editProductModal').on('show.bs.modal', function () {
          // select2
          $('#productCategory').select2({
              'dropdownParent': $('#editProductModal'),
              tags: true
          });

          $.get("{{ route('productCategories') }}", function (result) {
              $('#productCategory').empty();
              result.forEach(function (element) {
                $('#productCategory').append(`<option value='${element.id}'>${element.name_spanish}</option>`);
              });


                     var category=$("#_category").val();
           $("#productCategory").val(category);
           $("#productCategory").select2().trigger('change');
          });
     

       });

        //
        $('#editProductModal').on('hidden.bs.modal', function () {
            $(this).empty();
        });

        $(document).on('click', '.btn_editar', function (e) {
            e.preventDefault();

            let product_id = $(this).attr('href');
            $.get(`/admin/products/${product_id}/edit`, function (result) {

                $('#editProductModal').empty().append(result).modal('show');

                           $('#community_2').select2({
                        ajax: {
                            url: '/getCommunities',
                            dataType: 'json'
                        },
                        dropdownParent: $("#box_2")

                    });

    
    $(".image-editor-product").cropit({
        exportZoom: 1.25,
        imageBackground: true,
        imageBackgroundBorderWidth: 20,
        imageSrc:"{{asset('/img/thumbnail/') }}/"+$("#_img").val()
    });

    $('.image-editor-product .rotate-cw').click(function() {
        $(".image-editor-product").cropit('rotateCW');
    });
    
    $('.image-editor-product .rotate-ccw').click(function() {
        $(".image-editor-product").cropit('rotateCCW');
    });



            });

        });
        

        $(document).on('click', '#sendEditProductForm', function(e){
            e.preventDefault();

            let imageData = $(".image-editor-product").cropit('export',{
                                type: 'image/jpeg',
                                quality: .9,
                                originalSize:true
                                });

            let data = new FormData($('#editProductForm')[0]);
            console.log($('#editProductForm').serialize());

               $.ajax({
                   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                   processData: false,
                   contentType: false,
                   enctype: 'multipart/form-data',
                   url: $('#editProductForm').attr('action'),
                   type: 'POST',
                   data: data,
                   success: function (result){

                        swal(result.state, result.message, result.type)
                        .then((value) => {
                            $('#productsTable').DataTable().ajax.reload(null, false);
                            $('#editProductModal').modal('hide');
                        })
                        .catch( function(){
                            $('#productsTable').DataTable().ajax.reload(null, false);
                            $('#editProductModal').modal('hide');
                        });

                   },
                   error: function (e) {
                       console.log(e.responseJSON);
                       $.each(e.responseJSON.errors, function (index, element) {
                           if ($.isArray(element)) {
                               toastr.error(element[0]);
                           }
                       });
                   }
               });
       });

    });
</script>