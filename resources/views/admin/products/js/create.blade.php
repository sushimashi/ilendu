<script type="text/javascript">
    $(function () {
       $('#community').select2({
                        ajax: {
                            url: '/getCommunities',
                            dataType: 'json'
                        },
                        dropdownParent: $("#box")

                    });

        // select2
       $('#product_category').select2({
           'dropdownParent': $('#createProductModal'),
           tags: true
       });

       $('#createProductModal').on('show.bs.modal', function () {
          $.get("{{ route('productCategories') }}", function (result) {
              $('#product_category').empty();
              result.forEach(function (element) {
                $('#product_category').append(`<option value='${element.id}'>${element.name_spanish}</option>`);
              });
          });
       });

       $('#createProductModal').on('shown.bs.modal', function (e) {

           $(".image-editor-createProduct").cropit({
               exportZoom: 1.25,
               imageBackground: true,
               imageBackgroundBorderWidth: 20,
               imageState: { src: "{{ asset('images/no_photo_icon.png') }}"}
           });

       });

       $('.image-editor-createProduct .rotate-cw').click(function() {
           $(".image-editor-createProduct").cropit('rotateCW');
       });
       
       $('.image-editor-createProduct .rotate-ccw').click(function() {
           $(".image-editor-createProduct").cropit('rotateCCW');
       });

        function clearForm() {
            $('#product_category').select2('destroy');
            $("#createProductForm").trigger('reset');
            $('#product_category').select2({
                'dropdownParent': $('#createProductModal'),
                tags: true
            });
             $(".image-editor-createProduct").cropit('destroy');
             $('.cropit-preview').empty();
        }
           
        // Registrar Producto
        $(document).on('click', '#sendCreateProductForm', function(e){
            e.preventDefault();

            let imageData = $(".image-editor-createProduct").cropit('export',{
                                type: 'image/jpeg',
                                quality: .9,
                                originalSize:true
                                });

            let data = new FormData($('#createProductForm')[0]);

            console.log($('#createProductForm').serialize());
            $.ajax({
                processData: false,
                contentType: false,
                enctype: 'multipart/form-data',
                url: $('#createProductForm').attr('action'),
                type: $('#createProductForm').attr('method'),
                data: data,
                success: function (result) {

                    swal(result.state, result.message, result.type)
                            .then((value) => {
                               $('#productsTable').DataTable().ajax.reload(null, false);
                              $('#createProductModal').modal('hide');
                              clearForm();
                          })
                            .catch( function(){
                               $('#productsTable').DataTable().ajax.reload(null, false);
                              $('#createProductModal').modal('hide');
                              clearForm();
                          });

                },
                error: function (e) {
                    console.log(e);
                    $.each(e.responseJSON.errors, function (index, element) {
                        if ($.isArray(element)) {
                            toastr.error(element[0]);
                        }
                    });
                }
              });
        });

    });
</script>