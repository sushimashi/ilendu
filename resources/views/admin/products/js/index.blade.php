<script>
    $(function () {
 
    $('#checkall').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.products').each(function() { //loop through each checkbox
        //alert("check");
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        }else{
           $('.products').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
        //alert("uncheck");
            });        
        }
  }); 


 $(document).on("click",".delete_multiple",function(e){
      
      e.preventDefault();
      
    if ( !$(".products").is(':checked') )
    {
         alert("Por favor selecciona al menos una caja!..");
        
        return false; 
    }
    else
    {
      if(confirm('Estas seguro de querer eliminar a estos productos?') == true)
      {
          $("#listing_form").attr("action", "/admin/products/delete_multi");
          var url  = $("#listing_form").attr("action");
          var data = $("#listing_form").serialize();
          $.ajax({
            url : url ,
            method : 'post',
            data : data,
            success : function(data){
                //Reload Table Data 
                $("#productsTable").DataTable().ajax.reload(null, false);
                $("#checkall").prop("checked","");
                
                var json_data = $.parseJSON(data);
              
                if( json_data.status == 200 )
                {
                   msg_display( 200, json_data.message );
                }
                else
                {
                  msg_display( 500, json_data.message );

                }
            }
          });
        }
        else
        {
          return false;
        }
      }
      
    });

        $('#products').addClass('active');

        var _table = $("#productsTable");
        var obj_datatable = {
            'ajax': {
                "url" : `/api/productsDataTable`,
                "type": "GET",
                dataSrc: '',
            },
            "responsive": true,
            "autoWidth": false,
            'columns': [
                {
                    render:function(data,type,row)
                    {

                        return '<input type="checkbox"  id="chk" data-id="'+row.id+'" name="chk[]" class="product_check  products text-center" value="'+row.id+'">';
                    },
                                         className: "text-center"

                },
                {data: 'product_name',className: "text-center"},
                { 
                    render: function(data, type, row){

                    return '<img src="/img/'+row.product_img+'" width="100px">'; 
                    },
                     className: "text-center"
                },
                 { 
                    render: function(data, type, row){

                    if(row.borrowed==1)
                    {
                      return "Prestado";
                    }else{
                      return "Disponible";
                    }
                    },
                     className: "text-center"
                },
                {
                     render: function (data, type, row) {
                        
                        let btn_habilitar = "",
                            btn_editar = "",
                            btn_eliminar="",
                            btn_premium="",
                            exhibit, exhibitTitle, premium, premiumTitle;

                            if(row.in_front == 1){
                                exhibit = 'fa fa-arrow-down';
                                exhibitTitle = 'Quitar de Exhibicion';
                            } else{
                                exhibit = 'fa fa-arrow-up';
                                exhibitTitle = 'Exhibir';
                            }

                            if (row.is_premium === 1){
                                premium = 'premium';
                                premiumTitle = 'Quitar Categoria Premium';
                            } else{
                                premium = '';
                                premiumTitle = 'Añadir Categoria Premium';
                            }

                            //ruta edit
                            btn_editar += '<a  href="'+ row.id +'" title="Editar" class="btn btn-sm btn-success btn_editar"><i class="fa fa-pencil"></i></a>';

                            // exhibir producto
                            btn_habilitar += `<a href="#" class=" ${exhibit} btn btn-sm btn-secondary btn_habilitar" title="${exhibitTitle}" id="product_${row.id}" data-product_id="${row.id}"><i class=" "></i></a>`;

                            // premium
                            btn_premium +=  
                                    `<label data-product_id="${row.id}" title="${premiumTitle}" class="${premium} btn_premium btn btn-sm ">
                                        <i class="fa fa-star"></i>
                                    </label>`;

                            //ruta destroy
                           btn_eliminar = '<a style="margin: 0 auto;" href="' + row.id + '" class="btn btn-sm btn-danger _delete" title="Eliminar" title="Premium"><i class="fa fa-times"></i></a>';


                            return  btn_editar+" "+ btn_habilitar +" "+ btn_premium +" "+ btn_eliminar;
                    }
                }
            ]
        };

        _table.DataTable(obj_datatable);

        // Eliminar Producto
         $('body').on('click', 'tbody ._delete', function (e) {
            
            product_id = $(this).attr('href');
            token = $("input[name=_token]").val();
            e.preventDefault();
            swal({
                title: '',
                text: '¿Eliminar Producto?',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    url: "{{ url('/admin/products/') }}/" + product_id,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    datatype: 'json',
                    success: function (result) {
                        _table.DataTable().ajax.reload(null, false);
                        toastr.success(result.message);
                    },
                    error: function (e) {
                        console.log(e);
                        $.each(e.responseJSON.errors, function (index, element) {
                            if ($.isArray(element)) {
                                toastr.error(element[0]);
                            }
                        });
                    }
                });
            }).catch(swal.noop);
        });

         // exhibir producto
         $(document).on('click', '.btn_habilitar', function(e){
             e.preventDefault();
             
             let product_id = $(this).attr('data-product_id');
             $.get(`/api/product/${product_id}/productDisplay`, function(result){
                
                _table.DataTable().ajax.reload(null, false);
                 toastr.success(result.message);

             });
         });

         // añadir a categoria "Premium"
         $(document).on('click', '.btn_premium', function(e){
             e.preventDefault();
             
             let product_id = $(this).attr('data-product_id');
             $.get(`/api/product/${product_id}/premiumCategory`, function(result){
                
                _table.DataTable().ajax.reload(null, false);
                 toastr.success(result.message);

             });
         });


    });
</script>
