<div class="modal-dialog" role="document">
    <div class="modal-content">
      
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="text-center modal-title">
                Editar Producto
            </h4>
        </div>

            <div class="modal-body">
                <form enctype="multipart/form-data" id="editProductForm" action="{{ route('updateProduct', ['product_id' => $product->id]) }}" method="POST" autocomplete="off">

                    {{ csrf_field() }}
                    <input type="hidden" id="_category" name="" value="{{$product->product_category}}">
                    <input type="hidden" id="_img" name="" value="{{$product->product_img}}">

                    <div class="row text-center" >
                      <div class="col-xs-2"><br></div>

                        <div class="col-xs-4">
                        <div class="form-group">
                          <label for="product_name">Nombre:</label>
                          <input type="text" class=" form-control" name="product_name" id="product_name" value="{{ $product->product_name }}" >
                        </div>
                      </div>
                      <div class="col-xs-4">
                        <div class="form-group">
                          <label for="product_category">Categoria:</label>
                          
                          <select style="width: 100%;" class="form-control" id="productCategory" name="product_category">
                          </select>
                        </div>
                      </div> 

                    </div>

                    <div class="row text-center">
                        <div class="col-xs-3"><br></div>

                         <div class="col-xs-2">
  
                            <div class="form-group">
                  <label for="communities">@lang('keywords.give_away')
                                         <input
                                            @if ($product->give_away === 1)
                                 checked
                               @endif 
                                          style="" value="1" type="checkbox"  style="width: 100%;" class="form-control" name='give_away'>

                  </label>

                  </div>
                    </div>
                      <div class="col-xs-2">
                          <div class="form-group">
                            <label for="is_premium">
                              Premium
                              <input
                              @if ($product->is_premium === 1)
                                 checked
                               @endif 
                              class="form-control" style="width: 100%;" type="checkbox" name="is_premium">
                            </label> 
                          </div>
                      </div>  

                      <div class="col-xs-2">
                          <div class="form-group">
                            <label for="is_premium">
                              Unlimited
                              <input
                              @if ($product->share_all === 1)
                                 checked
                               @endif 
                              class="form-control" style="width: 100%;" type="checkbox" name="share_all">
                            </label> 
                          </div>
                      </div>                  
                    </div>

                    <div class="row text-center">
                          {{-- Foto --}}
                          <div class="col-xs-12">
                              <div class="form-group">

                                  {!! Form::label('productImage', 'Foto:') !!}
                                 <div class="row image-editor-product">

                                      <div class="col-md-12">

                                        <center>
                                          <div class="cropit-preview">
                                              
                                          </div>
                                          <br><br>

                                          <div class="rotate">
                                            <span  class="fa fa-repeat rotate-cw icon-rotate-right"></span>
                                            <span class="fa fa-repeat rotate-ccw icon-rotate-left"></span>
                                          </div>

                                          <span class="fa fa-file-picture-o pic-small"></span>

                                          <input type="range" class="cropit-image-zoom-input">

                                          <span class="fa fa-file-picture-o pic-big"></span>

                                          <br><br>

                                        </center>

                                      </div>

                                      <div class="col-md-4 col-md-offset-4 col-xs-offset-4">
                                          <span class="btn btn-default btn-file">
                                              Subir archivo <input name="productImage" class="cropit-image-input" type="file">
                                          </span>
                                      </div>

                                 </div>

                              </div>
                          </div>
                    </div>

                    <div class="row my-1 text-center">
                      <div class="col-xs-12">
                        
                        <div class="form-group">
                          <label for="product_desc">Descripcion:</label>
                          <textarea style="width: 100%;" class="form-control" id="product_desc" name="product_description" placeholder="Coloque una descripcion" row my-1s="3" >{{$product->product_desc}}</textarea>
                        </div>
                        
                      </div>
                    </div>

                        <div class="row">

            

                  <div class="col-xs-12">
  
                            <div class="form-group" id="box_2">
                  <label for="communities">@lang('keywords.communities')</label><br>
                   <select class="form-control" name="coms[]" style="width: 100%;" id="community_2" multiple="multiple">
                    
                            @foreach($communities as $c)

                      <option value="{{$c->id}}" selected="selected">{{$c->name}}</option>

                    @endforeach

                    </select>
                  </div>
                    </div>

                    <div class="col-xs-12">
                            <div class="form-group">
          <label for="return_term">@lang('keywords.return_term')</label>
        
        <select class="form-control" name="return_term">
          <option value="1"  {{$product->return_term==1?"selected":''}} >@lang('keywords.1_week')</option>
          <option value="2" {{$product->return_term==2?"selected":''}}>@lang('keywords.2_week')</option>
          <option value="3" {{$product->return_term==3?"selected":''}}>@lang('keywords.3_week')</option>
          <option value="4" {{$product->return_term==4?"selected":''}}>@lang('keywords.4_week')</option>
        </select>
            </div>
                    </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                            Cerrar
                        </button>
                        <button type="submit" class=" btn btn-primary" id="sendEditProductForm">
                            Salvar
                        </button>
                    </div>

                </form>
            </div>
        
        </div>
    </div>


