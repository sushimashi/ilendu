@extends('admin.layouts.layout')

@section('content')
    <div class="block full">
        
        <div class="block-title">
            <h2>Productos</h2>
        </div>
    
        @include('admin.products.productsTabs')

        <div class="row">

              


            <div class="col-md-6 text-left">
              <div class="text-center form-group">
               </div>
            </div>
            <div class="col-md-6 text-right">
                <a data-toggle="modal" href="#createProductModal" class="btn btn-info btn-effect-ripple"><i
                            class="fa fa-plus"></i> Nuevo</a>

         <a  class="btn btn-danger btn-effect-ripple delete_multiple" id="delete_multiple" >Eliminar</a> 
            </div>
            <div class="col-md-12">
                <div class="table-responsive">
                    <div class="dataTables_wrapper form-inline no-footer">
                        <div class="_tabla">
                        <form name="listing_form" id="listing_form" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <table class="table responsive table-vcenter dataTable no-footer" id="productsTable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center"><input type="checkbox" name="selected_pages[]" id="checkall"></th>
                                        <th class="text-center">Nombre</th>
                                        <th class="text-center">Foto</th>
                                        <th class="text-center">Estado</th>
                                        <th class="text-center">Opciones</th>

                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table> 

                            </form>                       
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="createProductModal" tabindex="-1" role="dialog" aria-labelledby="createProductModal" aria-hidden="true">
           @include('admin.products.create')
        </div>

                                    {{-- Ver Producto --}}
        <div class="modal fade" id="editProductModal" tabindex="-1" role="dialog" aria-labelledby="editProductModal" aria-hidden="true">
        </div>

    </div>
@endsection

@push('js')
 <script type="text/javascript">
        $(function(){

 // select2
        $('#countryOfNewUser').select2();
        $('#stateOfNewUser').select2();
        // $('#cityOfNewUser').select2({ tags: true });
        $('#cityOfNewUser').select2();


        // agregar estados segun pais seleccionado
        $("#countryOfNewUser").on("change",function(){

          $('#stateOfNewUser').empty();
            if ( $(this).val() === '' ){
                $("#stateOfNewUser").append(`<option value=""> - @lang('keywords.none')  - </option>`);
                $("#cityOfNewUser").empty().append(`<option value=""> - @lang('keywords.none')  - </option>`);
                return;
            } 



           //  /country/{country_id}/products
           $.get(`/api/country/${ $('#countryOfNewUser').val() }/products`, function(result){


                if( result>0)
                {

             $("#count_products").html(result)


                } else {
                $("#count_products").html(0)

                }

            });

          $.get(`/api/country/${ $('#countryOfNewUser').val() }/states`, function(result){


                if( result.length > 0 )
                {
                    $("#stateOfNewUser").append(`<option value=""> - @lang('citiesView.selectState')  - </option>`);

                    result.forEach(element => {

                        var data = {
                            id: element.id,
                            text: element.name
                        };

                        var newOption = new Option(data.text, data.id, false, false);
                        $("#stateOfNewUser").append(newOption);
                    });


                } else {

                    $("#stateOfNewUser").append(`<option value=""> - @lang('citiesView.selectState')  - </option>`)
                                        .trigger('change'); 

                }

            });
$('#stateOfNewUser').val($('#stateOfNewUser option:eq(1)').val()).trigger('change');

        });

        // agregar ciudades segun estado seleccionado
        $("#stateOfNewUser").on("change",function(){

            $('#cityOfNewUser').empty();

            if ($(this).val() === ''){
                $("#cityOfNewUser").append(`<option value=""> - @lang('keywords.none')  - </option>`);
                return;
            }

            $.get(`/api/state/${ $("#stateOfNewUser").val() }/cities`, function(result){

                if( result.length > 0 )
                {
                    $("#cityOfNewUser").append(`<option value=""> - @lang('citiesView.selectCity')  - </option>`);
                    result.forEach(element => {
                       
                        var data = {
                            id: element.id,
                            text: element.name
                        };

                        var newOption = new Option(data.text, data.id, false, false);
                        $("#cityOfNewUser").append(newOption);
                    });

                } else {

                    $("#cityOfNewUser").append(`<option value=""> - @lang('keywords.none')  - </option>`);

                }

            });

$('#cityOfNewUser').val($('#cityOfNewUser option:eq(1)').val()).trigger('change');


        });

           $("#cityOfNewUser").on("change",function(){


            $("#city_id").val($(this).val());

            $("#productsTable").DataTable().ajax.url('/api/productsDataTable?city_id='+$('#city_id').val()).load();




        });

        })
       


    </script>
    @include ('plugins.datatable')
    @include('admin.products.js.index')
    @include('admin.products.js.edit')

@endpush