  <!-- Main content -->
  <section class="content container-fluid">
      <ul class="nav nav-tabs" role="tablist">  

          <li role="presentation" id="products" class="tab"><a href="{{ url('/admin/products') }}">Tablero</a></li>

          <li role="presentation" id="premiumProducts" class="tab"><a href=" {{ route('premiumProducts') }} ">
          	Productos Premium
          </a></li>

          <li role="presentation" id="activeProducts" class="tab"><a href="{{ route('displayedProducts') }}">
            Productos En Exhibicion
          </a></li>

          <li role="presentation" id="inactiveProducts" class="tab"><a href=" {{ route('undisplayedProducts') }}">
            Productos Sin Exhibir
          </a></li>
        
         <li role="presentation" id="borrowedProducts" class="tab"><a href="{{ route('borrowedProducts') }}">
            Productos prestados
          </a></li>

      </ul>
  </section>
 <div class="col-md-2 text-left">
              <div class="text-center form-group">
                   <div class="form-group">
                               {!! Form::label('countryOfNewUser', 'Pais:') !!}
                              <div>
                                  
                                 <select class="form-control" style="width: 70%" name="country_value" id="countryOfNewUser" >
                                  <option value="" >-- Seleccione --</option>
                                    @foreach($countries as $c)
                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                    @endforeach
                              </select>

                              </div>
                             

                            </div>
               </div>
            </div>

            <div class="col-md-2 text-left">
              <div class="text-center form-group">
                   <div class="form-group">
                               {!! Form::label('stateOfNewUser', 'Estado:') !!}
                              <div>
                                 <select class="form-control" style="width: 70%" name="state_value" id="stateOfNewUser" >
                                  <option value="" >-- Seleccione --</option>
                                
                              </select>  

                              </div>
                             

                            </div>
               </div>
            </div>

            <div class="col-md-2 text-left">
              <div class="text-center form-group">
                   <div class="form-group">
                               {!! Form::label('cityOfNewUser', 'Ciudad:') !!}
                              

                              <div>
                                  
                                 <select class="form-control" style="width: 70%" name="city_value" id="cityOfNewUser" >
                                  <option value="" >-- Seleccione --</option>
                                
                              </select>
    
                              </div>
                           
                            </div>
               </div>
            </div>

            <div class="col-md-1 text-left">
              <div class="text-center form-group">
                   <div class="form-group">
                               <label>Cantidad de productos</label>
                              

                              <div>
                                  
                                <span id="count_products">{{$count_products}}</span>
    
                              </div>
                           
                            </div>
               </div>
            </div>

              <div class="col-md-2 text-left">
              <div class="text-center form-group">
                   <div class="form-group">
                               <label>Cantidad total de productos</label>
                              

                              <div>
                                  
                                <span id="count_products">{{$count_total_products}}</span>
    
                              </div>
                           
                            </div>
               </div>
            </div>
          <input type="hidden" name="city_id" id="city_id" value="">
