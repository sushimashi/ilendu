@extends('admin.layouts.layout')

@section('content')
    <div class="block full">
        <div class="block-title">
            <h2>Listado de Usuarios</h2>
        </div>
        @permission('usuarios_crear')
        <div class="row">
            <div class="col-md-12 text-right">
                <a href="{{ url('admin/config/usuarios/create')}}" class="btn btn-info btn-effect-ripple"><i
                            class="fa fa-plus"></i> Nuevo</a>
            </div>
        </div>
        @endpermission
        <div class="row">
            <br/><br/>
            <div class="table-responsive">
                <div class="container-fluid">
                    <table id="datatable_user"
                           class="table responsive table_btn table-vcenter dataTable no-footer no-wrap"
                           role="grid" aria-describedby="example-datatable_info" width="100%">
                        <thead>
                        <tr>
                            <th>Nombres</th>
                            <th>Teléfono</th>
                            <th>E-mail</th>
                            <th>Rol</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@include ('plugins.datatable')
@include('admin.config.usuarios.js.table')
@endpush