<script>
    $(function () {
        var datatable_user = $("#datatable_user");

        cargar_datos();

        function cargar_datos() {
            datatable_user.DataTable({
                'ajax': {
                    "url": '{{url("/admin/config/usuarios")}}',
                    "type": "GET",
                    dataSrc: ''
                },
                'columns': [
                {
                        render: function (data, type, row) {
                            var logo = "<img src=\"" + row.url_foto_perfil + "\" style=\"width:50px;border-radius:25px;\"alt=\"\"> <span>" + row.nombre_completo + "</span>";
                            return (logo);
                        }
                    },
                    {data: 'telefono'},
                    {data: 'email'},
                    {data: 'rol'},
                    {
                        render: function (data, type, row) {
                            var btn_editar = "";
                            var ruta = "{{url('/admin/config/usuarios/')}}/" + row.id + "/edit"
                            @permission('usuarios_editar')
                                btn_editar = '<a href="' + ruta + '" class="btn btn-sm btn-success edit_user" title="Editar" data-toggle="tooltip"><i class="fa fa-pencil"></i></a>';
                                    @endpermission

                            var btn_eliminar = "";
                            @permission('usuarios_eliminar')
                            if (row.estado == false) {
                                ruta = '{{url('/admin/config/usuarios/')}}/' + row.id;
                                btn_eliminar = '<form method="POST" class="frm-desactivar habilitar_user" id="desactivar' + row.id + '" name="desactivar' + row.id + '" action="' + ruta + '" data-esp="desactivar">';
                                btn_eliminar += '{{ csrf_field() }}';
                                btn_eliminar += '<input type="hidden" name="_method" value="PUT">';
                                btn_eliminar += '<input type="hidden" name="habilitar" value="true">';
                                btn_eliminar += '<button class="btn btn-sm btn-warning" type="submit" title="Desactivar" data-toggle="tooltip"><i class="fa fa-check"></i></button>';
                                btn_eliminar += '</form>';
                            } else {
                                btn_eliminar = '<a href="' + row.id + '" class="btn btn-sm btn-danger delete_user" title="Eliminar" data-toggle="tooltip"><i class="fa fa-trash"></i></a>';
                            }
                            @endpermission

                                return (btn_editar + ' ' + btn_eliminar);
                        }
                    }
                ]
            });
        }


        $('body').on('click', 'tbody .delete_user', function (e) {
            idUser = $(this).attr('href');
            token = $("input[name=_token]").val();
            e.preventDefault();
            swal({
                title: '',
                text: '¿Seguro desea eliminar el registro?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    url: "{{url('/admin/config/usuarios/')}}/" + idUser,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    datatype: 'json',
                    success: function (respuesta) {
                        if (respuesta.success) {
                            datatable_user.DataTable().ajax.reload();
                            toastr.success(respuesta.mensaje);
                            if (respuesta.cerrar_sesion == true) {
                                window.location.href = "{{url('/login')}}";
                            }
                        } else {
                            toastr.error(respuesta.error);
                        }
                    },
                    error: function (e) {
                        console.log(e);
                        $.each(e.responseJSON.errors, function (index, element) {
                            if ($.isArray(element)) {
                                toastr.error(element[0]);
                            }
                        });
                    }
                });
            }).catch(swal.noop);
        });


        $('body').on('click', 'tbody .habilitar_user', function (e) {
            ruta = $(this).attr('action');
            var data = $(this).serialize();
            e.preventDefault();
            swal({
                title: '',
                text: '¿Seguro desea habilitar el usuario?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    url: ruta,
                    type: 'POST',
                    datatype: 'json',
                    data: data,
                    success: function (respuesta) {
                        if (respuesta.success) {
                            datatable_user.DataTable().ajax.reload();
                            toastr.success(respuesta.mensaje);
                        } else {
                            toastr.error(respuesta.error);
                        }
                    },
                    error: function (e) {
                        console.log(e);
                        $.each(e.responseJSON.errors, function (index, element) {
                            if ($.isArray(element)) {
                                toastr.error(element[0]);
                            }
                        });
                    }
                });
            }).catch(swal.noop);
        });

    });
</script>