<script>
    $(function () {
      
      var formulario_nuevo_user = $("#formulario_nuevo_user");
        var image_editor_user = $('#cargar-imagen');
        var hidden_image_data_user = image_editor_user.find(".hidden-image-data");
        var rotate_cw_user = image_editor_user.find(".rotate-cw");
        var rotate_ccw_user = image_editor_user.find(".rotate-ccw");

          image_editor_user.cropit();
   rotate_cw_user.click(function () {
            image_editor_user.cropit('rotateCW');
        });
        rotate_ccw_user.click(function () {
            image_editor_user.cropit('rotateCCW');
        });
      formulario_nuevo_user.on('submit', function (e) {
            e.preventDefault();
                var form = $('#formulario_nuevo_user')[0];
                var imageData = image_editor_user.cropit('export',{
                                    type: 'image/jpeg',
                                    quality: .9,
                                    originalSize:true
                                    });
          hidden_image_data_user.val(imageData);

                var formData = new FormData(form);
                $.ajax({
                    url: formulario_nuevo_user.attr('action'),
                    type: formulario_nuevo_user.attr('method'),
                    enctype: 'multipart/form-data',
                    data: formData,
                    processData: false,
                    contentType: false,
                success: function (respuesta) {
                    if (respuesta.success) {
                        window.location.href = "{{url('admin/config/usuarios')}}";
                    }
                    else {
                        toastr.error(respuesta.error);
                    }
                },
                error: function (e) {
                    console.log(e);
                    $.each(e.responseJSON.errors, function (index, element) {
                        if ($.isArray(element)) {
                            toastr.error(element[0]);
                        }
                    });
                }
            });
        });
    });
</script>