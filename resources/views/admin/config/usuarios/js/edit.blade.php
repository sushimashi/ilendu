<script>
    $(function () {

        var formulario_edit_user = $("#formulario_edit_user");
        var image_editor_user = $('.image-editor-user');
        var hidden_image_data_user = image_editor_user.find(".hidden-image-data");
        var rotate_cw_user = image_editor_user.find(".rotate-cw");
        var rotate_ccw_user = image_editor_user.find(".rotate-ccw");

        var options = {
            onText: "Si",
            offText: "No",
        };

        $("input[name='cambiar_imagen']").bootstrapSwitch(options);


        image_editor_user.cropit({
            exportZoom: 1.25,
            imageBackground: true,
            imageBackgroundBorderWidth: 20,
            imageState: {
                src: "{{url($usuario->foto_perfil)}}",
            },
        });
        rotate_cw_user.click(function () {
            image_editor_user.cropit('rotateCW');
        });
        rotate_ccw_user.click(function () {
            image_editor_user.cropit('rotateCCW');
        });

        formulario_edit_user.on('submit', function (e) {
            e.preventDefault();
            var form = $('#formulario_edit_user')[0];
            var imageData = image_editor_user.cropit('export', {
                type: 'image/jpeg',
                quality: .9,
                originalSize: true
            });
            hidden_image_data_user.val(imageData);

            var formData = new FormData(form);
            $.ajax({
                url: formulario_edit_user.attr('action'),
                type: formulario_edit_user.attr('method'),
                enctype: 'multipart/form-data',
                data: formData,
                processData: false,
                contentType: false,
                success: function (respuesta) {
                    if (respuesta.success) {
                        window.location.href = "{{url('admin/config/usuarios')}}";
                    }
                    else {
                        toastr.error(respuesta.error);
                    }
                },
                error: function (e) {
                    console.log(e);
                    $.each(e.responseJSON.errors, function (index, element) {
                        if ($.isArray(element)) {
                            toastr.error(element[0]);
                        }
                    });
                }
            });
        });

        $("[name='cambiar_imagen']").on('switchChange.bootstrapSwitch', function (event, state) {
            if (state == true) {
                $("#cargar-imagen").removeClass('hide');
                $("#cambiar_imagen").val(1);
                $("#foto_actual_j").addClass('hide');
            } else {
                $("#cargar-imagen").addClass('hide');
                $("#cambiar_imagen").val(0);
                $("#foto_actual_j").removeClass('hide');
            }
        });
    });
</script>