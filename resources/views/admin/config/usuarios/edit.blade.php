@extends('admin.layouts.layout')

@section('content')
    <div class="block full">
        <div class="block-title">
            <h2>Editar Usuario</h2>
        </div>
        <form enctype="multipart/form-data" action="{{url("admin/config/usuarios/$usuario->id")}}" method="POST"
              class="form-horizontal form-bordered" autocomplete="off" id="formulario_edit_user">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_inicio" value="1">
            <input type="hidden" name="habilitar" value="0">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="nombres">Nombres:</label>
                        <div class="col-md-9">
                            <input id="nombres" name="nombres" class="form-control" type="text"
                                   value="{{ $usuario->nombres }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="apellidos">Apellidos:</label>
                        <div class="col-md-9">
                            <input id="apellidos" name="apellidos" class="form-control" type="text"
                                   value="{{ $usuario->apellidos }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="email">E-mail:</label>
                        <div class="col-md-9">
                            <input id="email" name="email" class="form-control" type="email"
                                   value="{{ $usuario->email }}">
                            <input name="anterior_email" class="form-control" type="hidden"
                                   value="{{ $usuario->email }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="telefono">Teléfono:</label>
                        <div class="col-md-9">
                            <input id="telefono" name="telefono" class="form-control" type="text"
                                   value="{{ $usuario->telefono }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="rol">Rol:</label>
                        <div class="col-md-9">
                            <select id="rol" name="rol" class="form-control">
                                <option>-Seleccione</option>
                                @foreach($roles as $rol)
                                    <option value="{{ $rol->id }}"
                                            @if($usuario->getRolId() ==$rol->id) selected @endif>{{$rol->display_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="password">Contraseña:</label>
                        <div class="col-md-9">
                            <input id="password" name="password" class="form-control" type="password">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="repClave">Repetir Contraseña:</label>
                        <div class="col-md-9">
                            <input id="repClave" name="repClave" class="form-control" type="password">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="switch-imagen" class="col-md-12">
                    <center>
                        <img src="" alt="" id="foto_actual_j" style="max-width:150px;border-radius:25px;">
                        <br>
                        <br>
                        <label for="">Cambiar Foto</label><br>
                        <br>
                        <input type="checkbox" name="cambiar_imagen" id="cambiar_imagen"
                               class="switch switch_edit_select form-control" value="1">
                        <br><br>
                    </center>
                </div>
                <div id="cargar-imagen" class="hide col-md-12">
                    <div class="row image-editor-user">
                        <div class="col-md-12">
                            <center>
                            <div class="cropit-preview"></div>
                            <br><br>
                            <div class="rotate">
                                <span class="fa fa-repeat rotate-cw icon-rotate-right"></span>
                                <span class="fa fa-repeat rotate-ccw icon-rotate-left"></span>
                            </div>
                            <span class="fa fa-file-picture-o pic-small"></span>
                            <input type="range" class="cropit-image-zoom-input">
                            <span class="fa fa-file-picture-o pic-big"></span>
                            <input type="hidden" name="foto_perfil" class="hidden-image-data"/><br><br>
                            </center>
                        </div>

                        <div class="col-md-4 col-md-offset-5 col-xs-offset-4">
<span class="btn btn-default btn-file" style="margin-left: 9%;">
    Subir archivo <input class="cropit-image-input" type="file">
</span>


                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-effect-ripple" id="cambioUser">Actualizar
                        </button>
                        <a href="{{ url('admin/config/usuarios') }}" class="btn btn-default btn-effect-ripple">Atras</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('js')
@include('plugins.cropit')
@include ('plugins.bootstrap-switch')
@include('admin.config.usuarios.js.edit')

@endpush