@extends('admin.layouts.layout')

@section('content')
<div class="block full">
    <div class="block-title">
        <h2>Generales</h2>
    </div>

    <form action="{{url('admin/config')}}" method="POST" class="form-horizontal form-bordered" autocomplete="off"
          id="formulario_configuraciones" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="email_notificaciones">E-mail
                            Notificaciones:</label>
                        <div class="col-md-9">
                            <input id="email_notificaciones" name="email_notificaciones" class="form-control"
                                   type="text" value='{{ $email->value}}'>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="duracion_sesion">Duración de la Sesión</label>
                        <div class="col-md-9">
                            <input id="duracion_sesion" name="duracion_sesion" class="form-control" type="number"
                                   value='{{ $duracionSesion->value}}'>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="email_notificaciones">E-mail pie url :</label>
                        <div class="col-md-9">
                           <input type="file" name="imagen"  accept="image/*">
                        </div>  
                    </div>
                </div>
            </div>

           
            <div class="row">
                <div class="col-md-11 text-right">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-effect-ripple" id="cambioCliente">
                            Actualizar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@push('js')
@include ('plugins.numeric')
<script>
    $('input#duracion_sesion').numeric();
</script>
@endpush

