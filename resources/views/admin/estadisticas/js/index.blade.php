    <script type="text/javascript">

    $(function(){

$("#filter_date").on("click",function(){




$.ajax({
                url: '/admin/estadisticas-date',
                type: 'get',
                data: {date:$("#datepicker_year").val(),country_id:$(".country_id ").val(),city_id:$(".city_id").val()},
                success: function (respuesta) {
                 var data = respuesta.data;

                 console.log(data)
                 $("#products_by_country").html(respuesta.products_by_country)
                 $(".users_by_country").html(respuesta.users_by_country)

                        Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Registered users'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Users: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: data.length ? data : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});


/*Type of users*/

  var type = respuesta.type;

    Highcharts.chart('container_type', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'User type'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: type
    }]
});

        /*Container type bar*/
   var type_bar = respuesta.type_b;
    Highcharts.chart('container_type_bar', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Type of users'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Users: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: type_bar.length ? type_bar : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});






                },
                error: function (e) {
                    console.log(e);
                    $.each(e.responseJSON.errors, function (index, element) {
                        if ($.isArray(element)) {
                            toastr.error(element[0]);
                        }
                    });
                }
            });


})

$('#datepicker_year').datepicker({
    format: "mm-yyyy",
    viewMode: "months", 
    minViewMode: "months"
});



// The on tab shown event

$("#filter_button").on("click",function(){


var a=$('.nav-tabs .active').text()



    $.ajax({
            url: '/admin/estadisticas',
            type: 'GET',
            data: {country_id:$(".country_id").val(),city_id:$('.city_id').val()},
            success: function (result) {
if(a=="Users")
{
                var data = result.data;
                 console.log(data)

   $("#products_by_country").html(result.products_by_country)
                 $(".users_by_country").html(result.users_by_country)
     Highcharts.chart('container', {
    chart: {
        type: 'column',
        reflow: false
   
    },
    title: {
        text: 'Registered users'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Users: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: data.length ? data : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});

/*Type of users*/

  var type = result.type

    Highcharts.chart('container_type', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        reflow: false

    },
    title: {
        text: 'User type'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: type
    }]
});

        /*Container type bar*/
   var type_bar = result.type_b
    Highcharts.chart('container_type_bar', {
    chart: {
        type: 'column',
        reflow: false

    },
    title: {
        text: 'Type of users'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Users: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: type_bar.length ? type_bar : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});

}

     /*Countries*/
    var countries=result.country_arr

      Highcharts.chart('container_countries', {
    chart: {
        type: 'column',
        reflow: false

    },
    title: {
        text: 'Countries'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Countries: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: countries.length ? countries : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});


   /*Communities*/
    var communities=result.communities

      Highcharts.chart('container_communities', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Communities'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Comunities: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: communities.length ? communities : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});


 /*Products*/
    var products=result.products

      Highcharts.chart('container_products', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Products'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Products: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: products.length ? products : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});

      /*Borrowed Products*/
    var prestados=result.prestados;

      Highcharts.chart('container_products_borrowed', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Borrowed'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Products: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: prestados.length ? prestados : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});

            },
            error: function (e) {
                console.log(e);
                $.each(e.responseJSON.errors, function (index, element) {
                    if ($.isArray(element)) {
                        toastr.error(element[0]);
                    }
                });
            }
        });
})

  



    var data = {!! json_encode($data) !!};


     Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Registered users'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Users: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: data.length ? data : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});


/*Type of users*/

  var type = {!! json_encode($type) !!};

    Highcharts.chart('container_type', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'User type'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: type
    }]
});

        /*Container type bar*/
   var type_bar = {!! json_encode($type_b) !!};
    Highcharts.chart('container_type_bar', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Type of users'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Users: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: type_bar.length ? type_bar : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});


      /*Countries*/
    var all_countries={!! json_encode($all_countries) !!};


      Highcharts.chart('container_all_countries', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Users By Countries'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Countries: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: all_countries.length ? all_countries : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});


     /*Countries*/
    var countries={!! json_encode($country_arr) !!};

    console.log(countries);

      Highcharts.chart('container_countries', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Countries'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Countries: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: countries.length ? countries : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});


   /*Communities*/
    var communities={!! json_encode($communities) !!};

      Highcharts.chart('container_communities', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Communities'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Comunities: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: communities.length ? communities : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});


 /*Products*/
    var products={!! json_encode($products) !!};

      Highcharts.chart('container_products', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Products'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Products: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: products.length ? products : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});

      /*Borrowed Products*/
    var prestados={!! json_encode($prestados) !!};

      Highcharts.chart('container_products_borrowed', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Borrowed'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        type: 'category',
        labels: {
            rotation: -45,
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        enabled: false
    },
    tooltip: {
        pointFormat: 'Products: <b>{point.y:.1f} </b>'
    },
    series: [{
        name: 'Population',
        data: prestados.length ? prestados : [0],
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.0f}', // one decimal
            y: 10, // 10 pixels down from the top
            style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif'
            }
        }
    }]
});

    })
    </script>