@extends('admin.layouts.layout')
@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/dashboard.css') }}">
    <style type="text/css">
        .highcharts-container {
    width: 100%;
}
.highcharts-container svg {
    width: 100%;
    display: flex;
}
        .highcharts-figure, .highcharts-data-table table {
    min-width: 320px; 
    max-width: 800px;
    margin: 1em auto;
}

.highcharts-data-table table {
    font-family: Verdana, sans-serif;
    border-collapse: collapse;
    border: 1px solid #EBEBEB;
    margin: 10px auto;
    text-align: center;
    width: 100%;
    max-width: 500px;
}
.highcharts-data-table caption {
    padding: 1em 0;
    font-size: 1.2em;
    color: #555;
}
.highcharts-data-table th {
    font-weight: 600;
    padding: 0.5em;
}
.highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
    padding: 0.5em;
}
.highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
    background: #f8f8f8;
}
.highcharts-data-table tr:hover {
    background: #f1f7ff;
}

.resize{
 width:100%;
 margin: 0 auto;
}

input[type="number"] {
    min-width: 50px;
}
    </style>
@endpush

@section('content')
    <div class="block full">

        <div class="block-title">
            <h2>Estadisticas</h2>
        </div>

        {{-- envia a metodo create --}}
      

        <div class="row">


    <ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#users">Users</a></li>
  <li><a data-toggle="tab" href="#countries">Users by countries</a></li>
  <li><a data-toggle="tab" href="#communities">Communities</a></li>
  <li><a data-toggle="tab" href="#products">Products</a></li>

</ul>
 <div class="col-md-12">
        <div class="col-md-8">
                 <div class="form-group" style="display: inline-block; width: 40%;">
                      <label>Country:</label>
                     <select class="form-control country_id"  name="country_id">
                         <option value="43" selected> Chile</option>
                     </select>
                  </div>

                    <div class="form-group" style="display: inline-block; width: 40%;">
                      <label>City:</label>
                     <select class="form-control city_id"  name="city_id">
                       <option value="0" selected>None</option>
                     </select>
                  </div>

                     <div class="form-group" id="filter_button" style="display: inline-block; width: 10%;">
                    <button class="btn btn-primary">Filtrar</button>
                  </div>
        </div>
    </div>
<div class="tab-content">
  <div id="users" class="tab-pane fade in active">

        <div class="col-md-4">
        <h4 class="text-left">Total de usuarios por pais: <span class="users_by_country">{{$users_by_country}}</span></h4>
        </div>

        <div class="col-md-4">
        <h4 class="text-left">Usuarios totales de ilendu: {{$all}}</h4>
      </div>
    <div class="col-md-12">

 
        <div class="col-md-8">
             

                    <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label" for="año">Año:</label>
                            <input type="text" id="datepicker_year" name="fecha"
                                   class="form-control datepicker_year"  value='{{old("fecha")}}'>
                    </div>
                </div>
                    <div class="col-md-3">

                     <div class="form-group" id="filter_button" >
                        <br>
                    <button class="btn btn-primary" id="filter_date" style="margin-top: 3%;">Filtrar</button>
                  </div>
                  </div>

        </div>
    </div>
   <div class="col-md-6">
                <br/><br/>
                 <div id="container"  class="resize"></div>

            </div>


   <div class="col-md-3">
                <br/><br/>
                 <div id="container_type" class="resize"></div>

            </div>

  <div class="col-md-3">
                <br/><br/>
                 <div id="container_type_bar" class="resize"></div>

            </div>

               <div class="col-md-12">
                <br/><br/>
                 <div id="container_all_countries"  class="resize"></div>

            </div>

  </div>
  <div id="countries" class="tab-pane fade">
      <div class="col-md-4">
        <h4 class="text-left">Total de usuarios por pais: <span class="users_by_country">{{$users_by_country}}</span></h4>
      </div>

   <div class="col-md-12">
                <br/><br/>
                 <div id="container_countries" class="resize"></div>

            </div>

  </div>
  <div id="communities" class="tab-pane fade">



     <div class="col-md-4">
        <h4 class="text-left">Total de comunidades: {{$all_com}}</h4>
      </div>
   <div class="col-md-12">
                <br/><br/>
                 <div  id="container_communities" class="resize"></div>

            </div>


  </div>

<div id="products" class="tab-pane fade">
  <div class="col-md-4">
        <h4 class="text-left">Total de productos por pais: <span id="products_by_country" >{{$products_by_country}}</span></h4>
      </div>

   <div class="col-md-4">
        <h4 class="text-left">Total de productos de ilendu: {{$all_products}}</h4>
      </div>

   <div class="col-md-6">
                <br/><br/>
                 <div id="container_products" class="resize"></div>

            </div>

       <div class="col-md-6">
                <br/><br/>
                 <div id="container_products_borrowed" class="resize"></div>

            </div>


  </div>


</div>

          
        </div>
    </div>

                            {{-- crear categoria --}}
    <div class="modal fade" id="createCategoryModal" tabindex="-1" role="dialog" aria-labelledby="createCategoryModal" aria-hidden="true">
       @include('admin.categories.create')
    </div>

                                {{-- Ver Categoria --}}
    <div class="modal fade" id="editCategoryModal" tabindex="-1" role="dialog" aria-labelledby="editCategoryModal" aria-hidden="true">
    </div>

@endsection

@push('js')
    @include ('plugins.datatable')

    <script type="text/javascript" src="{{ asset('plugins/highcharts/code/highcharts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/highcharts/code/highcharts-more.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/highcharts/code/modules/exporting.js') }}"></script>


    @include('admin.estadisticas.js.index')

    <script type="text/javascript">
        
        $(function(){
              $(".country_id").select2({
  ajax: {
    url: '/countries-ajax',
    dataType: 'json'
    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
  }
});

                 $(".city_id").select2({
  ajax: {
    url: '/cities-ajax/'+$(".country_id").val(),
    dataType: 'json'
    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
  }
}); 

$(".country_id").on("change",function(){

      $(".city_id").select2({
  ajax: {
    url: '/cities-ajax/'+$(".country_id").val(),
    dataType: 'json'
    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
  }
}); 
})

 
        })
    </script>

        <script type="text/javascript">
         window.onresize = function() {
    //- Remove empty charts if you are using multiple charts in single page 
    //- there may be empty charts
    Highcharts.charts = Highcharts.charts.filter(function(chart){
        return chart !== undefined;
    });

    Highcharts.charts.forEach(function(chart) {
        var height = chart.renderTo.chartHeight;
        //- If you want to preserve the actual height and only want to update 
        //- the width comment the above line.
        //- var height = chart.chartHeight; 
        var width = chart.renderTo.clientWidth;
        //- The third args is an animation set to true. 
        chart.setSize(width, height, true);
    });
};
    </script>
@endpush