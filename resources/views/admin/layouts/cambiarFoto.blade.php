<div class="modal fade bs-example-modal-lg" id="cambiar_foto" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form role="form" enctype="multipart/form-data" action="{{url('/admin/config/cambiarFoto/'.Auth::user()->id)}}" autocomplete="off" method="POST"
                   id="form_cambiar_foto">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title text-center">Cambiar Foto de Perfil</h4>
                </div>

                <div class="modal-body">

                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_inicio" value="0">
                    <input type="hidden" name="habilitar" value="0">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row image-editor-perfil">
                                    <div class="col-md-12">
                                        <center>
                                            <div class="cropit-preview" >
                                            </div>
                                            <br><br>
                                            <div class="rotate">
                                                <span  class="fa fa-repeat rotate-cw icon-rotate-right"></span>
                                                <span class="fa fa-repeat rotate-ccw icon-rotate-left"></span>
                                            </div>
                                            <span class="fa fa-file-picture-o pic-small"></span>
                                            <input type="range" class="cropit-image-zoom-input">
                                            <span class="fa fa-file-picture-o pic-big"></span>
                                            {{-- <input type="hidden" name="foto_perfil" class="hidden-image-data"> --}}
                                            <br><br>
                                        </center>
                                    </div>

                                    <div class="col-md-4 col-md-offset-5 col-xs-offset-4">
                                        <span class="btn btn-default btn-file" style="margin-left: 9%;">
                                            Subir archivo 
                                            <input class="cropit-image-input" name="foto_perfil" type="file">
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary">Actualizar</button>
                </div>

            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->