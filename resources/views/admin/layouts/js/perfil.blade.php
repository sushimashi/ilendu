<script>
	$(function () {

		var frm_perfil_user = $(".frm_perfil_user");
        var form_cambiar_foto = $("#form_cambiar_foto");

        $(".image-editor-perfil").cropit({
          exportZoom: 1.25,
          imageBackground: true,
          imageBackgroundBorderWidth: 20,
          imageState: {
            src: "/img/profile/{{Auth::user()->profile_img}}",
          }
        });

        $('.rotate-cw').click(function() {
            $(".image-editor-perfil").cropit('rotateCW');
        });
        $('.rotate-ccw').click(function() {
            $(".image-editor-perfil").cropit('rotateCCW');
        });

		frm_perfil_user.on('submit', function (e) {
            e.preventDefault();
            $.ajax({
                url: frm_perfil_user.attr('action'),
                type: frm_perfil_user.attr('method'),
                data: frm_perfil_user.serialize(),
                datatype: 'json',
                success: function (respuesta) {
                    if (respuesta.success) {
                        toastr.success(respuesta.mensaje);
                        App.sidebar('close-sidebar-alt');
                       $(".text-nombre").html($("#profile-nombre").val() + ' '+ $("#profile-apellido").val());
                    }
                    else {
                        toastr.error(respuesta.error);
                    }
                },
                error: function (e) {
                    console.log(e);
                    $.each(e.responseJSON.errors, function (index, element) {
                        if ($.isArray(element)) {
                            toastr.error(element[0]);
                        }
                    });
                }
            });
        });

        function readURL(input, preview) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $(preview)
                    .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

        form_cambiar_foto.on('submit', function (e) {
            e.preventDefault();
            
                var form = $('#form_cambiar_foto')[0];
                var imageData =$(".image-editor-perfil").cropit('export',{
                                    type: 'image/jpeg',
                                    quality: .9,
                                    originalSize:true
                                    });
                // $('.image-editor-perfil .hidden-image-data').val(imageData);

                var formData = new FormData(form);
                $.ajax({
                    url: form_cambiar_foto.attr('action'),
                    type: form_cambiar_foto.attr('method'),
                    enctype: 'multipart/form-data',
                    data: formData,
                    processData: false,
                    contentType: false,
                success: function (respuesta) {
                    if (respuesta.success) {
                        toastr.success(respuesta.mensaje);
                        //App.sidebar('close-sidebar-alt');
                        $(".avatar-foto").attr('src', respuesta.foto_url);
                        $("#img_perfil").attr('src', respuesta.foto_url);
                        $("#cambiar_foto").modal('hide');
                    }
                    else {
                        toastr.error(respuesta.error);
                    }
                },
                error: function (e) {
                    console.log(e);
                    $.each(e.responseJSON.errors, function (index, element) {
                        if ($.isArray(element)) {
                            toastr.error(element[0]);
                        }
                    });
                }
            });
        });

       //  $("#cambiar_foto").on('show.bs.modal', function () {
       //      $(".image-editor-perfil").cropit('destroy');
       //      $(".image-editor-perfil .cropit-preview").find("img").attr("src", null);
       //      $(".image-editor-perfil").cropit();
       // });

	});
</script>