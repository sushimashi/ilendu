<!-- Wrapper for scrolling functionality -->
<div id="sidebar-scroll">
    <!-- Sidebar Content -->
    <div class="sidebar-content">
        <!-- Sidebar Navigation -->
        <ul class="sidebar-nav">
            <li>
                <a href="{{url("/admin")}}" class="{{$menuService->isActive($url,"/admin",true)}}"><i class="gi gi-compass sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Inicio</span></a>
            </li>
            {{--<li class="sidebar-separator">
                <i class="fa fa-ellipsis-h"></i>
            </li>--}}
            {{-- @permission('clients_see') --}}


            <li>
                <a href="{{url('/admin/users')}}" class="{{$menuService->isActive($url,"/admin/users")}}"><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Usuarios</span></a>
            </li>

              <li>
                <a href="{{url('/admin/borrows')}}" class="{{$menuService->isActive($url,"/admin/borrows")}}"><i class="fa fa-file sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Solicitudes</span></a>
             </li>

             <li>
                <a href="{{url('/admin/estadisticas')}}" class="{{$menuService->isActive($url,"/admin/estadisticas")}}"><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Estadisticas</span></a>
             </li>



                 <li>
                <a href="{{url('/admin/feedback')}}" class="{{$menuService->isActive($url,"/admin/feedback")}}"><i class="fa fa-commenting sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">FeedBack</span></a>
            </li>
            {{-- @endpermission --}}

            {{-- @permission('communities_see') --}}
            <!-- Comunidades -->
            <li>
                <a href="{{url('/admin/communities')}}" class="{{$menuService->isActive($url,"/admin/communities")}}"><i class="fa fa-thumbs-o-up sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Comunidades</span></a>
            </li>
            {{-- @endpermission --}}

            <!-- Categorias -->
              <li>
                <a href="{{url('/admin/categories')}}" class="{{$menuService->isActive($url,"/admin/categories")}}"><i class="fa fa-sticky-note-o sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Categorias</span></a>
            </li>

            {{-- Productos--}}
              <li>
                <a href="{{url('/admin/products')}}" class="{{$menuService->isActive($url,"/admin/products")}}"><i class="fa fa-dropbox sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Productos</span></a>
            </li>

            {{-- Ciudades--}}
              <li>
                <a href="{{url('/admin/cities')}}" class="{{$menuService->isActive($url,"/admin/cities")}}"><i class="fa fa-building sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Ciudades</span></a>
            </li>

            {{-- Membresias--}}
              <li>
                <a href="{{url('/admin/fees')}}" class="{{$menuService->isActive($url,"/admin/fees")}}"><i class="fa fa-star sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Membresias</span></a>
            </li>
            <!--
            <li class="{{$menuService->isActive($url,"/admin/usuarios")." ".$menuService->isActive($url,"/admin/roles")." ".$menuService->isActive($url,"/admin/permisos")." ".$menuService->isActive($url,"/admin/retenedores")}}">
                <a href="#" class="sidebar-nav-menu"><i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-cog sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Configuración</span></a>
                <ul>
                     <li>
                        <a href="{{ url('admin/config')}}" class="{{$menuService->isActive($url,"/admin/config",true)}}">Generales</a>
                    </li>
                    {{-- @permission('usuarios_ver') --}}
                    <li>
                        <a href="{{ url('admin/config/usuarios')}}" class="{{$menuService->isActive($url,"/admin/config/usuarios")}}">Usuarios</a>
                    </li>
                    {{-- @endpermission --}}
                    {{-- @permission('privilegios_ver') --}}
                    <li class="{{$menuService->isActive($url,"/admin/config/privilegios")}}">
                        <a href="#" class="sidebar-nav-submenu"><i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><span class="sidebar-nav-mini-hide">Privilegios</span></a>
                        <ul>
                            <li>
                                <a href="{{url('/admin/config/privilegios/roles')}}" class="{{$menuService->isActive($url,"/admin/config/privilegios/roles")}}">Roles</a>
                            </li>
                            <li>
                                <a href="{{url('/admin/config/privilegios/permisos')}}" class="{{$menuService->isActive($url,"/admin/config/privilegios/permisos")}}">Permisos</a>
                            </li>
                        </ul>
                    </li>
                    {{-- @endpermission  --}}
             
                </ul>
            </li>
            -->
        </ul>
        <!-- END Sidebar Navigation -->
    </div>
    <!-- END Sidebar Content -->
</div>
<!-- END Wrapper for scrolling functionality -->