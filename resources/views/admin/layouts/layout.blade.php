<!DOCTYPE html>
<!--[if IE 9]>
<html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" lang="es"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>{!! config('app.name', 'Laravel')  !!}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Icons -->
    <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('img/icon57.png') }}" sizes="57x57">
    <link rel="apple-touch-icon" href="{{ asset('img/icon72.png') }}" sizes="72x72">
    <link rel="apple-touch-icon" href="{{ asset('img/icon76.png') }}" sizes="76x76">
    <link rel="apple-touch-icon" href="{{ asset('img/icon114.png') }}" sizes="114x114">
    <link rel="apple-touch-icon" href="{{ asset('img/icon120.png') }}" sizes="120x120">
    <link rel="apple-touch-icon" href="{{ asset('img/icon144.png') }}" sizes="144x144">
    <link rel="apple-touch-icon" href="{{ asset('img/icon152.png') }}" sizes="152x152">
    <link rel="apple-touch-icon" href="{{ asset('img/icon180.png') }}" sizes="180x180">
    <link rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}">
    <!-- END Icons -->

    <!-- Stylesheets -->
    <!-- Bootstrap is included in its original form, unaltered -->
    <link rel="stylesheet" href="{{ asset('css/appui/bootstrap.min.css') }}">

    <!-- Related styles of various icon packs and plugins -->
    <link rel="stylesheet" href="{{ asset('css/appui/plugins.css') }}">

    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    <link rel="stylesheet" href="{{ asset('css/appui/main.css') }}">

    <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

    <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
    <link rel="stylesheet" href="{{ asset('css/appui/themes.css') }}">
    <link rel="stylesheet" href="{{ asset('css/appui/themes/social.css') }}">

    <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">

    <link rel="stylesheet" href="{{asset('/css/layout.css')}}">
    <!-- END Stylesheets -->

    <!-- Modernizr (browser feature detection library) -->
    <script src="{{ asset('js/appui/vendor/modernizr-3.3.1.min.js') }}"></script>
    <link rel="stylesheet" href="{{asset('css/toastr.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}">


    <link rel="stylesheet" href="{{asset('css/layout.css')}}">
    <link rel="stylesheet" href="{{asset('css/cropit.css')}}">
    
    {{-- sweetAlert2 --}}
    <link rel="stylesheet" href="{{asset('plugins/sweetalert2/dist/sweetalert2.min.css')}}">

    {{-- custom styles --}}
    <link rel="stylesheet" href="{{asset('css/styles2.css')}}">

    {{-- DataTables --}}
    <link rel="stylesheet" href="{{asset('plugins/datatables/Buttons-1.5.1/css/buttons.bootstrap.min.css')}}">

    @stack("css")

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<!-- Page Wrapper -->
<!-- In the PHP version you can set the following options from inc/config file -->
<!--
    Available classes:

    'page-loading'      enables page preloader
-->
<div id="page-wrapper" class="page-loading">
    <!-- Preloader -->
    <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
    <!-- Used only if page preloader enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
    <div class="preloader">
        <div class="inner">
            <!-- Animation spinner for all modern browsers -->
            <div class="preloader-spinner themed-background hidden-lt-ie10"></div>

            <!-- Text for IE9 -->
            <h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
        </div>
    </div>
    <!-- END Preloader -->

    <!-- Page Container -->
    <!-- In the PHP version you can set the following options from inc/config file -->
    <!--
        Available #page-container classes:

        'sidebar-light'                                 for a light main sidebar (You can add it along with any other class)

        'sidebar-visible-lg-mini'                       main sidebar condensed - Mini Navigation (> 991px)
        'sidebar-visible-lg-full'                       main sidebar full - Full Navigation (> 991px)

        'sidebar-alt-visible-lg'                        alternative sidebar visible by default (> 991px) (You can add it along with any other class)

        'header-fixed-top'                              has to be added only if the class 'navbar-fixed-top' was added on header.navbar
        'header-fixed-bottom'                           has to be added only if the class 'navbar-fixed-bottom' was added on header.navbar

        'fixed-width'                                   for a fixed width layout (can only be used with a static header/main sidebar layout)

        'enable-cookies'                                enables cookies for remembering active color theme when changed from the sidebar links (You can add it along with any other class)
    -->
    <div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
        <!-- Alternative Sidebar -->
        <div id="sidebar-alt" tabindex="-1" aria-hidden="true">
            <!-- Toggle Alternative Sidebar Button (visible only in static layout) -->
            <a href="javascript:void(0)" id="sidebar-alt-close" onclick="App.sidebar('toggle-sidebar-alt');"><i
                        class="fa fa-times"></i></a>

            <!-- Wrapper for scrolling functionality -->
            <div id="sidebar-scroll-alt">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Profile -->
                    <div class="sidebar-section">

                        <h2 class="text-light">Perfil</h2>

                        <form action="{{ route('editUser', ['user_id' => Auth::user()->id ]) }}" method="post"
                              class="form-control-borderless frm_perfil_user" autocomplete="off">

                            {{ csrf_field() }}

                            <div class="form-group">
                                <div class="col-xs-12">

                                    <img src="/img/profile/{{Auth::user()->profile_img}}" id="img_perfil" alt="" class="img-thumbnail" data-toggle="modal" data-target="#cambiar_foto">

                                    <label class="label_img_perfil" data-toggle="modal" data-target="#cambiar_foto">
                                        Cambiar imagen
                                    </label>
                                    
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="profile-nombre">Nombres:</label>
                                <input type="text" id="profile-nombre" name="first_name" class="form-control"
                                       value="{{ Auth::user()->first_name }}">
                            </div>

                            <div class="form-group">
                                <label for="profile-apellido">Apellidos:</label>
                                <input type="text" id="profile-apellido" name="last_name" class="form-control"
                                       value="{{ Auth::user()->last_name }}">
                            </div>

                            <div class="form-group">
                                <label for="profile-email">Email:</label>
                                <input type="email" id="profile-email" name="email" class="form-control"
                                       value="{{ Auth::user()->email }}">
                                       <input id="anterior_email" name="anterior_email" class="form-control" type="hidden" value="{{ Auth::user()->email }}">
                            </div>

                           {{--  <div class="form-group">
                                <label for="profile-telefono">Teléfono:</label>
                                <input type="text" id="profile-telefono" name="telefono" class="form-control"
                                       value="{{ Auth::user()->telefono }}">
                            </div> --}}

                            <div class="form-group">
                                <label for="profile-password">Nueva Contraseña:</label>
                                <input type="password" id="profile-password" name="password" class="form-control">
                            </div>

                           {{--  <div class="form-group">
                                <label for="profile-password-confirm">Confirmar Contraseña:</label>
                                <input type="password" id="profile-password-confirm" name="repClave"
                                       class="form-control">
                            </div> --}}

                            <div class="form-group remove-margin">
                                <div class="col-xs-6">
                                    <button type="submit" class="btn btn-effect-ripple btn-primary">
                                        Guardar
                                    </button>
                                </div>
                                <div class="col-xs-6">
                                    <button type="button" class="btn btn-effect-ripple btn-default" onclick="App.sidebar('close-sidebar-alt');this.blur();">
                                        Cerrar
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- END Profile -->
                </div>
                <!-- END Sidebar Content -->
            </div>
            <!-- END Wrapper for scrolling functionality -->
        </div>
        <!-- END Alternative Sidebar -->

        <!-- Main Sidebar -->
        <div id="sidebar">
            <!-- Sidebar Brand -->
            <div id="sidebar-brand" class="themed-background">
                <a href="{{url("/account")}}" class="sidebar-title">
                    <i class="fa fa-cube"></i> 
                    <span class="sidebar-nav-mini-hide">{!! config('app.name', 'Laravel')  !!}</span>
                </a>
            </div>
            <!-- END Sidebar Brand -->

            @include('admin.layouts.menu')

        </div>
        <!-- END Main Sidebar -->

        <!-- Main Container -->
        <div id="main-container">
            <!-- Header -->
            <!-- In the PHP version you can set the following options from inc/config file -->
            <!--
                Available header.navbar classes:

                'navbar-default'            for the default light header
                'navbar-inverse'            for an alternative dark header

                'navbar-fixed-top'          for a top fixed header (fixed main sidebar with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                    'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

                'navbar-fixed-bottom'       for a bottom fixed header (fixed main sidebar with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                    'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
            -->
            <header class="navbar navbar-inverse navbar-fixed-top">
                <!-- Left Header Navigation -->
                <ul class="nav navbar-nav-custom">
                    <!-- Main Sidebar Toggle Button -->
                    <li>
                        <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                            <i class="fa fa-ellipsis-v fa-fw animation-fadeInRight" id="sidebar-toggle-mini"></i>
                            <i class="fa fa-bars fa-fw animation-fadeInRight" id="sidebar-toggle-full"></i>
                        </a>
                    </li>
                    <!-- END Main Sidebar Toggle Button -->

                    <!-- Header Link -->
                    <li class="hidden-xs animation-fadeInQuick">
                        <a href=""><strong>{{$titulo}}</strong></a>
                    </li>
                    <!-- END Header Link -->
                </ul>
                <!-- END Left Header Navigation -->

                <!-- Right Header Navigation -->
                <ul class="nav navbar-nav-custom pull-right">


                    <!-- END Alternative Sidebar Toggle Button -->
                    <!-- User Dropdown -->
                    <li class="dropdown">
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                            
                            <img src="{{ isset(Auth::user()->profile_img) ? 
                                        '/img/profile/'.Auth::user()->profile_img :
                                        'images/user-img.png' 
                                     }}" alt="avatar" class="avatar-foto">

                            <strong class="text-nombre">
                                {{ Auth::user()->getFullName() }}
                            </strong>

                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <!-- Alternative Sidebar Toggle Button -->
                            <li>
                                <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt');this.blur();">
                                    Perfil <i class="fa fa-picture-o fa-fw pull-right"></i>
                                </a>
                            </li>
                            {{--<li>
                                <a href="javascript:void(0)">
                                    <i class="fa fa-pencil-square fa-fw pull-right"></i>
                                    Link #2
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="fa fa-picture-o fa-fw pull-right"></i>
                                    Link #3
                                </a>
                            </li>
                            <li class="divider">
                            <li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="gi gi-settings fa-fw pull-right"></i>
                                    Link #1
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="gi gi-lock fa-fw pull-right"></i>
                                    Link #2
                                </a>
                            </li>
                            --}}
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="gi gi-lock fa-fw pull-right"></i>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                    Cerrar sesion
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END User Dropdown -->
                </ul>
                <!-- END Right Header Navigation -->
            </header>
            <!-- END Header -->
            @include('admin.layouts.cambiarFoto')
            <!-- Page content -->
            <div id="page-content">
                @include('flash::message')
                @if (count($errors) > 0)
                    @include('partials.errors')
                @endif
                @yield('content')
            </div>
            <!-- END Page Content -->
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->

</div>
<!-- END Page Wrapper -->

<!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
<script src="{{ asset('js/appui/vendor/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('js/appui/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/appui/plugins.js') }}"></script>
<script src="{{ asset('js/appui/app.js') }}"></script>
<script src="{{asset('js/toastr.min.js')}}"></script>
<script src="{{asset('js/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/jquery.cropit.js')}}"></script>

@include('admin.layouts.js.perfil')

@stack("js")

</body>
</html>
