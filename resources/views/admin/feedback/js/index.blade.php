<script>
    $(function () {

        var _table = $("#_table");
        
        var obj_datatable = {
            'ajax': {
                "url": '{{ url("/admin/feedback") }}',
                "type": "GET",
                dataSrc: '',
            },
            "responsive": true,
            "autoWidth": false,
            'columns': [
                {data: 'name',className: "text-center"},
                {data: 'message',className: "text-center"},
                {

                     render: function (data, type, row) {
           

                    return  '<a href="' + row.url + '" class="text-center" title="' + row.url + '">' + row.url + '</a>';
                    },className: "text-center"
                },
                {

                     render: function (data, type, row) {
                        var btn_eliminar="";
                        var btn_ver="";
                         // habilitar comunidad
                        btn_ver = '<a href="' + row.id + '" class="btn btn-sm btn-success _see " title="Ver"><i class="fa fa-eye"></i></a>';

                        
                        //ruta destroy
                        btn_eliminar = '<a href="' + row.id + '" class="btn btn-sm btn-danger _delete " title="Eliminar"><i class="fa fa-times"></i></a>';

                            return  btn_ver+' '+btn_eliminar;
                    }
                }
            ]
        };
       
        _table.DataTable(obj_datatable);

        
         $('body').on('click', '._see', function (e) {
            e.preventDefault();
            
            id = $(this).attr('href');

          
                $.ajax({
                    url: "{{url('/admin/feedback/')}}/" + id,
                    type: 'GET',
                    datatype: 'json',
                    success: function (result) {
                            $("#xModal").empty(result);
                            $("#xModal").append(result);
                            $("#xModal").modal('show');

                    },
                    error: function (e) {
                        console.log(e);
                        $.each(e.responseJSON.errors, function (index, element) {
                            if ($.isArray(element)) {
                                toastr.error(element[0]);
                            }
                        });
                    }
                });
        });

            
         $('body').on('click', 'tbody ._delete', function (e) {
            e.preventDefault();
            
            category_id = $(this).attr('href');
            token = $("input[name=_token]").val();

            swal({
                title: '',
                text: '¿Eliminar Feed?',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    url: "{{url('/admin/feedback/')}}/" + category_id,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    datatype: 'json',
                    success: function (result) {
                        if (result.success) {
                            _table.DataTable().ajax.reload();
                            toastr.success(result.message);
                        } else {
                            toastr.error(result.error);
                        }
                    },
                    error: function (e) {
                        console.log(e);
                        $.each(e.responseJSON.errors, function (index, element) {
                            if ($.isArray(element)) {
                                toastr.error(element[0]);
                            }
                        });
                    }
                });
            }).catch(swal.noop);
        });



    });
</script>
