<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="text-center modal-title">
                {{$feedback->name}}
            </h4>
        </div>

            <div class="modal-body">
                <p class="text-center">
                  {{$feedback->message}}
                </p>
            </div>
        
        </div>
    </div>
</div>


