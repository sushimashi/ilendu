@extends('admin.layouts.layout')

@section('content')
    <div class="block full">

        <div class="block-title">
            <h2>Feedback</h2>
        </div>

        {{-- envia a metodo create --}}
   
        <div class="row">
            <div class="col-md-12">
                <br/><br/>
                <div class="table-responsive">
                    <div class="dataTables_wrapper form-inline no-footer">
                        <div class="_tabla">
                            <table class="table responsive table-vcenter dataTable no-footer" id="_table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">Nombre</th>
                                        <th class="text-center">Mensaje</th>
                                        <th class="text-center">url</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

                            {{-- crear categoria --}}
<div class="modal fade"
         id="xModal"
         tabindex="-1"
         role="dialog"
         aria-labelledby="xModal"
         aria-hidden="true"
         onClick="">
        <!--<div class="modal fade pantalla" id="modal"  onClick="modalClose()">-->
    </div>


@endsection

@push('js')
    @include ('plugins.datatable')
    @include('admin.feedback.js.index')
@endpush