<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="text-center modal-title">
                Nueva Categoria
            </h4>
        </div>

            <div class="modal-body">
                <form id="createCategoryForm" action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data" autocomplete="off">

                    {{ csrf_field() }}

                    <div class=" row text-center">

                         <div class=" my-1 col-6">
                            <div class="form-group">
                            {!! Form::label('name', 'Nombre (Español):') !!}
                                <input type="text" class="form-control" name="name_spanish"  style="width: 70%; margin: 0 auto;" placeholder="Categoria">
                            </div> 

                            <div class="form-group">
                            {!! Form::label('name', 'Nombre ingles:') !!}
                                <input type="text" class="form-control" name="name"  style="width: 70%; margin: 0 auto;" placeholder="Categoria">
                            </div>

                                <div class="form-group">
                            {!! Form::label('price', 'Precio de Envio') !!}
                                <input type="text" class="form-control" style="width: 70%; margin: 0 auto;" name="price"  value="" placeholder="Precio">
                            </div>

                        </div>

                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                            Cerrar
                        </button>
                        <button type="submit" class="btn btn-primary" id="sendCreateCategoryForm">
                            Crear
                        </button>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
@push('js')
    @include ('admin.categories.js.create')
@endpush

