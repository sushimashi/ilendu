<div class="modal-dialog" role="document">
    <div class="modal-content">

        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <h4 class="text-center modal-title">
                Editar Categoria
            </h4>

        </div>

            <div class="modal-body">
                <form id="updateCategoryForm" action="{{ route('updateCategory', ['id' => $category->id]) }}" method="POST" enctype="multipart/form-data" autocomplete="off">

                    {{ csrf_field() }}

                    <div class=" row text-center">

                        <div class=" my-1 col-6">
                            
                            <div class="form-group">
                            {!! Form::label('name', 'Nombre (Español):') !!}
                                <input type="text" class="form-control" style="width: 70%; margin: 0 auto;" name="name_spanish"  value="{{ $category->name_spanish }}">
                            </div>

                            <div class="form-group">
                            {!! Form::label('name', 'Nombre (Ingles):') !!}
                                <input type="text" class="form-control" style="width: 70%; margin: 0 auto;" name="name"  value="{{ $category->name }}">
                            </div>

                               <div class="form-group">
                            {!! Form::label('price', 'Precio De Envio') !!}
                                <input type="text" class="form-control" style="width: 70%; margin: 0 auto;" name="price"  value="{{ $category->price }}">
                            </div>

                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                            Cerrar
                        </button>
                        <button type="submit" class="btn btn-info" id="sendUpdateCategoryForm">
                            Salvar
                        </button>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
