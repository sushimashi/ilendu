@extends('admin.layouts.layout')

@section('content')
    <div class="block full">

        <div class="block-title">
            <h2>Categorias</h2>
        </div>

        {{-- envia a metodo create --}}
       <div class="row">
            <div class="col-md-12 text-right">
                <a data-target='#createCategoryModal' data-toggle='modal' href="#" class="btn btn-info btn-effect-ripple"><i class="fa fa-plus"></i>
                 Nuevo
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <br/><br/>
                <div class="table-responsive">
                    <div class="dataTables_wrapper form-inline no-footer">
                        <div class="_tabla">
                            <table class="table responsive table-vcenter dataTable no-footer" id="_table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">Nombre</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

                            {{-- crear categoria --}}
    <div class="modal fade" id="createCategoryModal" tabindex="-1" role="dialog" aria-labelledby="createCategoryModal" aria-hidden="true">
       @include('admin.categories.create')
    </div>

                                {{-- Ver Categoria --}}
    <div class="modal fade" id="editCategoryModal" tabindex="-1" role="dialog" aria-labelledby="editCategoryModal" aria-hidden="true">
    </div>

@endsection

@push('js')
    @include ('plugins.datepicker')
    @include ('plugins.datatable')
    @include('admin.categories.js.index')
    @include ('admin.categories.js.edit')


@endpush