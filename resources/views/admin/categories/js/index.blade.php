<script>
    $(function () {

        var _table = $("#_table");
        
        var obj_datatable = {
            'ajax': {
                "url": '{{ url("/admin/categories") }}',
                "type": "GET",
                dataSrc: '',
            },
            "responsive": true,
            "autoWidth": false,
            'columns': [
                {data: 'name_spanish',className: "text-center"},
                {
                     render: function (data, type, row) {
                        var btn_editar = "";
                        var btn_eliminar="";

                        //ruta edit
                        btn_editar += '<a href="'+row.id+'" title="Editar" class="btn btn-sm btn-success editCategory"><i class="fa fa-pencil"></i></a>';

                        //Habilitar / Deshabilitar Categoria
                        btn_habilitar = `<a href="${row.id}" title="${row.is_active == 1 ? 'Deshabilitar' : 'Habilitar'}" class=" btn btn-sm enableCategory">
                            <i class="${row.is_active == 1 ?  'fa fa-arrow-down' : 'fa fa-arrow-up'}"></i>
                        </a>`;
                        
                        //ruta destroy
                        btn_eliminar = '<a href="' + row.id + '" class="btn btn-sm btn-danger _delete " title="Eliminar"><i class="fa fa-times"></i></a>';

                            return  btn_editar+" "+btn_habilitar+" "+btn_eliminar;
                    }
                }
            ]
        };
       
        _table.DataTable(obj_datatable);
            
         $('body').on('click', 'tbody ._delete', function (e) {
            e.preventDefault();
            
            category_id = $(this).attr('href');
            token = $("input[name=_token]").val();

            swal({
                title: '',
                text: '¿Eliminar Categoria?',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    url: "{{url('/admin/categories/')}}/" + category_id,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    datatype: 'json',
                    success: function (result) {
                        if (result.success) {
                            _table.DataTable().ajax.reload();
                            toastr.success(result.message);
                        } else {
                            toastr.error(result.error);
                        }
                    },
                    error: function (e) {
                        console.log(e);
                        $.each(e.responseJSON.errors, function (index, element) {
                            if ($.isArray(element)) {
                                toastr.error(element[0]);
                            }
                        });
                    }
                });
            }).catch(swal.noop);
        });

         //Habilitar / Deshabilitar Categoria
         $('body').on('click', 'tbody .enableCategory', function (e) {
            e.preventDefault();
            
            category_id = $(this).attr('href');

            $.ajax({
                url: "{{url('/api/enableCategory/')}}/" + category_id,
                datatype: 'json',
                success: function (result) {
                    _table.DataTable().ajax.reload();
                    toastr.success(result.message);
                },
                error: function (e) {
                    console.log(e);
                    $.each(e.responseJSON.errors, function (index, element) {
                        if ($.isArray(element)) {
                            toastr.error(element[0]);
                        }
                    });
                }
            }).catch(swal.noop);

         });

    });
</script>
