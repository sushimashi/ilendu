<script type="text/javascript">

    $("#sendCreateCategoryForm").click( function (e) {
        e.preventDefault();       

        if($(this).hasClass('disabled'))
            return;
        
        $(this).addClass('disabled');


        let formData = new FormData($('#createCategoryForm')[0]);

        $.ajax({
            url: $("#createCategoryForm").attr('action'),
            type: $("#createCategoryForm").attr('method'),
            enctype: 'multipart/form-data',
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {
                if (result.success) {
                    swal("Exito!", "Categoria registrada exitosamente!", "success")
                        .then((value) => {
                            $("#createCategoryModal").modal('hide');
                            $("#_table").DataTable().ajax.reload(null, false);
                            $("#createCategoryForm").trigger('reset');
                        })
                        .catch( function(){
                            $("#createCategoryModal").modal('hide');
                            $("#_table").DataTable().ajax.reload(null, false);
                            $("#createCategoryForm").trigger('reset');
                        });
                } else {
                    toastr.error(result.error);
                }
            },
            error: function (e) {
                console.log(e);
                $.each(e.responseJSON.errors, function (index, element) {
                    if ($.isArray(element)) {
                        toastr.error(element[0]);
                    }
                });
            },
            complete: function(){
                $('#sendCreateCategoryForm').removeClass('disabled');
            }
        });
    });

</script>