<script type="text/javascript">

    // cargar modal 
    $(document).on('click','.editCategory', function(e){
        e.preventDefault();
        category_id = $(this).attr('href');
        $.get(`/admin/categories/${category_id}/edit`, function(result){
            $('#editCategoryModal').empty().append(result).modal('show');
        });
    });

    function clearForm(){
        $("#editCategoryModal").modal('hide');
        $("#_table").DataTable().ajax.reload(null, false);
        $("#updateCategoryForm").trigger('reset');
    }

    // actualizar categoria
    $(document).on('click', "#sendUpdateCategoryForm",  function (e) {
        e.preventDefault();    

        if($(this).hasClass('disabled'))
            return;   

        $(this).addClass('disabled');

        let formData = new FormData($('#updateCategoryForm')[0]);

        $.ajax({
            url: $("#updateCategoryForm").attr('action'),
            type: $("#updateCategoryForm").attr('method'),
            enctype: 'multipart/form-data',
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {
                if (result.success) {
                    swal("Exito!", "Categoria actualizada!", "success")
                        .then((value) => {
                            clearForm();
                        })
                        .catch( function(){
                            clearForm();
                        });
                } else {
                    toastr.error(result.error);
                }
            },
            error: function (e) {
                console.log(e);
                $.each(e.responseJSON.errors, function (index, element) {
                    if ($.isArray(element)) {
                        toastr.error(element[0]);
                    }
                });
            },
            complete: function(){
                $('#sendUpdateCategoryForm').removeClass('disabled');
            }
        });
    });

</script>