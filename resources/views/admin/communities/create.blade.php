<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="text-center modal-title">
                Nueva Comunidad
            </h4>
        </div>

            <div class="modal-body">
                <form enctype="multipart/form-data" id="createCommunityForm" action="{{ url('/admin/communities') }}" method="POST" autocomplete="off">

                    {{ csrf_field() }}

                    <div class="row text-center">
                      <div class="col-xs-push-2 col-xs-4">
                        <div class="form-group">
                          <label for="name">Nombre:</label>
                          <input type="text" class=" form-control" name="name" id="name" >
                        </div>
                      </div>

                      <div class="col-xs-push-2 col-xs-4">
                          <div class="form-group">
                              <label for="status">
                                Activa
                                <input checked class="form-control" style="width: 100%;" type="checkbox" name="status">
                              </label> 
                          </div>
                      </div>                    
                    </div>

                    <div class="row text-center">
                          {{-- Foto --}}
                          <div class="col-xs-12">
                              <div class="form-group">

                                  {!! Form::label('communityImage', 'Foto:') !!}
                                 <div class="row image-editor-createCommunity">

                                      <div class="col-md-12">

                                        <center>
                                          <div class="cropit-preview">
                                              
                                          </div>
                                          <br><br>

                                          <div class="rotate">
                                            <span  class="fa fa-repeat rotate-cw icon-rotate-right"></span>
                                            <span class="fa fa-repeat rotate-ccw icon-rotate-left"></span>
                                          </div>

                                          <span class="fa fa-file-picture-o pic-small"></span>

                                          <input type="range" class="cropit-image-zoom-input">

                                          <span class="fa fa-file-picture-o pic-big"></span>

                                          <br><br>

                                        </center>

                                      </div>

                                      <div class="col-md-4 col-md-offset-4 col-xs-offset-4">
                                          <span class="btn btn-default btn-file">
                                              Subir archivo <input name="communityImage" class="cropit-image-input" type="file">
                                          </span>
                                      </div>

                                 </div>

                              </div>
                          </div>
                    </div>

                    <div class="row my-1 text-center">
                      <div class="col-xs-12">
                        
                        <div class="form-group">
                          <label for="description">Descripcion:</label>
                          <textarea style="width: 100%;" class="form-control" id="description" name="description" row my-1s="3" ></textarea>
                        </div>
                        
                      </div>
                    </div>


                    <div class="row my-1 ">
                      <div class="col-xs-12">
                        
                        <div class="form-group">
                       
                          <label>Status:</label>
                          <select name="public" class="form-control" >
                              <option value="0">Private</option>
                              <option value="1">Public</option>
                          </select>

                        </div>
                        
                      </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                            Cerrar
                        </button>
                        <button type="submit" class=" btn btn-primary" id="sendCreateCommunityForm">
                            Crear
                        </button>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
@push('js')
  @include('admin.communities.js.create')
@endpush

