  <!-- Main content -->
  <section class="content container-fluid">
      <ul class="nav nav-tabs" role="tablist">  

          <li role="presentation" id="allCommunities" class="tab"><a href="{{ url('/admin/communities') }}">Tablero</a></li>

          <li role="presentation" id="inactiveCommunities" class="tab"><a href=" {{ route('inactiveCommunities') }}">
            Inactivas
          </a></li>

      </ul>
  </section>
