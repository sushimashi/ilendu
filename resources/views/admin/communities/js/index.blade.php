<script>
    $(function () {

        $('#allCommunities').addClass('active');

        var _table = $("#communitiesTable");
        var obj_datatable = {
            'ajax': {
                "url" : `/api/communitiesDataTable`,
                "type": "GET",
                dataSrc: '',
                'country_id':44
            },
            "responsive": true,
            "autoWidth": false,
            'columns': [
                {data: 'name',className: "text-center"},
                {data: 'cant_users',className: "text-center"},
                {data: 'cant_prods',className: "text-center"},
                
                {
                     render: function (data, type, row) {
                        
                        let btn_habilitar = "",
                            btn_editar = "",
                            btn_eliminar="",
                            enable, enableTitle, btn_usuarios;

                            if(row.status == 1){
                                enable = 'fa fa-arrow-down';
                                enableTitle = 'Deshabilitar';
                            } else{
                                enable = 'fa fa-arrow-up';
                                enableTitle = 'Habilitar';
                            }

                            //ruta edit
                            btn_editar += '<a  href="'+ row.id +'" title="Editar" class="btn btn-sm btn-success btn_editar"><i class="fa fa-pencil"></i></a>';

                            // ver usuarios de comunidad
                            btn_usuarios = `<a href="#" class=" btn btn-sm btn-secondary btn_usuarios" title="usuarios" id="community_${row.id}" data-community_id="${row.id}" data-community_name="${row.name}"><i class="fa fa-users "></i></a>`;

                             // ver productos de comunidad
                            btn_productos = `<a href="#" class=" btn btn-sm btn-secondary btn_productos" title="productos" id="community_${row.id}" data-community_id="${row.id}" data-community_name="${row.name}"><i class="fa fa-shopping-cart "></i></a>`;

                            // habilitar comunidad
                            btn_habilitar += `<a href="#" class=" btn btn-sm btn-secondary btn_habilitar" title="${enableTitle}" id="community_${row.id}" data-community_id="${row.id}"><i class="${enable} "></i></a>`;

                            //ruta destroy
                           btn_eliminar = '<a style="margin: 0 auto;" href="' + row.id + '" class="btn btn-sm btn-danger _delete" title="Eliminar" ><i class="fa fa-times"></i></a>';


                            return  btn_editar +" "+ 
                                    btn_habilitar +" "+
                                    btn_usuarios +" "+
                                    btn_productos +" "+
                                    btn_eliminar;
                    }
                }
            ]
        };

        _table.DataTable(obj_datatable);
            
        // Eliminar Comunidad
         $('body').on('click', 'tbody ._delete', function (e) {
            
            community_id = $(this).attr('href');
            token = $("input[name=_token]").val();
            e.preventDefault();
            swal({
                title: '',
                text: '¿Eliminar Comunidad?',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    url: "{{ url('/admin/communities/') }}/" + community_id,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    datatype: 'json',
                    success: function (result) {
                        _table.DataTable().ajax.reload(null, false);
                        toastr.success(result.message);
                    },
                    error: function (e) {
                        console.log(e);
                        $.each(e.responseJSON.errors, function (index, element) {
                            if ($.isArray(element)) {
                                toastr.error(element[0]);
                            }
                        });
                    }
                });
            }).catch(swal.noop);
        });

         // Habilitar Comunidad
         $(document).on('click', '.btn_habilitar', function(e){
             e.preventDefault();
             
             let community_id = $(this).attr('data-community_id');
             $.get(`/api/community/${community_id}/enableCommunity`, function(result){
                
                _table.DataTable().ajax.reload(null, false);
                 toastr.success(result.message);

             });
         });

    });
</script>
