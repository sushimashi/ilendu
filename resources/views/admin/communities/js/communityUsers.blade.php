<script>
    $(function () {

        let community_id;



        // Productos por Comunidad
        $(document).on('click', '.btn_productos', function(e){
            e.preventDefault();

            let community_name = $(this).attr('data-community_name');
            community_id = $(this).attr('data-community_id');
        
            $('.community_name').text(community_name);               

               // DataTable

               if ( $.fn.DataTable.isDataTable( '#communityProductsTable' ) ) {
                $.get(`/api/community/${community_id}/communityProducts`, 
                    function(result){
                        $('#communityProductsTable').DataTable().clear();
                        $('#communityProductsTable').DataTable().rows.add(result);
                        $('#communityProductsTable').DataTable().draw();  ;
                });
               }

               var _table = $("#communityProductsTable");
               var obj_datatable = {
                   'ajax': {
                       "url" : `/api/community/${community_id}/communityProducts`,
                       "type": "GET",
                       dataSrc: '',
                        "data": function ( d ) {
        d.estado = $('#estado_producto').val();
    }
                   },
                   "responsive": true,
                   "autoWidth": false,
                   'columns': [
                       {
                          render: function(data, type, row) {
                            return row.product_name;
                          }, 
                          className: "text-center"

                       },{
                            render: function (data, type, row) {
                               
                              

                                   // remover de comunidad
                                  btn_eliminar = 
                                    `<a style="margin: 0 auto;" href="${row.id}" class="btn btn-sm btn-danger removeProductOfCommunity" title="Remover" ><i class="fa fa-times"></i></a>`;

                                   return   btn_eliminar;
                           }
                       }
                   ]
               };

               _table.DataTable(obj_datatable);
               $('#communityProductsModal').modal('show'); 

               setTimeout(function(){
                  $($.fn.dataTable.tables(true)).DataTable()
                     .columns.adjust()
                     .responsive.recalc();                              
               }, 250);
        });

        // Usuarios por Comunidad
        $(document).on('click', '.btn_usuarios', function(e){
            e.preventDefault();

            let community_name = $(this).attr('data-community_name');
            community_id = $(this).attr('data-community_id');
        
            $('#community_name').text(community_name);               

               // DataTable

               if ( $.fn.DataTable.isDataTable( '#communityUsersTable' ) ) {
                $.get(`/api/community/${community_id}/communityUsers`, 
                    function(result){
                        $('#communityUsersTable').DataTable().clear();
                        $('#communityUsersTable').DataTable().rows.add(result);
                        $('#communityUsersTable').DataTable().draw();  ;
                });
               }

               var _table = $("#communityUsersTable");
               var obj_datatable = {
                   'ajax': {
                       "url" : `/api/community/${community_id}/communityUsers`,
                       "type": "GET",
                       dataSrc: '',
                   },
                   "responsive": true,
                   "autoWidth": false,
                   'columns': [
                       {
                          render: function(data, type, row) {
                            return row.first_name + " " + row.last_name;
                          }, 
                          className: "text-center"

                       },{
                            render: function (data, type, row) {
                               
                               let btn_habilitar = "",
                                   btn_eliminar="",
                                   approve, approveTitle, btn_usuarios;

                                   if(row.pivot.is_approve == 1){
                                       approve = 'fa fa-arrow-down';
                                       approveTitle = 'Desaprobar';
                                   } else{
                                       approve = 'fa fa-arrow-up';
                                       approveTitle = 'Aprobar';
                                   }

                                   // habilitar comunidad
                                   btn_habilitar += `<a href="${row.id}" class=" btn btn-sm btn-secondary approveUser" title="${approveTitle}" id="user_${row.id}" ><i class="${approve} "></i></a>`;

                                   // remover de comunidad
                                  btn_eliminar = 
                                    `<a style="margin: 0 auto;" href="${row.id}" class="btn btn-sm btn-danger removeOfCommunity" title="Remover" ><i class="fa fa-times"></i></a>`;

                                   return  btn_habilitar +" "+
                                           btn_eliminar;
                           }
                       }
                   ]
               };

               _table.DataTable(obj_datatable);
               $('#communityUsersModal').modal('show'); 

               setTimeout(function(){
                  $($.fn.dataTable.tables(true)).DataTable()
                     .columns.adjust()
                     .responsive.recalc();                              
               }, 250);
        });

        // remover productos de Comunidad
         $(document).on('click', '.removeProductOfCommunity', function (e) {
            e.preventDefault();
            
            product_id = $(this).attr('href');
            swal({
                title: '',
                text: '¿Remover este producto de la Comunidad?',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    url: `/api/community/${community_id}/product/${product_id}/removeProductOfCommunity`,
                    type: 'GET',
                    datatype: 'json',
                    success: function (result) {
                        $('#communityProductsTable').DataTable().ajax.reload(null, false);
                        toastr.success(result.message);
                    },
                    error: function (e) {
                        console.log(e);
                        $.each(e.responseJSON.errors, function (index, element) {
                            if ($.isArray(element)) {
                                toastr.error(element[0]);
                            }
                        });
                    }
                });
            }).catch(swal.noop);
        });

        // remover usuario de Comunidad
         $(document).on('click', '.removeOfCommunity', function (e) {
            e.preventDefault();
            
            user_id = $(this).attr('href');
            swal({
                title: '',
                text: '¿Remover este Usuario de la Comunidad?',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    url: `/api/community/${community_id}/user/${user_id}/removeOfCommunity`,
                    type: 'GET',
                    datatype: 'json',
                    success: function (result) {
                        $('#communityUsersTable').DataTable().ajax.reload(null, false);
                        toastr.success(result.message);
                    },
                    error: function (e) {
                        console.log(e);
                        $.each(e.responseJSON.errors, function (index, element) {
                            if ($.isArray(element)) {
                                toastr.error(element[0]);
                            }
                        });
                    }
                });
            }).catch(swal.noop);
        });

         // aprobar / desaprobar usuario en comunidad
         $(document).on('click', '.approveUser', function(e){
             e.preventDefault();
             
             let user_id = $(this).attr('href');
             $.get(`/api/community/${community_id}/user/${user_id}/approveUser`, function(result){
                
                $('#communityUsersTable').DataTable().ajax.reload(null, false);
                 toastr.success(result.message);

             });
         });

    });
</script>
