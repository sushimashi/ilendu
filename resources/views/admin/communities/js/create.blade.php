<script type="text/javascript">
    $(function () {

      $('#createCommunityModal').on('shown.bs.modal', function (e) {

          $(".image-editor-createCommunity").cropit({
              exportZoom: 1.25,
              imageBackground: true,
              imageBackgroundBorderWidth: 20,
              imageState: { src: "{{ asset('/images/no_photo_icon.png') }}"}
          });

      });

      $('.image-editor-createCommunity .rotate-cw').click(function() {
          $(".image-editor-createCommunity").cropit('rotateCW');
      });
      
      $('.image-editor-createCommunity .rotate-ccw').click(function() {
          $(".image-editor-createCommunity").cropit('rotateCCW');
      });

        function clearForm() {
            $("#createCommunityForm").trigger('reset');
            $(".image-editor-createCommunity").cropit('destroy');
            $('.cropit-preview').empty();
        }
           
        // Registrar Comunidad
        $(document).on('click', '#sendCreateCommunityForm', function(e){
            e.preventDefault();

            if($(this).hasClass('disabled'))
              return;

            $(this).addClass('disabled');

            let imageData = $(".image-editor-createCommunity").cropit('export',{
                                type: 'image/jpeg',
                                quality: .9,
                                originalSize:true
                                });

            let data = new FormData($('#createCommunityForm')[0]);

            console.log($('#createCommunityForm').serialize());
            $.ajax({
                processData: false,
                contentType: false,
                enctype: 'multipart/form-data',
                url: $('#createCommunityForm').attr('action'),
                type: $('#createCommunityForm').attr('method'),
                data: data,
                success: function (result) {

                    swal(result.state, result.message, result.type)
                            .then((value) => {
                               $('#communitiesTable').DataTable().ajax.reload(null, false);
                              $('#createCommunityModal').modal('hide');
                              clearForm();
                          })
                            .catch( function(){
                               $('#communitiesTable').DataTable().ajax.reload(null, false);
                              $('#createCommunityModal').modal('hide');
                              clearForm();
                          });

                },
                error: function (e) {
                    console.log(e);
                    $.each(e.responseJSON.errors, function (index, element) {
                        if ($.isArray(element)) {
                            toastr.error(element[0]);
                        }
                    });
                },
                complete: function(){
                  $('#sendCreateCommunityForm').removeClass('disabled');
                }
              });
        });

    });
</script>