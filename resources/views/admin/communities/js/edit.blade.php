<script type="text/javascript">
    $(function () {

        //
        $('#editCommunityModal').on('hidden.bs.modal', function () {
            $(this).empty();
        });

        $(document).on('click', '.btn_editar', function (e) {
            e.preventDefault();

            let community_id = $(this).attr('href');
            $.get(`/admin/communities/${community_id}/edit`, function (result) {

                $('#editCommunityModal').empty().append(result).modal('show');

            });

        });
        
        $(document).on('click', '#sendEditCommunityForm', function(e){
            e.preventDefault();

            if($(this).hasClass('disabled'))
              return;

            $(this).addClass('disabled');

            let imageData = $(".image-editor-community").cropit('export',{
                                type: 'image/jpeg',
                                quality: .9,
                                originalSize:true
                                });

            let data = new FormData($('#editCommunityForm')[0]);
            console.log($('#editCommunityForm').serialize());

               $.ajax({
                   headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                   processData: false,
                   contentType: false,
                   enctype: 'multipart/form-data',
                   url: $('#editCommunityForm').attr('action'),
                   type: 'POST',
                   data: data,
                   success: function (result){

                        swal(result.state, result.message, result.type)
                        .then((value) => {
                            $('#communitiesTable').DataTable().ajax.reload(null, false);
                            $('#editCommunityModal').modal('hide');
                        })
                        .catch( function(){
                            $('#communitiesTable').DataTable().ajax.reload(null, false);
                            $('#editCommunityModal').modal('hide');
                        });

                   },
                   error: function (e) {
                       console.log(e.responseJSON);
                       $.each(e.responseJSON.errors, function (index, element) {
                           if ($.isArray(element)) {
                               toastr.error(element[0]);
                           }
                       });
                   },
                   complete: function(){
                     $('#sendEditCommunityForm').removeClass('disabled');
                   }
               });
       });

    });
</script>