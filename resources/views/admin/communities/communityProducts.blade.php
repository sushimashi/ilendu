<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4  class="text-center modal-title community_name">
            </h4>
        </div>

        <div class="modal-body">
              <div class="row">
                <div class="col-md-4">
              <div class=" form-group">
                                  
                                 <select class="form-control"  name="estado_producto" id="estado_producto" >
                                  <option value="" >-- Seleccione --</option>
                                    <option value="all">Todos</option>
                                    <option value="prestados">prestados</option>
                                    <option value="disponibles">Disponibles</option>

                              </select>

                             

               </div>
            </div>
                  <div class="col-md-12">
                      <div class="table-responsive">
                          <div class="dataTables_wrapper form-inline no-footer">
                              <div class="_tabla">
                                  <table class="table responsive table-vcenter dataTable no-footer" id="communityProductsTable" cellspacing="0" width="100%">
                                      <thead>
                                          <tr>
                                              <th class="text-center">Nombre</th>
                                              <th class="text-center">Opciones</th>
                                          </tr>
                                      </thead>
                                      <tbody></tbody>
                                  </table>                        
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
        </div>

          <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                  Cerrar
              </button>
          </div>
    </div>       
</div>

@push('js')
<script type="text/javascript">
  $(function(){
$(document).on("change","#estado_producto",function(){
 $("#communityProductsTable").DataTable().ajax.reload();


})


  })

</script>
@endpush

