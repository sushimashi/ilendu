@extends('admin.layouts.layout')

@section('content')

    <div class="block full">
        
        <div class="block-title">
            <h2>Comunidades Inactivas</h2>
        </div>
    
        @include('admin.communities.communitiesTabs')

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <div class="dataTables_wrapper form-inline no-footer">
                        <div class="_tabla">
                            <table class="table responsive table-vcenter dataTable no-footer" id="inactiveCommunitiesTable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">Nombre</th>
                                        <th class="text-center">Usuarios</th>
                                        <th class="text-center">Productos</th>

                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>

                                {{-- usuarios de comunidad --}}
        <div class="modal fade" id="communityUsersModal" tabindex="-1" role="dialog" aria-labelledby="communityUsersModal" aria-hidden="true">
           @include('admin.communities.communityUsers')
        </div>

                                       {{-- productos de comunidad --}}
        <div class="modal fade" id="communityProductsModal" tabindex="-1" role="dialog" aria-labelledby="communityProductsModal" aria-hidden="true">
           @include('admin.communities.communityProducts')
        </div>
    </div>
@endsection

@push('js')

    @include ('plugins.datatable')
    @include('admin.communities.js.inactiveCommunities')
    @include('admin.communities.js.communityUsers')

@endpush