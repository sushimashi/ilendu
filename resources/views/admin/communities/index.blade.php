@extends('admin.layouts.layout')

@section('content')

    <div class="block full">
        
        <div class="block-title">
            <h2>Comunidades</h2>
        </div>
    
        @include('admin.communities.communitiesTabs')

        <div class="row">
            <div class="col-md-2 text-left">
              <div class="text-center form-group">
                   <div class="form-group">
                               {!! Form::label('countryOfNewUser', 'Pais:') !!}
                              <div>
                                  
                                 <select class="form-control" style="width: 70%" name="country_value" id="countryOfNewUser" >
                                  <option value="" >-- Seleccione --</option>
                                    @foreach($countries as $c)
                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                    @endforeach
                              </select>

                              </div>
                             

                            </div>
               </div>
            </div>

            <div class="col-md-2 text-left">
              <div class="text-center form-group">
                   <div class="form-group">
                               {!! Form::label('stateOfNewUser', 'Estado:') !!}
                              <div>
                                 <select class="form-control" style="width: 70%" name="state_value" id="stateOfNewUser" >
                                  <option value="" >-- Seleccione --</option>
                                
                              </select>  

                              </div>
                             

                            </div>
               </div>
            </div>

            <div class="col-md-2 text-left">
              <div class="text-center form-group">
                   <div class="form-group">
                               {!! Form::label('cityOfNewUser', 'Ciudad:') !!}
                              

                              <div>
                                  
                                 <select class="form-control" style="width: 70%" name="city_value" id="cityOfNewUser" >
                                  <option value="" >-- Seleccione --</option>
                                
                              </select>
    
                              </div>
                           
                            </div>
               </div>
            </div>
            <div class="col-md-6 text-right">
                <a data-toggle="modal" href="#createCommunityModal" class="btn btn-info btn-effect-ripple"><i
                            class="fa fa-plus"></i> Nuevo</a>
            </div>
            <div class="col-md-12">
                    <input type="hidden" name="city_id" id="city_id" value="">
                <div class="table-responsive">
                    <div class="dataTables_wrapper form-inline no-footer">
                        <div class="_tabla">
                            <table class="table responsive table-vcenter dataTable no-footer" id="communitiesTable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">Nombre</th>
                                        <th class="text-center">Usuarios</th>
                                        <th class="text-center">Productos</th>

                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="createCommunityModal" tabindex="-1" role="dialog" aria-labelledby="createCommunityModal" aria-hidden="true">
           @include('admin.communities.create')
        </div>

                                    {{-- Ver Comunidad --}}
        <div class="modal fade" id="editCommunityModal" tabindex="-1" role="dialog" aria-labelledby="editCommunityModal" aria-hidden="true">
        </div>

                                {{-- usuarios de comunidad --}}
        <div class="modal fade" id="communityUsersModal" tabindex="-1" role="dialog" aria-labelledby="communityUsersModal" aria-hidden="true">
           @include('admin.communities.communityUsers')
        </div>

                               {{-- productos de comunidad --}}
        <div class="modal fade" id="communityProductsModal" tabindex="-1" role="dialog" aria-labelledby="communityProductsModal" aria-hidden="true">
           @include('admin.communities.communityProducts')
        </div>

    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(function(){

 // select2
        $('#countryOfNewUser').select2();
        $('#stateOfNewUser').select2();
        // $('#cityOfNewUser').select2({ tags: true });
        $('#cityOfNewUser').select2();


        // agregar estados segun pais seleccionado
        $("#countryOfNewUser").on("change",function(){

          $('#stateOfNewUser').empty();
            if ( $(this).val() === '' ){
                $("#stateOfNewUser").append(`<option value=""> - @lang('keywords.none')  - </option>`);
                $("#cityOfNewUser").empty().append(`<option value=""> - @lang('keywords.none')  - </option>`);
                return;
            } 

          $.get(`/api/country/${ $('#countryOfNewUser').val() }/states`, function(result){

                if( result.length > 0 )
                {
                    $("#stateOfNewUser").append(`<option value=""> - @lang('citiesView.selectState')  - </option>`);

                    result.forEach(element => {

                        var data = {
                            id: element.id,
                            text: element.name
                        };

                        var newOption = new Option(data.text, data.id, false, false);
                        $("#stateOfNewUser").append(newOption);
                    });


                } else {

                    $("#stateOfNewUser").append(`<option value=""> - @lang('citiesView.selectState')  - </option>`)
                                        .trigger('change'); 

                }

            });
$('#stateOfNewUser').val($('#stateOfNewUser option:eq(1)').val()).trigger('change');

        });

        // agregar ciudades segun estado seleccionado
        $("#stateOfNewUser").on("change",function(){

            $('#cityOfNewUser').empty();

            if ($(this).val() === ''){
                $("#cityOfNewUser").append(`<option value=""> - @lang('keywords.none')  - </option>`);
                return;
            }

            $.get(`/api/state/${ $("#stateOfNewUser").val() }/cities`, function(result){

                if( result.length > 0 )
                {
                    $("#cityOfNewUser").append(`<option value=""> - @lang('citiesView.selectCity')  - </option>`);
                    result.forEach(element => {
                       
                        var data = {
                            id: element.id,
                            text: element.name
                        };

                        var newOption = new Option(data.text, data.id, false, false);
                        $("#cityOfNewUser").append(newOption);
                    });

                } else {

                    $("#cityOfNewUser").append(`<option value=""> - @lang('keywords.none')  - </option>`);

                }

            });

$('#cityOfNewUser').val($('#cityOfNewUser option:eq(1)').val()).trigger('change');


        });

           $("#cityOfNewUser").on("change",function(){


            $("#city_id").val($(this).val());

            $("#communitiesTable").DataTable().ajax.url('/api/communitiesDataTable?city_id='+$('#city_id').val()).load();




        });

        })
       


    </script>
    @include ('plugins.datatable')
    @include('admin.communities.js.index')
    @include('admin.communities.js.edit')
    @include('admin.communities.js.communityUsers')

@endpush