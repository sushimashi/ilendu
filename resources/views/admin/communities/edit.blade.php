<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="text-center modal-title">
                Editar Comunidad
            </h4>
        </div>

            <div class="modal-body">
                <form enctype="multipart/form-data" id="editCommunityForm" action="{{ route('updateCommunity', ['community_id' => $community->id]) }}" method="POST" autocomplete="off">

                    {{ csrf_field() }}

                    <div class="row text-center">
                      <div class="col-xs-push-2 col-xs-4">
                        <div class="form-group">
                          <label for="name">Nombre:</label>
                          <input type="text" class=" form-control" name="name" id="name" value="{{ $community->name }}">
                        </div>
                      </div>

                      <div class="col-xs-push-2 col-xs-4">
                          <div class="form-group">
                              <label for="status">
                                Activa
                                <input
                                  @if ($community->status == 1)
                                    checked
                                  @endif  
                                 class="form-control" style="width: 100%;" type="checkbox" name="status">
                              </label> 
                          </div>
                      </div>                    
                    </div>

                    <div class="row text-center">
                          {{-- Foto --}}
                          <div class="col-xs-12">
                              <div class="form-group">

                                  {!! Form::label('communityImage', 'Foto:') !!}
                                 <div class="row image-editor-community">

                                      <div class="col-md-12">

                                        <center>
                                          <div class="cropit-preview">
                                              
                                          </div>
                                          <br><br>

                                          <div class="rotate">
                                            <span  class="fa fa-repeat rotate-cw icon-rotate-right"></span>
                                            <span class="fa fa-repeat rotate-ccw icon-rotate-left"></span>
                                          </div>

                                          <span class="fa fa-file-picture-o pic-small"></span>

                                          <input type="range" class="cropit-image-zoom-input">

                                          <span class="fa fa-file-picture-o pic-big"></span>

                                          <br><br>

                                        </center>

                                      </div>

                                      <div class="col-md-4 col-md-offset-4 col-xs-offset-4">
                                          <span class="btn btn-default btn-file">
                                              Subir archivo <input name="communityImage" class="cropit-image-input" type="file">
                                          </span>
                                      </div>

                                 </div>

                              </div>
                          </div>
                    </div>

                    <div class="row my-1 text-center">
                      <div class="col-xs-12">
                        
                        <div class="form-group">
                          <label for="description">Descripcion:</label>
                          <textarea style="width: 100%;" class="form-control" id="description" name="description" row my-1s="3" >{{ $community->description }}</textarea>
                        </div>
                        
                      </div>
                    </div>

                    <div class="row my-1">
                                            <div class="col-xs-12">

                           <div class="form-group">
                          <label>Status:</label>
                          <select name="public" class="form-control" >
                           
                              <option value="0" {{$community->public==0?'selected':''}}>Private</option>
                              <option value="1" {{$community->public==1?'selected':''}}>Public</option>
                          

                          </select>
                      </div>
                    </div>
                      </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                            Cerrar
                        </button>
                        <button type="submit" class=" btn btn-primary" id="sendEditCommunityForm">
                            Salvar
                        </button>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
<script type="text/javascript">

    $(".image-editor-community").cropit({
        exportZoom: 1.25,
        imageBackground: true,
        imageBackgroundBorderWidth: 20,
        imageState: { src: "{{ isset($community->com_image) ? 
                              '/community_image/'.$community->com_image:
                              '/images/no_photo_icon.png'
                            }}" 
                    }
    });

    $('.image-editor-community .rotate-cw').click(function() {
        $(".image-editor-community").cropit('rotateCW');
    });
    
    $('.image-editor-community .rotate-ccw').click(function() {
        $(".image-editor-community").cropit('rotateCCW');
    });

</script>

