@extends('admin.layouts.layout')

@section('content')
    <div class="block full">
        
        <div class="block-title">
            <h2>Ciudades</h2>
        </div>
    
        @include('admin.cities.citiesTabs')

        <div class="row">
            <br>
            <div class="col-md-6 text-left">
              <div class="text-center form-group">
                 {!! Form::label('countryTableFilter', 'Pais:') !!}
                
                <select class="form-control form-control-lg" style="width: 100%" id="countryTableFilter" >
                    <option value="" >-- Seleccione --</option>
                    @foreach ($countries as $country)
                         <option 
                         @if ($country->id === 44)
                              selected 
                         @endif
                         value="{{ $country->id }}">{{ $country->name }}</option>
                    @endforeach
                </select>
              </div>
            </div>
            <div class="col-md-6 text-right">
               <a data-toggle="modal" href="#createCityModal" class="btn btn-info btn-effect-ripple">
                    <i class="fa fa-plus"></i>
                     Nuevo
               </a>
            </div>
            <br>
            <div class="col-md-12">
                <div class="table-responsive">
                    <div class="dataTables_wrapper form-inline no-footer">
                        <div class="_tabla">
                            <table class="table responsive table-vcenter dataTable no-footer" id="citiesTable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">Nombre</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="createCityModal" tabindex="-1" role="dialog" aria-labelledby="createCityModal" aria-hidden="true">
           @include('admin.cities.create')
        </div>

                                    {{-- Ver Ciudad --}}
        <div class="modal fade" id="editCityModal" tabindex="-1" role="dialog" aria-labelledby="editCityModal" aria-hidden="true">
        </div>

    </div>
@endsection

@push('js')
    @include ('plugins.datatable')
    @include('admin.cities.js.index')
    @include('admin.cities.js.edit')
@endpush