<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="text-center modal-title">
                Registrar Ciudad
            </h4>
        </div>

            <div class="modal-body">
                <form id="createCityForm" action="{{ route('cities.store') }}" method="POST" autocomplete="off">

                    {{ csrf_field() }}

                    <div class=" row text-center">

                        {{-- Pais --}}
                        <div class="my-1 col-12">
                            <div class="form-group">
                               {!! Form::label('country', 'Pais:') !!}
                              
                              <select class="form-control" style="width: 70%" name="country" id="country" >
                                  <option value="" >-- Seleccione --</option>
                                  @foreach ($countries as $country)
                                       <option value="{{ $country->id }}">{{ $country->name }}</option>
                                  @endforeach
                              </select>

                            </div>
                        </div>

                        {{-- Estado --}}
                        <div class=" my-1 col-12">
                            <div class="form-group">
                                {!! Form::label('state', 'Estado:') !!}

                                <select class="form-control " 
                                    style="width: 70%" name="state" id="state" >
                                        <option value="" >-- Seleccione --</option>
                                </select>

                            </div>
                        </div>

                        {{-- Ciudad --}}
                        <div class=" my-1 col-12">
                            <div class="form-group">
                            {!! Form::label('city', 'Ciudad:') !!}
                                <input type="text" class="form-control " style="width: 70%; display: inline-block;" name="city"  placeholder="Nueva Ciudad">
                            </div>
                        </div>


                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                            Cerrar
                        </button>
                        <button type="submit" class="btn btn-primary" id="sendCreateCityForm">
                            Crear
                        </button>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
@push('js')
  @include('admin.cities.js.create')
@endpush

