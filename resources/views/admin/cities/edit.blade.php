<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="text-center modal-title">
                Editar Ciudad
            </h4>
        </div>

            <div class="modal-body">
                <form id="editCityForm" action="{{ route('updateCity', ['city' => $city]) }}" method="POST" autocomplete="off">

                    {{ csrf_field() }}

                    <div class=" row text-center">

                        {{-- Pais --}}
                        <div class="my-1 col-12">
                            <div class="form-group">
                               {!! Form::label('newCountry', 'Pais:') !!}
                              
                              <select class="form-control" style="width: 70%" name="newCountry" id="newCountry" >
                                  <option value="" >-- Seleccione --</option>
                                  @foreach($countries as $country)
                                    <option 
                                    @if($country->id === $city->country_id)
                                      selected
                                    @endif  
                                    value="{{ $country->id }}">
                                      {{ $country->name }}
                                    </option>
                                  @endforeach  
                              </select>

                            </div>
                        </div>

                        {{-- Estado --}}
                        <div class=" my-1 col-12">
                            <div class="form-group">
                                {!! Form::label('newState', 'Estado:') !!}

                                <select class="form-control " 
                                    style="width: 70%" name="newState" id="newState" >
                                        <option value="" >-- Seleccione --</option>
                                </select>

                            </div>
                        </div>

                        {{-- Ciudad --}}
                        <div class=" my-1 col-12">
                            <div class="form-group">
                            {!! Form::label('newCity', 'Ciudad:') !!}
                              <input type="text" class="form-control " style="width: 70%; display: inline-block;" name="newCity" id="newCity" value="{{ $city->name }}">
                            </div>
                        </div>


                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                            Cerrar
                        </button>
                        <button type="submit" class=" btn btn-primary" id="sendEditCityForm">
                            Salvar
                        </button>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>

