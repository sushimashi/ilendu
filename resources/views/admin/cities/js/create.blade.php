<script type="text/javascript">
    $(function () {

        let country_id = $('#countryTableFilter').val();

        $('#countryTableFilter').on('change', function () {
            country_id = $(this).val();
        });

        // select2
        $('#country').select2({
            dropdownParent: $('#createCityModal'),
        });

        $('#state').select2({
            dropdownParent: $('#createCityModal'),
        });

        $("#country").on("change",function(){

          $('#state').empty();
            if ( $(this).val() === '' ){
                $("#state").append(`<option value=""> - Ninguno  - </option>`);
                return;
            } 

          $.get(`/api/country/${ $(this).val() }/states`, function(result){

                if( result.length > 0 )
                {
                    result.forEach(element => {
                        
                        var data = {
                            id: element.id,
                            text: element.name
                        };

                        var newOption = new Option(data.text, data.id, false, false);
                        $("#state").append(newOption);
                    });

                    $("#state").trigger('change');

                } else {

                    $("#state").append(`<option value=""> - Ninguno  - </option>`)
                               .trigger('change'); 

                }

            });

        })

        function clearForm() {
            $("#state").empty().append(`<option value=""> - Ninguno  - </option>`);
            $('#country').select2('destroy');
            $('#state').select2('destroy');
            $("#createCityForm").trigger('reset');
           $('#country').select2({
               dropdownParent: $('#createCityModal'),
           });
           $('#state').select2({
               dropdownParent: $('#createCityModal'),
           });
        }
           
        $("#sendCreateCityForm").on('click', function (e) {
            e.preventDefault();

            if($(this).hasClass('disabled'))
                return;

            $(this).addClass('disabled');
            
            var data = $('#createCityForm').serialize();
            console.log(data);
           
            $.ajax({
                url: $("#createCityForm").attr('action'),
                type: $("#createCityForm").attr('method'),
                data: data,
                success: function (result) {

                    swal("Exito!", "Ciudad creada exitosamente", "success")
                            .then((value) => {
                                $('#citiesTable').DataTable().ajax.url(`/api/country/${country_id}/citiesDatatable`).load(null, false);
                                clearForm();
                                $("#createCityModal").modal('hide');
                            })
                            .catch( function(){
                                $('#citiesTable').DataTable().ajax.url(`/api/country/${country_id}/citiesDatatable`).load(null, false);
                                clearForm();
                                $("#createCityModal").modal('hide');
                            });

                },
                error: function (e) {
                    console.log(e);
                    $.each(e.responseJSON.errors, function (index, element) {
                        if ($.isArray(element)) {
                            toastr.error(element[0]);
                        }
                    });
                },
                complete: function(){
                    $('#sendCreateCityForm').removeClass('disabled');
                }
            });
        });

    });
</script>