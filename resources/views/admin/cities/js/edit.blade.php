<script type="text/javascript">
    $(function () {

        let country_id = $('#countryTableFilter').val();

        $('#countryTableFilter').on('change', function () {
            country_id = $(this).val();
        });

        $(document).on("change","#newCountry", function(){

          $('#newState').empty();
            if ( $(this).val() === '' ){
                $("#newState").append(`<option value=""> - Ninguno  - </option>`);
                $("#newCity").empty().append(`<option value=""> - Ninguno  - </option>`);
                $('#sendEditCityForm').addClass('disabled');
                return;
            } 

          $.get(`/api/country/${ $(this).val() }/states`, function(result){

                if( result.length > 0 )
                {
                    result.forEach(element => {
                        
                        var data = {
                            id: element.id,
                            text: element.name
                        };

                        var newOption = new Option(data.text, data.id, false, false);
                        $("#newState").append(newOption);
                    });

                    $("#newState").trigger('change');

                } else {

                    $("#newState").append(`<option value=""> - Ninguno  - </option>`)
                               .trigger('change'); 

                }

            });

        })

        $(document).on('change', '#newCity',function () {
            if( $(this).val() === '' ){
                $('#sendEditCityForm').addClass('disabled');
            }else{
                $('#sendEditCityForm').removeClass('disabled');
            }
        });

        function clearForm() {
            $("#newState").empty().append(`<option value=""> - Ninguno  - </option>`);
            $('#newCountry').select2('destroy');
            $('#newState').select2('destroy');
            $("#createCityForm").trigger('reset');
            $('#newCountry').select2({ dropdownParent: $('#editCityModal') });
            $('#newState').select2({ dropdownParent: $('#editCityModal') });
        }
           

        $(document).on('click',"#sendEditCityForm", function (e) {
            e.preventDefault();

            if ($(this).hasClass('disabled'))
                return;
            
            var data = $('#editCityForm').serialize();
            console.log(data);
           
            $.ajax({
                url: $("#editCityForm").attr('action'),
                type: $("#editCityForm").attr('method'),
                data: data,
                success: function (result) {

                    swal("Exito!", "Ciudad actualizada exitosamente", "success")
                            .then((value) => {
                                $('#citiesTable').DataTable().ajax.url(`/api/country/${country_id}/citiesDatatable`).load(null, false);
                                clearForm();
                                $("#editCityModal").modal('hide');
                            })
                            .catch( function(){
                                $('#citiesTable').DataTable().ajax.url(`/api/country/${country_id}/citiesDatatable`).load(null, false);
                                clearForm();
                                $("#editCityModal").modal('hide');
                            });

                },
                error: function (e) {
                    console.log(e);
                    $.each(e.responseText.errors, function (index, element) {
                        if ($.isArray(element)) {
                            toastr.error(element[0]);
                        }
                    });
                }
            });
        });

    });
</script>