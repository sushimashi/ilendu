<script>
    $(function () {

    let country_id = $('#countryTableFilter').val();
    $('#cities').addClass('active');

        var _table = $("#citiesTable");
        var obj_datatable = {
            'ajax': {
                "url" : `/api/country/${ country_id }/citiesDatatable`,
                "type": "GET",
                dataSrc: '',
            },
            "responsive": true,
            "autoWidth": false,
            'columns': [
                {data: 'name',className: "text-center"},
                {
                     render: function (data, type, row) {
                        
                        let btn_deshabilitar = "",
                            btn_editar = "",
                            btn_eliminar="";

                            //ruta edit
                            btn_editar += '<a href="'+ row.id +'" title="Editar" class="btn btn-sm btn-success btn_editar"><i class="fa fa-pencil"></i></a>';


                            if(row.status==1)
                            {
                                // deshabilitar ciudad
                            btn_deshabilitar += `<a href="#" class=" btn btn-sm btn-secondary btn_deshabilitar" title="Deshabilitar" id="city_${row.id}" data-city_id="${row.id}"><i class="fa fa-arrow-down "></i></a>`;   
                        }else{
                           // habilitar ciudad
                            btn_deshabilitar += `<a href="#" class=" btn btn-sm btn-secondary btn_deshabilitar" title="Habilitar" id="city_${row.id}" data-city_id="${row.id}"><i class="fa fa-arrow-up "></i></a>`;    
                        }
                                                   
                            //ruta destroy
                           btn_eliminar = '<a href="' + row.id + '" class="btn btn-sm btn-danger _delete btn-tabla" title="Eliminar" data-toggle="tooltip"><i class="fa fa-times"></i></a>';


                            return  btn_editar+" " + btn_deshabilitar +" "+btn_eliminar;
                    }
                }
            ]
        };

        _table.DataTable(obj_datatable);

        $('#countryTableFilter').select2();

        $('#countryTableFilter').on('change', function () {
                
            country_id = $(this).val();
            $.get(`/api/country/${country_id}/citiesDatatable`, function(result){
                _table.DataTable().clear();
                _table.DataTable().rows.add(result);
                _table.DataTable().draw();    
            });

        });

        // editar ciudad
        $(document).on('click', '.btn_editar', function (e) {
            e.preventDefault();

            let city_id = $(this).attr('href');
            $.get(`/admin/cities/${city_id}/edit`, function (result) {

                $('#editCityModal').empty().append(result).modal('show');

            }).done(function(){
            
                $.get(`/api/city/${city_id}/edit`, function(result){

                    $('#newCountry').val(result.country_id);

                    $("#newState").empty().append(
                        `<option value="${result.state_id}">
                            ${result.state_name}
                        </option>`);

                }).then(value => {
                    // select2
                    $('#newCountry').select2({
                        dropdownParent: $('#editCityModal'),
                        // tags:true
                    });

                    $('#newState').select2({
                        dropdownParent: $('#editCityModal'),
                        // tags:true
                    });
                    //
                });

            });
            
        });

        // Eliminar Ciudad
         $('body').on('click', 'tbody ._delete', function (e) {
            
            city_id = $(this).attr('href');
            token = $("input[name=_token]").val();
            e.preventDefault();
            swal({
                title: '',
                text: '¿Seguro desea eliminar la ciudad?',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    url: "{{ url('/admin/cities/')}}/" + city_id,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    datatype: 'json',
                    success: function (result) {
                        _table.DataTable().ajax.url(`/api/country/${country_id}/citiesDatatable`).load(null, false);
                        toastr.success(result.message);
                    },
                    error: function (e) {
                        console.log(e);
                        $.each(e.responseJSON.errors, function (index, element) {
                            if ($.isArray(element)) {
                                toastr.error(element[0]);
                            }
                        });
                    }
                });
            }).catch(swal.noop);
        });

         // habilitar / deshabilitar ciudad
         $(document).on('click', '.btn_deshabilitar', function(e){
             e.preventDefault();
             
             let city_id = $(this).attr('data-city_id');
             $.get(`/api/city/${city_id}/enableCity`, function(result){

                _table.DataTable().ajax.url(`/api/country/${country_id}/citiesDatatable`).load(null, false);
                 toastr.success(result.message);

             });
         });

    });
</script>
