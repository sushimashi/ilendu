<script>
    $(function () {

    let country_id = $('#countryTableFilter').val();
    $('#disabledCities').addClass('active');

        var _table = $("#disabledCities_table");
        
        var obj_datatable = {
            'ajax': {
                "url": `/api/country/${ country_id }/disabledCitiesDatatable`,
                "type": "GET",
                dataSrc: '',
            },
            "responsive": true,
            "autoWidth": false,
            'columns': [
                {data: 'name',className: "text-center"},
                {
                     render: function (data, type, row) {
                        
                            // habilitar ciudad
                        let  btn_habilitar = `<a href="#" class="btn btn-sm btn-secondary btn_habilitar" title="Habilitar" id="city_${row.id}" data-city_id="${row.id}"><i class="fa fa-arrow-up "></i></a>`;                          
                            return btn_habilitar ;
                    }
                }
            ]
        };

        _table.DataTable(obj_datatable);

        $('#countryTableFilter').select2();

        $('#countryTableFilter').on('change', function () {
                
            country_id = $(this).val();
            $.get(`/api/country/${country_id}/disabledCitiesDatatable`, function(result){
                _table.DataTable().clear();
                _table.DataTable().rows.add(result);
                _table.DataTable().draw();    
            });

        });

        // eliminar ciudad
         $('body').on('click', 'tbody ._delete', function (e) {
            idCliente = $(this).attr('href');
            token = $("input[name=_token]").val();
            e.preventDefault();
            swal({
                title: '',
                text: '¿Seguro desea eliminar el registro?',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    url: "{{url('/admin/categorias/')}}/" + idCliente,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    datatype: 'json',
                    success: function (respuesta) {
                        if (respuesta.success) {
                            _table.DataTable().ajax.url(`/api/country/${country_id}/disabledCitiesDatatable`).load(null, false);
                            toastr.success(respuesta.mensaje);
                        } else {
                            toastr.error(respuesta.error);
                        }
                    },
                    error: function (e) {
                        console.log(e);
                        $.each(e.responseJSON.errors, function (index, element) {
                            if ($.isArray(element)) {
                                toastr.error(element[0]);
                            }
                        });
                    }
                });
            }).catch(swal.noop);
        });

         // habilitar ciudad
         $(document).on('click', '.btn_habilitar', function(e){
             e.preventDefault();
             
             let city_id = $(this).attr('data-city_id');
             $.get(`/api/city/${city_id}/enableCity`, function(result){

                 _table.DataTable().ajax.url(`/api/country/${country_id}/disabledCitiesDatatable`).load(null, false);
                 toastr.success(result.message);

             });
         });

    });
</script>
