  <!-- Main content -->
  <section class="content container-fluid">
      <ul class="nav nav-tabs" role="tablist">  

          <li role="presentation" id="cities" class="tab"><a href="{{ route('cities.index') }}">Tablero</a></li>

          <li role="presentation" id="disabledCities" class="tab"><a href="{{ route('disabledCities') }}">Ciudades Deshabilitadas</a></li>

      </ul>
  </section>
