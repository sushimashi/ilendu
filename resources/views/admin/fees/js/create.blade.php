<script type="text/javascript">

    $("#sendCreateFeesForm").click( function (e) {
        e.preventDefault();       

        if($(this).hasClass('disabled'))
            return;

        $(this).addClass('disabled');

        let formData = new FormData($('#createFeesForm')[0]);

        $.ajax({
            url: $("#createFeesForm").attr('action'),
            type: $("#createFeesForm").attr('method'),
            enctype: 'multipart/form-data',
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {
                if (result.success) {
                    swal("Exito!", "Plan de Membresia registrado exitosamente!", "success")
                        .then((value) => {
                            $("#createFeesModal").modal('hide');
                            $("#_table").DataTable().ajax.reload(null, false);
                            $("#createFeesForm").trigger('reset');
                        })
                        .catch( function(){
                            $("#createFeesModal").modal('hide');
                            $("#_table").DataTable().ajax.reload(null, false);
                            $("#createFeesForm").trigger('reset');
                        });
                } else {
                    toastr.error(result.error);
                }
            },
            error: function (e) {
                console.log(e);
                $.each(e.responseJSON.errors, function (index, element) {
                    if ($.isArray(element)) {
                        toastr.error(element[0]);
                    }
                });
            },
            complete: function(){
                $("#sendCreateFeesForm").removeClass('disabled');
            }
        });
    });

</script>