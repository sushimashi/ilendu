<script>
    $(function () {

        var _table = $("#_table");
        
        var obj_datatable = {
            'ajax': {
                "url": '{{ url("/admin/fees") }}',
                "type": "GET",
                dataSrc: '',
            },
            "responsive": true,
            "autoWidth": false,
            'columns': [
                {data: 'fees',className: "text-center"},
                {data: 'fees_plan',className: "text-center"},
                {
                     render: function (data, type, row) {

                        //ruta edit
                        btn_editar = '<a href="'+row.id +'" title="Editar" class="btn btn-sm btn-success editPlanFees"><i class="fa fa-pencil"></i></a>';

                        //ruta destroy
                       btn_eliminar = '<a href="' + row.id + '" class="btn btn-sm btn-danger _delete" title="Eliminar"><i class="fa fa-times"></i></a>';

                        return  btn_editar+" "+btn_eliminar;
                    }
                }
            ]
        };

        _table.DataTable(obj_datatable);

         $('body').on('click', 'tbody ._delete', function (e) {
            e.preventDefault();

            membership_id = $(this).attr('href');
            token = $("input[name=_token]").val();

            swal({
                title: '',
                text: '¿Eliminar Plan de Tarifa?',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then(function () {
                $.ajax({
                    url: "{{url('/admin/fees/')}}/" + membership_id,
                    headers: {'X-CSRF-TOKEN': token},
                    type: 'DELETE',
                    datatype: 'json',
                    success: function (result) {
                        if (result.success) {

                            _table.DataTable().ajax.reload();
                            toastr.success(result.message);

                        } else {
                            toastr.error(result.error);
                        }
                    },
                    error: function (e) {
                        console.log(e);
                        $.each(e.responseJSON.errors, function (index, element) {
                            if ($.isArray(element)) {
                                toastr.error(element[0]);
                            }
                        });
                    }
                });
            }).catch(swal.noop);
        });

    });
</script>
