<script type="text/javascript">

    // cargar modal 
    $(document).on('click','.editPlanFees', function(e){
        e.preventDefault();
        planFee_id = $(this).attr('href');
        $.get(`/admin/fees/${planFee_id}/edit`, function(result){
            $('#editFeesModal').empty().append(result).modal('show');
        });
    });

    function clearForm(){
        $("#editFeesModal").modal('hide');
        $("#_table").DataTable().ajax.reload(null, false);
        $("#editFeesForm").trigger('reset');
    }

    // actualizar Tarifa
    $(document).on('click', "#sendEditFeesForm",  function (e) {
        e.preventDefault();    

        if($(this).hasClass('disabled'))
            return;

        $(this).addClass('disabled');   

        let formData = new FormData($('#editFeesForm')[0]);

        $.ajax({
            url: $("#editFeesForm").attr('action'),
            type: $("#editFeesForm").attr('method'),
            enctype: 'multipart/form-data',
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {
                if (result.success) {
                    swal("Exito!", "Plan de Tarifa actualizado!", "success")
                        .then((value) => {
                            clearForm();
                        })
                        .catch( function(){
                            clearForm();
                        });
                } else {
                    toastr.error(result.error);
                }
            },
            error: function (e) {
                console.log(e);
                $.each(e.responseJSON.errors, function (index, element) {
                    if ($.isArray(element)) {
                        toastr.error(element[0]);
                    }
                });
            },
            complete: function(){
                $("#sendEditFeesForm").removeClass('disabled');
            }
        });
    });

</script>