@extends('admin.layouts.layout')

@section('content')
    <div class="block full">
        <div class="block-title">
            <h2>Membresias</h2>
        </div>
        {{-- envia a metodo create --}}
       <div class="row">
            <div class="col-md-12 text-right">
                <a data-toggle="modal" data-target="#createFeesModal" href="#" class="btn btn-info btn-effect-ripple">
                    <i class="fa fa-plus"></i>
                    Nuevo
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12"><br/><br/>
                <div class="table-responsive">
                    <div class="dataTables_wrapper form-inline no-footer">
                        <div class="_tabla">
                            <table class="table responsive table-vcenter dataTable no-footer" id="_table" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th class="text-center">Tarifa</th>
                                        <th class="text-center">Plan</th>
                                        <th class="text-center">Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
                                    {{-- Crear Membresia --}}
    <div class="modal fade" id="createFeesModal" tabindex="-1" role="dialog" aria-labelledby="createFeesModal" aria-hidden="true">
       @include('admin.fees.create')
    </div>

                                {{-- Ver Membresia --}}
    <div class="modal fade" id="editFeesModal" tabindex="-1" role="dialog" aria-labelledby="editFeesModal" aria-hidden="true">
    </div>

@endsection

@push('js')

    @include ('plugins.datatable')
    @include('admin.fees.js.index')
    @include ('admin.fees.js.edit')

@endpush