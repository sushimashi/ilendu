<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="text-center modal-title">
                Editar Tarifa
            </h4>
        </div>

            <div class="modal-body">
                <form enctype="multipart/form-data" id="editFeesForm" action="{{route('updateRates', ['id' => $membership->id])}}" method="POST" autocomplete="off">

                    {{ csrf_field() }}

                    <div class="row text-center">

                      <div class="col-md-6">
                          <div class="form-group">
                              <label class=" control-label" for="fees">Tarifa:</label>
                              <input class="form-control" type="number" min="0.5" id="fees" name="fees" placeholder="Tarifa" value='{{ $membership->fees }}'>
                          </div>
                      </div>

                      <div class="col-md-6">
                          <div class="form-group">
                              <label class=" control-label" for="fees_plan">
                                Plan de Tarifa:
                              </label>
                              <select class="form-control" name="fees_plan" id="fees_plan">
                                  <option disabled>-- Seleccione --</option>
                                    @foreach($plans as $plan)
                                        <option
                                      @if( $membership->fees_plan == $plan )
                                        selected
                                      @endif
                                        value="{{$plan}}">
                                            {{$plan}}
                                        </option>
                                    @endforeach
                              </select>
                          </div>
                      </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
                            Cerrar
                        </button>
                        <button type="submit" class=" btn btn-info" id="sendEditFeesForm">
                            Salvar
                        </button>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>

