@if ($paginator->hasPages())
    <div class="row">
        <div class="col-sm-5 hidden-xs">
            <div class="dataTables_info" id="role_table_info" role="status" aria-live="polite">
                <strong>{{$paginator->firstItem()}}</strong>-<strong>{{$paginator->lastItem()}}</strong> of <strong>{{$paginator->total()}}</strong>
            </div>
        </div>
        <div class="col-sm-7 col-xs-12 clearfix">
            <div class="dataTables_paginate paging_bootstrap" id="role_table_paginate">
                <ul class="pagination pagination-sm remove-margin">
                    {{-- Previous Page Link --}}
                    @if ($paginator->onFirstPage())
                        <li class="prev disabled">
                            <a href="javascript:void(0)">
                                <i class="fa fa-chevron-left"></i>
                            </a>
                        </li>
                    @else
                        <li class="prev">
                            <a href="{{ $paginator->previousPageUrl() }}">
                                <i class="fa fa-chevron-left"></i>
                            </a>
                        </li>
                    @endif
                    {{-- Pagination Elements --}}
                    @foreach ($elements as $element)
                        {{-- "Three Dots" Separator --}}
                        @if (is_string($element))
                            <li class="disabled" aria-disabled="true"><span>{{ $element }}</span></li>
                        @endif

                        {{-- Array Of Links --}}
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <li class="active">
                                        <a href="javascript:void(0)">{{ $page }}</a>
                                    </li>
                                @else
                                    <li>
                                        <a href="{{ $url }}">{{ $page }}</a>
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                    {{-- Next Page Link --}}
                    @if ($paginator->hasMorePages())
                        <li class="next">
                            <a href="{{ $paginator->nextPageUrl() }}" rel="next">
                                <i class="fa fa-chevron-right"></i>
                            </a>
                        </li>
                    @else
                        <li class="next disabled" aria-disabled="true">
                            <a href="javascript:void(0)">
                                <i class="fa fa-chevron-right"></i>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endif
