

<!-- Modal -->
<div class="modal fade" id="pleaseWaitDialog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
  <div class="modal-header">
          <h1>Espere un momento...</h1>
      </div>
     <div id="ajax_loader">
            <img src="{{asset('img/loader.gif')}}" style="display: block; margin-left: auto; margin-right: auto;">
      </div>
    </div>
  </div>
</div>