<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('/product/search/{keywords}/{category?}/{page?}/{sort?}', 'ProductAmazonController@search');
Route::post('/product/search', 'ProductAmazonController@formSearch');

Route::post('/callback', 'CheckoutController@store');

Route::get('/borrows/{status}/borrowsDatatable', 'admin\BorrowController@search');
Route::get('/borrows-country', 'admin\BorrowController@searchCountry');

	// Usuarios
	// estados por pais
Route::get('/country-select', 'admin\CityController@countrySelect');

// ciudades por estado
Route::get('/country-select/{country_id}/states', 'admin\CityController@statesByCountrySelect');

Route::get('/state-select/{state_id}/cities', 'admin\CityController@cityByStateSelect');

// tabla de usuarios
Route::get('/usersTable', 'admin\UserController@Datatable')->name('usersTable');

	// Ciudades
	
// tabla de ciudades
Route::get('/country/{country_id}/citiesDatatable', 'admin\CityController@Datatable')->name('citiesDatatable');

// tabla de ciudades deshabilitadas
Route::get('/country/{country_id}/disabledCitiesDatatable', 'admin\CityController@disabledCitiesDatatable');

// Habilitar / deshabilitar ciudad
Route::get('/city/{city_id}/enableCity', 'admin\CityController@enableCity');

// estados por pais
Route::get('/country/{country_id}/states', 'admin\CityController@statesByCountry');

// ciudades por estado
Route::get('/state/{state_id}/cities', 'admin\CityController@cityByState');

Route::get('/country/{country_id}/products','admin\CityController@productsByCountry');

// Editar Ciudad
Route::get('/city/{city_id}/edit', 'admin\CityController@editCity');

	/* Productos */

// DataTable
Route::get('/productsDataTable', 'admin\ProductAdminController@DataTable')->name('productsDataTable');

// premium Products DataTable
Route::get('/premiumProductsDataTable', 'admin\ProductAdminController@premiumProductsDataTable')->name('premiumProductsDataTable');

// borrowed Products DataTable
Route::get('/borrowedProductsDataTable', 'admin\ProductAdminController@borrowedProductsDataTable');

// displayed Products DataTable
Route::get('/displayedProductsDataTable', 'admin\ProductAdminController@displayedProductsDataTable')->name('displayedProductsDataTable');

// undisplayed Products DataTable
Route::get('/undisplayedProductsDataTable', 'admin\ProductAdminController@undisplayedProductsDataTable')->name('undisplayedProductsDataTable');

// exhibir producto
Route::get('/product/{product_id}/productDisplay', 'admin\ProductAdminController@displayProduct')->name('displayProduct');

// categoria "Premium"
Route::get('/product/{product_id}/premiumCategory', 'admin\ProductAdminController@premiumCategory');

// obtener categorias
Route::get('/productCategories', 'admin\ProductAdminController@productCategories')->name('productCategories');

	/* Comunidades */

// DataTable
Route::get('/communitiesDataTable', 'admin\CommunityController@DataTable')->name('communitiesDataTable');

// DataTable comunidades inactivas
Route::get('/inactiveCommunitiesDataTable', 'admin\CommunityController@inactiveCommunitiesDataTable')->name('inactiveCommunitiesDataTable');

// habilitar / deshabilitar comunidad
Route::get('/community/{community_id}/enableCommunity', 'admin\CommunityController@enableCommunity')->name('enableCommunity');

// Usuarios por Comunidades
Route::get('/community/{community_id}/communityUsers', 'admin\CommunityController@communityUsers');

// Usuarios por Comunidades
Route::get('/community/{community_id}/communityProducts', 'admin\CommunityController@communityProducts');

// remover Usuarios de Comunidad
Route::get('/community/{community_id}/user/{user_id}/removeOfCommunity', '
	admin\CommunityController@removeUserOfCommunity');


// remover Products de Comunidad
Route::get('/community/{community_id}/product/{product_id}/removeProductOfCommunity', 'admin\CommunityController@removeProductOfCommunity');

// aprobar/ desaprobar Usuarios de Comunidad
Route::get('/community/{community_id}/user/{user_id}/approveUser', 'admin\CommunityController@approveUser');

/* Categorias */

Route::get('/enableCategory/{id}', 'admin\CategoryController@enableCategory');
