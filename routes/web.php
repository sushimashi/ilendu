<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


*/
//public routes
Route::get('add-more-time/{id}','ProductController@more_time');
Route::post('add-more-time','ProductController@add_more_time');

Route::get('/is_notified', 'ProductController@is_notified');
Route::get('countries-ajax','CityUserController@countries');
Route::get('cities-ajax/{id}','CityUserController@cities');
Route::get('category/{category_id}','CategoryController@getProductsByCategory');
Route::post('get-messages/{id}','HomeController@get_messages');
Route::post('send-messages','HomeController@send_message');

Route::get('country-ajax','CityUserController@country_ajax');

Route::get('get-dispatch-info/{id}','admin\DispatchController@get_info');

Route::resource('/reputation','ProfileController');

Route::get('/ver','ProductController@ver');

Route::get('set-com',function(){

$cities=App\City::all();

foreach ($cities as $key => $value) 
{
	$com=App\Community::create([
		"name"=>$value->name,
		"description"=>$value->name." community",
		"status"=>1,
		"public"=>1,
		"created_by"=>1,
		"city_id"=>$value->id
	]);

	$com->communityUsers()->attach(1,['is_approve'=>1]);

	if (count($value->products)) 
	{

		foreach ($value->products as $key => $p) 
		{
			$com->communityProducts()->attach($p->id);

		}

	}
}

return "success";

});

Route::get('json',function(){
$path = storage_path() . "/app/public/tapa.json"; // ie: /var/www/laravel/app/storage/json/filename.json

$json = json_decode(file_get_contents($path), true); 
$data=[];
$i=0;
foreach ($json as $key => $value) {

	#return $value;
	//titles
	$value["title"]=str_replace(['  '],'',$value["title"]);
	$value["title"]=preg_replace("/[\n\r\t]/","",$value["title"]);

	//description
	$arr_desc=array('\\"','<ul>','</ul>','<li>','</li>','<br>','<strong>','</strong>','<strike>','</strike>','<noscript>','</noscript>','<div>','</div>','<p>','</p>','<b>','</b>','<i>','</i>','<em>','</em>','<blockquote>','</blockquote>');

	$value["product_description"]=str_replace($arr_desc,'', $value["product_description"]);
	
	$value["product_description"] = preg_replace("/[\n\r\t]/","",$value["product_description"]); 
	$value["product_price"] = preg_replace("/[\n\r\t]/","",$value["product_price"]); 


	$data[$i]["title"]=$value["title"];
	$data[$i]["product_description"]=$value["product_description"];


	//if is base64

	#$contains=str_contains('base64',$value["product_image"]);

		//return strlen($value["product_image"]);

	$url=preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#',$value["product_image"], $match);

	$data[$i]["match"]=$match;

	$count=count($match[0]);

	if ($count>1) 
	{
		$cleaned=strstr($match[0][0], '"', true);

		if ($cleaned==false) 
		{
			$cleaned=$match[0][0].'.jpg';
		}

	}elseif($count==1){

		$cleaned=strstr($match[0][0], '"', true);
		if ($cleaned==false) 
		{
			$cleaned=$match[0][0].'.jpg';
		}

	}else
	{

		$cleaned=null;
	}


	#$data[$i]["match"]=$cleaned;
	$img='hard_cover_'.rand().'.jpg';
	$base='img/thumbnail/'.$img;
	$data[$i]["product_image"]=$img;
	
	$ch = curl_init($cleaned);
	$fp = fopen(public_path($base), 'wb');
	curl_setopt($ch, CURLOPT_FILE, $fp);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_exec($ch);
	curl_close($ch);
	fclose($fp);
	
	$value["product_price"]=str_replace(['desde',' ','US$'],'',$value["product_price"]);
	$data[$i]["product_price"]=$value["product_price"];



	$value["product_author"]=str_replace(['Visita la página de Amazon'],'',$value["product_author"]);
	$data[$i]["product_author"]=$value["product_author"];


	$i++;
}




foreach ($data as $key => $value) 
{
	$c=count($value["product_description"]);
	if ($c<1) 
	{
		$value["product_description"][0]=null;
	}

	$product= App\Product::create([
		"product_name"=>$value["title"],
		"product_desc"=>$value["product_description"][0],
		"price"=>$value["product_price"],
		"product_img"=>$value["product_image"],
		"author"=>$value["product_author"],
		"product_category"=>10,
		"is_active"=>1,
		"in_front"=>1,
		"city_id"=>5167,
		"user_id"=>1,
		"share_all"=>1,
		"country_id"=>44,
		"is_premium"=>1
	]);

	$p2=App\ProductImage::create([
		"photo"=>$product->product_img,
		"product_id"=>$product->id
	]);

}
return "success";

});

Route::resource('products', 'ProductController', ['names' => [
	'create' => 'createproduct',
	'index' => 'product',
	'show' => 'productdetail',
	'store' => 'addproduct',
	'destroy' => 'deleteproduct',

]]);
//Route::get('demos/jquery-image-upload','CommunityController@showJqueryImageUpload');

Route::group(['prefix' => 'admin', 'namespace' => 'admin', 'middleware' => ['auth','admin','web']], function () {
// Inicio

	Route::get('/', function () {
		$titulo = "Inicio";
		return view('admin.index', compact("titulo"));
	});



	/*Despacho*/
	Route::resource('/shipping-records','DispatchController');

	/*Feedback*/
	Route::resource('/feedback', 'FeedBackController');

	#solicitudes
	Route::resource('/borrows', 'BorrowController');
	Route::get('/borrows-status/{id}', 'BorrowController@status');

	/*Estadisticas*/
	Route::resource('/estadisticas', 'EstadisticaController');
	Route::get('estadisticas-date','EstadisticaController@date');
	/* Usuarios */
	Route::resource('/users', 'UserController');
	Route::post('users/role-admin/{id}','UserController@addRole');

	Route::post('/updateUser/{user_id}', 'UserController@updateUser')->name('updateUser');
	/* Categorias */
	Route::resource('/categories', 'CategoryController');
// actualizar categoria
	Route::post('/updateCategory/{category_id}', 'CategoryController@updateCategory')->name('updateCategory');
	/* Ciudades */
	Route::resource('/cities', 'CityController');
// editar ciudad
	Route::post('/updateCity/{city_id}', 'CityController@updateCity')->name('updateCity');
// deshabilitar ciudad
	Route::get('/disabledCities', 'CityController@disabledCities')->name('disabledCities');
	/* Productos (administrador) */
	Route::resource('/products', 'ProductAdminController');
	Route::post('products/delete_multi', 'ProductAdminController@delete_multi');

// editar producto
	Route::post('/updateProduct/{product_id}', 'ProductAdminController@updateProduct')->name('updateProduct');
// productos "premium"
	Route::get('/premiumProducts', 'ProductAdminController@premiumProducts')->name('premiumProducts');
// productos en Exhibicion
	Route::get('/displayedProducts', 'ProductAdminController@displayedProducts')->name('displayedProducts');
//productos prestados
	Route::get('/borrowedProducts', 'ProductAdminController@borrowedProducts')->name('borrowedProducts');

// productos sin Exhibir
	Route::get('/undisplayedProducts', 'ProductAdminController@undisplayedProducts')->name('undisplayedProducts');
	/* Comunidades */
	Route::resource('/communities', 'CommunityController');
// actualizar comunidad
	Route::post('/community/{community_id}/updateCommunity', 'CommunityController@updateCommunity')->name('updateCommunity');
// Comunidades inactivas
	Route::get('inactiveCommunities', 'CommunityController@inactiveCommunities')->name('inactiveCommunities');
	/* Planes de Tarifas (Membresias) */
	Route::resource('/fees', 'FeesController');
// actualizar plan de tarifa
	Route::post('/updateRates/{fee_id}', 'FeesController@updateRates')->name('updateRates');
	/* Config */
	Route::group(['prefix' => 'config', 'namespace' => 'Config'], function () {
// editar datos de usuario logueado
		Route::post('user/{user_id}/editData', 'UsuarioController@editUser')->name('editUser');
		Route::resource('/', 'ConfigController')->only([
			'index', 'store'
		]);
		Route::put('/usuariosSide','UsuarioController@usuariosSide');
		Route::resource('/usuarios', 'UsuarioController');
		Route::put('/cambiarFoto/{user_id}', 'UsuarioController@cambiarFoto');
		Route::group(['prefix' => 'privilegios', 'namespace' => 'Privilegios'], function () {
			Route::resource('/roles', 'RolesController');
			Route::resource('/permisos', 'PermissionsController');
		});
	});
});
//delete order
Route::post('deleteOrder','ProductController@deleteOrder');
Route::post('returnedOrder/{id}','ProductController@returnedOrder');
Route::post('received-order/{id}','ProductController@receivedOrder');


Route::resource('/plans','PlanController');
Route::get('/subscription/create', 'SubscriptionController@create');
Route::get('/subscription/init', 'SubscriptionController@pay');
Route::get('/subscription/card_inscription_return', 'SubscriptionController@cardInscriptionReturn');
Route::get('/subscription/success/{subscription_id}', 'SubscriptionController@success');
//pago
Route::post('/charge/pay', 'ChargeController@pay');
Route::get('/charge/return_after_form', 'ChargeController@returnAfterForm');
//search communities
Route::get('/search-communities','CommunityController@filter_communities');
Route::get('/search-communities/name','CommunityController@filter_communities_by_name');
//search categories
Route::get('/search-name','CategoryController@getProductsByName');
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'HomeController@switchLang']);
Route::get('/user/products/{id}','ProductController@getusersproducts');
Route::post('/demos/jquery-image-upload/{id}/{image}','CommunityController@saveJqueryImageUpload');
Route::get('/', "HomeController@index")->name('home');
Route::get('/logout', ['as' => 'logout', 'uses' => 'HomeController@logout']);
// Añadir Nueva Ciudad
Route::post('/addcity', "HomeController@addCity");
Route::get('/cityproduct/{id}','ProductController@cityproduct');
Route::get('/categoryproduct/{id}','ProductController@categoryproduct');
Route::get('/category-filter','CategoryController@categoryByFilter');


Route::post('/newsletter', "HomeController@newsletter");
Route::group(['middleware' => ['auth']], function () {
	Route::get('/dump', ['as' => 'dump', 'uses' => 'HomeController@dump']);

	Route::get('addmoney/stripe', array('as' => 'addmoney.paywithstripe','uses' => 'StripeController@payWithStripe'));


	Route::get('charge/success/{id}',array('as' => 'charge.success','uses' => 'StripeController@charge'));

	Route::post('addmoney/stripe', array('as' => 'addmoney.stripe','uses' => 'StripeController@postPaymentWithStripe'));


// account
	Route::get('/account', ['as' => 'account', 'uses' => 'HomeController@account']);
//
	Route::post('/profile', ['as' => 'profile', 'uses' => 'HomeController@profile']);
//public profile
	Route::get('/changeStatus/{id}/{status}', 'ProductController@changeStatus');
	Route::get('/product/request/{id}','ProductController@borrow_detail');
//score
	Route::resource('score','ScoreController');
	Route::post('/profileimage', 'HomeController@image');
	Route::get('/borrow/{id}', 'ProductController@productBorrow');
	Route::post('/creditcardDetail', 'ProductController@creditcardDetail');
	Route::post('payment/{product_id}', 'CheckoutController@QvoPayment');
	Route::get('payment/callback/{product_id}','CheckoutController@callbackQvo');
	Route::get('/joincommunity/{id}/{status?}','CommunityController@joincommunity');
	Route::get('/unjoincommunity/{id}/{status?}','CommunityController@unjoincommunity');
	Route::resource('/communities','CommunityController');
	Route::get('/requestemail/{id}/{uid}','CommunityController@email');
});
Route::get('/unsubscribe/{email}', 'HomeController@unsubscribe');
Route::group(['middleware' => ['auth']], function () {
	Route::get('/getproductlist','ProductController@productlistdata');
	Route::get('/getuserlist','CityUserController@userlistdata');
	Route::get('/productlist', 'ProductController@productList')->middleware(['auth','role']);
	Route::post('productlist/edit/{id}', 'ProductController@productListedit')->name('edit');
	Route::get('product-communities/{id}','ProductController@product_communities');
	Route::get('/productdetails/{id}', 'ProductController@productDetail');
	Route::post('/productcomment', 'ProductController@productComment');
	Route::get('/productstatuschange/{id}/{status}', 'ProductController@productStatusChange');
	Route::get('/productfrontpage/{id}/{status}', 'ProductController@productFrontPage');
	Route::post('/membership','DashboardController@membership');
	Route::get('/userproduct/{id}','DashboardController@userProductDetail');
	//Route::resource('/dashboard','DashboardController');
	Route::get('/city','CityUserController@index');
});


/*Route::get('/auth0/callback', ['as' => 'logincallback', 'uses' => '\Auth0\Login\Auth0Controller@callback']);*/
/*Route::get('/callback', 'CheckoutController@store')->middleware('auth');*/


Route::get('/privacy','HomeController@privacy');

Route::get('user_communities','ProductController@user_communities');
Route::get('getCommunities','ProductController@getCommunities');
Route::get('community_products','CommunityController@community_products');
Route::get('community_categories','CommunityController@community_categories');
Route::post('products/edit/{id}','ProductController@edit')->name('editproduct');
Route::get('/communitychangeStatus/{id}/{userid}/{status}', 'CommunityController@changeStatus');
Route::get('/updatecategory','DashboardController@updateCategory');
Route::get('/search','ProductController@search');
Route::get('getstate/{id}', 'HomeController@getState');
Route::get('/getcity/{id}', 'HomeController@getCity');
Route::get('/categorysearch/{id}/{search?}','CommunityController@categorysearch');
Route::get('/catsearch/{search?}','HomeController@catsearch');
Route::get('/prodsearch/{search?}','HomeController@prodsearch');
/* user for facebook login */
Route::get('auth/{provider}', 'Auth\SocialiteController@redirectToFacebook');
Route::get('auth/{provider}/callback', 'Auth\SocialiteController@handleFacebookCallback');
Auth::routes();//this create the auth routes
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('feedback','FeedBackController');

Route::get('setcity/{city_id}','HomeController@setcity');
Route::get("/lender", "HomeController@lenders");

Route::get("/about", "HomeController@about");

Route::get("/security", "HomeController@security");


Route::get("/borrow", "HomeController@borrows");
Route::get("/borrow-share/{id}", "ProductController@productBorrowAll");
Route::get('/upload', 'ImageprocessController@upload');
Route::post('/upload', 'ImageprocessController@postupload');
Route::post('/cover-image-save', 'ImageprocessController@postimgAdjustpostion');
//score