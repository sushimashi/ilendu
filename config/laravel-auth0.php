<?php

return array(

	/*
		    |--------------------------------------------------------------------------
		    |   Your auth0 domain
		    |--------------------------------------------------------------------------
		    |   As set in the auth0 administration page
		    |
	*/

	/*'domain' => 'ilendudev.auth0.com',*/
	'domain' => 'ilenduenv.auth0.com',
	/*
		    |--------------------------------------------------------------------------
		    |   Your APP id
		    |--------------------------------------------------------------------------
		    |   As set in the auth0 administration page
		    |
	*/

	/*'client_id' => 'ascg2D18mT9FbRaLPiU3HDWKzD0SU0xX',*/
	'client_id' => 'rud1FEdTAyTEEOJmUQSE3TQIiim42esS',

	/*
		    |--------------------------------------------------------------------------
		    |   Your APP secret
		    |--------------------------------------------------------------------------
		    |   As set in the auth0 administration page
		    |
	*/
	/*'client_secret' => '3OGCGuAKd9AIlFR4TDeOPrPykUmeSNgTZx6SGyGlsSj2He4dSM8Q2zsD9n4fT-aB',*/
	'client_secret' => 'OzYDaXPdwNwWvgljxdI1bVUtx6hzySLFbMl9IketpT_teNjK5DAmayjlpgzSHtr0',

	/*
		    |--------------------------------------------------------------------------
		    |   The redirect URI
		    |--------------------------------------------------------------------------
		    |   Should be the same that the one configure in the route to handle the
		    |   'Auth0\Login\Auth0Controller@callback'
		    |
	*/

	/*'redirect_uri' => 'https://ilendu.co/auth0/callback',*/
	'redirect_uri' => 'http://ec2-18-216-147-146.us-east-2.compute.amazonaws.com:8081/auth0/callback',

	/*
		    |--------------------------------------------------------------------------
		    |   Persistence Configuration
		    |--------------------------------------------------------------------------
		    |   persist_user            (Boolean) Optional. Indicates if you want to persist the user info, default true
		    |   persist_access_token    (Boolean) Optional. Indicates if you want to persist the access token, default false
		    |   persist_id_token        (Boolean) Optional. Indicates if you want to persist the id token, default false
		    |
	*/

	'persist_user' => true,
	'persist_access_token' => true,
	'persist_id_token' => true,

	/*
		    |--------------------------------------------------------------------------
		    |   The authorized token issuers
		    |--------------------------------------------------------------------------
		    |   This is used to verify the decoded tokens when using RS256
		    |
	*/
	// 'authorized_issuers'  => [ 'https://XXXX.auth0.com/' ],

	/*
		    |--------------------------------------------------------------------------
		    |   The authorized token audiences
		    |--------------------------------------------------------------------------
		    |
	*/
	// 'api_identifier'  => '',

	/*
		    |--------------------------------------------------------------------------
		    |   The secret format
		    |--------------------------------------------------------------------------
		    |   Used to know if it should decode the secret when using HS256
		    |
	*/
	// 'secret_base64_encoded'  => true,

	/*
		    |--------------------------------------------------------------------------
		    |   Supported algs by your API
		    |--------------------------------------------------------------------------
		    |   Algs supported by your API
		    |
	*/
	// 'suported_algs'        => ['HS256'],

);
