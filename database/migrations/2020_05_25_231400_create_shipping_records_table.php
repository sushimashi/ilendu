<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('weight')->nullable();
            $table->float('price')->nullable();
            $table->integer('transaction_id')->unsigned()->nullable();
            $table->foreign('transaction_id')->references('id')->on('transaction')
                ->onUpdate('cascade')->onDelete('cascade');  
            $table->integer('borrow_id')->unsigned()->nullable();
            $table->foreign('borrow_id')->references('id')->on('borrowers')
                ->onUpdate('cascade')->onDelete('cascade');  
            $table->tinyInteger('status')
                    ->default(0);        
            $table->date('dispatch_date')->nullable();
            $table->float('total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_records');
    }
}
