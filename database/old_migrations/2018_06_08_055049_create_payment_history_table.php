<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_history', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_id');
            $table->integer('product_id')->unsigned();
            $table->float('price');
            $table->integer('quantity');
            $table->string('card_holder_name');
            $table->string('address');
            $table->string('address2')->nullable();
            $table->string('city');
            $table->string('country');
            $table->integer('zip');
            $table->string('phone',20)->nullable();
            $table->string('credit_card_processed');
            $table->text('raw_data');
            $table->timestamps();
            
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_history');
    }
}
