<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('products', function (Blueprint $table) {

			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('product_category')->unsigned();
			$table->string('product_name');
			$table->text('product_desc');
			$table->string('product_img');
			$table->text('comment')->nullable();
			$table->integer('city_id')->unsigned()->nullable();
			$table->tinyInteger('is_active')
					->default(0)
					->comment("0='Inactive',1='Active'");
			$table->tinyInteger('is_premium')
					->default(0)
					->comment("0='Inactive',1='Active'");
			$table->tinyInteger('in_front')->default(0)
					->comment("0='No',1='Yes'");
			$table->timestamps();

			$table->foreign('user_id')
					->references('id')
					->on('users')
					->onDelete('cascade')
					->onUpdate('cascade');

			$table->foreign('product_category')
					->references('id')
					->on('categories')
					->onDelete('cascade')
					->onUpdate('cascade');

			$table->foreign('city_id')
					->references('id')
					->on('cities')
					->onDelete('cascade')
					->onUpdate('cascade');

            $table->tinyInteger('share_all')
                    ->default(0);


           $table->foreign('country_id')
					->references('id')
					->on('countries')
					->onDelete('cascade')
					->onUpdate('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('products');
	}
}
