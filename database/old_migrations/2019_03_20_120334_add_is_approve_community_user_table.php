<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsApproveCommunityUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('community_user', function (Blueprint $table) {
            $table->tinyInteger('is_approve')->nullable()->default(0)->comment("0=pendding,1=approve,2=reject");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('community_user', function (Blueprint $table) {
            $table->dropColumn('is_approve');
        });
    }
}
