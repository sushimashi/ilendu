<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('expireMonth')->after('credit_card')->nullable();
            $table->string('expireYear')->after('expireMonth')->nullable();
            $table->string('CVC')->after('expireYear')->nullable();
            $table->date('expire_date')->after('CVC')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('expireMonth');
            $table->dropColumn('expireYear');
            $table->dropColumn('CVC');
            $table->dropColumn('expire_date');
        });
    }
}
