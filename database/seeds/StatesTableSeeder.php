<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 100 ; $i++) { 
        	
        	\App\State::create([
                'name' => $faker->state,
                'country_id' => $faker->numberBetween( $min = 1, $max = 99)
        	]);

        }

    }
}
