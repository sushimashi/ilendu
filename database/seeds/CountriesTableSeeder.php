<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 100 ; $i++) { 
        	
        	\App\Country::create([
                'name' => $faker->country
        	]);

        }

    }
}
