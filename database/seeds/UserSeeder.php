<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Community;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // $user = \App\User::create([
       //      'name' => 'Admin',
       //      'email' => 'admin@ilendu.com',
       //      'password' => Hash::make('admin'),
       //      'remember_token' => str_random(10),
       //  ]);
       // $admin = \App\Role::where('name', 'admin')->first();
       // $user->attachRole($admin);

       $faker = Faker\Factory::create();
       $n = 4;
       for ($j=0; $j < $n; $j++) { 

          for ($i=0; $i < 100 ; $i++) { 
              $user = \App\User::create([
                   'name' => $faker->name,
                   'email' => $faker->email,
                   'password' => $faker->password,
                   'remember_token' => str_random(10),
                   'country_id' => $faker->numberBetween($min = 1, $max = 247),
                   'state_id' => $faker->numberBetween($min = 1, $max = 4028),
                   'city_id' => $faker->numberBetween($min = 1, $max = 47953),
                   'profile_img' => $faker->imageUrl()
               ]);

             $community = Community::find($j+4);
             $community->communityUsers()->attach($user->id);
           }

       }
    }
}
