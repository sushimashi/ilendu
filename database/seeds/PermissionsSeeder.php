<?php

use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Permission::create([
            'name'=> 'privilegios_ver',
            'display_name'=>'Privilegios Ver',
            'description'=>'Ver'
        ]);

        \App\Permission::create([
            'name'=> 'privilegios_crear',
            'display_name'=>'Privilegios Crear',
            'description'=>'Crear'
        ]);

        \App\Permission::create([
            'name'=> 'privilegios_editar',
            'display_name'=>'Privilegios Editar',
            'description'=>'Editar'
        ]);

        \App\Permission::create([
            'name'=> 'privilegios_eliminar',
            'display_name'=>'Privilegios Eliminar',
            'description'=>'Eliminar'
        ]);

        \App\Permission::create([
            'name'=> 'users_see',
            'display_name'=>'See Users',
            'description'=>'See'
        ]);
        \App\Permission::create([
            'name'=> 'users_create',
            'display_name'=>'Create Users',
            'description'=>'Create'
        ]);
        \App\Permission::create([
            'name'=> 'users_edit',
            'display_name'=>'Edit User',
            'description'=>'Edit'
        ]);
        \App\Permission::create([
            'name'=> 'users_delete',
            'display_name'=>'Delete Users',
            'description'=>'Delete'
        ]);

        \App\Permission::create([
            'name'=> 'clients_see',
            'display_name'=>'See Client',
            'description'=>'See'
        ]);
        \App\Permission::create([
            'name'=> 'clients_create',
            'display_name'=>'Create Client',
            'description'=>'Create'
        ]);
        \App\Permission::create([
            'name'=> 'clients_edit',
            'display_name'=>'Edit Client',
            'description'=>'Edit'
        ]);
        \App\Permission::create([
            'name'=> 'clients_delete',
            'display_name'=>'Delete Client',
            'description'=>'Delete'
        ]);

        \App\Permission::create([
            'name'=> 'communities_see',
            'display_name'=>'See community',
            'description'=>'See'
        ]);
        \App\Permission::create([
            'name'=> 'communities_create',
           'display_name'=>'Create community',
            'description'=>'Create'
        ]);
        \App\Permission::create([
            'name'=> 'communities_edit',
            'display_name'=>'Edit community',
            'description'=>'Edit'
        ]);
        \App\Permission::create([
            'name'=> 'communities_delete',
             'display_name'=>'Delete community',
            'description'=>'Delete'
        ]);



        
    }
}
