<?php

use Illuminate\Database\Seeder;
use App\Community;
use App\City;

class CommunitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$cities=City::all();

    	foreach ($cities as $key => $value)
    	 {
    		   Community::create([
    		   	"name"=>$value->name,
    		   	"description"=>$value->name,
    		   	"status"=>1,
    		   	"created_by"=>1,
    		   	"public"=>1,
    		   	"city_id"=>$value->id
    		   ]);
    	  }
     
    }
}
