<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');


        // $permission = \App\Permission::first();
        // if (!$permission) {
        //     $this->call(PermissionsSeeder::class);
        // }

        // $role = App\Role::first();
        // if (!$role) {
        //     $this->call(RoleSeeder::class);
        // }

        // $user = \App\User::first();
        // if (!$user) {
        //     $this->call(UserSeeder::class);
        // }
        if (\Illuminate\Support\Facades\App::environment('local')) {
            DB::table('role_user')->truncate();
            DB::table('permission_role')->truncate();
            \App\Role::truncate();
            \App\User::truncate();
            \App\Permission::truncate();        
            $this->call(PermissionsSeeder::class);
            $this->call(RoleSeeder::class);
            $this->call(UserSeeder::class);
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);

    }
}
