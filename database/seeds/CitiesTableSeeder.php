<?php

use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 100 ; $i++) { 
        	
        	\App\City::create([
                'name' => $faker->city,
                'state_id' => $faker->numberBetween( $min = 1, $max = 99)
        	]);

        }
    }
}
