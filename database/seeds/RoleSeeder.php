<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Relations\Relation;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rol = new \App\Role();
        $rol->name='admin';
        $rol->display_name='Administrador';
        $rol->description = 'Administrador';
        $rol->save();

        $permisos = \App\Permission::all();

        $rol->attachPermissions($permisos);
    }
}
